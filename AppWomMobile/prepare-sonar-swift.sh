#!/bin/bash

# Este script se encarga de instalar las dependencias necesarias para ejecutar
# el cliente de SonarQube (sonar-scanner) para proyectos iOS. Estas
# dependencias se basan en el uso del plugin sonar-swift para Sonar.
#
# Para más información visite https://github.com/Backelite/sonar-swift
# 
# El desarrollo de este script tiene por objetivo el ser ejecutado durante la
# inicialización de una maquina OSX para que pueda ejecutar posteriormente
# el script ./run-sonar-swift.sh mediante la línea de comandos.

# 1.- Instalando sonar-scanner
brew install sonar-scanner

# 2.- Instalando xcpretty...
echo "Instalando xcpretty..."
mkdir temp
cd temp
git clone https://github.com/Backelite/xcpretty.git
cd xcpretty
git checkout fix/duration_of_failed_tests_workaround
gem build xcpretty.gemspec
sudo gem install --both xcpretty-0.2.2.gem
cd ..

# 3.- Instalando swiftlint...
echo "Instalando swiftlint..."
brew install swiftlint

# 4.- Instalando tailor...
echo "Instalando tailor..."
brew install tailor

# 5.- Instalando slather...
echo "Instalando slather..."
gem install slather

# 6.- Instalando slather...
echo "Instalando slather..."
sudo pip install lizard
