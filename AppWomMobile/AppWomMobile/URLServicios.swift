//
//  URLServicios.swift
//  AppWomMobile
//
//  Archivo con direcciones para conectarse a servicios.
//  URL UAT http://10.120.145.148:9203
//
//  @author Esteban Pavez A. (TINet).
//

import Foundation

struct URLServicios {
    // ip utilizada con servicios segurizados
    static let ipSegurizada = "https://api.wom.cl"
    // ip utilizada con servicios no segurizados
    static let ipNoSegurizada = "http://10.120.147.79"
    // Servicio que trae las bolsas históricas
    static let historialBolsas = "{ip}/{server}/CentroDeGestionWom/{service}PSCentroDeGestionWomREST/RecuperaInfoBolsa"
    // Datos consumo home
    static let datosConsumoHome = "{ip}/{server}/PrepaidWave/{service}PSServicePrepaidWaveREST/lTQueryUnitTotalExtendidaFullWS"
    // Datos cliente
    static let datosCliente = "{ip}/{server}/ConsultaInfoCliente/{service}PSConsultaDatosClienteREST/ConsultaInfoCliente"
    // Catalogo de productos
    static let catalogoProductos = "{ip}/{server}/PortalCautivo/{service}PSBSCSPortalCautivoServiceREST/getNextelProductCatalog"
    // Bolsas disponibles
    static let bolsasDisponibles = "{ip}/{server}/PortalCautivo/{service}PSUpsellingCompraBolsasREST/getUpsellingCompraBolsas"
    // Compra Bolsas
    static let compraBolsas = "{ip}/{server}/PortalCautivo/{service}PSBSCSPortalCautivoServiceREST/setNextelActiveProduct"
    // Valida Bolsas
    static let validaBolsas = "{ip}/{server}/PortalCautivo/{service}PSValidaVentaBolsasREST/getValidaVentaBolsas"
    // Obtener balance
    static let obtenerBalance = "{ip}/{server}/SearchBalancesICC/{service}PSSearchBalancesICCServiceREST/getSearchBalanceIcc"
    // Obtener balance
    static let obtenerDatosPlan = "{ip}/{server}/PortalCautivo/{service}PSBSCSPortalCautivoServiceREST/getNextelCustomerInfo"
    // Detalle de llamados
    static let recuperaDetalleLlamados = "{ip}/{server}/CentroDeGestionWom/{service}PSCentroDeGestionWomREST/RecuperaDetalleLlamados"
    // Recupera info del tipo de plan
    static let recuperaInfoCliente = "{ip}/{server}/CentroDeGestionWom/{service}PSCentroDeGestionWomREST/RecuperaInfoCliente"
    // nuevo servicio para obtener datos de las bolsas del cliente
    static let consultaConsumo = "{ip}/{server}/ConsultaInfoCliente/{service}PSConsultaConsumoREST/consultaConsumo"
    // url para consultar recargas del cliente
    static let consultaRecarga = "{ip}/{server}/LocalWebPortal/{service}PSCustomerRechargeServiceREST"

    // New Womer
    static let newWomer = "https://newwomer.wom.cl/newwomer/index.html?linea="
    // Obtener información de new womer
    static let obtenerInfoNewWomer = "https://newwomer.wom.cl/newwomer/api/newwomers"

    // Tipos de homes
    static let obtenerTipoHome = "https://rateplan.wom.cl/api/jsonws/informacionconceptoplan-service-portlet.conceptoplan/obtener-concepto-plan"

    // obtener token
    static let obtenerToken = "https://seguridad.wom.cl/auth/realms/{realm}/protocol/openid-connect/token"
    // Solicitar codigo verificación
    static let solicitarCodigoVerificacion = "{ip}/{server}/Enrolamiento/{service}PSSolicitaCodigoVerificacionREST/solicitaCodigoVerificacion"
    // Obtiene IMSI
    static let obtieneIMSI = "{ip}/{server}/Enrolamiento/{service}PSObtieneImsiREST/obtenerImsi"

    // Centro de ayuda
    static let centroDeAyuda = "https://wom.inbenta.com/?noheader=1"
    // link para recargar saldo
    static let recargarSaldo = "https://www.recargaenlinea.cl/rele/faces/index.xhtml?ixemp=108&ver={version}&app=&linea={numero}&saldoactual={saldo}"
    // link para gestor
    static let gestor = "https://gestor.wom.cl/userportal/"
    // Pagar cuenta
    static let pagarCuenta = "https://pdr.wom.devetel.cl/pago_facil/pagarCuenta?rut_usuario={rut}&rut_dv={dv}"
    // Sucursal online BAM
    static let sucursalOnline = "http://www.wom.cl/sucursal-online"

    // url para cerrar vista recarga
    static let urlCerrarRecarga = "https://www.recargaenlinea.cl/rele/faces/index.xhtml?ixemp=108&ver=app-1&app="
    // url que indica que se debe mostrar boton para cerrar vista
    static let urlDesplegarCerrarVista = "https://www.recargaenlinea.cl/rele/faces/contextwom_1.xhtml"
    // url que indica que se debe ocultar boton para cerrar vista
    static let urlOcultarCerrarVista = "https://www.recargaenlinea.cl/ext/exitoWom2.jsp"

    // url de consulta de promociones activas
    static let urlObtenerPromociones = "{ip}/{server}/CampaignFunnelManagement/CampaignManagementRestService/v/1/getOffers"
    // url para la activación de promociones
    static let urlActivarPromocion = "{ip}/{server}/CampaignFunnelManagement/CampaignManagementRestService/v/1/eventNotifier"

    // url para obtener facturas
    static let urlGetInvoice = "{ip}/{server}/LocalWebPortal/{service}PSCustomerInvoiceServiceREST/getInvoice"
    // url para obtener url para descargar pdf de factura
    static let urlGetInvoicePdfURI = "{ip}/{server}/LocalWebPortal/{service}PSCustomerInvoiceServiceREST/getInvoicePdfUri"

    // url para validar el estado de servicio roaming
    static let urlValidarServicioRoaming = "{ip}/{server}/BSCSWebBundles/{service}PSVentaBolsasRValidacionREST/DBAVentaBolsasRValidacion"
    // url para comprar bolsa roaming
    static let urlCompraBolsaRoaming = "{ip}/{server}/BSCSWebBundles/{service}PSVentaBolsasRREST/DBAVentaBolsasR"
}
