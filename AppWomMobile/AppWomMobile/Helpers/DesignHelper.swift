//
//  DesignHelper.swift
//  AppWomMobile
//
//  Clase que tiene elementos que ayudan en temas de diseño
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

struct DesignHelper {
    /**
     Retorna color en RGB segun codigo
     - parameter rgbValue: codigo en rgb de color.
     - returns: color en rgb
    */
    static func UIColorFromRGB(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    /// Agrega un borde redondo a la imagen
    ///
    /// - parameter imageView: Imageview del elemento con la imagen
    ///
    /// - returns: Imageview con el borde redondeado
    static func redondearImagen(_ imageView: UIImageView) -> UIImageView {

        imageView.layer.borderWidth = 4
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true


        return imageView
    }
    /// Corrige la orientación de la imagen obtenida de galería.
    ///
    /// - parameter imagen: imágen que se corrige orientación.
    ///
    /// - returns: UIImage corregida
    static func corregirOrientacionImagen(_ imagen: UIImage) -> UIImage {

        if imagen.imageOrientation == UIImageOrientation.up {
            return imagen
        }

        UIGraphicsBeginImageContextWithOptions(imagen.size, false, imagen.scale)
        let rect = CGRect(x: 0, y: 0, width: imagen.size.width, height: imagen.size.height)
        imagen.draw(in: rect)

        let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }


    /// Realiza la animación para el circulo y el nivel de new womer.
    ///
    /// - parameter vistaFondo: circulo blanco de fondo de new womer
    static func realizarAnimacionNivelWomer(_ vistaFondo: UIImageView) {
        // solo debe entrar la segunda vez que entra a esta funcion
        if Globales.validadorAnimacionEncabezado == 1 {
            vistaFondo.isHidden = false
            vistaFondo.transform = CGAffineTransform(scaleX: 0, y: 0)
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveLinear, animations: {
                vistaFondo.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion: nil)
        } else if Globales.validadorAnimacionEncabezado > 1 {
            vistaFondo.isHidden = false
        }
        Globales.validadorAnimacionEncabezado += 1
    }


    /// Agrega animación a vista pop up
    ///
    /// - Parameter vista: vista que se aplica animacion
    static func mostrarAnimacionPopUp(_ vista: UIView) {
        // escala con la animación inicial
        let escalaAnimacionInicial = CGFloat(1.3)
        // escala con la animación final
        let escalaAnimacionFinal = CGFloat(1.0)
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // transparencia inicial de la vista
        let transparenciaInicial = CGFloat(0.0)
        // transparencia final de la vista
        let transparenciaFinal = CGFloat(1.0)

        vista.transform = CGAffineTransform(scaleX: escalaAnimacionInicial, y: escalaAnimacionInicial)
        vista.alpha = transparenciaInicial

        UIView.animate(withDuration: duracionAnimacion, animations: {
            vista.alpha = transparenciaFinal
            vista.transform = CGAffineTransform(scaleX: escalaAnimacionFinal, y: escalaAnimacionFinal)
            }, completion: nil)
    }

    /// Agrega animación a vista pop up
    ///
    /// - Parameter vista: vista que se aplica animacion
    static func mostrarAnimacionDifuminacionBoton(_ boton: UIButton) {
        // escala con la animación inicial
        let escalaAnimacionInicial = CGFloat(1.3)
        // escala con la animación final
        let escalaAnimacionFinal = CGFloat(1.0)
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // transparencia inicial de la vista
        let transparenciaInicial = CGFloat(0.0)
        // transparencia final de la vista
        let transparenciaFinal = CGFloat(1.0)

        boton.transform = CGAffineTransform(scaleX: escalaAnimacionInicial, y: escalaAnimacionInicial)
        boton.alpha = transparenciaInicial

        UIView.animate(withDuration: duracionAnimacion, animations: {
            boton.alpha = transparenciaFinal
            boton.transform = CGAffineTransform(scaleX: escalaAnimacionFinal, y: escalaAnimacionFinal)
        }, completion: nil)
    }

    /// Bloquea el rebote de la parte inferior del scrollview
    ///
    /// - Parameter scrollView: objeto de scrollview que se bloqueará
    static func bloquearBounceInferior(scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height {
            scrollView.setContentOffset(CGPoint.init(x: scrollView.contentOffset.x, y: (scrollView.contentSize.height - scrollView.frame.size.height)), animated: false)
        }
    }

    /// Maneja la forma en que se muestra la animacion para la vista de pagar cuenta para controlados y postpagos
    ///
    /// - Parameters:
    ///   - contadorAnimacionVistaConDeuda: contador que se mantiene para mostrar o no la animacion
    ///   - viewSectionEncabezado: clase de encabezado para controlado y postpago
    static func animacionPagarCuenta(contadorAnimacionVistaConDeuda: inout Int, viewSectionEncabezado: CeldaEncabezadoHomeControladoIphone) {
        let seMuestraAnimacion = 1
        let noSeMuestraAnimacion = 0
        let ingresoPrimeraVez = -1

        if contadorAnimacionVistaConDeuda == seMuestraAnimacion {
            viewSectionEncabezado.animarVistaConDeuda()
            contadorAnimacionVistaConDeuda = noSeMuestraAnimacion
        }
        if contadorAnimacionVistaConDeuda == ingresoPrimeraVez {
            contadorAnimacionVistaConDeuda = seMuestraAnimacion
        }
        if contadorAnimacionVistaConDeuda == noSeMuestraAnimacion {
            viewSectionEncabezado.mostrarVistaPagarCuentaSinAnimacion()
        }
    }

    /// Se modifica un texto dentro del original para poder cambiarle fuente
    ///
    /// - Parameters:
    ///   - texto: Texto original
    ///   - sizeTextoModificado: Tamaño de texto modificado
    ///   - sizeFuente: Tamaño de fuente total
    ///   - tipoFuente: Tipo de fuente de texto modificado
    ///   - textoModificado: String con texto modificado
    ///   - colorFuente: Color de la fuente
    /// - Returns: Texto modificado
    static func modificarAtributosTexto(_ texto: String, sizeTextoModificado: Int, sizeFuente: Int, tipoFuente: String, textoModificado: String, colorFuente: UIColor, espaciado: CGFloat) -> NSAttributedString {
        // string que se transformará
        let string = texto as NSString

        let stringAtribuido = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName:UIFont (name: tipoFuente, size: CGFloat (sizeFuente))!])

        let fuenteBoldAtribuido = [NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (sizeTextoModificado))!]

        // Partes del string que serán en negrita
        stringAtribuido.addAttributes(fuenteBoldAtribuido, range: string.range(of: "\(textoModificado)"))
        stringAtribuido.addAttribute(NSForegroundColorAttributeName, value: colorFuente, range: string.range(of: "\(textoModificado)"))

        let textRange = NSRange(location: 0, length: stringAtribuido.length)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = espaciado
        stringAtribuido.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range: textRange)

        return stringAtribuido
    }

    /// Funcion que genera boton comun en WOM
    ///
    /// - Parameters:
    ///   - view: vista con boton
    ///   - textoBoton: texto de boton
    ///   - sizeFuente: tamaño de la fuente
    static func generarVistaBoton(view: UIView, textoBoton: String, sizeFuente: CGFloat) {
        view.layer.cornerRadius = 6
        view.layer.borderWidth = 2.5
        view.layer.borderColor = DesignHelper.UIColorFromRGB(0xBA007C).cgColor
        let textLayer = UILabel()
        textLayer.lineBreakMode = .byWordWrapping
        textLayer.numberOfLines = 0
        textLayer.textColor = UIColor.white
        textLayer.textAlignment = .center
        let textContent = textoBoton
        let textString = NSMutableAttributedString(string: textContent, attributes: [
            NSFontAttributeName: UIFont(name: Constants.TipoFuente.ceraBold, size: sizeFuente)!
            ])
        let textRange = NSRange(location: 0, length: textString.length)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1
        textString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: textRange)
        textLayer.attributedText = textString
        textLayer.sizeToFit()
        textLayer.frame.size = view.frame.size
        textLayer.textAlignment = .center
        view.addSubview(textLayer)
    }
}
