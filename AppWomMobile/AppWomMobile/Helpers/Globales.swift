//
//  Globales.swift
//  AppWomMobile
//
//  Clase que contiene variables globales de la aplicación
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import AVFoundation

struct Globales {
    // Indicador de bypass en la aplicación
    static var modoTest = true
//    static var modoTest = false
    // Indicador para guardar los datos de usuario al llegar a home
    static var guardarDatosUsuario = true
    // Indicador de uso de servicios segurizados
    static var esSegurizado = true
    // Indicador de activacion de modulos campaign manager
    static var modoCampaignManager = true
    // indicador de home seleccionado (a falta de servicios)
    static var homeSeleccionado = String()
    // numero telefonico del cliente
    static var numeroTelefono = String()
    // codigo del plan/prepago del cliente
    static var datosCliente = EncabezadoHomeTO()
    // datos de las llamadas del cliente
    static var datosLlamadas = RecuperaDetalleLlamadosTO()
    // nombre de womer
    static var nombreCliente = String()
    // apellido de womer
    static var apellidoCliente = String()
    // indica si el periodo es mes
    static var esMes = false
    // nivel en new womer
    static var nivelWomer = "0"
    //muestra celda sobre consumo
    static var showSobreconsumo = false
    // indicador de nivel maximo
    static var esWomerNivelMax = false
    // lista de datos de plan y bolsas del womer
    static var listaBolsasCliente = [String]()
    // lista de datos de plan y bolsas del womer promocionales
    static var listaBolsasPromoCliente = [String]()
    // lista con los grupos de las bolsas
    static var listaGrupoBasales = [GrupoBolsaTO]()
    // lista con los grupos de las bolsas
    static var listaGrupoBolsas = [GrupoBolsaTO]()
    // lista con los grupos de las bolsas
    static var listaGrupoBolsasPromocionales = [GrupoBolsaTO]()
    // booleano que indica si el dispositivo es de la linea de iphone 5 (5c, 5s, SE)
    static var isIphone5 = false
    // booleano que indica si el dispositivo es de la linea de iphone 6 (6c, 6s, 7)
    static var isIphone6 = false
    // booleano que indica si el dispositivo es de la linea de iphone Plus (6 plus, 7 plus)
    static var isIphonePlus = false
    // tab bar seleccionado
    static var tabBarSeleccionado = UITabBarController()
    // arreglo con las bolsas totales que existen en WOM
    static var arregloBolsasCatalogo = [String: CatalogoBolsaTO]()
    // token en la aplicacion
    static var tokenApp = TokenTO()
    // IMSI obtenido al loguearse
    static var IMSI = String()
    // segmento obtenido cliente
    static var segmentClient = ""
    // Imagen perfil seleccionada
    static var imagenPerfil = UIImage(named: Constants.Imagenes.imgAvatarDefecto)
    // Imagen con icono por defecto
    static var imagenPerfilDefecto = UIImage(named: Constants.Imagenes.imgAvatarDefecto)
    // Imagen perfil seleccionada temporal
    static var imagenPerfilTemporal = UIImage(named: Constants.Imagenes.imgAvatarDefecto)
    // Indicador que determina si ya se encuentra en el home
    static var enHome = false
    // arreglo con las bolsas totales que existen en WOM
    static var arregloAnimacionBolsas = [String: Bool]()
    // anchura dispositivo
    static var anchuraDispositivo = CGFloat()
    // controlador del home
    static var homeController = UIViewController()
    // validador para animacion nivel new womer
    static var validadorAnimacionEncabezado = 0
    // sesion de twitter
    static var loginTwitter = false
    // dia de recarga
    static var diaRecarga = Date()
    //fecha
    static var fechaHoy = ""
    // monto bolsa
    static var montoBolsa = 0
    // tipo bolsa
    static var tipoBolsa = ""
    // nombre bolsa
    static var nombreBolsa = ""
    // Fecha para utilizar al haber problemas de conversión
    static var indicadorProblemaFechas = false
    // arreglo de promociones
    static var promociones = [PromocionesTO]()
    // listado con la data de las imagenes descargadas
    static var listadoImagenesPromos = [String: UIImage]()
    // Booleano que indica si la promoción se activó correctamente
    static var confirmacionPromocion = false
    // Imagen de home banner
    static var imagenPromoPrincipal = UIImage()
    // Valida que se pueda obtener la imagen de banner
    static var validadorImagenBanner = true
    // Indicador de que home es de tipo Controlado
    static var esControlado = false
    // Indicador de que home es de tipo Prepago
    static var esPrepago = false
    // Indicador de que home es de tipo Business
    static var esBusiness = false
    // Indicador de que home es de tipo Postpago
    static var esPostpago = false
    // Hashmap que guarda el ultimo estado en que quedaron la svistas de area facturacion
    static var estadoVistasAreaFacturacion: [String: Bool] = ["vistaPagarCuenta": false,
                                                                "vistaDetalleCiclo": false,
                                                                "vistaMultilinea": false]
    // contacto de whatsapp darkside activado
    static var contactoWhatsappDarkside = ContactoWhatsappTO()
    // contacto de whatsapp roaming activado
    static var contactoWhatsappRoaming = ContactoWhatsappTO()
    // Objeto de paises con listado de los 3 tipos para roaming
    static var paisesRoaming = PaisesTO()
    // Indicador de error por cliente en limbo
    static var esErrorLimbo = false
    // Imagen de error darkside
    static var imagenDarkside = UIImageView()
    // Imagen de error limbo
    static var imagenLimbo = UIImageView()
    // Reproductor de video de fondo
    static var avPlayer = AVPlayer()
    // Indica si popup multilinea se encuentra abierto
    static var modoMultilinea = false
    /**
     Funcion que indica si el dispositivo es iphone 6 o 6s

     - parameter altura: altura de la vista para calcular
     - returns: booleano indicando si es iphone6 o no
    */
    static func esIphone6(_ altura: Int) -> Bool {
        if altura == 667 {
            return true
        } else {
            return false
        }
    }
    /**
     Funcion que indica si el dispositivo es iphone 6 plus o 6s plus

     - parameter altura: altura de la vista para calcular.
     - returns: booleano indicando si es iphone6 plus o no.
     */
    static func esIphone6plus(_ altura: Int) -> Bool {
        if altura == 736 {
            return true
        } else {
            return false
        }
    }
    // Arreglo con que se valida la visualización de las pantallas del tutorial
    static var arregloValidacionSlidesTutorial = [false, false, false]
}
