//
//  Logger.swift
//  AppWomMobile
//
//  Clase que disponibiliza función para loguear.
//  @author Esteban Pavez A. (TINet).
//

import Foundation

struct Logger {

    static func log(_ message: String = "", _ path: String = #file, _ function: String = #function) {
        let file = path.components(separatedBy: "/").last!.components(separatedBy: ".").first!
        NSLog("%@.%@: %@", file, function, message)
    }
}
