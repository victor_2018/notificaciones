//
//  AppWomMobile-Bridging-Header.h
//  AppWomMobile
//
//  Clase bridge para poder utilizar clases que están en objective-c en Swift.
//  @author Esteban Pavez A. (TINet).
//

#ifndef AppWomMobile_Bridging_Header_h
#define AppWomMobile_Bridging_Header_h

#import "Cipher.h"
#import "NSData+Base64.h"

#endif /* AppWomMobile_Bridging_Header_h */
