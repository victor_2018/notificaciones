//
//  Util.swift
//  AppWomMobile
//
//  Clase que contiene funciones de uso general
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

struct Util {
    /**
     Le agrega fondo gradiente a tabla enviada por parametro
     - parameter tabla: tabla que se desea agregar color gradiente
     - parameter topColor: color superior de gradiente
     - parameter bottomColor: color inferior de gradiente
    */
    static func setTableViewBackgroundGradient(_ tabla: UITableView, _ topColor: UIColor, _ bottomColor: UIColor) {

        let gradientBackgroundColors = [topColor.cgColor, bottomColor.cgColor]
        let gradientLocations = [0.0, 1.0]

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientBackgroundColors
        gradientLayer.locations = gradientLocations as [NSNumber]

        gradientLayer.frame = tabla.bounds
        gradientLayer.frame.size.width = gradientLayer.frame.size.width * 2
        let backgroundView = UIView(frame: tabla.bounds)
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        tabla.backgroundView = backgroundView
    }
    /**
     Función para transformar la fecha desde string y mostrarla con formato enviado

     - parameter fecha: fecha que se desea transformar
     - parameter formato: formato en que viene la fecha en string
     - returns: fecha formateada
     */
    static func transformarFechaString(_ fecha: String, formato: String) -> Date {
        let formatoFecha: DateFormatter = DateFormatter()
        formatoFecha.dateFormat = formato
        formatoFecha.timeZone = NSTimeZone(name: "UTC")! as TimeZone

        var fechaFormateada = Date()

        if formatoFecha.date(from: fecha) != nil {
            fechaFormateada = formatoFecha.date(from: fecha)!
        } else {
            Globales.indicadorProblemaFechas = true
        }

        return fechaFormateada
    }
    /**
     Función para transformar la fecha desde nsDate a string

     - parameter fecha: fecha que se desea transformar
     - parameter formato: formato a transformar fecha
     - returns: fecha formateada
     */
    static func transformarFechaNSDate(_ fecha: Date, formato: String) -> String {
        let formatoFecha: DateFormatter = DateFormatter()
        formatoFecha.dateFormat = formato
        formatoFecha.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        var fechaFormateada = String()

        fechaFormateada = formatoFecha.string(from: fecha)

        return fechaFormateada
    }
    /**
     Función para transformar de kilos a megas/gigas.
     - parameter dato: kilos que se transformaran.
     - parameter dato2: dato que se sumará al primer dato.
     - returns: kilos transformados.
     */
    static func transformarKilos(_ dato: String, dato2: String) -> Bolsa {
        let limiteMegas = CGFloat(1024)
        let limiteGigas = CGFloat(1048576)
        var kilosRestantes = CGFloat(Int(dato)!)
        var kilosConsumidos = CGFloat()
        var kilosFinales = CGFloat()

        if !dato2.isEmpty {
            kilosConsumidos = CGFloat(Int(dato2)!)
            kilosRestantes += kilosConsumidos
        }

        if kilosRestantes >= limiteMegas && kilosRestantes < limiteGigas {
            kilosFinales = kilosRestantes / limiteMegas
            return Bolsa(datoIn: kilosFinales, datoEnKilosIn: kilosRestantes, metricaIn: "MB")
        } else if kilosRestantes >= limiteGigas {
            kilosFinales = kilosRestantes / limiteGigas
            return Bolsa(datoIn: kilosFinales, datoEnKilosIn: kilosRestantes, metricaIn: "GB")
        } else {
            kilosFinales = CGFloat(Int(dato)!) / limiteMegas
            return Bolsa(datoIn: kilosFinales, datoEnKilosIn: kilosRestantes, metricaIn: "MB")
        }
    }
    /**
     Función para transformar de segundos a minutos.
     - parameter dato: segundos que se transformaran.
     - parameter dato2: segundos que se sumaran a primer dato.
     - returns: kilos transformados.
     */
    static func transformarSegundos(_ dato: String, dato2: String) -> Bolsa {
        let limiteMinutos = CGFloat(60)
        var minutosRestantes = CGFloat(Int(dato)!)
        var minutosConsumidos = CGFloat()

        if !dato2.isEmpty {
            minutosConsumidos = CGFloat(Int(dato2)!)
            minutosRestantes += minutosConsumidos
        }

        let minutosTransformados = Int(minutosRestantes / limiteMinutos)
        return Bolsa(datoIn: CGFloat(minutosTransformados), datoEnKilosIn: CGFloat(minutosRestantes), metricaIn: "minutos")
    }
    /**
     Función para transformar de minutos a días.
     - parameter minutos: minutos que se transformaran.
     - returns: dias transformados.
     */
    static func transformarMinutosADias(_ minutos: Int) -> Int {
        var dias: Int

        dias = minutos / 1440
        return dias
    }
    /**
     Función para transformar de minutos a horas.
     - parameter minutos: minutos que se transformaran.
     - returns: horas transformados.
     */
    static func transformarMinutosAHoras(_ minutos: Int) -> Int {
        var horas: Int

        horas = minutos / 60
        return horas
    }
    /**
     Función para eliminar decimales "0" de numeros float.
     - parameter numero: número que se eliminarán "0".
     - returns: número sin ceros, en caso de solo tenerlos.
     */
    static func verificarCerosEnDecimales(_ numero: String) -> String {
        var numeroFinal = [String]()
        numeroFinal = numero.components(separatedBy: ".")
        let stringDecimales = numeroFinal[1]

        if stringDecimales[0] == "0" && stringDecimales[1] == "0" {
            return numeroFinal[0]
        } else if stringDecimales[0] != "0" && stringDecimales[1] == "0" {
            let numero = numeroFinal[0]
            let numero1: String = numeroFinal[1]
            let numero2: String = numero1[0]
            return "\(numero),\(numero2)"
        } else {
            let numero1: String = numeroFinal[1]
            let numero2: String = numero1[0]
            let numero3: String = numero1[1]
            return "\(numeroFinal[0]),\(numero2)\(numero3)"
        }
    }
    /// Se modifica un texto dentro de otro para dejarlo en negrita y algún color especificado.
    ///
    /// - Parameters:
    ///   - texto: Texto completo que contiene algo en negrita.
    ///   - tamFuente: tamaño fuente.
    ///   - tipoFuente: tipo de la fuente del texto
    ///   - textoModificado: texto específico en negrita.
    ///   - colorFuente: color en que quedara texto en negrita.
    /// - Returns: string con texto en negrita entre medio.
    static func modificarAtributosTexto(_ texto: String, tamFuente: Int, tipoFuente: String, textoModificado: String, textoSecundarioModificado: String, colorFuente: UIColor) -> NSAttributedString {
        // tamaño de la fuente de la letra
        let tamanioFuente = tamFuente
        // string que se transformará
        let string = texto as NSString

        let stringAtribuido = NSMutableAttributedString(string: string as String, attributes: [NSFontAttributeName:UIFont (name: tipoFuente, size: CGFloat (tamanioFuente))!])

        let fuenteBoldAtribuido = [NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (tamanioFuente))!]

        // Partes del string que serán en negrita
        stringAtribuido.addAttributes(fuenteBoldAtribuido, range: string.range(of: "\(textoModificado)"))
        stringAtribuido.addAttributes(fuenteBoldAtribuido, range: string.range(of: "\(textoSecundarioModificado)"))

        stringAtribuido.addAttribute(NSForegroundColorAttributeName, value: colorFuente, range: string.range(of: "\(textoModificado)"))

        return stringAtribuido
    }

    /**
     Alerta general de servicio.
     - parameter viewController: Controlador donde se mostrará alerta de error.
     */
    static func alertErrorGeneral(_ viewController: UIViewController) {
        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "excepcionesWom") as! ExcepcionesWOMIphone

        controller.esErrorGeneral = true
        controller.delegate = viewController

        if Globales.enHome && viewController.navigationController != nil {
            controller.errorComoVista = true
            viewController.navigationController!.pushViewController(controller, animated: false)
        } else {
            viewController.present(controller, animated: true, completion: nil)
        }
    }

    /**
     Formatea el número enviado al estilo "$ 1.000"
     - parameter pesos: valor en pesos que se desea formatear.
     - returns: valor formateado, en caso de llegar vacio se devuelve vacio.
     */
    static func formatearPesos(_ pesos: String) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = " "
        numberFormatter.locale = Locale(identifier: "es_CL")
        if !pesos.isEmpty {
            return "$\(numberFormatter.string(from: Int(pesos)! as NSNumber)!)"
        } else {
            return ""
        }
    }
    /**
     Determina el tipo de dispositivo en que se esta corriendo la aplicacion.
     - parameter altura: altura del dispositivo.
     */
    static func determinarDispositivo(_ altura: CGFloat) {
        let iphone5 = CGFloat(568.0)
        let iphone6 = CGFloat(667.0)
        let iphonePlus = CGFloat(736.0)

        switch altura {
        case iphone5:
            Globales.isIphone5 = true
        case iphone6:
            Globales.isIphone6 = true
        case iphonePlus:
            Globales.isIphonePlus = true
        default:
            Logger.log("DISPOSITIVO NO IDENTIFICADO: \(altura)")
        }
    }
    /// Muestra popup con error general.
    ///
    /// - Parameter vistaActual: vista que arroja error.
    static func desplegarErrorGeneral(_ vistaActual: PopupComprarBolsaIphone) {
        let errorGeneral = UIStoryboard(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "errorGeneral") as! ErrorGeneral
        errorGeneral.delegate = vistaActual
        vistaActual.addChildViewController(errorGeneral)
        errorGeneral.view.frame = vistaActual.view.frame
        vistaActual.view.addSubview(errorGeneral.view)
        errorGeneral.didMove(toParentViewController: vistaActual)
    }
    /**
     obtiene el indicador correspondiente a la seccion solicitada en el indice.
     - parameter secciones: arreglo con los titulos de las secciones.
     - parameter indice: indice de seccion solicitada.
     - returns: indicador de seccion.
     */
    static func obtenerTipoSeccion(_ secciones: [String], indice: Int) -> String {
        for index in 0...secciones.count {
            switch secciones[index] {
            case NSLocalizedString("titulo_seccion_datos_moviles", comment: "titulo seccion datos moviles"):
                return Constants.Indicadores.seccionDatosMoviles
            case NSLocalizedString("titulo_seccion_minutos", comment: "titulo seccion minutos"):
                return Constants.Indicadores.seccionMinutos
            case NSLocalizedString("titulo_seccion_mensajes", comment: "titulo seccion mensajes"):
                return Constants.Indicadores.seccionSMS
            default:
                return ""
            }
        }
        return ""
    }
    /**
     Agrega un loader/Activity indicator a la vista enviada.
     - parameter vista: vista a la que se le agrega el indicador de actividad
     */
    static func agregarActivityIndicator(_ vista: UIView) {
        let loader = UIImageView(frame: CGRect(x: vista.frame.size.width / 2, y: vista.frame.size.height / 2, width: 200, height: 100))
        let viewBackground = UIView(frame: vista.frame)
        let view = UIView(frame: vista.frame)

        var imgListArray = [UIImage]()
        for imagenIndex in 0...22 {

            let stringImagen: String = "loading00\(imagenIndex).png"
            let image = UIImage(named: stringImagen)

            imgListArray.append(image!)
        }

        loader.animationImages = imgListArray
        loader.animationDuration = 1
        loader.center = vista.center
        loader.startAnimating()

        viewBackground.backgroundColor = DesignHelper.UIColorFromRGB(0x2D1540)
        viewBackground.alpha = CGFloat(0.9)

        view.addSubview(viewBackground)
        view.addSubview(loader)
        view.contentMode = .scaleAspectFit
        view.tag = Constants.Tags.tagLoader

        vista.addSubview(view)
    }
    /// Agrega un controller a una vista contenedora
    ///
    /// - Parameters:
    ///   - controller: vista que se levantará como popup
    ///   - contenedor: vista contenedora de popup
    static func agregarPopUp(_ controller: UIViewController, contenedor: UITabBarController) {

        contenedor.addChildViewController(controller)
        controller.view.frame = contenedor.view.frame
        contenedor.view.addSubview(controller.view)
        controller.didMove(toParentViewController: contenedor)
    }
    /**
     Agrega un loader/Activity indicator a la vista enviada.

     - parameter vista: vista a la que se le agrega el indicador de actividad
     */
    static func removerActivityIndicator(_ vista: UIView) {

        for subview in vista.subviews {
            if subview.tag == Constants.Tags.tagLoader {
                subview.removeFromSuperview()
            }
        }
    }
    /// Encripta string con Cripto
    ///
    /// - Parameters:
    ///   - stringACodificar: string que sera codificado
    ///   - llave: llave identificador del string codificado
    /// - Returns: string codificado
    static func codificarStringCripto(_ stringACodificar: String, llave: String) -> Data {
        let cipher = Cipher(key: llave)
        let stringCodificadoUTF8 = stringACodificar.data(using: String.Encoding.utf8)
        let stringCodificado = cipher?.encrypt(stringCodificadoUTF8)

        return stringCodificado!
    }
    /// Decodifica string con Cripto
    ///
    /// - Parameters:
    ///   - stringCodificado: string que será decodificado
    ///   - llave: llave identificador del string codificado
    /// - Returns: string decodificado
    static func decodificarStringCripto(_ stringCodificado: Data, llave: String) -> String {
        let cipher = Cipher(key: llave)
        let dataDecripted = cipher?.decrypt(stringCodificado).base64EncodedString()
        if dataDecripted != nil {
            if let base64Decoded = Data(base64Encoded: dataDecripted!, options: NSData.Base64DecodingOptions(rawValue: 0)).map({
                NSString(data: $0, encoding: String.Encoding.utf8.rawValue)
            }) {
                // Convert back to a string
                return base64Decoded! as String
            }
        }
        return ""
    }
    /// Obtiene el dispositivo iphone que se esta utilizando
    ///
    /// - Returns: nombre de dispositivo
    static func getDevice() -> String {
        var sysInfo = utsname()
        uname(&sysInfo)
        let machine = Mirror(reflecting: sysInfo.machine)
        let identifier = machine.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier as String
    }
    /// Formatea seguidores de facebook y twitter
    ///
    /// - Parameter numero: numero de seguidores
    /// - Returns: numero de seguidores formateado
    static func formateaFollow (_ numero: NSNumber) -> String {

        let numeroInt = Int(numero)
        var valor = ""
        if numeroInt > 1000000 {
            let newNumero = numeroInt / 100000
            let numeroEntero = newNumero / 10
            let numeroDecimal = newNumero % 10

            valor = String(numeroEntero) + "." + String(numeroDecimal) + "M"
        } else if numeroInt > 1000 {
            let newNumero = numeroInt / 100
            let numeroEntero = newNumero / 10
            let numeroDecimal = newNumero % 10

            valor = String(numeroEntero) + "." + String(numeroDecimal) + "K"
        } else {
            valor = String(numeroInt)
        }
        return valor
    }
 }
