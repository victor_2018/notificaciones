//
//  UtilFirebase.swift
//  AppWomMobile
//
//  Clase que contiene funciones de uso general con firebase.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import SwiftyJSON

struct UtilFirebase {

    /// Transforma la data obtenida de Firebase a objeto JSON
    ///
    /// - Parameters:
    ///   - ruta: Ruta de firebase en base de datos
    ///   - onCompletion: json con datos de firebase, retorno booleano que indica si se hizo la operacion correcta o no
    static func obtenerJSONDeFirebase(_ ruta: String, onCompletion: @escaping (_ json: JSON, _ retorno: Bool) -> ()) {
//        if Reachability.isConnectedToNetwork() {
//            var ref: DatabaseReference!
//            ref = Database.database().reference()
//            ref.child(ruta).observe(.value, with: { snapshot in
//                if snapshot.value is NSNull {
//                    Logger.log("Valor no encontrado en \(ruta)")
//                    DispatchQueue.main.async(execute: {
//                        ref.child(ruta).removeAllObservers()
//                        onCompletion(JSON.null, false)
//                    })
//                } else {
//                    let snap = snapshot.value as! [String: AnyObject]
//                    let json: JSON = JSON(snap)
//                    Logger.log("JSON obtenido de Firebase en \(ruta): \(json)")
//
//                    DispatchQueue.main.async(execute: {
//                        ref.child(ruta).removeAllObservers()
//                        onCompletion(json, true)
//                    })
//                }
//            })
//        } else {
            onCompletion(JSON.null, false)
//        }
    }

    /// Obtiene contacto activo para whatsapp darkside
    ///
    /// - Parameter onCompletion: indicador de operacion correcta
    static func obtenerContactosWhatsappDarkside(onCompletion: @escaping (_ retorno: Bool) -> ()) {
        UtilFirebase.obtenerJSONDeFirebase(Constants.Firebase.rutaWhatsappDarkside) { json, retorno in
            if retorno {
                for contacto in json["numeros"].array! {
                    if contacto["activo"].boolValue == true {
                        Globales.contactoWhatsappDarkside.mensaje = json["mensaje"].stringValue
                        Globales.contactoWhatsappDarkside.numero = contacto["numero"].stringValue
                        break
                    }
                    // se resetea en caso de hace cambio en tiempo real
                    Globales.contactoWhatsappDarkside.mensaje = ""
                    Globales.contactoWhatsappDarkside.numero = ""
                }
                Logger.log("Contacto whatsapp darkside: \(Globales.contactoWhatsappDarkside.numero)")
            } else {
                Logger.log("Fallo llamada a Firebase")
                Globales.contactoWhatsappDarkside.mensaje = ""
                Globales.contactoWhatsappDarkside.numero = ""
            }
            onCompletion(retorno)
        }
    }

    /// Obtiene contacto activo para whatsapp roaming
    ///
    /// - Parameter onCompletion: indicador de operacion correcta
    static func obtenerContactosWhatsappRoaming(onCompletion: @escaping (_ retorno: Bool) -> ()) {
        UtilFirebase.obtenerJSONDeFirebase(Constants.Firebase.rutaWhatsappRoaming) { json, retorno in
            if retorno {
                for contacto in json["numeros"].array! {
                    if contacto["activo"].boolValue == true {
                        Globales.contactoWhatsappRoaming.mensaje = json["mensaje"].stringValue
                        Globales.contactoWhatsappRoaming.numero = contacto["numero"].stringValue
                        print(Globales.contactoWhatsappRoaming.mensaje)
                        break
                    }
                    // se resetea en caso de hace cambio en tiempo real
                    Globales.contactoWhatsappRoaming.mensaje = ""
                    Globales.contactoWhatsappRoaming.numero = ""
                }
                Logger.log("Contacto whatsapp roaming: \(Globales.contactoWhatsappRoaming.numero)")
            } else {
                Logger.log("Fallo llamada a Firebase")
                Globales.contactoWhatsappRoaming.mensaje = ""
                Globales.contactoWhatsappRoaming.numero = ""
            }
            onCompletion(retorno)
        }
    }

    /// Obtiene listado de paises para roaming
    ///
    /// - Parameter onCompletion: indicador de operacion correcta
    static func obtenerListadoPaisesRoaming(onCompletion: @escaping (_ retorno: Bool) -> ()) {
        UtilFirebase.obtenerJSONDeFirebase(Constants.Firebase.rutaListadoPaisesRoaming) { json, retorno in
            if retorno {
                for contacto in json["zonas"].array! {
                    switch contacto["codigo"].stringValue {
                    case Constants.Indicadores.roamingPreferente:
                        for pais in contacto["paises"].array! {
                            if pais["activo"].boolValue == true {
                                Globales.paisesRoaming.paisesPreferente.append("\n\n\(pais["nombre"].stringValue)")
                            }
                        }
                    case Constants.Indicadores.roamingEuropa:
                        for pais in contacto["paises"].array! {
                            if pais["activo"].boolValue == true {
                                Globales.paisesRoaming.paisesEuropa.append("\n\n\(pais["nombre"])")
                            }
                        }
                    case Constants.Indicadores.roamingRestoDelMundo:
                        for pais in contacto["paises"].array! {
                            if pais["activo"].boolValue == true {
                                Globales.paisesRoaming.paisesRestoDelMundo.append("\n\n\(pais["nombre"])")
                            }
                        }
                    default:
                        Logger.log("codigo no existente: \(contacto["codigo"].stringValue)")
                    }
                }
                Logger.log("Contacto whatsapp roaming: \(Globales.contactoWhatsappRoaming.numero)")
            } else {
                Logger.log("Fallo llamada a Firebase")
            }
            onCompletion(retorno)
        }
    }

    /// Obtiene el estado de los servicios en la app, junto con listado de numeros disponibles para navegar
    ///
    /// - Parameter onCompletion: indicador de operacion correcta
    static func validarEstadoServicios(onCompletion: @escaping (_ retorno: Bool, _ numerosHabilitados: [String], _ estadoServicios: Bool) -> ()) {
        UtilFirebase.obtenerJSONDeFirebase(Constants.Firebase.rutaMantencion) { json, retorno in
            var listadoNumeros = [String]()
            var estadoServicios = Bool()
            if retorno {
                estadoServicios = json["estado"].boolValue
                for numero in json["numeros"].array! {
                    listadoNumeros.append(numero["numero"].stringValue)
                }
            }
            onCompletion(retorno, listadoNumeros, !estadoServicios)
        }
    }
}
