//
//  UtilMultilinea.swift
//  AppWomMobile
//
//  Clase que contiene funciones utilitarias de compra bolsa.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

struct UtilMultilinea {
    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func buscarDatosPlan(telefono: String, esPostpago: Bool, onCompletion: @escaping (_ retorno: Bool, _ datosCliente: EncabezadoHomeTO) -> ()) {
        let datosCliente = EncabezadoHomeTO()
        datosCliente.numeroTelefono = telefono
        GetNextelCustomerInfo.sharedInstance.getDatosPlan(telefono,
                                                          onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
                                                            if json["error_code"].stringValue == "0" {
                                                                DispatchQueue.main.async(execute: {
                                                                    datosCliente.nombrePlan = json["Desc_IDPlan"].stringValue
                                                                    datosCliente.idFormaPago = json["iDFormPago"].stringValue
                                                                    datosCliente.tipoPlan = json["segment_client"].stringValue.uppercased()

                                                                    if !esPostpago {
                                                                        UtilMultilinea.obtenerValorSaldo(telefono: telefono) { retorno, saldo, saldoFormateado in
                                                                            if retorno {
                                                                                datosCliente.saldo = saldo
                                                                                datosCliente.saldoFormateado = saldoFormateado
                                                                                onCompletion(retorno, datosCliente)
                                                                            } else {
                                                                                onCompletion(retorno, datosCliente)
                                                                            }
                                                                        }
                                                                    } else {
                                                                        onCompletion(true, datosCliente)
                                                                    }
                                                                })
                                                            } else {
                                                                DispatchQueue.main.async(execute: {
                                                                    Logger.log("error tipo: \(json["error_code"].stringValue)")
                                                                    onCompletion(false, datosCliente)
                                                                })
                                                            }
                                                          } else {
                                                            DispatchQueue.main.async(execute: {
                                                                Logger.log("error de servicio buscarDatosPlan")
                                                                onCompletion(false, datosCliente)
                                                            })
                                                            }
        })
    }

    /// Función donde se llama al servicio que trae los datos del saldo del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerValorSaldo(telefono: String, _ onCompletion: @escaping (_ retorno: Bool, _ saldo: Int, _ saldoFormateado: String) -> ()) {
        let valorPositivo = "Y"
        var saldo = Int()
        var saldoFormateado = String()
        GetSearchBalanceIcc.sharedInstance.getBalance(telefono, detalle: valorPositivo,
                                                      onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
                                                        if json["error_code"] != JSON.null && json["error_code"].stringValue == "0" && Int(json["totalBalance"].stringValue) != nil {
                                                            saldo = Int(json["totalBalance"].stringValue)!
                                                            saldoFormateado = Util.formatearPesos(json["totalBalance"].stringValue)

                                                            DispatchQueue.main.async(execute: {
                                                                onCompletion(true, saldo, saldoFormateado)
                                                            })
                                                        } else {
                                                            DispatchQueue.main.async(execute: {
                                                                Logger.log("error de servicio obtener valor saldo")
                                                                onCompletion(false, 0, "")
                                                            })
                                                        }
                                                      } else {
                                                        DispatchQueue.main.async(execute: {
                                                            Logger.log("error de servicio obtener valor saldo")
                                                            onCompletion(false, 0, "")
                                                        })
                                                        }
        })
    }

    /// Retorna el rateplan del numero en los contratos del cliente
    ///
    /// - Parameter numero: numero al cual buscar rateplan
    /// - Returns: rateplan del numero
    static func obtenerRatePlan(numero: String) -> Int {
        for linea in Globales.datosCliente.contratos {
            if linea.nroCelular == numero {
                return linea.ratePlan
            }
        }
        return 0
    }

    /// Genera el label donde se muestra el numero de telefono en multilinea
    ///
    /// - Parameters:
    ///   - labelNumeroTelefono: UILabel donde se asigna string
    ///   - numeroTelefono: numero de telefono de cuenta hija
    static func generarLabelTelefono(labelNumeroTelefono: UILabel, numeroTelefono: String) {
        let posicionEspacioVacio = 3

        let numeroConEspaciado = UtilApp.insertarCaracterEnString(caracter: " ", texto: numeroTelefono, posicion: posicionEspacioVacio)
        let textoLinea = NSLocalizedString("lbl_telefono_multilinea", comment: "label de numero telefono").replacingOccurrences(of: "{linea}", with: numeroConEspaciado)

        labelNumeroTelefono.attributedText = DesignHelper.modificarAtributosTexto(textoLinea, sizeTextoModificado: 14, sizeFuente: 14, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: "+\(numeroConEspaciado)", colorFuente: UIColor.white, espaciado: 1.5)
    }

    /// Genera el label donde se muestra el saldo de la cuenta hija
    ///
    /// - Parameters:
    ///   - labelSaldo: UILabel donde se asigna string
    ///   - saldo: saldo de la cuenta hija
    static func generarLabelSaldo(labelSaldo: UILabel, saldo: String) {
        labelSaldo.isHidden = false
        let textoSaldo = NSLocalizedString("lbl_saldo_multilinea", comment: "lbl con saldo multilinea").replacingOccurrences(of: "{saldo}", with: saldo)
        labelSaldo.attributedText = DesignHelper.modificarAtributosTexto(textoSaldo, sizeTextoModificado: 14, sizeFuente: 14, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: "\(saldo)", colorFuente: UIColor.white, espaciado: 1.5)
    }

    /// Se ajusta vista de pop up en flujo compra bolsa dentro del popup inicial de multilinea
    ///
    /// - Parameters:
    ///   - esMultilinea: indicador de si es multilinea
    ///   - popupController: controlador de popup que se modificará
    ///   - controllerPopupMultilinea: controlador de popup inicial de multilinea
    static func ajustarPopupInterno(esMultilinea: Bool, popupController: UIViewController, controllerPopupMultilinea: PopupCompraBolsasMultilinea) {
        if esMultilinea {
            popupController.view.frame.size.height = controllerPopupMultilinea.view.frame.size.height
            popupController.view.frame.size.width = controllerPopupMultilinea.view.frame.size.width
            popupController.view.viewWithTag(1)?.frame.size.height = (controllerPopupMultilinea.view.viewWithTag(1)?.frame.height)!
            popupController.view.viewWithTag(1)?.frame.size.width = (controllerPopupMultilinea.view.viewWithTag(1)?.frame.width)!
            popupController.view.viewWithTag(1)?.frame.origin.y = CGFloat(0)
            popupController.view.viewWithTag(1)?.frame.origin.x = CGFloat(0)
        }
    }

    /// Genera el label donde se muestra el numero de telefono en multilinea popup compra bolsa
    ///
    /// - Parameters:
    ///   - labelNumeroTelefono: UILabel donde se asigna string
    ///   - numeroTelefono: numero de telefono de cuenta hija
    static func generarLabelTelefonoPopupCompraBolsa(labelNumeroTelefono: UILabel, numeroTelefono: String, texto: String) {
        let posicionEspacioVacio = 3

        let numeroConEspaciado = UtilApp.insertarCaracterEnString(caracter: " ", texto: numeroTelefono, posicion: posicionEspacioVacio)
        let textoLinea = texto.replacingOccurrences(of: "{linea}", with: numeroConEspaciado)

        labelNumeroTelefono.attributedText = DesignHelper.modificarAtributosTexto(textoLinea, sizeTextoModificado: 14, sizeFuente: 14, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: "+\(numeroConEspaciado)", colorFuente: UIColor.white, espaciado: 1.5)
        labelNumeroTelefono.textAlignment = .center
    }
}
