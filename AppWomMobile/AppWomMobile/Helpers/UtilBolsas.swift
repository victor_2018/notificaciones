//
//  UtilBolsas.swift
//  AppWomMobile
//
//  Clase que contiene funciones de uso general de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

struct UtilBolsas {

    /// Aplica la animacion al agregar bolsas al desagruparlsa
    ///
    /// - Parameters:
    ///   - tablaBolsas: tabla principal
    ///   - listadoBolsasAgrupada: listado donde se muestran las bolsas
    ///   - listadoBolsas: listado con todas las bolsas disponibles
    ///   - indexPath: indexPath de la celda seleccionada
    static func aplicarAnimacionCelda(_ tablaBolsas: UITableView, listadoBolsasAgrupada: inout [BolsaTO], listadoBolsas: inout [BolsaTO], indexPath: IndexPath) {
        var contadorFilas = 1

        if UtilBolsas.validarFilaAgrupadaEnLista(indexPath.section, fila: indexPath.row, listaBolsas: listadoBolsasAgrupada) {
            if listadoBolsasAgrupada[indexPath.row].colapsado {
                for bolsa in listadoBolsas {
                    if bolsa.agrupacion == listadoBolsasAgrupada[indexPath.row].agrupacion {
                        listadoBolsasAgrupada.insert(bolsa, at: indexPath.row + contadorFilas)
                        tablaBolsas.beginUpdates()
                        tablaBolsas.insertRows(at: [
                            IndexPath(row: indexPath.row + contadorFilas, section: indexPath.section)
                            ], with: .automatic)
                        tablaBolsas.endUpdates()
                        contadorFilas += 1
                    }
                }
            } else {
                for bolsa in listadoBolsas {
                    if bolsa.agrupacion == listadoBolsasAgrupada[indexPath.row].agrupacion {
                        listadoBolsasAgrupada.remove(at: indexPath.row + 1)
                        tablaBolsas.beginUpdates()
                        tablaBolsas.deleteRows(at: [
                            IndexPath(row: indexPath.row + 1, section: indexPath.section)
                            ], with: .automatic)
                        tablaBolsas.endUpdates()
                    }
                }
            }
            listadoBolsasAgrupada[indexPath.row].colapsado = !listadoBolsasAgrupada[indexPath.row].colapsado
        } else {
            listadoBolsasAgrupada[indexPath.row].colapsado = !listadoBolsasAgrupada[indexPath.row].colapsado
            tablaBolsas.reloadRows(at: [indexPath], with: .automatic)
        }

    }

    /// Valida si la fila enviada es agrupada
    ///
    /// - Parameters:
    ///   - seccion: seccion de fila
    ///   - fila: indice de fila
    ///   - listaBolsas: lista de las bolsas dentro de la seccion
    /// - Returns: Retorna verificacion de si es celda agrupada o no
    static func validarFilaAgrupadaEnLista(_ seccion: Int, fila: Int, listaBolsas: [BolsaTO]) -> Bool {
        if listaBolsas.count > 0 && listaBolsas[fila].cantidadAgrupacion > 1 {
            return true
        } else {
            return false
        }
    }

    /// Genera los arreglos de las bolsas de la tabla del home
    ///
    /// - Parameters:
    ///   - listadoGrupoBolsas: Listado con las agrupaciones de las bolsas
    ///   - listadoBolsasAgrupada: Listado con als bolas visibles en la tabla
    ///   - listadoBolsas: listado con todas las bolsas disponibles del cliente
    ///   - tipoBolsa: tipo de bolsa (DATOS, MINUTOS, SMS)
    static func generarArreglosHome(_ listadoGrupoBolsas: [GrupoBolsaTO], listadoBolsasAgrupada: inout [BolsaTO], listadoBolsas: inout [BolsaTO], tipoBolsa: String) {
        for agrupacion in listadoGrupoBolsas {
            if [Constants.Indicadores.tipoTraficoDatos, Constants.Indicadores.tipoTraficoMinutos].contains(tipoBolsa) && tipoBolsa == agrupacion.tipo && agrupacion.tipo != Constants.Indicadores.tipoBasal {
                if agrupacion.cantidadAgrupacion > 1 {
                    listadoBolsasAgrupada.append(BolsaTO(grupo: agrupacion, esPromo: false, esCabecera: true))
                }

                for (index, bolsa) in agrupacion.bolsas.enumerated() {
                    if agrupacion.cantidadAgrupacion > 1 {
                        bolsa.esHijo = true
                        bolsa.indiceEnListado = index
                        bolsa.agrupacion = agrupacion.idAgrupacion
                    } else {
                        bolsa.esHijo = false
                        bolsa.agrupacion = agrupacion.idAgrupacion
                        listadoBolsasAgrupada.append(bolsa)
                    }
                    if !bolsa.esBasal {
                        listadoBolsas.append(bolsa)
                    }
                }
            } else if tipoBolsa == agrupacion.tipo {
                for bolsa in agrupacion.bolsas {
                    bolsa.agrupacion = agrupacion.idAgrupacion
                    listadoBolsasAgrupada.append(bolsa)
                }
            }
        }
    }
    /// Funcion que crea objeto de seccion interna
    ///
    /// - Parameter seccion: indice de seccion
    /// - Returns: objeto de seccion
    static func obtenerCeldaSeccionInterna(_ tablaBolsas: UITableView, seccion: Int, seccionBasales: Int, seccionBolsasCompradas: Int, seccionBolsasPromo: Int, tipoTrafico: String) -> CeldaSeccionBolsasInternaIphone {
        let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSeccionBolsasInternaIphone")! as! CeldaSeccionBolsasInternaIphone

            switch seccion {
            case seccionBasales:
                if tipoTrafico == Constants.Indicadores.tipoTraficoDatos {
                    viewSection.asignarDatos(Constants.Indicadores.tipoBasal)
                } else if tipoTrafico == Constants.Indicadores.tipoTraficoMinutos {
                    viewSection.asignarDatos(Constants.Indicadores.tipoBolsaMinutosBasal)
                } else {
                    viewSection.asignarDatos(Constants.Indicadores.tipoBolsaSMS)
                }
            case seccionBolsasCompradas:
                viewSection.asignarDatos(Constants.Indicadores.tipoBolsaComprada)
            case seccionBolsasPromo:
                viewSection.asignarDatos(Constants.Indicadores.tipoBolsaPromocional)
            default:
                Logger.log()
            }

        return viewSection
    }

    /// Retorna la cantidad de filas para secciones
    ///
    /// - Parameters:
    ///   - largoBolsas: Largo de listadode bolsas
    ///   - indicadorBolsasVacias: Variable que guarda booleano que indica si el listado esta vacio
    /// - Returns: cantidad de filas en seccion
    static func determinarNumeroFilas(_ largoBolsas: Int, indicadorBolsasVacias: inout Bool, esconderSinBolsas: Bool) -> Int {
        // en caso de que no existan bolsas en listados
        let seccionSinBolsas = 1

        if largoBolsas > 0 {
            indicadorBolsasVacias = false
            return largoBolsas
        } else if esconderSinBolsas {
            indicadorBolsasVacias = true
            return 0
        } else {
            indicadorBolsasVacias = true
            return seccionSinBolsas
        }
    }

    /// Primero verifica de que tipo de categoria es la bolsa en el ciclo, y luego verifica si es bolsa agrupada.
    ///
    /// - Parameter bolsas: Listado de bolsas totales
    static func verificarCategoriaBolsa(_ bolsas: [JSON]) {
        var banderaHybrid = false
        for bolsa in bolsas {
            switch bolsa["idCategoria"].intValue {
            case Constants.Indicadores.categoriaBasal:
                // Validación para verificar si existe basal voz para caso de home hibridos
                if bolsa["tipoAgrupacion"].stringValue == Constants.Indicadores.tipoTraficoMinutos {
                    banderaHybrid = true
                }
                verificarBolsaAgrupada(bolsa, listadoBolsas: &Globales.listaGrupoBasales)
            case Constants.Indicadores.categoriaBolsasCompradas:
                verificarBolsaAgrupada(bolsa, listadoBolsas: &Globales.listaGrupoBolsas)
            case Constants.Indicadores.categoriaPromocionales:
                verificarBolsaAgrupada(bolsa, listadoBolsas: &Globales.listaGrupoBolsasPromocionales)
            default:
                Logger.log("Categoria no existente")
            }
        }

        verificarBolsaBasalHibrido(banderaHybrid: banderaHybrid)
    }

    /// Se verifica la existencia de una bolsa basal VOZ para hibridos.
    /// Si no existe se agrega una bolsa manualmente con
    ///
    /// - Parameter banderaHybrid: Bandera que indica si existe una basal de voz
    static func verificarBolsaBasalHibrido(banderaHybrid: Bool) {
        // Datos de bolsa basal generada en duro para que se visualice fila
        let idCategoria = 1
        let tipoAgrupacionBolsa = "VOZ"
        let nombreCategoriaBolsa = "Basal"
        let redBolsa = "Transversales"
        let consumidoBolsa = 120
        let idAgrupacionBolsa = 11

        if [Constants.TipoHome.homeControladoAntiguo, Constants.TipoHome.homeControladoSimple, Constants.TipoHome.homePostpagoHibrido].contains(Globales.homeSeleccionado) && !banderaHybrid && Globales.datosLlamadas.minutosDisponibles > 0 {

            let jsonObject: JSON = [
                "idCategoria": idCategoria,
                "nombreCategoria": nombreCategoriaBolsa,
                "idAgrupacion": idAgrupacionBolsa,
                "tipoAgrupacion": tipoAgrupacionBolsa,
                "nombreComercial": NSLocalizedString("label_minutos_plan", comment: "Texto que se visualiza en basal minutos"),
                "red": redBolsa,
                "totalBolsa": 0,
                "vigenteBolsa": 0,
                "consumidoBolsa": consumidoBolsa
            ]

            verificarBolsaAgrupada(jsonObject, listadoBolsas: &Globales.listaGrupoBasales)
        }
    }

    /// Verifica si la bolsa enviada pertenece en el listado de agrupadas, si no crea una nueva
    ///
    /// - Parameters:
    ///   - bolsa: Bolsa en el ciclo
    ///   - listadoBolsas: Listado de bolsas correspondientes a la categoria de la bolsa del ciclo
    static func verificarBolsaAgrupada(_ bolsa: JSON, listadoBolsas: inout [GrupoBolsaTO]) {

        for bolsaAgrupada in listadoBolsas {
            if bolsaAgrupada.idAgrupacion == bolsa["idAgrupacion"].intValue {
                let bolsaNueva = BolsaTO(json: bolsa, esPromo: bolsa["idCategoria"].intValue == 3, tipoBolsa: bolsa["tipoAgrupacion"].stringValue, esBasal: bolsa["idCategoria"].intValue == 1, tipoTematica: bolsa["red"].stringValue.lowercased() == Constants.Indicadores.tipoTematica)

                bolsaAgrupada.cantidadAgrupacion = bolsaAgrupada.cantidadAgrupacion + 1
                bolsaAgrupada.consumido = bolsaAgrupada.consumido + bolsa["consumidoBolsa"].intValue
                bolsaAgrupada.totalAgrupacion = bolsaAgrupada.totalAgrupacion + bolsa["totalBolsa"].intValue
                bolsaAgrupada.vigente = bolsaAgrupada.vigente + bolsa["vigenteBolsa"].intValue
                bolsaAgrupada.bolsas.append(bolsaNueva)
                return
            }
        }

        let grupoBolsa = GrupoBolsaTO(bolsa: bolsa)

        let bolsaNueva = BolsaTO(json: bolsa, esPromo: bolsa["idCategoria"].intValue == 3, tipoBolsa: bolsa["tipoAgrupacion"].stringValue, esBasal: bolsa["idCategoria"].intValue == 1, tipoTematica: bolsa["red"].stringValue.lowercased() == Constants.Indicadores.tipoTematica)

        grupoBolsa.bolsas.append(bolsaNueva)
        listadoBolsas.append(grupoBolsa)
    }

    /// Abre en browser la pagina web para recargar saldo
    ///
    /// - Parameter saldoFaltante: En caso de abrir recarga desde comprar bolsa, es el saldo que falta para comprar bolsa seleccionada
    static func abrirRecargaBrowser(saldoFaltante: String, numeroTelefono: String) {
        let numeroCortado = String(numeroTelefono.characters.dropFirst(2))

        var stringURLRecargar = URLServicios.recargarSaldo.replacingOccurrences(of: "{numero}", with: numeroCortado)
        stringURLRecargar = stringURLRecargar.replacingOccurrences(of: "{saldo}", with: String(Globales.datosCliente.saldo))

        if Globales.tabBarSeleccionado.selectedIndex == Constants.Indicadores.tabComprarBolsas {
            stringURLRecargar = stringURLRecargar.replacingOccurrences(of: "{version}", with: Constants.Indicadores.recargarCompraBolsas)
            stringURLRecargar = stringURLRecargar + "&montorecarga=\(saldoFaltante)"
        } else {
            stringURLRecargar = stringURLRecargar.replacingOccurrences(of: "{version}", with: Constants.Indicadores.recargarHome)
        }

        let urlRecarga = URL(string: stringURLRecargar)

        UIApplication.shared.openURL(urlRecarga!)
    }

    /**
     Función que estructura las filas de historial de bolsas
     - parameter bolsa:   bolsa con datos para asignar
     - parameter vigenciaMultiple:   indicador de fila agrupada
     */
    static func asignarDiaVencimientoBolsa(_ bolsa: BolsaTO, vigenciaMultiple: Int) -> String {
        var diaMes = ""
        var esMes = false
        var periodo = ""
        let diasEnMinutos = 1440
        let limiteMesesIlimitada = 72
        var diaVencimiento = ""

        diaMes = UtilBolsas.asignarDiasVencimiento(esMes: &esMes, bolsa: bolsa)

        if esMes {
            periodo = NSLocalizedString("lbl_meses", comment: "label para meses")
        } else {
            periodo = NSLocalizedString("lbl_dias", comment: "label para dias")
        }

        if vigenciaMultiple > 1 {
            diaVencimiento = NSLocalizedString("label_vigencia_multiple", comment: "label que indica que la vigencia se compone de mas bolsas")
        } else if bolsa.diaVencimiento != nil {
            let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.minute, from: Date(), to: bolsa.fechaTerminoDate as Date, options: NSCalendar.Options.init(rawValue: 0))
            let minutosBolsa = components.minute

            if Globales.indicadorProblemaFechas {
                diaVencimiento =  ""
            } else if bolsa.tipoTematica == true && minutosBolsa! < diasEnMinutos {
                diaVencimiento = UtilBolsas.determinarVencimientoTematicas(bolsa)
            } else if [Constants.Indicadores.tipoTraficoDatos, Constants.Indicadores.tipoTraficoMinutos].contains(bolsa.tipoBolsa) && !bolsa.esBasal && !bolsa.promocional {
                diaVencimiento = UtilHome.validarVencimientoBolsasCompradas(bolsa: bolsa, diaVencimiento: Int(diaMes)!)
            } else if Int(bolsa.diaVencimiento)! < 1 {
                diaVencimiento = NSLocalizedString("label_vence", comment: "label que cambia de vence a vence") + " " + NSLocalizedString("label_vencimiento_hoy", comment: "label indica que hoy vence la bolsa")
            } else if Int(diaMes) == 1 {
                diaVencimiento = NSLocalizedString("label_vence_manana", comment: "label que muestra vencimiento de bolsa mañana")
            } else if Int(diaMes)! > limiteMesesIlimitada && esMes {
                diaVencimiento = NSLocalizedString("label_vence_nunca", comment: "label que despliega vigencia para siempre")
                bolsa.esColapsable = false
            } else {
                diaVencimiento = NSLocalizedString("label_vence_en", comment: "label que cambia de vence a vence en") + " \(diaMes) \(periodo)"
            }
        } else {
            diaVencimiento = ""
        }

        if bolsa.esBasal {
            diaVencimiento = UtilHome.validarVencimientoBolsasBasales(bolsa: bolsa)
        }

        return diaVencimiento
    }

    /// Transforma fecha de vencimiento a mes si es mayor a 3 meses, si no lo muestra como días
    ///
    /// - Parameters:
    ///   - esMes: indicador de si está transformado a mes
    ///   - bolsa: bolsa con los datos de esta
    /// - Returns: retorna el dia de vencimiento
    static func asignarDiasVencimiento(esMes: inout Bool, bolsa: BolsaTO) -> String {
        var diaMes = ""
        if bolsa.diaVencimiento != nil {
            let diasVenc = Int(bolsa.diaVencimiento)
            if diasVenc! > 90 {
                diaMes = String(Int(bolsa.diaVencimiento)! / 30 )
                esMes = true
            } else {
                diaMes = bolsa.diaVencimiento
            }
        }
        return diaMes
    }

    /// Determina el texto que va en la sección de vencimiento de las bolsas tematicas
    ///
    /// - Parameter bolsa: Bolsa con los datos para ver vigencia
    /// - Returns: texto con vencimiento de bolsa
    static func determinarVencimientoTematicas(_ bolsa: BolsaTO) -> String {
        let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.minute, from: Date(), to: bolsa.fechaTerminoDate as Date, options: NSCalendar.Options.init(rawValue: 0))
        let minutos = components.minute
        let divisorADias = 60

        if minutos! < divisorADias {
            return NSLocalizedString("label_vence_en", comment: "label que cambia de vence a vence en") + " " + String(describing: minutos) + " " + NSLocalizedString("label_minutos", comment: "label que indica los minutos")
        } else {
            let horas = minutos! / divisorADias
            let resto = minutos! % divisorADias

            return NSLocalizedString("label_vence_en", comment: "label que cambia de vence a vence en") + " " + String(horas) + " " + NSLocalizedString("label_horas", comment: "label que indica las horas") + " " + String(resto) + " " + NSLocalizedString("label_minutos", comment: "label que indica los minutos")
        }
    }

    /// Corrige el problema en caso de que exista un problema con las fechas
    ///
    /// - Parameters:
    ///   - bolsa: objeto con datos de la bolsa
    ///   - fechaActivada: Enlace a label en vista de fecha de activación
    ///   - fechaVencimientoDesplegable: Enlace a label en vista de fecha de vencimiento
    ///   - horaActivacion: Enlace a label en vista de hora de activación
    ///   - horaFinalizacion: Enlace a label en vista de hora de finalización de bolsa
    static func correccionProblemaFechas(bolsa: BolsaTO, fechaActivada: UILabel, fechaVencimientoDesplegable: UILabel, horaActivacion: UILabel, horaFinalizacion: UILabel) {
        if Globales.indicadorProblemaFechas {
            fechaActivada.text = ""
            fechaVencimientoDesplegable.text = ""
            horaActivacion.text = ""
            horaFinalizacion.text = ""
        } else {
            fechaActivada.text = bolsa.fechaActivada
            fechaVencimientoDesplegable.text = bolsa.fechaVencimientoDesplegable
            horaActivacion.text = bolsa.horaActivada
            horaFinalizacion.text = bolsa.horaVencimiento
        }
    }

    /// Añade o resta dias (segun el signo del número) a la fecha enviada
    ///
    /// - Parameters:
    ///   - fecha: fecha en formato NSDate
    ///   - diasASumar: dias que se sumaran/restaran
    ///   - formato: formato en que se entregará la fecha en string
    /// - Returns: fecha formateada con la modificación
    static func sumarDiasFecha(fecha: Date, diasASumar: Int, formato: String) -> String {
        var newDateComponents = DateComponents()
        newDateComponents.day = diasASumar
        let fechaModificada = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: fecha, options: NSCalendar.Options.init(rawValue: 0))

        return Util.transformarFechaNSDate(fechaModificada!, formato: formato)
    }

}
