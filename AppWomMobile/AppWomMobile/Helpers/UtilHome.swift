//
//  UtilHome.swift
//  AppWomMobile
//
//  Clase que contiene funciones de uso general de los homes de la app.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

struct UtilHome {
    /// Funcion que retorna celda para la seccion de minutos.
    ///
    /// - Parameters:
    ///   - indexPath: Objeto con indice de seccion y fila.
    ///   - tablaBolsas: UITableView de tabla de bolsas
    ///   - bolsasDatosPromoVacia: Indicador de bolsas de datos promocionales vacia
    ///   - bolsasDatosCompradasVacia: Indicador de bolsas de datos compradas vacia
    ///   - bolsasDatosBasalesVacia: Indicador de bolsas de datos basales vacia
    ///   - bolsasDatosPromoAgrupada: Indicador de bolsas de agrupadas vacia
    ///   - bolsasDatosBasales: arreglo con bolsas basales
    ///   - bolsasDatosAgrupada: arreglo con bolsas agrupadas
    ///   - seccionBolsasPromo: Indice de seccion bolsas promocionales
    ///   - seccionBasales: Indice de seccion bolsas basales
    ///   - seccionBolsasCompradas: Indice de seccion bolsas compradas
    /// - Returns: Celda con estilo de bolsa de datos
    static func obtenerCeldaDatos(_ indexPath: IndexPath, tablaBolsas: UITableView, bolsasDatosPromoVacia: Bool, bolsasDatosCompradasVacia: Bool, bolsasDatosBasalesVacia: Bool, bolsasDatosPromoAgrupada: [BolsaTO], bolsasDatosBasales: [BolsaTO], bolsasDatosAgrupada: [BolsaTO], seccionBolsasPromo: Int, seccionBasales: Int, seccionBolsasCompradas: Int) -> UITableViewCell {

        if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasCompradas, indexPath: indexPath, bolsasAgrupada: bolsasDatosAgrupada) {
            let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
            return celda
        } else {
            switch indexPath.section {
            case seccionBasales:
                return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionDatosMovilesBasales, indexPath: indexPath, indicadorBolsasVacia: bolsasDatosBasalesVacia, listadoBolsas: bolsasDatosBasales, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_datos", comment: "mensaje sin bolsas de datos"))
            case seccionBolsasCompradas:
                return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionDatosMovilesBolsasCompradas, indexPath: indexPath, indicadorBolsasVacia: bolsasDatosCompradasVacia, listadoBolsas: bolsasDatosAgrupada, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_datos", comment: "mensaje sin bolsas de datos"))
            case seccionBolsasPromo:
                if !bolsasDatosPromoVacia {
                    return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionDatosMovilesBolsasPromocionales, indexPath: indexPath, indicadorBolsasVacia: bolsasDatosPromoVacia, listadoBolsas: bolsasDatosPromoAgrupada, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_datos_promo", comment: "mensaje sin bolsas de datos"))
                }
            default:
                Logger.log()
            }
        }
        return UITableViewCell()
    }

    /// Obtiene la celda con el estilo de la celda promocional
    ///
    /// - Parameters:
    ///   - tablaBolsas: UITableView de la vista
    ///   - seccion: indice de sección
    ///   - indexPath: Objeto con indice de seccion y fila.
    ///   - indicadorBolsasVacia: booleano que indica si la bolsa está vacía
    ///   - listadoBolsas: listado con objetos de bolsas
    ///   - mensajeSinBolsas: label que se muestra en el caso de que no existan bolsas
    /// - Returns: Objeto con celda promocional
    static func obtenerCeldaPromocional(tablaBolsas: UITableView, seccion: String, indexPath: IndexPath, indicadorBolsasVacia: Bool, listadoBolsas: [BolsaTO], mensajeSinBolsas: String) -> UITableViewCell {
        if !indicadorBolsasVacia {
            let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasPromocionalesHome", for: indexPath) as! CeldaBolsaPromocionalesHomeIphone
            celda.asignarDatos(listadoBolsas[indexPath.row], seccion: seccion, fila: indexPath.row)
            return celda
        } else if [Constants.Indicadores.seccionDatosMovilesBolsasCompradas, Constants.Indicadores.seccionMinutosBolsasCompradas].contains(seccion) {
            let celdaComprarBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaComprarBolsas", for: indexPath) as! CeldaComprarBolsasIphone
            return celdaComprarBolsas
        } else {
            let celdaSinBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSinBolsas", for: indexPath) as! CeldaSinBolsas
            celdaSinBolsas.asignarDatos(mensajeSinBolsas)
            return celdaSinBolsas
        }
    }

    /// Funcion que retorna celda para la seccion de minutos.
    ///
    /// - Parameters:
    ///   - indexPath:  Objeto con indice de seccion y fila.
    ///   - esPostpagoLibre: Indicador de home postpagoLibre
    ///   - tablaBolsas: UITableView de tabla de bolsas
    ///   - bolsasMinutosVacia: Indicador de bolsas de minutos vacia
    ///   - bolsasMinutos: arreglo de bolsas de minutos
    /// - Returns: Celda con estilo para minutos
    static func obtenerCeldaVozPostpago(_ indexPath: IndexPath, esPostpagoLibre: Bool, tablaBolsas: UITableView, bolsasMinutosVacia: Bool, bolsasMinutos: [BolsaTO]) -> UITableViewCell {
        if esPostpagoLibre {
            switch indexPath.row {
            case 0:
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasHome", for: indexPath) as! CeldaBolsasHomeIphone
                celda.asignarDatos(BolsaTO(postpagoLibre: true), seccion: Constants.Indicadores.seccionMinutos, fila: indexPath.row)
                return celda
            case 1:
                let celdaMinutosConsumidos = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaMinutosConsumidos", for: indexPath) as! CeldaMinutosConsumidosIphone
                celdaMinutosConsumidos.asignarDatos(String(Globales.datosLlamadas.minutosIntraRut))
                return celdaMinutosConsumidos
            case 2:
                let celdaSobreconsumo = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSobreconsumo", for: indexPath) as! CeldaSobreconsumoIphone
                celdaSobreconsumo.asignarDatos(UtilHome.obtenerSobreconsumo())

                if UtilHome.obtenerSobreconsumo() == "0" {
                    Globales.showSobreconsumo = false
                } else {
                    Globales.showSobreconsumo = true
                }
                return celdaSobreconsumo
            default:
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            }
        } else {
            if !bolsasMinutosVacia {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasHome", for: indexPath) as! CeldaBolsasHomeIphone
                celda.asignarDatos(bolsasMinutos[indexPath.row], seccion: Constants.Indicadores.seccionMinutos, fila: indexPath.row)
                return celda
            } else {
                let celdaSinBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSinBolsas", for: indexPath) as! CeldaSinBolsas
                celdaSinBolsas.asignarDatos(NSLocalizedString("label_sin_bolsas_voz", comment: "mensaje sin bolsas de voz"))
                return celdaSinBolsas
            }
        }
    }

    /// Obtiene valor del sobreconsumo
    ///
    /// - returns: sobreconsumo
    static func obtenerSobreconsumo() -> String {
        if Globales.datosLlamadas.minutosDisponibles == -1 {
            return String(0)
        } else if Globales.datosLlamadas.minutosDisponibles - Globales.datosLlamadas.minutosOffNet - Globales.datosLlamadas.minutosOnNet < 0 {
            return String((Globales.datosLlamadas.minutosDisponibles - Globales.datosLlamadas.minutosOffNet - Globales.datosLlamadas.minutosOnNet) * -1)
        } else {
            return String(0)
        }
    }

    /// Genera la celda de tipo CeldaBolsaHome para basales y bolsas antiguas
    ///
    /// - Parameters:
    ///   - indexPath: objeto con índice de celda
    ///   - tablaBolsas: UITableView de la tabla de bolsas
    ///   - listadoBolsas: Listado con los datos de las bolsas
    ///   - seccion: Indice de sección
    ///   - labelSinBolsas: Texto que va en la celda sin bolsas
    /// - Returns: Celda con los datos enviados
    static func obtenerCeldaBolsaHome(_ indexPath: IndexPath, tablaBolsas: UITableView, listadoBolsas: [BolsaTO], seccion: String, labelSinBolsas: String) -> UITableViewCell {
        if listadoBolsas.count > 0 {
            let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasHome", for: indexPath) as! CeldaBolsasHomeIphone
            celda.asignarDatos(listadoBolsas[indexPath.row], seccion: seccion, fila: indexPath.row)
            return celda
        } else {
            let celdaSinBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSinBolsas", for: indexPath) as! CeldaSinBolsas
            celdaSinBolsas.asignarDatos(labelSinBolsas)
            return celdaSinBolsas
        }
    }

    /// Genera la bolsa para celda de SMS
    ///
    /// - Parameters:
    ///   - indexPath: objeto con índice de celda
    ///   - tablaBolsas: UITableView de la tabla de bolsas
    ///   - bolsasMinutos: arreglo con bolsas de Minutos
    ///   - bolsasMinutosVacia: indicador de si no existen bolsas
    /// - Returns: Vista de celda con estilo de Minutos
    static func obtenerCeldaVoz(_ indexPath: IndexPath, tablaBolsas: UITableView, bolsasMinutos: [BolsaTO], bolsasMinutosVacia: Bool) -> UITableViewCell {
        if !bolsasMinutosVacia {
            let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasHome", for: indexPath) as! CeldaBolsasHomeIphone
            celda.asignarDatos(bolsasMinutos[indexPath.row], seccion: Constants.Indicadores.seccionMinutos, fila: indexPath.row)
            return celda
        } else {
            let celdaSinBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSinBolsas", for: indexPath) as! CeldaSinBolsas
            celdaSinBolsas.asignarDatos(NSLocalizedString("label_sin_bolsas_voz", comment: "mensaje sin bolsas de voz"))
            return celdaSinBolsas
        }
    }

    /// Genera la bolsa para celda de SMS
    ///
    /// - Parameters:
    ///   - indexPath: objeto con índice de celda
    ///   - tablaBolsas: UITableView de la tabla de bolsas
    ///   - bolsasMensajes: arreglo con bolsas de SMS
    ///   - bolsasMensajesVacia: indicador de si no existen bolsas
    /// - Returns: Vista de celda con estilo de SMS
    static func obtenerCeldaSMS(_ indexPath: IndexPath, tablaBolsas: UITableView, bolsasMensajes: [BolsaTO], bolsasMensajesVacia: Bool) -> UITableViewCell {
        if !bolsasMensajesVacia {
            let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasHome", for: indexPath) as! CeldaBolsasHomeIphone
            celda.asignarDatos(bolsasMensajes[indexPath.row], seccion: Constants.Indicadores.seccionSMS, fila: indexPath.row)
            return celda
        } else {
            let celdaSinBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSinBolsas", for: indexPath) as! CeldaSinBolsas
            celdaSinBolsas.asignarDatos(NSLocalizedString("label_sin_bolsas_SMS", comment: "mensaje sin bolsas sms"))
            return celdaSinBolsas
        }
    }

    /// Retorna altura de secciones para home Controlado
    ///
    /// - Parameters:
    ///   - seccion: índice de sección
    ///   - seccionEncabezado: índice de sección encabezado
    ///   - seccionPiePagina: índice de sección de pie de página
    /// - Returns: valor de altura de celda
    static func calcularAlturaSeccionControlado(seccion: Int, seccionEncabezado: Int, seccionPiePagina: Int, seccionBolsasMinutosPromo: Int, seccionBolsasPromo: Int, seccionDatosBasal: Int, seccionMinutosBasal: Int, bolsasMinutosPromo: [BolsaTO], bolsasDatosPromo: [BolsaTO]) -> CGFloat {
        let heightEncabezado = CGFloat(475)
        let heightEncabezadoConDeuda = CGFloat(540)
        let heightSecciones = CGFloat(70)
        let heightSeccionPiePagina = CGFloat(20)
        let heightSeccionBasales = CGFloat(55)

        if seccion == seccionEncabezado {
            if Globales.datosCliente.tieneDeuda {
                return heightEncabezadoConDeuda
            } else {
                return heightEncabezado
            }
        } else if seccion == seccionDatosBasal || seccion == seccionMinutosBasal {
            return heightSeccionBasales
        } else if seccion == seccionBolsasMinutosPromo && bolsasMinutosPromo.count == 0 {
            return 0
        } else if seccion == seccionBolsasPromo && bolsasDatosPromo.count == 0 {
            return 0
        } else if seccion == seccionPiePagina {
            return heightSeccionPiePagina
        }

        return heightSecciones
    }

    /// Retorna el valor de la celda
    ///
    /// - Parameters:
    ///   - indexPath: Objeto con indices de las celdas
    ///   - seccionEncabezado: índice sección encabezado
    ///   - validacionFilaInfoDatos: booleano que indica si la seccion es la de información
    ///   - listadoBolsasAgrupada: Listado de bolsas agrupadas
    /// - Returns: retorna altura de sección
    static func determinarAlturaAnimacionCeldaBolsas(indexPath: IndexPath, seccionEncabezado: Int, validacionFilaInfoDatos: Bool, listadoBolsasAgrupada: [BolsaTO]) -> CGFloat {
        let alturaFilaInfoDatos = CGFloat(20)

        if indexPath.section == seccionEncabezado {
            return 0
        } else if validacionFilaInfoDatos {
            return alturaFilaInfoDatos
        } else if UtilBolsas.validarFilaAgrupadaEnLista(indexPath.section, fila: indexPath.row, listaBolsas: listadoBolsasAgrupada) {
            return CGFloat(85)
        } else if listadoBolsasAgrupada.count > 0 && listadoBolsasAgrupada[indexPath.row].colapsado {
            return CeldaBolsasHomeIphone.heightDefecto
        } else if listadoBolsasAgrupada.count > 0 && !listadoBolsasAgrupada[indexPath.row].colapsado {
            return CeldaBolsasHomeIphone.heightExpandida
        }

        return CeldaBolsasHomeIphone.heightDefecto
    }

    /// Determina el indice de las seccione según el tipo de home
    ///
    /// - Parameters:
    ///   - tipoHome: tipo home ingresado
    ///   - cantidadSecciones: cantidad de secciones totales
    ///   - seccionDatosMoviles: indice seccion datos móviles
    ///   - seccionBasales: indice seccion basales
    ///   - seccionBolsasCompradas: indice seccion bolsas de datos compradas
    ///   - seccionBolsasPromo: indice seccion bolsas de datos promocionales
    ///   - seccionMinutos: indice seccion minutos
    ///   - seccionSMS: indice seccion SMS
    ///   - seccionPiePagina: indice seccion pie de página
    static func determinarSecciones(cantidadSecciones: inout Int, seccionDatosMoviles: inout Int, seccionBasales: inout Int, seccionBolsasCompradas: inout Int, seccionBolsasPromo: inout Int, seccionMinutos: inout Int, seccionMinutosBasales: inout Int, seccionMinutosCompradas: inout Int, seccionMinutosPromo: inout Int, seccionSMS: inout Int, seccionPiePagina: inout Int, seccionSMSBasal: inout Int) {
        switch Globales.homeSeleccionado {
        case Constants.TipoHome.homePostpagoHibrido, Constants.TipoHome.homeControladoAntiguo:
            cantidadSecciones = 10
            seccionDatosMoviles = 1
            seccionBasales = 2
            seccionBolsasPromo = 3
            seccionBolsasCompradas = 4
            seccionMinutos = 5
            seccionMinutosBasales = 6
            seccionMinutosPromo = 7
            seccionMinutosCompradas = 8
            seccionSMS = 9
            seccionSMSBasal = 10
            seccionPiePagina = 11
        case Constants.TipoHome.homePostpagoVozSMS:
            cantidadSecciones = 4
            seccionMinutos = 1
            seccionMinutosBasales = 2
            seccionSMS = 3
            seccionSMSBasal = 4
            seccionPiePagina = 5
            seccionDatosMoviles = -1
            seccionBolsasCompradas = -1
            seccionBolsasPromo = -1
            seccionBasales = -1
            seccionMinutosCompradas = -1
            seccionMinutosPromo = -1
        case Constants.TipoHome.homePrepago:
            cantidadSecciones = 6
            seccionDatosMoviles = 1
            seccionBasales = -1
            seccionBolsasPromo = 2
            seccionBolsasCompradas = 3
            seccionMinutos = 4
            seccionMinutosBasales = -1
            seccionMinutosPromo = 5
            seccionMinutosCompradas = 6
            seccionPiePagina = 7
            seccionSMS = -1
        case Constants.TipoHome.homeControladoSimple:
            cantidadSecciones = 8
            seccionDatosMoviles = 1
            seccionBasales = 2
            seccionBolsasPromo = 3
            seccionBolsasCompradas = 4
            seccionMinutos = 5
            seccionMinutosBasales = 6
            seccionMinutosCompradas = -1
            seccionMinutosPromo = -1
            seccionSMS = 7
            seccionSMSBasal = 8
            seccionPiePagina = 9
        default:
            Logger.log("default switch determinarSecciones")
        }
    }

    /// Valida si se debe mostrar la fila de pie de seccion
    ///
    /// - Parameters:
    ///   - seccionActual: seccion en la que se esta recorriendo
    ///   - seccionEnBusqueda: seccion en la que se desea agregar pie de seccion
    ///   - indexPath: objeto de indexpath con seccion y fila
    ///   - bolsasAgrupada: listado de bolsas
    /// - Returns: validador de si se debe mostrar fila
    static func validadorFilaPieSeccion(seccionEnBusqueda: Int, indexPath: IndexPath, bolsasAgrupada: [BolsaTO]) -> Bool {
        if indexPath.section == seccionEnBusqueda && bolsasAgrupada.count > 0 && bolsasAgrupada.count == indexPath.row || indexPath.section == seccionEnBusqueda && bolsasAgrupada.count == 0 && indexPath.row == 1 {
            return true
        } else {
            return false
        }
    }
    /// Determina vencimiento para bolsas hijas (agrupadas)
    ///
    /// - Parameters:
    ///   - bolsa: objeto con datos de bolsa
    ///   - diaMes: dias o meses vencimiento
    /// - Returns: String con mensaje de vencimiento
    static func validarVencimientoBolsasCompradas(bolsa: BolsaTO, diaVencimiento: Int) -> String {
        if diaVencimiento < 1 {
            return NSLocalizedString("label_bolsa_termina_hoy", comment: "label que indica que bolsa termina hoy")
        } else if diaVencimiento == 1 {
            return NSLocalizedString("label_bolsa_termina_manana", comment: "label que indica que bolsa termina mañana")
        } else {
            return NSLocalizedString("label_bolsa_termina", comment: "label de término de ciclo").replacingOccurrences(of: "{fecha}", with: bolsa.fechaVencimientoDesplegable)
        }
    }
    /// Determina vencimiento para bolsas
    ///
    /// - Parameter bolsa: objeto con datos de la bolsa
    /// - Returns: String con mensaje de vencimiento
    static func validarVencimientoBolsasBasales(bolsa: BolsaTO) -> String {
        var newDateComponents = DateComponents()
        newDateComponents.day = 1
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        let fechaModificada = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: Globales.datosCliente.proximoConsumoDate, options: NSCalendar.Options.init(rawValue: 0))
        var texto = ""
        let components = Calendar.current.dateComponents([.day], from: calendar.startOfDay(for: Date()), to: fechaModificada!)

        if fechaModificada == Date() || components.day! < 1 {
            texto = NSLocalizedString("label_vence_basal_hoy", comment: "label vencimiento basales")
            texto = texto.replacingOccurrences(of: "{datos}", with: Constants.Hashmap.textoBasal[bolsa.tipoBolsa]!)
        } else {
            texto = NSLocalizedString("label_vence_basal", comment: "label vencimiento basales")
            texto = texto.replacingOccurrences(of: "{datos}", with: Constants.Hashmap.textoBasal[bolsa.tipoBolsa]!)
            texto = texto.replacingOccurrences(of: "{fecha}", with: UtilBolsas.sumarDiasFecha(fecha: Globales.datosCliente.proximoConsumoDate, diasASumar: 1, formato: "d/MM/yy"))
        }
        return texto
    }
    /// Se verifica si se debe mostrar consumo y/o restante de la bolsa en base de parametro plataforma en consulta consumo
    ///
    /// - Parameters:
    ///   - bolsa: objeto con los datos de la bolsa
    ///   - restanteBolsa: label para consumo
    ///   - labelVence: label para vencimiento
    static func verificarIndicadorPlataforma(bolsa: BolsaTO, restanteBolsa: UILabel, labelVence: UILabel) {
        switch bolsa.plataforma {
        case Constants.Indicadores.soloConsumo:
            restanteBolsa.isHidden = false
            labelVence.isHidden = true
        case Constants.Indicadores.soloVigencia:
            restanteBolsa.isHidden = true
            labelVence.isHidden = false
        case Constants.Indicadores.soloNombre:
            restanteBolsa.isHidden = true
            labelVence.isHidden = true
        default:
            restanteBolsa.isHidden = false
            labelVence.isHidden = false
        }
    }
}
