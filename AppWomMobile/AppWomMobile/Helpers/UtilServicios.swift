//
//  UtilServicios.swift
//  AppWomMobile
//
//  Clase que contiene funciones para llamadas de servicio principales.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

class UtilServicios {

    /// Cantidad de servicios finalizados
    static var validadorServicios = 0
    /// Cantidad de servicios totales en llamada asincrona
    static var cantidadAsincronaTotal = 2
    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func buscarDatosCliente(_ onCompletion: @escaping (_ retorno: Bool) -> () ) {
        PSDatosConsultaCliente.sharedInstance.getDatosCliente(Globales.numeroTelefono, linea: Constants.Indicadores.tipoConsultaLinea,
                                                              onCompletion: {  (json: JSON) in if let results = json["respConsultarInfoCliente"]["ConsultaInfoClienteLineaResponse"].array {
                                                                for entry in results {
                                                                    Globales.datosCliente = EncabezadoHomeTO(json: entry)
                                                                }
                                                                DispatchQueue.main.async(execute: {
                                                                    var respuestaServicioPrincipal = Bool()
                                                                    if ![Constants.TipoHome.homeControladoSimple, Constants.TipoHome.homePrepago, Constants.TipoHome.homeControladoAntiguo].contains(Globales.homeSeleccionado) {
                                                                        self.buscarDatosClientePorRUT { retorno in
                                                                            validadorServicios += 1
                                                                            respuestaServicioPrincipal = retorno
                                                                            if validadorServicios == cantidadAsincronaTotal {
                                                                                validadorServicios = 0
                                                                                onCompletion(retorno)
                                                                            }
                                                                        }
                                                                        self.obtenerFacturas { retorno in
                                                                            validadorServicios += 1
                                                                            if validadorServicios == cantidadAsincronaTotal {
                                                                                validadorServicios = 0
                                                                                onCompletion(respuestaServicioPrincipal)
                                                                            }
                                                                        }
                                                                    } else {
                                                                        self.buscarDatosPlan { retorno in
                                                                            onCompletion(retorno)
                                                                        }
                                                                    }
                                                                })
                                                              } else {
                                                                DispatchQueue.main.async(execute: {
                                                                    onCompletion(false)
                                                                    Logger.log("error de servicio buscar datos cliente")
                                                                })
                                                                }
        })
    }

    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func buscarDatosClientePorRUT(_ onCompletion: @escaping (_ retorno: Bool) -> () ) {
        PSDatosConsultaCliente.sharedInstance.getDatosCliente(Globales.datosCliente.rut, linea: Constants.Indicadores.tipoConsultaRUT,
                                                              onCompletion: {  (json: JSON) in if json["respConsultarInfoCliente"]["errorCode"].stringValue == "0" {
                                                                Globales.datosCliente.contratos.removeAll()
                                                                if let results = json["respConsultarInfoCliente"]["ConsultaInfoClienteRutResponse"][0]["customer"].array {
                                                                    for cliente in results {
                                                                        for contrato in cliente["contract"].arrayValue {
                                                                            if contrato["estado"].stringValue.uppercased() == "A" || contrato["estado"].stringValue.uppercased() == "S" {
                                                                                Globales.datosCliente.contratos.append(ContractTO(json: contrato))
                                                                            }
                                                                        }
                                                                    }

                                                                    if Globales.datosCliente.contratos.count == 1 {
                                                                        Globales.datosCliente.esBigBoss = false
                                                                    } else if !Globales.datosCliente.contratos.isEmpty {
                                                                        Globales.datosCliente.contratos.sort(by: { $0.fechaActivacion < $1.fechaActivacion })
                                                                        Globales.datosCliente.esBigBoss = UtilApp.identificarBigBoss(listadoClientes: Globales.datosCliente.contratos, numero: Globales.numeroTelefono)
                                                                    }

                                                                    DispatchQueue.main.async(execute: {
                                                                        self.buscarDatosPlan { retorno in
                                                                            onCompletion(retorno)
                                                                        }
                                                                    })
                                                                } else {
                                                                    DispatchQueue.main.async(execute: {
                                                                        onCompletion(false)
                                                                        Logger.log("error de servicio buscar datos cliente por rut: no trajo datos")
                                                                    })
                                                                }
                                                              } else {
                                                                DispatchQueue.main.async(execute: {
                                                                    onCompletion(false)
                                                                    Logger.log("error de servicio buscar datos cliente por rut")
                                                                })
                                                                }
        })
    }
    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func buscarDatosPlan(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        GetNextelCustomerInfo.sharedInstance.getDatosPlan(Globales.numeroTelefono,
                                                          onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
                                                            if json["error_code"].stringValue == "0" {
                                                                DispatchQueue.main.async(execute: {
                                                                    Globales.datosCliente.nombrePlan = json["Desc_IDPlan"].stringValue
                                                                    Globales.datosCliente.idFormaPago = json["iDFormPago"].stringValue
                                                                    Globales.datosCliente.tipoPlan = json["segment_client"].stringValue.uppercased()

                                                                    if Globales.datosCliente.nombrePlan == nil || Globales.datosCliente.idFormaPago == nil || Globales.datosCliente.tipoPlan == nil {
                                                                        Logger.log("Error GetNextelCustomerInfo no trajo datos")
                                                                        onCompletion(false)
                                                                    } else {
                                                                        self.recuperarInfoCliente { retorno in
                                                                            onCompletion(retorno)
                                                                        }
                                                                    }
                                                                })
                                                            } else {
                                                                DispatchQueue.main.async(execute: {
                                                                    Logger.log("error tipo: \(json["error_code"].stringValue)")
                                                                    onCompletion(false)
                                                                })
                                                            }
                                                          } else {
                                                            DispatchQueue.main.async(execute: {
                                                                Logger.log("error de servicio buscarDatosPlan")
                                                                onCompletion(false)
                                                            })
                                                            }
        })
    }

    /// Función se obtiene el tipo de Home segun el rateplan.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerTipoHome(_ numero: String, onCompletion: @escaping (_ retorno: Bool) -> ()) {
        var numeroTelefono = ""
        if numero != "" {
            numeroTelefono = Globales.numeroTelefono
        } else {
            numeroTelefono = Constants.Login.numeroDemo
        }
        ObtenerTipoHome.sharedInstance.getTipoHome(String(numeroTelefono),
                                                   onCompletion: {  (json: JSON) in
                                                    let respuesta = json.stringValue
                                                    if respuesta != "" && respuesta.components(separatedBy: ":")[0] != "ERROR" {
                                                        Globales.homeSeleccionado = respuesta
                                                        DispatchQueue.main.async(execute: {
                                                            Logger.log("finalizacion servicio obtencion tipo home")
                                                            onCompletion(true)
                                                        })
                                                    } else {
                                                        DispatchQueue.main.async(execute: {
                                                            Logger.log("error de servicio obtener tipo home")
                                                            onCompletion(false)
                                                        })
                                                    }
        })

    }
    /// Función donde se llama al servicio que trae los datos del saldo del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerValorSaldo(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        let valorPositivo = "Y"
        GetSearchBalanceIcc.sharedInstance.getBalance(Globales.numeroTelefono, detalle: valorPositivo,
                                                      onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
                                                        if json["error_code"] != JSON.null && json["error_code"].stringValue == "0" && Int(json["totalBalance"].stringValue) != nil {
                                                            Globales.datosCliente.saldo = Int(json["totalBalance"].stringValue)!
                                                            Globales.datosCliente.saldoFormateado = Util.formatearPesos(json["totalBalance"].stringValue)

                                                            DispatchQueue.main.async(execute: {
                                                                var respuestaServicioPrincipal = Bool()
                                                                if Globales.datosCliente.urlFactura == "" && ![Constants.TipoHome.homeControladoSimple, Constants.TipoHome.homePrepago, Constants.TipoHome.homeControladoAntiguo].contains(Globales.homeSeleccionado) {

                                                                    self.obtenerFacturas { retorno in
                                                                        validadorServicios += 1
                                                                        if validadorServicios == cantidadAsincronaTotal {
                                                                            validadorServicios = 0
                                                                            onCompletion(respuestaServicioPrincipal)
                                                                        }
                                                                    }
                                                                } else {
                                                                    cantidadAsincronaTotal = 1
                                                                }
                                                                self.consultaConsumo { retorno in
                                                                    validadorServicios += 1
                                                                    respuestaServicioPrincipal = retorno
                                                                    if validadorServicios == cantidadAsincronaTotal {
                                                                        validadorServicios = 0
                                                                        onCompletion(retorno)
                                                                    }
                                                                }

                                                            })
                                                        } else {
                                                            DispatchQueue.main.async(execute: {
                                                                Logger.log("error de servicio obtener valor saldo")
                                                                onCompletion(false)
                                                            })
                                                        }
                                                      } else {
                                                        DispatchQueue.main.async(execute: {
                                                            Logger.log("error de servicio obtener valor saldo")
                                                            onCompletion(false)
                                                        })
                                                        }
        })
    }

    /// Función donde se llama al servicio que trae los datos de llamadas del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func recuperarDatosLlamados(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        let fechaInicial = Util.transformarFechaNSDate(Globales.datosCliente.inicioConsumoDate, formato: "yyyy-MM-dd")
        let fechaTermino = Util.transformarFechaNSDate(Globales.datosCliente.proximoConsumoDate, formato: "yyyy-MM-dd")
        RecuperaDetalleLlamados.sharedInstance.getDetalleLlamados(Globales.numeroTelefono, fechaDesde: fechaInicial, fechaHasta: fechaTermino,
                                                                  onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
                                                                    //Se guardan valores obtenidos previamente en recuperaInfoCliente
                                                                    let minutosDisponibles = Globales.datosLlamadas.minutosDisponibles
                                                                    let smsIlimitado = Globales.datosLlamadas.smsIlimitado

                                                                    Globales.datosLlamadas = RecuperaDetalleLlamadosTO(json: json)
                                                                    Globales.datosLlamadas.minutosDisponibles = minutosDisponibles
                                                                    Globales.datosLlamadas.smsIlimitado = smsIlimitado

                                                                    DispatchQueue.main.async(execute: {
                                                                        if Globales.datosLlamadas.minutosDisponibles == nil {
                                                                            Logger.log("Error RecuperaDetalleLlamados no trajo datos")
                                                                            onCompletion(false)
                                                                        } else {
                                                                            self.consultaConsumo { retorno in
                                                                                onCompletion(retorno)
                                                                            }
                                                                        }
                                                                    })
                                                                  } else {
                                                                    DispatchQueue.main.async(execute: {
                                                                        Logger.log("error de servicio recupera detalle llamados")
                                                                        onCompletion(false)
                                                                    })
                                                                    }
        })
    }

    /// Función donde se llama al servicio que recupera informacion del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func recuperarInfoCliente(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        RecuperaInfoCliente.sharedInstance.getInfoPlanCliente(Globales.numeroTelefono, tipoConsulta: Constants.Indicadores.tipoConsultaLinea,
                                                              onCompletion: {  (json: JSON) in if let results = json["return"].array {
                                                                for respuesta in results {
                                                                    Globales.datosLlamadas.minutosDisponibles = respuesta["minutosPlan"].intValue
                                                                    if respuesta["cantidadMensajes"].stringValue == Constants.Indicadores.smsIlimitado {
                                                                        Globales.datosLlamadas.smsIlimitado = true
                                                                    }
                                                                }
                                                                DispatchQueue.main.async(execute: {
                                                                    onCompletion(true)
                                                                })
                                                              } else {
                                                                DispatchQueue.main.async(execute: {
                                                                    Logger.log("error de servicio recuperarInfoCliente")
                                                                    onCompletion(false)
                                                                })
                                                                }
        })
    }

    /// Función donde se llama al servicio que obtiene facturas del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerFacturas(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
// Comentado por definición de negocio
//        GetInvoice.sharedInstance.getFacturas(Globales.datosCliente.idCliente,
//                                                              onCompletion: {  (json: JSON) in if let results = json["ItemInvoices"]["invoice"].array {
//                                                                if json["ItemInvoices"]["errorCode"] == "0" {
//                                                                    DispatchQueue.main.async(execute: {
//                                                                        self.obtenerURIPDFFacturas(idFactura: results[0]["invoiceId"].stringValue) { retorno in
//                                                                            onCompletion(retorno)
//                                                                        }
//                                                                    })
//                                                                }
//                                                              } else {
//                                                                DispatchQueue.main.async(execute: {
//                                                                    Logger.log("error de servicio obtenerFacturas")
                                                                    onCompletion(true)
//                                                                })
//                                                                }
//        })
    }

    /// Función donde se llama al servicio que obtiene la url para descargar pdf de factura del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerURIPDFFacturas(idFactura: String, onCompletion: @escaping (_ retorno: Bool) -> ()) {
        GetInvoicePDFURI.sharedInstance.getPDFURIFactura(Globales.datosCliente.idCliente, idFactura: idFactura,
                                              onCompletion: {  (json: JSON) in if json["InvoicePdfInfoRes"]["errorCode"].stringValue == "0" {
                                                    print(json)
                                                    Globales.datosCliente.urlFactura = json["InvoicePdfInfoRes"]["url"].stringValue

                                                    DispatchQueue.main.async(execute: {
                                                        onCompletion(true)
                                                    })
                                              } else {
                                                DispatchQueue.main.async(execute: {
                                                    Logger.log("error de servicio obtenerURIPDFFacturas")
                                                    onCompletion(true)
                                                })
                                                }
        })
    }
    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerPromociones(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        var bodyJson = [String: String]()

        bodyJson["username"] = ""
        bodyJson["password"] = ""
        bodyJson["MSISDN"] = Globales.numeroTelefono
        bodyJson["Channel"] = "MI_WOM"
        bodyJson["TransactionID"] = "1"
        bodyJson["param"] = ""
        bodyJson["value"] = ""

        ObtenerPromociones.sharedInstance.obtenerPromociones(bodyJson,
                                                    onCompletion: {  (json: JSON) in
                                                    if json.dictionaryObject != nil && json["getOffersReturn"]["statusCode"].intValue == 0 {
                                                        Globales.promociones.removeAll()

                                                        for promocion in json["getOffersReturn"]["offers"].array! {
                                                            let promocionObj = PromocionesTO(json: promocion)
                                                            Globales.promociones.append(promocionObj)

                                                            self.generarImagenesPromociones(promocionObj)
                                                        }

                                                        DispatchQueue.main.async(execute: {
                                                            onCompletion(true)
                                                        })
                                                    } else {
                                                        DispatchQueue.main.async(execute: {
                                                            Logger.log("error de servicio obtener promociones")
                                                            onCompletion(true)
                                                        })
                                                        }})
    }

    /// Obtiene imagenes de promociones desde las URL y las asigna al arreglo global
    ///
    /// - Parameter promocion: objeto de promociones
    static func generarImagenesPromociones(_ promocion: PromocionesTO) {

        if Globales.validadorImagenBanner {
            Logger.log("Obteniendo imagen banner de promo: \(promocion.idPromo)")
            let urlImagen: URL = URL(string: promocion.urlImagenHomeBanner)!
            let imagen = CIImage(contentsOf: urlImagen)
            if imagen != nil {
                Globales.imagenPromoPrincipal = UIImage(ciImage: imagen!)
                Logger.log("Asignación correcta de imagen banner de promo: \(promocion.idPromo)")
            } else {
                Logger.log("imagen banner de promo nula: \(promocion.idPromo)")
            }
        }

        Globales.validadorImagenBanner = false

        Logger.log("Obteniendo imagen de promo: \(promocion.idPromo)")
        let urlImagen: URL = URL(string: promocion.urlImagenListado)!
        let imagen = CIImage(contentsOf: urlImagen)

        if imagen != nil {
            Globales.listadoImagenesPromos[promocion.idPromo] = UIImage(ciImage: imagen!)

            Logger.log("Asignación correcta de imagen de promo: \(promocion.idPromo)")
        } else {
            Logger.log("Hubo un problema al generar imagen de promo: \(promocion.idPromo)")
        }
    }
    /// Función donde se llama al servicio que obtiene el token para el login.
    ///
    /// - parameter clientId:       clientId de llamada por iFacil o Servicios
    /// - parameter clientSecret:   clientSecret de llamada por iFacil o Servicios
    /// - parameter numeroTelefono: numero telefono de cliente
    /// - parameter password:       password que puede ser numero de cliente si es ifacil, o IMSI si es servicios
    /// - parameter realm:          realm que se modifica en URL
    /// - parameter onCompletion:   retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerToken(_ clientId: String, clientSecret: String, numeroTelefono: String, password: String, grantType: String, refreshToken: String, realm: String, onCompletion: @escaping (_ retorno: Bool) -> ()) {
        var newDateComponents = DateComponents()

        ObtenerToken.sharedInstance.getToken(clientId, clientSecret: clientSecret, username: numeroTelefono, password: password, grantType: grantType, refreshToken: refreshToken, realm: realm, onCompletion: {
            (json: JSON) in
            if json.dictionaryObject != nil {
                Globales.tokenApp = TokenTO(json: json)

                newDateComponents.second = Int(Globales.tokenApp.expiracionToken)!
                Globales.tokenApp.dateExpiracionToken = NSCalendar.current.date(byAdding: newDateComponents, to: Date())

                newDateComponents.second = Int(Globales.tokenApp.expiracionTokenRefresco)!
                Globales.tokenApp.dateExpiracionTokenRefresco = NSCalendar.current.date(byAdding: newDateComponents, to: Date())

                onCompletion(true)
            } else {
                onCompletion(false)
            }
        })
    }

    /// Función donde se llama al servicio que solicita el codigo verificador que se envia por sms.
    ///
    /// - parameter numeroTelefono: número de teléfono que será enviado sms
    /// - parameter onCompletion:   retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func solicitarCodigoVerificador(_ numeroTelefono: String, onCompletion: @escaping (_ retorno: Bool) -> ()) {
        var jsonBody = [String: String]()
        jsonBody["numero"] = numeroTelefono
        PSSolicitaCodigoVerificacion.sharedInstance.solicitarCodigo(jsonBody, onCompletion: {
            (json: JSON) in
            if json.dictionaryObject != nil && json["error"].stringValue == "" {
                onCompletion(true)
            } else if json["error"].stringValue != "" {
                DispatchQueue.main.async(execute: {
                    onCompletion(false)
                })
            } else {
                onCompletion(false)
            }
        })
    }

    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - parameter codigo:       codigo sms que es ingresado por el usuario.
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func obtenerIMSI(_ codigo: String, onCompletion: @escaping (_ retorno: Bool, _ error: Int) -> ()) {
        PSObtieneIMSI.sharedInstance.getIMSI(codigo, onCompletion: {  (json: JSON, error: NSError?) in
            if json.dictionaryObject != nil && error == nil {
                Globales.IMSI = json["imsi"].stringValue
                DispatchQueue.main.async(execute: {
                    onCompletion(true, 0)
                })
            } else if error?.code != nil {
                DispatchQueue.main.async(execute: {
                    onCompletion(false, (error?.code)!)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    onCompletion(false, 0)
                })
            }})
    }

    /// Realiza llamadas de servicios tanto post como get
    ///
    /// - parameter request:      request con que se realiza la llamada del servicio
    /// - parameter controller:   clase controladora del servicio
    /// - parameter onCompletion: retorna respuesta del servicio
    static func realizarLlamadaServicio(_ request: NSMutableURLRequest, controller: URLSessionDelegate, onCompletion: @escaping ServiceResponse) {
        var requestIn = request as URLRequest
        requestIn.addValue("\(Constants.Login.headerAuthorization) \(Globales.tokenApp.token!)", forHTTPHeaderField: "Authorization")
        Logger.log("cabecera: \(Constants.Login.headerAuthorization) \(Globales.tokenApp.token!)")
        let urlconfig = URLSessionConfiguration.default

        urlconfig.timeoutIntervalForRequest = Constants.Servicios.timeout
        urlconfig.timeoutIntervalForResource = Constants.Servicios.timeout

        let session = URLSession(configuration: urlconfig, delegate: controller, delegateQueue: nil)

        let task = session.dataTask(with: requestIn, completionHandler: { (data, response, error) -> Void in
            guard error == nil && data != nil else {
                // chequear por errores en conexión a la red
                Logger.log("error en la conexión")
                onCompletion(JSON.null, nil)
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == Constants.Login.errorAutorizacion {
                Logger.log("statusCode es 401")
                Logger.log("response = \(String(describing: response))")
                onCompletion(JSON.null, NSError(domain: "", code: Constants.Login.errorAutorizacion, userInfo: [:]))
            } else if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // chequear errores http
                Logger.log("statusCode deberia ser 200, pero es \(httpStatus.statusCode)")
                Logger.log("response = \(String(describing: response))")
                onCompletion(JSON.null, NSError(domain: "", code: httpStatus.statusCode, userInfo: [:]))
            } else {
                let json: JSON = JSON(data: data!)

                if Globales.segmentClient == "" {
                    Globales.segmentClient = json["segment_client"].stringValue
                }

                Logger.log("response json: \(json)")
                onCompletion(json, nil)
            }

            let responseString = String(data: data!, encoding: String.Encoding.utf8)
            Logger.log("responseString = \(String(describing: responseString))")
        })
        task.resume()
    }

    /// Se consulta el consumo del cliente.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func consultaConsumo(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        ConsultaConsumo.sharedInstance.getConsumo(Globales.numeroTelefono,
                                                  onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
                                                    Globales.listaGrupoBasales.removeAll()
                                                    Globales.listaGrupoBolsas.removeAll()
                                                    Globales.listaGrupoBolsasPromocionales.removeAll()
                                                    let bolsas = json["bolsas"].array

                                                    if bolsas == nil {
                                                        onCompletion(true)
                                                    } else {
                                                        UtilBolsas.verificarCategoriaBolsa(bolsas!)

                                                        DispatchQueue.main.async(execute: {
                                                            self.obtenerPromociones { retorno in
                                                                onCompletion(retorno)
                                                            }
                                                        })
                                                    }
                                                  } else {
                                                    DispatchQueue.main.async(execute: {
                                                        Logger.log("error de servicio consulta consumo")
                                                        onCompletion(false)
                                                    })
                                                    }
        })
    }

    /// LLama a servicio donde se obtiene el periodo de recarga del cliente
    ///
    /// - Parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func getPeriodoRecarga(_ onCompletion: @escaping (_ retorno: Bool) -> ()) {
        PSCustomerRechargeService.sharedInstance.getPeriodoRecarga(Globales.numeroTelefono,
                                                                    onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {

                                                                        if json["return"]["errorCode"].stringValue == "1" {
                                                                            DispatchQueue.main.async(execute: {
                                                                                Logger.log("error logico servicio periodo recarga")
                                                                                onCompletion(false)
                                                                            })

                                                                        } else {
                                                                            let strDate = json["return"]["recharge"].array![0]

                                                                            let fecha = strDate["dateRecharge"].stringValue
                                                                            let fechaShort = fecha[fecha.startIndex...fecha.index(fecha.startIndex, offsetBy: 10)]
                                                                            let dateFormatter = DateFormatter()
                                                                            dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
                                                                            dateFormatter.dateFormat = "yyyy-MM-dd'T'"
                                                                            let date = dateFormatter.date(from: fechaShort)
                                                                            Globales.diaRecarga = (date! as NSDate) as Date
                                                                            DispatchQueue.main.async(execute: {
                                                                                Logger.log("finaliza servicio customer recharge")
                                                                                onCompletion(true)
                                                                            })
                                                                        }
                                                                                                                                                } else {
                                                                        DispatchQueue.main.async(execute: {
                                                                            Logger.log("error al cargar catalogo")
                                                                            onCompletion(false)
                                                                        })
                                                                        }
        })
    }
}
