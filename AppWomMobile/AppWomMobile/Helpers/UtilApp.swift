//
//  UtilApp.swift
//  AppWomMobile
//
//  Clase que contiene funciones de uso general en relación al entorno de la aplicación de wom
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import Firebase

struct UtilApp {
    static var storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
    /**
     Función que abre tabBarController según el tipo de linea.
     - parameter controller: Controlador donde se llama la función.
     */
    static func abrirHome(_ controller: UIViewController) {
        Logger.log("Home seleccionado: \(Globales.homeSeleccionado)")

        switch Globales.homeSeleccionado {
        case Constants.TipoHome.homePrepago:
            Globales.esPrepago = true
            abrirHomePrepago(controller) // Prepago
        case Constants.TipoHome.homePostpagoHibrido, Constants.TipoHome.homeControladoAntiguo, Constants.TipoHome.homeControladoSimple:
            Globales.esControlado = true
            abrirHomeControlado(controller) // Controlado
        case Constants.TipoHome.homePostpagoDatosSMS, Constants.TipoHome.homePostpagoLibre:
            Globales.esPostpago = true
            abrirHomePostpago(controller) // Postpago
        case Constants.TipoHome.homeBusiness:
            Globales.esBusiness = true
            abrirHomePostpago(controller) // Business
        case Constants.TipoHome.homePostpagoVozSMS:
            if Globales.segmentClient == "Hybrid" {
                Globales.esControlado = true
                abrirHomeControlado(controller) // Controlado
            } else {
                Globales.esPostpago = true
                abrirHomePostpago(controller) // Postpago
            }
        default:
            Logger.log("Home seleccionado no existente")
        }
    }
    /**
     Función que abre tabBarController tipo controlado.
     - parameter controller: Controlador donde se llama la función.
     */
    static func abrirHomeControlado(_ controller: UIViewController) {
        if let controladorTabControlado = storyboard.instantiateViewController(withIdentifier: "tabBarControlado") as? TabBarControllerIphone {
            verificarDatosLogin()
            Globales.tabBarSeleccionado = controladorTabControlado
            controller.present(controladorTabControlado, animated: true, completion: nil)
        }
    }
    /**
     Función que abre tabBarController tipo prepago.
     - parameter controller: Controlador donde se llama la función.
     */
    static func abrirHomePrepago(_ controller: UIViewController) {
        if let controladorTabPrepago = storyboard.instantiateViewController(withIdentifier: "tabBarPrepago") as? TabBarControllerIphone {
            verificarDatosLogin()
            Globales.tabBarSeleccionado = controladorTabPrepago
            controller.present(controladorTabPrepago, animated: true, completion: nil)
        }
    }
    /**
     Función que abre tabBarController tipo postpago.
     - parameter controller: Controlador donde se llama la función.
     */
    static func abrirHomePostpago(_ controller: UIViewController) {
        if let controladorTabPostpago = storyboard.instantiateViewController(withIdentifier: "tabBarPostpago") as? TabBarControllerIphone {
            verificarDatosLogin()
            Globales.tabBarSeleccionado = controladorTabPostpago
            controller.present(controladorTabPostpago, animated: true, completion: nil)
        }
    }
    /// Guarda los datos de aplicación
    static func verificarDatosLogin() {
        // Estado de aplicación que ya se revisó tutorial
        let estadoTutorialRevisado = 1

        if PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.validacionAvance) != nil && Globales.guardarDatosUsuario {
            let numeroEncriptado = Util.codificarStringCripto(Globales.numeroTelefono, llave: Constants.KeyPlist.numeroTelefono)
            let IMSIEncriptado = Util.codificarStringCripto(Globales.IMSI, llave: Constants.KeyPlist.IMSI)

            PlistManager.sharedInstance.saveValue(estadoTutorialRevisado as AnyObject, forKey: Constants.KeyPlist.validacionAvance)
            PlistManager.sharedInstance.saveValue(numeroEncriptado as AnyObject, forKey: Constants.KeyPlist.numeroTelefono)
            PlistManager.sharedInstance.saveValue(Globales.nombreCliente as AnyObject, forKey: Constants.KeyPlist.nombreCliente)
            PlistManager.sharedInstance.saveValue(Globales.apellidoCliente as AnyObject, forKey: Constants.KeyPlist.apellidoCliente)
            PlistManager.sharedInstance.saveValue(IMSIEncriptado as AnyObject, forKey: Constants.KeyPlist.IMSI)
        }

        Globales.enHome = true
    }

    /**
     Modifica URl en caso de que el indicador de servicios segurizados sea verdadero.
     - parameter url: URL que será modificada si el indicador es true.
     - returns: URL modificada o como está dependiendo el caso.
     */
    static func verificarURLSegurizada(_ url: String) -> String {
        if Globales.esSegurizado {
            var url = url.replacingOccurrences(of: "{service}", with: "")
            url = url.replacingOccurrences(of: "{ip}", with: URLServicios.ipSegurizada)
            return url.replacingOccurrences(of: "{server}", with: "mobile")
        } else {
            var url = url.replacingOccurrences(of: "{service}", with: "Services/")
            url = url.replacingOccurrences(of: "{ip}", with: URLServicios.ipNoSegurizada)
            return url.replacingOccurrences(of: "{server}", with: "esb")
        }
    }

    /// Función valida que el token este vigente
    ///
    /// - parameter clientId:       clientId de llamada por iFacil o Servicios
    /// - parameter clientSecret:   clientSecret de llamada por iFacil o Servicios
    /// - parameter password:       password que puede ser numero de cliente si es ifacil, o IMSI si es servicios
    /// - parameter realm:          realm que se modifica en URL
    /// - parameter onCompletion:   retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func validarToken(_ clientId: String, clientSecret: String, password: String, realm: String, onCompletion: @escaping (_ retorno: Bool) -> ()) {

        if Globales.tokenApp.dateExpiracionToken != nil {
            let componentToken = Calendar.current.dateComponents([.second], from: Date(), to: Globales.tokenApp.dateExpiracionToken)
            let componentTokenRefresco = Calendar.current.dateComponents([.second], from: Date(), to: Globales.tokenApp.dateExpiracionTokenRefresco)

            let segundosRestantesToken = componentToken.second
            let segundosRestantesTokenRefresco = componentTokenRefresco.second

            Logger.log("segundos restantes de token: \(String(describing: segundosRestantesToken))")
            Logger.log("segundos restantes de refresh token: \(String(describing: segundosRestantesTokenRefresco))")

            if segundosRestantesToken! <= 0 && segundosRestantesTokenRefresco! > 0 {
                UtilServicios.obtenerToken(clientId, clientSecret: clientSecret, numeroTelefono: Globales.numeroTelefono, password: password, grantType: Constants.Login.refreshToken, refreshToken: Globales.tokenApp.tokenRefresco, realm: realm, onCompletion: {
                    (retorno: Bool) in
                    if retorno {
                        DispatchQueue.main.async(execute: {
                            onCompletion(true)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            onCompletion(false)
                        })
                    }
                })
            } else if segundosRestantesToken! <= 0 && segundosRestantesTokenRefresco! <= 0 {
                UtilServicios.obtenerToken(clientId, clientSecret: clientSecret, numeroTelefono: Globales.numeroTelefono, password: password, grantType: Constants.Login.grantType, refreshToken: "", realm: realm, onCompletion: {
                    (retorno: Bool) in
                    if retorno {
                        DispatchQueue.main.async(execute: {
                            onCompletion(true)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            onCompletion(false)
                        })
                    }
                })
            } else {
                onCompletion(true)
            }
        } else {
            UtilServicios.obtenerToken(clientId, clientSecret: clientSecret, numeroTelefono: Globales.numeroTelefono, password: password, grantType: Constants.Login.grantType, refreshToken: "", realm: realm, onCompletion: {
                (retorno: Bool) in
                if retorno {
                    DispatchQueue.main.async(execute: {
                        onCompletion(true)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        onCompletion(false)
                    })
                }
            })
        }
    }

    /// Retorna el limite de caracteres en una linea para el nombre del cliente.
    ///
    /// - Returns: limite caracteres del nombre
    static func obtenerLimiteCaracteresNombre() -> Int {
        let limiteCaracteresIphone5 = 12
        let limiteCaracteresIphone6 = 14
        let limiteCaracteresIphonePlus = 15

        if Globales.isIphone5 {
            return limiteCaracteresIphone5
        } else if Globales.isIphone6 {
            return limiteCaracteresIphone6
        } else {
            return limiteCaracteresIphonePlus
        }
    }

    /// Retorna el limite de caracteres en una linea para el nombre del cliente.
    ///
    /// - Returns: limite caracteres del nombre
    static func obtenerLimiteCaracteresNombreBolsa() -> Int {
        let limiteCaracteresIphone5 = 20
        let limiteCaracteresIphone6 = 26
        let limiteCaracteresIphonePlus = 29

        if Globales.isIphone5 {
            return limiteCaracteresIphone5
        } else if Globales.isIphone6 {
            return limiteCaracteresIphone6
        } else {
            return limiteCaracteresIphonePlus
        }
    }


    /// Función que genera la "miga de pan" para ser registrada
    ///
    /// - Parameter texto: mensaje indicando la ubicación de la miga de pan.
    static func generarMigaDePan(_ texto: String) {
        let model = Util.getDevice()
        let fechaActual = Util.transformarFechaNSDate(Date(), formato: "ddMMyyyyHHmmss")

        Globales.fechaHoy = Util.transformarFechaNSDate(Date(), formato: "ddMMyyyyHHmmss")
        var stringMarcaBase = NSLocalizedString("marca_base", comment: "marca base miga")
        stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{linea}", with: Globales.numeroTelefono)

        if Globales.datosCliente.tipoPlan != nil {
            stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{segmento}", with: Globales.datosCliente.tipoPlan)
            stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{rateplan}", with: String(Globales.datosCliente.ratePlan))
        } else {
            stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{segmento}", with: "BAM")
            stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{rateplan}", with: "BAM")
        }
        stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{fecha}", with: fechaActual)
        stringMarcaBase = stringMarcaBase.replacingOccurrences(of: "{modelo}", with: model)
        let stringMigaPan = stringMarcaBase.components(separatedBy: ",")
        Analytics.logEvent("event_wom", parameters: [
            "linea": stringMigaPan[0],//numero de telefono
            "segmento": stringMigaPan[1],
            "canal":"app",
            "fecha": stringMigaPan[3],
            "funcionalidad": texto,
            "dispositivo": stringMigaPan[5]
            ])

        Logger.log("Generando miga de pan: \(stringMarcaBase)\(texto)")
    }
    /// Obtiene el nombre en twitter de usuario
    ///
    /// - Parameter data: data del servicio
    /// - Returns: retorna nombre de usuario en twitter
    static func obtenerNombreTwitter(_ data: Data) -> String {
        let data = data,
        jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let text = String(describing: jsonString)
        let rangeInicio: Range<String.Index> = text.range(of: "name")!
        let index: Int = text.characters.distance(from: text.startIndex, to: rangeInicio.lowerBound)
        let rangeFinal: Range<String.Index> = text.range(of: "screen_name")!
        let indexsa: Int = text.characters.distance(from: text.startIndex, to: rangeFinal.lowerBound)
        let length = text.characters.count - indexsa + 3
        let start = text.characters.index(text.startIndex, offsetBy: index + 7)
        let end = text.characters.index(text.endIndex, offsetBy: -length)

        if Globales.nombreCliente.isEmpty {
            Globales.nombreCliente = text[start..<end]
        }

        return text[start..<end]
    }


    /// Obtiene la cantidad de seguidores de twitter
    ///
    /// - Parameter data: datos de twitter
    /// - Returns: cantidad de seguidores
    static func obtenerSeguidoresTwitter(_ data: Data) -> Int {
        let data = data
        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        let text = String(describing: jsonString)
        let range: Range<String.Index> = text.range(of: "followers_count")!
        let index: Int = text.characters.distance(from: text.startIndex, to: range.lowerBound)
        let range2: Range<String.Index> = text.range(of: "friends_count")!
        let indexsa: Int = text.characters.distance(from: text.startIndex, to: range2.lowerBound)
        let length = text.characters.count - indexsa + 2
        let s = text
        let start = s.characters.index(s.startIndex, offsetBy: index + 17)
        let end = s.characters.index(s.endIndex, offsetBy: -length)
        let followers = Int(s[start..<end])

        if followers != nil {
            return followers!
        } else {
            return 0
        }
    }


    /// Abre la vista de error general
    ///
    /// - Parameters:
    ///   - textoError: Indica el mensaje de error que se muestra en el pop up
    ///   - controller: Controlador desde donde se arroja el error
    static func abrirPopupErrorServicio(textoError: String, controller: UIViewController) {
        var popupErrorGeneral = ErrorGeneral()
        popupErrorGeneral = storyboard.instantiateViewController(withIdentifier: "errorGeneral") as! ErrorGeneral

        popupErrorGeneral.delegate = controller

        popupErrorGeneral.textoError = textoError

        controller.addChildViewController(popupErrorGeneral)
        popupErrorGeneral.view.frame = controller.view.frame
        controller.view.addSubview(popupErrorGeneral.view)
        popupErrorGeneral.didMove(toParentViewController: controller)

        controller.navigationItem.setHidesBackButton(true, animated: true)
    }

    /// Agrega el cargando de la app en el lugar indicado en la vista
    static func agregarActivityIndicatorEspecifico(imageIndicador: UIImageView) {
        var imgListArray = [UIImage]()

        for imagenIndex in 0...22 {
            let stringImagen: String = "loading00\(imagenIndex).png"
            let image = UIImage(named: stringImagen)
            imgListArray.append(image!)
        }

        imageIndicador.animationImages = imgListArray
        imageIndicador.animationDuration = 1
        imageIndicador.startAnimating()
    }

    /// Agrega un caracter en el string enviao
    ///
    /// - Parameters:
    ///   - caracter: Caracter que se agregará al string
    ///   - texto: Texto al cual se le agregará un caracter
    ///   - posicion: Posición en el texto en que se agregará caracter
    /// - Returns: Retorna texto con caracter agregado
    static func insertarCaracterEnString(caracter: Character, texto: String, posicion: Int) -> String {
        var textoCambiado = texto
        textoCambiado.insert(caracter, at: (texto.index(texto.startIndex, offsetBy: posicion)))
        return textoCambiado
    }

    /// Valida el estado de mantenicon
    ///
    /// - Parameters:
    ///   - controller: controller que hace llamada a la funcion
    ///   - onCompletion: retorno indica si se debe o no mostrar pantalla de mantencion
    static func validarEstadoServicios(controller: UIViewController, abrirVista: Bool, onCompletion: @escaping (_ retorno: Bool) -> ()) {
        UtilFirebase.validarEstadoServicios() { retorno, listadoNumeros, estadoServicios in
            DispatchQueue.main.async(execute: {
                if retorno && !estadoServicios {
                    var indicadorHabilitado = false
                    for numero in listadoNumeros {
                        if Globales.numeroTelefono == numero {
                            Logger.log("Numero \(numero) se encuentra en listado habilitado para abrir app")
                            indicadorHabilitado = true
                            break
                        }
                    }
                    if !indicadorHabilitado && abrirVista {
                        Logger.log("Abriendo vista mantencion")
                        let mainStoryboard = UIStoryboard(name: "Main_iphone", bundle: Bundle.main)
                        let resultController = mainStoryboard.instantiateViewController(withIdentifier: "excepcionesWom") as! ExcepcionesWOMIphone
                        resultController.enMantencion = true
                        resultController.delegate = controller
                        controller.present(resultController, animated: true, completion: nil)
                        onCompletion(false)
                    }
                    if !abrirVista {
                        Logger.log("Servicio mantención aún activado")
                        onCompletion(false)
                    } else if indicadorHabilitado {
                        Logger.log("No se debe mostrar error")
                        onCompletion(true)
                    }
                } else {
                    onCompletion(true)
                }
            })
        }
    }
    /// Determina titulo de secciones segun el tipo de postpago.
    ///
    /// - Parameters:
    ///   - secciones: arreglo con todas las secciones
    ///   - seccionDatosMoviles: indice de datos moviles
    ///   - seccionBasales: indice de basales moviles
    ///   - seccionBolsasPromo: indice de bolsas datos promocionaels
    ///   - seccionBolsasCompradas: indice de bolsas compradas
    ///   - seccionMinutos: indice de seccion minutos
    ///   - seccionMinutosBasales: indice de seccion minutos basales
    ///   - seccionSMS: indice de seccion de SMS
    ///   - seccionSMSBasal: indice de seccion sms basal
    static func determinarSeccionesPostpago(secciones: inout [String], seccionDatosMoviles: inout Int, seccionBasales: inout Int, seccionBolsasPromo: inout Int, seccionBolsasCompradas: inout Int, seccionMinutos: inout Int, seccionMinutosBasales: inout Int, seccionSMS: inout Int, seccionSMSBasal: inout Int) {
        Logger.log("Home seleccionado: \(Globales.homeSeleccionado)")
        switch Globales.homeSeleccionado {
        case Constants.TipoHome.homePostpagoLibre:
            secciones = ["Encabezado", "Datos moviles", "Datos Basales", "Datos Compradas", "Datos Promocionales", "Seccion minutos", "Minutos Interna", "Seccion SMS", "SMS Interna"]
            seccionDatosMoviles = 1
            seccionBasales = 2
            seccionBolsasPromo = 3
            seccionBolsasCompradas = 4
            seccionMinutos = 5
            seccionMinutosBasales = 6
            seccionSMS = 7
            seccionSMSBasal = 8
        case Constants.TipoHome.homePostpagoVozSMS:
            secciones = ["Encabezado", "Seccion minutos", "Minutos Interna", "Seccion SMS", "Seccion SMS interna"]
            seccionMinutos = 1
            seccionMinutosBasales = 2
            seccionSMS = 3
            seccionSMSBasal = 4
            seccionDatosMoviles = -1
            seccionBolsasCompradas = -1
            seccionBolsasPromo = -1
            seccionBasales = -1
        case Constants.TipoHome.homePostpagoDatosSMS:
            secciones = ["Encabezado", "Datos moviles", "Datos Basales", "Datos Compradas", "Datos Promocionales", "Seccion SMS", "SMS Interna"]
            seccionDatosMoviles = 1
            seccionBasales = 2
            seccionBolsasPromo = 3
            seccionBolsasCompradas = 4
            seccionSMS = 5
            seccionSMSBasal = 6
            seccionMinutos = -1
        case Constants.TipoHome.homeBusiness:
            secciones = ["Encabezado", "Datos moviles", "Datos Basales", "Datos Compradas", "Datos Promocionales", "Seccion minutos"]
            seccionDatosMoviles = 1
            seccionBasales = 2
            seccionBolsasPromo = 3
            seccionBolsasCompradas = 4
            seccionMinutos = 5
            seccionMinutosBasales = 6
            seccionSMS = -1
        default:
            Logger.log("default switch determinarSecciones")
        }
    }

    /// Identifica si el cliente ingresado en la app es big boss
    ///
    /// - Parameters:
    ///   - listadoClientes: listado de lineas asociadas
    ///   - numero: numero ingresado en la app
    /// - Returns: indicador si es big boss o no
    static func identificarBigBoss(listadoClientes: [ContractTO], numero: String) -> Bool {
        let numeroAntiguo = listadoClientes[0].nroCelular

        if Globales.numeroTelefono == numeroAntiguo {
            Globales.datosCliente.contratos.remove(at: 0)
            return true
        } else {
            return false
        }
    }
}
