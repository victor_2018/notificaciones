//
//  UtilServiciosPromos.swift
//  AppWomMobile
//
//  Clase que contiene funciones para llamadas de servicio principales.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

class UtilServiciosPromos {

    /// Funcion donde se informa a CM de la activacion de la promoción.
    ///
    /// - Parameters:
    ///   - body: cuerpo del body para llamada post
    ///   - onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func activarPromo(promocion: PromocionesTO, onCompletion: @escaping (_ retorno: Bool) -> ()) {

        ActivarPromocion.sharedInstance.activarPromocion(self.armarBodyJsonActivarPromo(promocion: promocion), onCompletion: {  (json: JSON) in if json.dictionaryObject != nil && json["eventNotifierReturn"]["statusCode"].intValue == 0 {

                DispatchQueue.main.async(execute: {
                    Logger.log("finaliza servicio activacion promos")
                    onCompletion(true)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    Logger.log("error al activacion promos")
                    onCompletion(false)
                })
            }
        })
    }

    /// Se busca dentro del arreglo de promociones en las propiedades, una promo en especifico con el id enviado por parametro
    ///
    /// - Parameter idPromo: Id de promo que se debe
    static func actualizarFecha(_ idPromo: String) {
        var indice = 0
        var encontrado = false
        if let listaPromociones = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.listadoPromociones) {
            var promociones = listaPromociones as! Array<String>

            for promo in promociones {
                let stringArray = promo.components(separatedBy: ",")[0]

                if stringArray == idPromo {
                    promociones[indice] = "\(idPromo),\(Util.transformarFechaNSDate(Date(), formato: "yyyy-MM-dd'T'HH:mm:ss")),N"
                    encontrado = true

                    break
                }
                indice += indice + 1
            }

            if !encontrado {
                promociones.append("\(idPromo),\(Util.transformarFechaNSDate(Date(), formato: "yyyy-MM-dd'T'hh:mm:ss")),N")
            }

            PlistManager.sharedInstance.saveValue(promociones as AnyObject, forKey: Constants.KeyPlist.listadoPromociones)
        } else {
            // En caso de que no exista el arreglo en las propiedades se agrega
            var promociones = Array<String>()

            promociones.append("\(Globales.promociones[0].idPromo),\(Util.transformarFechaNSDate(Date(), formato: "yyyy-MM-dd'T'hh:mm:ss")),N")

            PlistManager.sharedInstance.addNewItemWithKey(Constants.KeyPlist.listadoPromociones, value: promociones as AnyObject)
        }
    }

    /// Remueve la promoción dentro del listado en propiedades
    ///
    /// - Parameter idPromo: id de la promoción a eliminar
    static func eliminarPromo(_ idPromo: String) {
        var indice = 0

        if let listaPromociones = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.listadoPromociones) {
            var promociones = listaPromociones as! Array<String>

            for promo in promociones {
                let stringArray = promo.components(separatedBy: ",")[0]

                if stringArray == idPromo {
                    promociones.remove(at: indice)
                    break
                }
                indice += indice + 1
            }
            PlistManager.sharedInstance.saveValue(promociones as AnyObject, forKey: Constants.KeyPlist.listadoPromociones)
        }
    }


    /// Asigna los datos para el badge en el tab bar de promos
    ///
    /// - Parameter tabBarController: controlador del Tab Bar
    static func generarBadgePromos(_ tabBarController: UITabBarController) {
        let indicePromociones = 4
        tabBarController.tabBar.items?[indicePromociones].badgeValue = String(Globales.promociones.count)
        if #available(iOS 10.0, *) {
            tabBarController.tabBar.items?[indicePromociones].setBadgeTextAttributes([NSFontAttributeName: UIFont(name: Constants.TipoFuente.ceraBold, size: 11)!], for: UIControlState())
        }
    }

    /// Despliega la ventana del home banner con promoción principal
    ///
    /// - Parameters:
    ///   - controlador: controlador del home
    ///   - tabBarController: tab controller del home
    static func desplegarHomeBanner(_ controlador: UIViewController, tabBarController: UITabBarController) {
        if Globales.promociones.count > 0 {
            let storyboard = UIStoryboard(name: "Tab5_iphone", bundle: nil)
            let popupActivacionPromo = storyboard.instantiateViewController(withIdentifier: "homeBannerIphone") as! HomeBannerIphone

            popupActivacionPromo.delegate = controlador
            popupActivacionPromo.view.tag = Constants.Tags.homeBanner

            Util.agregarPopUp(popupActivacionPromo, contenedor: tabBarController)
        }
    }

    /**
     Se arman parametros para enviar a llamada de servicio Post activar promo.

     - returns: cuerpo de json para enviar al servicio
     */
    static func armarBodyJsonActivarPromo(promocion: PromocionesTO) -> [String: AnyObject] {
        var bodyJson = [String: AnyObject]()
        var evento = [String: AnyObject]()
        var eventos = [[String: AnyObject]]()
        var params = [[String: AnyObject]]()
        var parametro = [String: AnyObject]()

        bodyJson["username"] = "" as AnyObject
        bodyJson["password"] = "" as AnyObject

        evento["MSISDN"] = Globales.numeroTelefono as AnyObject
        evento["eventNode"] = "" as AnyObject
        evento["eventType"] = "ADD_OFFER_EVT" as AnyObject
        evento["timeStamp"] = Util.transformarFechaNSDate(Date(), formato: "yyyy-MM-dd'T'HH:mm:ss") as AnyObject

        parametro["param"] = "offerID" as AnyObject
        parametro["value"] = promocion.idPromo as AnyObject

        params.append(parametro)

        evento["params"] = params as AnyObject

        eventos.append(evento)

        bodyJson["event"] = eventos as AnyObject

        return bodyJson
    }
}
