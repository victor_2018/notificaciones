//
//  UtilCompraBolsas.swift
//  AppWomMobile
//
//  Clase que contiene funciones utilitarias de compra bolsa.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

struct UtilCompraBolsas {
    // tipo de bolsa de datos
    static let tipoDatos = "DATOS"
    // tipo de bolsa SMS
    static let tipoSMS = "SMS"
    // tipo de bolsa voz
    static let tipoVoz = "VOZ"
    // tipo de bolsa mixta
    static let tipoMixtas = "MIXTAS"
    // tipo de bolsa tematica
    static let tipoTematicas = "TEMATICA"
    // tipo de bolsa tematica
    static let tipoRoaming = "ROAMING"

    /// Valida el tiempo de renovar los datos en cache de compra bolsa
    ///
    /// - parameter recargarServicios: indicador de si se deben recargar servicios
    /// - Returns: indicador de si la validacion del cache fue realizada con exito o no
    static func validarTiempoCaching(recargarServicios: Bool) -> Bool {
        if recargarServicios {
            Logger.log("Saltandose validación de tiempo por compra de bolsa")
            return true
        }
        if let tiempoCaching = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.tiempoCachingCompraBolsas) {
            var fechaCaching = Date()
            fechaCaching = tiempoCaching as! Date
            var newDateComponents = DateComponents()
            newDateComponents.minute = Constants.Indicadores.margenTiempoCaching
            let fechaActual = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: Date(), options: NSCalendar.Options.init(rawValue: 0))

            let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.minute, from: tiempoCaching as! Date, to: fechaActual!, options: NSCalendar.Options.init(rawValue: 0))

            Logger.log("fecha caching: \(fechaCaching) fecha Actual: \(String(describing: fechaActual)) min dif: \(String(describing: components.minute))")

            if components.minute! > 0 {
                Logger.log("Tiempo para caching ha sido superado")
                return true
            } else {
                Logger.log("No es tiempo aún para renovar caching")
                return false
            }
        }
        return true
    }

    /**
     Función que valida la primera regla de validaBolsas.

     - parameter reglasValidaciones: objeto de la regla de cantidad de bolsas
     - parameter objCantBolsas: objeto de la regla de cantidad de bolsas
     - parameter objLimiteCompra: objeto de la regla de limite de compra
     - parameter objEstadoCliente: objeto de la regla de estado de cliente
     */
    static func asignarObjetosReglas(_ reglasValidaciones: [ReglaValidacionBolsaTO], objCantBolsas: inout ReglaValidacionBolsaTO, objLimiteCompra: inout ReglaValidacionBolsaTO, objEstadoCliente: inout ReglaValidacionBolsaTO) {
        // parametro de validacion de cantidad de bolsas
        let parametroCantBolsas = "CANT_BOLSAS"
        // parametro de validacion de limite de compras de bolsas
        let parametroLimiteCompra = "LIMITE_COMPRA"
        // parametro de validacion estado cliente
        let parametroEstadoCliente = "ESTADO_CLIENTE"

        for regla in reglasValidaciones {
            switch regla.parametro {
            case parametroCantBolsas:
                objCantBolsas = regla
            case parametroLimiteCompra:
                objLimiteCompra = regla
            case parametroEstadoCliente:
                objEstadoCliente = regla
            default:
                Logger.log("Default switch validar bolsas")
            }
        }
    }


    /**
     Función que valida la primera regla de validaBolsas.

     - parameter objCantBolsas: objeto de la regla de cantidad de bolsas
     - parameter objLimiteCompra: objeto de la regla de limite de compra
     - parameter objEstadoCliente: objeto de la regla de estado de cliente

     - returns: booleano con respuesta de validacion
     */
    static func validarReglaUno(_ objCantBolsas: ReglaValidacionBolsaTO, objLimiteCompra: ReglaValidacionBolsaTO, objEstadoCliente: ReglaValidacionBolsaTO) -> Bool {
        // parametro de validacion regla no aplica
        let parametroNoAplica = "N"

        if (objCantBolsas.aplica != nil && objCantBolsas.aplica == parametroNoAplica) || (objLimiteCompra.aplica != nil && objLimiteCompra.aplica == parametroNoAplica) || (objEstadoCliente.aplica != nil && objEstadoCliente.aplica == parametroNoAplica) {
            return true
        } else {
            return false
        }
    }


    /**
     Función que valida la segunda regla de validaBolsas.

     - parameter objCantBolsas: objeto de la regla de cantidad de bolsas
     - parameter objLimiteCompra: objeto de la regla de limite de compra
     - parameter objEstadoCliente: objeto de la regla de estado de cliente

     - returns: booleano con respuesta de validacion
     */
    static func validarReglaDos(_ objCantBolsas: ReglaValidacionBolsaTO, objLimiteCompra: ReglaValidacionBolsaTO, objEstadoCliente: ReglaValidacionBolsaTO) -> Bool {
        // parametro de validacion regla aplica
        let parametroAplica = "Y"

        if (objCantBolsas.aplica != nil && objCantBolsas.aplica == parametroAplica) || (objLimiteCompra.aplica != nil && objLimiteCompra.aplica == parametroAplica) || (objEstadoCliente.aplica != nil && objEstadoCliente.aplica == parametroAplica) {
            return true
        } else {
            return false
        }
    }

    /**
     Función que obtiene el nombre de la imagen de la botonera de bolsas.

     - parameter tituloBolsa: titulo de la bolsa
     - returns: nombre de imagen
     */
    static func obtenerNombreIcono(_ tituloBolsa: String) -> String {
        switch tituloBolsa {
        case NSLocalizedString("tipo_bolsa_datos", comment: "texto datos"):
            return Constants.Imagenes.imgBtnBolsaDato
        case NSLocalizedString("tipo_bolsa_voz", comment: "texto voz"):
            return Constants.Imagenes.imgBtnBolsaVoz
        case NSLocalizedString("tipo_bolsa_sms", comment: "texto sms"):
            return Constants.Imagenes.imgBtnBolsaSMS
        case NSLocalizedString("tipo_bolsa_mixtas", comment: "texto mixtas"):
            return Constants.Imagenes.imgBtnBolsaMixta
        case NSLocalizedString("tipo_bolsa_tematicas", comment: "texto tematicas"):
            return Constants.Imagenes.imgBtnBolsaTematica
        case NSLocalizedString("tipo_bolsa_roaming", comment: "texto roaming"):
            return Constants.Imagenes.imgBtnBolsaRoaming
        default:
            Logger.log("default switch obtener imagen boton")
        }
        return ""
    }


    /**
     Función que genera los arreglos finales de bolsas disponibles para comprar.

     - parameter arregloBolsa: arreglo de la bolsa que se verificara
     - parameter tipoBolsa: tipo de bolsa que se esta generando
     - parameter limiteBolsaIn: limite de valor de bolsa que se puede comprar
     */
    static func generarArreglosFinales(_ arregloBolsa: [BolsaDisponibleTO], tipoBolsa: String, limiteBolsaIn: String, arregloBolsasCatalogo: [String: CatalogoBolsaTO], arregloBolsasDatosFinal: inout [Bolsa], arregloBolsasVozFinal: inout [Bolsa], arregloBolsasSMSFinal: inout [Bolsa], arregloBolsasMixtasFinal: inout [Bolsa], arregloBolsasTematicasFinal: inout [Bolsa]) {
        var limiteBolsa = limiteBolsaIn
        var bolsaCatalogo = CatalogoBolsaTO()
        var bolsaFinal = Bolsa()

        if arregloBolsasCatalogo.count > 0 {
            for bolsa in arregloBolsa {
                if let bolsaObtenida = arregloBolsasCatalogo[bolsa.codProducto] {
                    bolsaCatalogo = bolsaObtenida
                } else {
                    continue
                }

                if limiteBolsaIn.isEmpty {
                    limiteBolsa = bolsaCatalogo.valorBolsa
                }

                if bolsaCatalogo.esPromo == "N" && Int(bolsaCatalogo.valorBolsa)! <= Int(limiteBolsa)! {
                    Logger.log("Bolsa: \(bolsaCatalogo.nombre) pasó validación")

                    switch bolsaCatalogo.tipoTrafico {
                    case self.tipoDatos:
                        bolsaFinal = Util.transformarKilos(bolsaCatalogo.unidadBolsa, dato2: "")
                    case self.tipoVoz:
                        bolsaFinal = Util.transformarSegundos(bolsaCatalogo.unidadBolsa, dato2: "")
                    case self.tipoSMS:
                        bolsaFinal.dato = CGFloat(Int(bolsaCatalogo.unidadBolsa)!)
                        bolsaFinal.metrica = "SMS"
                    default:
                        Logger.log("Default switch generarArreglosFinales")
                    }

                    UtilCompraBolsas.asignarDatosBolsa(&bolsaFinal, bolsaCatalogo: bolsaCatalogo)
                    UtilCompraBolsas.asignarBolsaArregloFinal(bolsaCatalogo, bolsaFinal: bolsaFinal, tipoBolsa: tipoBolsa, arregloBolsasDatosFinal: &arregloBolsasDatosFinal, arregloBolsasVozFinal: &arregloBolsasVozFinal, arregloBolsasSMSFinal: &arregloBolsasSMSFinal, arregloBolsasMixtasFinal: &arregloBolsasMixtasFinal, arregloBolsasTematicasFinal: &arregloBolsasTematicasFinal)
                } else {
                    Logger.log("Bolsa: \(bolsaCatalogo.nombre) no pasó validación")
                }
            }
        }
    }

    /// Se asignan datos de la bolsa en catalogo a bolsa definitiva
    ///
    /// - Parameters:
    ///   - bolsaFinal: bolsa definitiva que se mostrará
    ///   - bolsaCatalogo: bolsa en catalogo
    static func asignarDatosBolsa(_ bolsaFinal: inout Bolsa, bolsaCatalogo: CatalogoBolsaTO) {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = " "
        numberFormatter.locale = Locale(identifier: "es_CL")

        bolsaFinal.valor = Int(bolsaCatalogo.valorBolsa)
        bolsaFinal.valorFormateado = "$\(numberFormatter.string(from: Int(bolsaCatalogo.valorBolsa)! as NSNumber) ?? "0")"
        bolsaFinal.vigencia = bolsaCatalogo.vigencia
        bolsaFinal.idProducto = bolsaCatalogo.idProducto
        bolsaFinal.idBSCS = bolsaCatalogo.idBSCS
        bolsaFinal.idPCRF = bolsaCatalogo.idPCRF
        bolsaFinal.nombre = bolsaCatalogo.nombre
        bolsaFinal.fechaActivacion = bolsaCatalogo.fechaActivacion
        bolsaFinal.fechaFinalizacion = bolsaCatalogo.fechaFinalizacion
        bolsaFinal.horaActivacion = bolsaCatalogo.horaActivacion
        bolsaFinal.horaFinalizacion = bolsaCatalogo.horaFinalizacion
        bolsaFinal.descripcion = bolsaCatalogo.descripcion

        if bolsaFinal.nombre.lowercased().range(of: "4g") != nil {
            bolsaFinal.es4G = true
        }
    }

    /**
     Función que genera los arreglos finales de bolsas disponibles para comprar.

     - parameter bolsaCatalogo: arreglo de la bolsa que se verificara
     - parameter bolsaFinal: tipo de bolsa que se esta generando
     - parameter tipoBolsa: limite de valor de bolsa que se puede comprar
     */
    static func asignarBolsaArregloFinal(_ bolsaCatalogo: CatalogoBolsaTO, bolsaFinal: Bolsa, tipoBolsa: String, arregloBolsasDatosFinal: inout [Bolsa], arregloBolsasVozFinal: inout [Bolsa], arregloBolsasSMSFinal: inout [Bolsa], arregloBolsasMixtasFinal: inout [Bolsa], arregloBolsasTematicasFinal: inout [Bolsa]) {
        bolsaFinal.tipo = tipoBolsa
        switch tipoBolsa {
        case self.tipoDatos:
            arregloBolsasDatosFinal.append(bolsaFinal)
        case self.tipoVoz:
            arregloBolsasVozFinal.append(bolsaFinal)
        case self.tipoSMS:
            arregloBolsasSMSFinal.append(bolsaFinal)
        case self.tipoMixtas:
            arregloBolsasMixtasFinal.append(bolsaFinal)
        case self.tipoTematicas:
            arregloBolsasTematicasFinal.append(bolsaFinal)
        default:
            Logger.log("default switch seleccion de bolsas")
        }
    }

    /// Genera el arreglo para las bolsas roaming
    ///
    /// - Parameters:
    ///   - arregloBolsasCatalogo: Arreglo de todo el catalogo de bolsas
    ///   - arregloBolsasRoaming: Arreglo que tendrá las bolsas roaming
    static func generarArregloBolsasRoaming(arregloBolsasCatalogo: [String: CatalogoBolsaTO], arregloBolsasRoaming: inout [Bolsa], cliente: EncabezadoHomeTO) {
        for bolsa in arregloBolsasCatalogo {
            if [Constants.Indicadores.roamingPreferente, Constants.Indicadores.roamingEuropa, Constants.Indicadores.roamingRestoDelMundo].contains(bolsa.value.tipoProductoBSCS)
                && bolsa.value.status.uppercased() == Constants.Indicadores.bolsaActivada
                && bolsa.value.segmentoProducto == cliente.tipoPlan
                && (bolsa.value.familyPlan.uppercased() == Constants.Indicadores.familyPlanAll || String(Globales.datosCliente.ratePlan).contains(bolsa.value.familyPlan.uppercased())) {
                var bolsaFinal = Bolsa()
                UtilCompraBolsas.asignarDatosBolsa(&bolsaFinal, bolsaCatalogo: bolsa.value)

                bolsaFinal.tipo = tipoRoaming
                bolsaFinal.tipoProductoBSCS = bolsa.value.tipoProductoBSCS
                arregloBolsasRoaming.append(bolsaFinal)
            }
        }
        arregloBolsasRoaming.sort(by: { $0.valor < $1.valor })
        arregloBolsasRoaming.sort(by: { $0.tipoProductoBSCS < $1.tipoProductoBSCS })
    }

    /// Se valida servicio Roaming.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    static func validarServicioRoaming(_ numero: String, onCompletion: @escaping (_ retorno: Bool) -> ()) {
        var jsonBody = [String: String]()
        jsonBody["EN_NUM_CELULAR"] = numero

        DBAVentaBolsasRValidacion.sharedInstance.validarServicioRoaming(jsonBody,
                                                                        onCompletion: { (json: JSON) in if json.dictionaryObject != nil {
                                                                            DispatchQueue.main.async(execute: {
                                                                                if json["SN_TIENE_ROA"].stringValue.uppercased() == Constants.Indicadores.roamingActivado {
                                                                                    onCompletion(true)
                                                                                } else {
                                                                                    onCompletion(false)
                                                                                }
                                                                            })
                                                                        } else {
                                                                            DispatchQueue.main.async(execute: {
                                                                                Logger.log("error de servicio validarServicioRoaming")
                                                                                onCompletion(false)
                                                                            })
                                                                            }
        })
    }

    /// Abre whatsapp para enviar mensaje predeterminado, si no tiene instalado whatsapp se abre appstore
    static func abrirWhatsapp(numero: String, mensaje: String) {
        let urlWhatsapp = "whatsapp://send?phone=+\(numero)&text=\(mensaje)"

        if let urlString = urlWhatsapp.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                } else if let url = URL(string: "itms-apps://itunes.apple.com/app/id310633997"),
                    UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                }
            }
        }
    }
}
