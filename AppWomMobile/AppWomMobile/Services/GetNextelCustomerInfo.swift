//
//  GetNextelCustomerInfo.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae todas las bolsas existentes.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias GetNextelCustomerInfoResponse = (JSON, NSError?) -> Void

class GetNextelCustomerInfo: NSObject, URLSessionDelegate {
    static let sharedInstance = GetNextelCustomerInfo()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.obtenerDatosPlan)

    func getDatosPlan(_ numeroTelefono: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, numeroTelefono: numeroTelefono, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, numeroTelefono: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "phone_number", value: numeroTelefono)
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date() as Date!
                        self.makeHTTPGetRequest(path, numeroTelefono: numeroTelefono, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
