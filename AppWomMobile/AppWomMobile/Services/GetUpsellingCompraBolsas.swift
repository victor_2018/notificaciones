//
//  GetUpsellingCompraBolsas.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae las bolsas disponibles para comprar.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias GetUpsellingCompraBolsasResponse = (JSON, NSError?) -> Void

class GetUpsellingCompraBolsas: NSObject, URLSessionDelegate {
    static let sharedInstance = GetUpsellingCompraBolsas()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.bolsasDisponibles)

    func getBolsasDisponibles(_ nroCel: String, ratePlan: String, segmentProd: String, clientName: String, trafficType: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, nroCel: nroCel, ratePlan: ratePlan, segmentProd: segmentProd, clientName: clientName, trafficType: trafficType, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, nroCel: String, ratePlan: String, segmentProd: String, clientName: String, trafficType: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "nro_cel", value: nroCel),
            URLQueryItem(name: "rate_plan", value: ratePlan),
            URLQueryItem(name: "segment_prod", value: segmentProd),
            URLQueryItem(name: "client_name", value: clientName),
            URLQueryItem(name: "traffic_type", value: trafficType)
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date() as Date!
                        self.makeHTTPGetRequest(path, nroCel: nroCel, ratePlan: ratePlan, segmentProd: segmentProd, clientName: clientName, trafficType: trafficType, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
