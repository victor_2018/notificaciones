//
//  PSSolicitaCodigoVerificacion.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que envia solicitud de sms a telefono celular.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias PSSolicitaCodigoVerificacionResponse = (JSON, NSError?) -> Void

class PSSolicitaCodigoVerificacion: NSObject, URLSessionDelegate {
    static let sharedInstance = PSSolicitaCodigoVerificacion()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.solicitarCodigoVerificacion)

    func solicitarCodigo(_ body: [String: String], onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPPostRequest(route, body: body, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a POST Request
    fileprivate func makeHTTPPostRequest(_ path: String, body: [String: String], onCompletion: @escaping ServiceResponse) {

        let request = NSMutableURLRequest(url: URL(string: path)!)
        request.httpMethod = "POST"

        Logger.log("request: \(request)")

        let headerRequest = "numero=\(Globales.numeroTelefono)"

        Logger.log("headerRequest token: \(headerRequest)")

        let stringData = headerRequest.data(using: String.Encoding.utf8)

        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = stringData

        UtilApp.validarToken(Constants.Login.clientIdIfacil, clientSecret: Constants.Login.clientSecretFacil, password: Globales.numeroTelefono, realm: Constants.Login.realmFacil, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date()
                        self.makeHTTPPostRequest(path, body: body, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
