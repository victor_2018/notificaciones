//
//  PSObtieneIMSI.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que obtiene IMSI.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias PSObtieneIMSIResponse = (JSON, NSError?) -> Void

class PSObtieneIMSI: NSObject, URLSessionDelegate {
    static let sharedInstance = PSObtieneIMSI()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.obtieneIMSI)

    func getIMSI(_ codigo: String, onCompletion: @escaping (JSON, NSError?) -> Void) {
        let route = baseURL
        makeHTTPPostRequest(route, codigo: codigo, onCompletion: { json, err in
            onCompletion(json as JSON, err as NSError?)
        })
    }

    // MARK: Perform a POST Request
    fileprivate func makeHTTPPostRequest(_ path: String, codigo: String, onCompletion: @escaping PSObtieneIMSIResponse) {
        let request = NSMutableURLRequest(url: URL(string: path)!)

        // Set the method to POST
        request.httpMethod = "POST"

        let headerRequest = "numero=\(Globales.numeroTelefono)&codigo=\(codigo)"

        Logger.log("headerRequest token: \(headerRequest)")

        let stringData = headerRequest.data(using: String.Encoding.utf8)

        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = stringData

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdIfacil, clientSecret: Constants.Login.clientSecretFacil, password: Globales.numeroTelefono, realm: Constants.Login.realmFacil, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date()
                        self.makeHTTPPostRequest(path, codigo: codigo, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
