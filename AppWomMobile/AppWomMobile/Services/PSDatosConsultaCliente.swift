//
//  PSDatosConsultaCliente.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae información de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias PSDatosConsultaClienteResponse = (JSON, NSError?) -> Void

class PSDatosConsultaCliente: NSObject, URLSessionDelegate {
    static let sharedInstance = PSDatosConsultaCliente()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.datosCliente)

    func getDatosCliente(_ identificador: String, linea: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, identificador: identificador, linea: linea, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, identificador: String, linea: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "identificador", value: identificador),
            URLQueryItem(name: "canal", value: linea),
            URLQueryItem(name: "usuario", value: "")
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date()
                        self.makeHTTPGetRequest(path, identificador: identificador, linea: linea, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
