//
//  RecuperaDetalleLlamados.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae informacion para identificar plan postpago.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias RecuperaDetalleLlamadosResponse = (JSON, NSError?) -> Void

class RecuperaDetalleLlamados: NSObject, URLSessionDelegate {
    static let sharedInstance = RecuperaDetalleLlamados()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.recuperaDetalleLlamados)

    func getDetalleLlamados(_ numeroCelular: String, fechaDesde: String, fechaHasta: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, numeroCelular: numeroCelular, fechaDesde: fechaDesde, fechaHasta: fechaHasta, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, numeroCelular: String, fechaDesde: String, fechaHasta: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "fechaDesde", value: fechaDesde),
            URLQueryItem(name: "fechaHasta", value: fechaHasta),
            URLQueryItem(name: "numeroCelular", value: numeroCelular)
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date() as Date!
                        self.makeHTTPGetRequest(path, numeroCelular: numeroCelular, fechaDesde: fechaDesde, fechaHasta: fechaHasta, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(nil, nil)
            }
        })
    }
}
