//
//  HistorialBolsasService.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae información de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias ServiceResponse = (JSON, NSError?) -> Void

class HistorialBolsasService: NSObject, URLSessionDelegate {
    static let sharedInstance = HistorialBolsasService()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.historialBolsas)

    func getHistorialBolsas(_ fechaDesde: String, fechaHasta: String, numeroCelular: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, fechaDesde: fechaDesde, fechaHasta: fechaHasta, numeroCelular: numeroCelular, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, fechaDesde: String, fechaHasta: String, numeroCelular: String, onCompletion: @escaping ServiceResponse) {
        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "fechaDesde", value: fechaDesde),
            URLQueryItem(name: "fechaHasta", value: fechaHasta),
            URLQueryItem(name: "numeroCelular", value: numeroCelular)
        ]

        let request = URLRequest(url: urlComponents.url!)
        Logger.log("request: \(request)")
        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request as! NSMutableURLRequest, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date()
                        self.makeHTTPGetRequest(path, fechaDesde: fechaDesde, fechaHasta: fechaHasta, numeroCelular: numeroCelular, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
