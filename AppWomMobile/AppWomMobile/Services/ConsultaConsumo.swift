//
//  ConsultaConsumo.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae todas las bolsas existentes.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias ConsultaConsumoResponse = (JSON, NSError?) -> Void

class ConsultaConsumo: NSObject, URLSessionDelegate {
    static let sharedInstance = ConsultaConsumo()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.consultaConsumo)

    func getConsumo(_ numero: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, numero: numero, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, numero: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "numero", value: numero),
            URLQueryItem(name: "canal", value: "WEBAPP")
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)
        let urlconfig = URLSessionConfiguration.default

        urlconfig.timeoutIntervalForRequest = Constants.Servicios.timeout
        urlconfig.timeoutIntervalForResource = Constants.Servicios.timeout

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdIfacil, clientSecret: Constants.Login.clientSecretFacil, password: Globales.numeroTelefono, realm: Constants.Login.realmFacil, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date()
                        self.makeHTTPGetRequest(path, numero: numero, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
