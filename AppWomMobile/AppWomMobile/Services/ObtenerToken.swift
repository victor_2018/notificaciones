//
//  ObtenerToken.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que obtiene token para login.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias ObtenerTokenResponse = (JSON, NSError?) -> Void

class ObtenerToken: NSObject, URLSessionDelegate {
    static let sharedInstance = ObtenerToken()

    let baseURL = URLServicios.obtenerToken

    func getToken(_ clientId: String, clientSecret: String, username: String, password: String, grantType: String, refreshToken: String, realm: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPPostRequest(route, clientId: clientId, clientSecret: clientSecret, username: username, password: password, grantType: grantType, refreshToken: refreshToken, realm: realm, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a POST Request
    fileprivate func makeHTTPPostRequest(_ path: String, clientId: String, clientSecret: String, username: String, password: String, grantType: String, refreshToken: String, realm: String, onCompletion: @escaping ServiceResponse) {

        let url = path.replacingOccurrences(of: "{realm}", with: realm)
        var request = URLRequest(url: URL(string: url)!)

        // Set the method to POST
        request.httpMethod = "POST"
        var headerRequest = ""
        if Globales.modoTest {
            headerRequest = "client_id=\(clientId)&client_secret=\(clientSecret)&username=56949465173&password=123456789123&grant_type=\(grantType)&refresh_token=\(refreshToken)"
            Logger.log("headerRequest token: \(headerRequest)")
        } else {
            headerRequest = "client_id=\(clientId)&client_secret=\(clientSecret)&username=\(username)&password=\(password)&grant_type=\(grantType)&refresh_token=\(refreshToken)"
            Logger.log("headerRequest token: \(headerRequest)")
        }

        let stringData = headerRequest.data(using: String.Encoding.utf8)

        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = stringData

        let urlconfig = URLSessionConfiguration.default

        urlconfig.timeoutIntervalForRequest = Constants.Servicios.timeout
        urlconfig.timeoutIntervalForResource = Constants.Servicios.timeout

        let session = URLSession(configuration: urlconfig, delegate: self, delegateQueue: nil)

        Logger.log("request: \(request)")

        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            guard error == nil && data != nil else {
                // check for fundamental networking error
                Logger.log("error=\(String(describing: error))")
                onCompletion(JSON.null, nil)
                return
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                Logger.log("statusCode deberia ser 200, pero es \(httpStatus.statusCode)")
                Logger.log("response = \(String(describing: response))")
                if httpStatus.statusCode == Constants.Login.errorAutorizacion {
                    Globales.esErrorLimbo = true
                }
                onCompletion(JSON.null, nil)
            } else {
                let json: JSON = JSON(data: data!)
                Logger.log("response json: \(json)")
                onCompletion(json, nil)
            }

            let responseString = String(data: data!, encoding: String.Encoding.utf8)
            Logger.log("responseString = \(String(describing: responseString))")
        })
        task.resume()
    }
}
