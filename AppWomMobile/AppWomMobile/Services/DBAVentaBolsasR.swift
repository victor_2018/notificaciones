//
//  DBAVentaBolsasR.swift
//  AppWomMobile
//
//  Clase para comprar bolsas roaming.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias DBAVentaBolsasRResponse = (JSON, NSError?) -> Void

class DBAVentaBolsasR: NSObject, URLSessionDelegate {
    static let sharedInstance = DBAVentaBolsasR()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.urlCompraBolsaRoaming)

    func comprarBolsaRoaming(_ body: [String: AnyObject], onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPPostRequest(route, body: body, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a POST Request
    fileprivate func makeHTTPPostRequest(_ path: String, body: [String: AnyObject], onCompletion: @escaping ServiceResponse) {
        let request = NSMutableURLRequest(url: URL(string: path)!)

        // Set the method to POST
        request.httpMethod = "POST"

        do {
            let jsonBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)

            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonBody

            Logger.log("request: \(request)")

            UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
                retorno in
                if retorno {
                    UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                        json, err in
                        if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                            self.contadorLlamadas += 1
                            Globales.tokenApp.dateExpiracionToken = Date()
                            self.makeHTTPPostRequest(path, body: body, onCompletion: { json, err in
                                onCompletion(json, err)
                            })
                        } else {
                            onCompletion(json, err)
                        }
                    })
                } else {
                    Logger.log("error carga Token")
                    onCompletion(JSON.null, nil)
                }
            })
        } catch {
            // Create your personal error
            onCompletion(JSON.null, nil)
        }
    }
}
