//
//  PSCustomerRechargeService.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que obtiene última recarga.
//  @author Juan Tamayo (TINet).
//

import SwiftyJSON

typealias PSCustomerRechargeServiceResponse = (JSON, NSError?) -> Void

class PSCustomerRechargeService: NSObject, URLSessionDelegate {
    static let sharedInstance = PSCustomerRechargeService()
    var contadorLlamadas = 0
    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.consultaRecarga)

    func getPeriodoRecarga(_ numero: String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            makeHTTPGetRequest(route, numero: numero, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }

        // MARK: Perform a GET Request
        fileprivate func makeHTTPGetRequest(_ path: String, numero: String, onCompletion: @escaping ServiceResponse) {

            var urlComponents = URLComponents(string: path)!

            urlComponents.queryItems = [
                URLQueryItem(name: "dirnum", value: numero),
                URLQueryItem(name: "numRows", value: "1")
            ]

            let request = NSMutableURLRequest(url: urlComponents.url!)

            Logger.log("request: \(request)")

            UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
                retorno in
                if retorno {
                    UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                        json, err in
                        if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                            self.contadorLlamadas += 1
                            Globales.tokenApp.dateExpiracionToken = Date()
                            self.makeHTTPGetRequest(path, numero: numero, onCompletion: { json, err in
                                onCompletion(json, err)
                            })
                        } else {
                            onCompletion(json, err)
                        }
                    })
                } else {
                    Logger.log("error carga Token")
                    onCompletion(nil, nil)
                }
            })
        }
}
