//
//  GetNextelProductCatalog.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae todas las bolsas existentes.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias GetNextelProductCatalogResponse = (JSON, NSError?) -> Void

class GetNextelProductCatalog: NSObject, URLSessionDelegate {
    static let sharedInstance = GetNextelProductCatalog()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.catalogoProductos)

    func getBolsasDisponibles(_ clientName: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, clientName: clientName, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, clientName: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "client_name", value: clientName)
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date() as Date!
                        self.makeHTTPGetRequest(path, clientName: clientName, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })

    }
}
