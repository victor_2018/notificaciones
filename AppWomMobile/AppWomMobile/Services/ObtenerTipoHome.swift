//
//  ObtenerTipoHome.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae el tipo de home segun rate plan.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias ObtenerTipoHomeResponse = (JSON, NSError?) -> Void

class ObtenerTipoHome: NSObject, URLSessionDelegate {
    static let sharedInstance = ObtenerTipoHome()

    let baseURL = URLServicios.obtenerTipoHome

    func getTipoHome(_ ratePlan: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, ratePlan: ratePlan, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, ratePlan: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "request", value: ratePlan)
        ]

        let request = URLRequest(url: urlComponents.url!)
        let urlconfig = URLSessionConfiguration.default

        urlconfig.timeoutIntervalForRequest = Constants.Servicios.timeout
        urlconfig.timeoutIntervalForResource = Constants.Servicios.timeout

        let session = URLSession(configuration: urlconfig, delegate: self, delegateQueue: nil)

        Logger.log("request: \(request)")

        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            if let jsonData = data {
                let json: JSON = JSON(data: jsonData)
                Logger.log("json: \(json)")
                if error != nil {
                    onCompletion(json, NSError())
                } else {
                    onCompletion(json, error as NSError?)
                }

            } else {
                Logger.log("error: \(String(describing: error))")
                if error != nil {
                    onCompletion(JSON.null, NSError())
                } else {
                    onCompletion(JSON.null, error as NSError?)
                }
            }
        })
        task.resume()
    }
}
