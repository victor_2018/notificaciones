//
//  ObtenerPromociones.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que obtiene listado de promociones.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias ObtenerPromocionesResponse = (JSON, NSError?) -> Void

class ObtenerPromociones: NSObject, URLSessionDelegate {
    static let sharedInstance = ObtenerPromociones()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.urlObtenerPromociones)

    func obtenerPromociones(_ body: [String: String], onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPPostRequest(route, body: body, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a POST Request
    fileprivate func makeHTTPPostRequest(_ path: String, body: [String: String], onCompletion: @escaping ObtenerPromocionesResponse) {
        let request = NSMutableURLRequest(url: URL(string: path)!)

        // Set the method to POST
        request.httpMethod = "POST"

        do {
            let jsonBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)

            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonBody

            Logger.log("request: \(request)")

            UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
                retorno in
                if retorno {
                    UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                        json, err in
                        if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                            self.contadorLlamadas += 1
                            Globales.tokenApp.dateExpiracionToken = Date() as Date!
                            self.makeHTTPPostRequest(path, body: body, onCompletion: { json, err in
                                onCompletion(json, err)
                            })
                        } else {
                            onCompletion(json, err)
                        }
                    })
                } else {
                    Logger.log("error carga Token")
                    onCompletion(JSON.null, nil)
                }
            })
        } catch {
            // Create your personal error
            onCompletion(JSON.null, nil)
        }
    }
}
