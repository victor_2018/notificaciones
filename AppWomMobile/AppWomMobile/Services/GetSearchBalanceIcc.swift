//
//  GetSearchBalanceIcc.swift
//  AppWomMobile
//
//  Clase para conectarse con servicio que trae valor del saldo.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

typealias GetSearchBalanceIccResponse = (JSON, NSError?) -> Void

class GetSearchBalanceIcc: NSObject, URLSessionDelegate {
    static let sharedInstance = GetSearchBalanceIcc()
    var contadorLlamadas = 0

    let baseURL = UtilApp.verificarURLSegurizada(URLServicios.obtenerBalance)

    func getBalance(_ numeroTelefono: String, detalle: String, onCompletion: @escaping (JSON) -> Void) {
        let route = baseURL
        makeHTTPGetRequest(route, numeroTelefono: numeroTelefono, detalle: detalle, onCompletion: { json, err in
            onCompletion(json as JSON)
        })
    }

    // MARK: Perform a GET Request
    fileprivate func makeHTTPGetRequest(_ path: String, numeroTelefono: String, detalle: String, onCompletion: @escaping ServiceResponse) {

        var urlComponents = URLComponents(string: path)!

        urlComponents.queryItems = [
            URLQueryItem(name: "phone_number", value: numeroTelefono),
            URLQueryItem(name: "detail_bucket", value: detalle)
        ]

        let request = NSMutableURLRequest(url: urlComponents.url!)

        Logger.log("request: \(request)")

        UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
            retorno in
            if retorno {
                UtilServicios.realizarLlamadaServicio(request, controller: self, onCompletion: {
                    json, err in
                    if err?.code != nil && err?.code == Constants.Login.errorAutorizacion && self.contadorLlamadas < Constants.Login.limiteErroresToken {
                        self.contadorLlamadas += 1
                        Globales.tokenApp.dateExpiracionToken = Date()
                        self.makeHTTPGetRequest(path, numeroTelefono: numeroTelefono, detalle: detalle, onCompletion: { json, err in
                            onCompletion(json, err)
                        })
                    } else {
                        onCompletion(json, err)
                    }
                })
            } else {
                Logger.log("error carga Token")
                onCompletion(JSON.null, nil)
            }
        })
    }
}
