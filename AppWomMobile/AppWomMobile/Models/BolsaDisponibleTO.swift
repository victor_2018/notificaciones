//
//  BolsaDisponibleTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de una bolsa disponible para comprar.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class BolsaDisponibleTO {
    var codProducto: String!
    var codBscs: String!

    required init(json: JSON) {
        codProducto = json["cod_prod"].stringValue
        codBscs = json["cod_bscs"].stringValue
    }

    required init() {
    }
}
