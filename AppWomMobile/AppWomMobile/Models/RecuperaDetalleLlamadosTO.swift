//
//  RecuperaDetalleLlamadosTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de llamados del cliente.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class RecuperaDetalleLlamadosTO {
    var minutosIlimitados: Int!
    var minutosIntraRut: Int!
    var minutosOffNet: Int!
    var minutosOnNet: Int!
    var minutosDisponibles: Int!
    var smsIlimitado = false

    required init(json: JSON) {
        minutosIlimitados = json["return"]["minutosIlimitados"].intValue
        minutosIntraRut = json["return"]["minutosIntraRut"].intValue
        minutosOffNet = json["return"]["minutosOffNet"].intValue
        minutosOnNet = json["return"]["minutosOnNet"].intValue
    }

    required init() {
    }
}
