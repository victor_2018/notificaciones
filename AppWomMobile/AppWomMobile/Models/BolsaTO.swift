//
//  BolsaHistorialTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de las bolsas del historial.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON
import MarqueeLabel

class BolsaTO {
    static let sharedInstance = BolsaTO()
    var nombreBolsa: String!
    var diaVencimiento: String!
    var restanteBolsa: Bolsa!
    var totalBolsa: Bolsa!
    var totalBolsaString: String!
    var fechaActivada: String!
    var fechaVencimientoDesplegable: String!
    var promocional: Bool!
    var tipoBolsa: String!
    var colapsado = true
    var esColapsable: Bool!
    var precio: String!
    var esBasal = false
    var idPCRF: String!
    var cantidadAgrupacion = 0
    var esCabecera = false
    var esHijo = false
    var categoria = 0
    var limite: Bool!
    var horario: String!
    var imagen: String!
    var muestraConsumo = 0
    var consumido: Bolsa!
    var agrupacion = -1
    var horaActivada: String!
    var horaVencimiento: String!
    var tipoTematica: Bool!
    var fechaInicioDate: Date!
    var fechaTerminoDate: Date!
    var indiceEnListado = 0
    var plataforma = ""

    required init(esColapsableIN: Bool) {
        esColapsable = esColapsableIN
    }

    required init(postpagoLibre: Bool) {
        var datoRestanteBolsa = Globales.datosLlamadas.minutosDisponibles - Globales.datosLlamadas.minutosOffNet
        datoRestanteBolsa -=  Globales.datosLlamadas.minutosOnNet
        let consumido = Globales.datosLlamadas.minutosOffNet - Globales.datosLlamadas.minutosOnNet

        if Globales.datosLlamadas.minutosDisponibles == -1 {
            nombreBolsa = NSLocalizedString("label_minutos_ilimitados", comment: "Label para minutos ilimitados")
            restanteBolsa = Bolsa(datoIn: consumido, datoEnKilosIn: datoRestanteBolsa, metricaIn: "min.")
        } else {
            nombreBolsa = "Minutos de tu plan \(Globales.datosLlamadas.minutosDisponibles!) minutos"
            restanteBolsa = Bolsa(datoIn: datoRestanteBolsa, datoEnKilosIn: datoRestanteBolsa, metricaIn: "min.")
        }

        esBasal = true
        esColapsable = false
        tipoBolsa = Constants.Indicadores.tipoTraficoMinutos

        totalBolsa = Bolsa(datoIn: Globales.datosLlamadas.minutosDisponibles, datoEnKilosIn: Globales.datosLlamadas.minutosDisponibles, metricaIn: "minutos")
        promocional = false
    }
    required init() {}

    required init(json: JSON, esPromo: Bool, tipoBolsa: String, esBasal: Bool, tipoTematica: Bool) {
        if !json["fechaInicio"].stringValue.isEmpty && !json["fechaTermino"].stringValue.isEmpty {
            let calendar: NSCalendar = NSCalendar.current as NSCalendar

            let fechaInicial = calendar.startOfDay(for: Date())
            let fechaFinal = calendar.startOfDay(for: Util.transformarFechaString(json["fechaTermino"].stringValue, formato: "yyyyMMddHHmmss"))

            let components = NSCalendar.current.dateComponents([.day], from: fechaInicial, to: fechaFinal)

            diaVencimiento = String(describing: components.day!)
            diaVencimiento = String(Int(diaVencimiento)!)

            fechaInicioDate = Util.transformarFechaString(json["fechaInicio"].stringValue, formato: "yyyyMMddHHmmss")
            fechaTerminoDate = Util.transformarFechaString(json["fechaTermino"].stringValue, formato: "yyyyMMddHHmmss")
            fechaActivada = Util.transformarFechaNSDate(fechaInicioDate, formato: "d/MM/yy")
            fechaVencimientoDesplegable = Util.transformarFechaNSDate(fechaTerminoDate, formato: "d/MM/yy")
            horaActivada = Util.transformarFechaNSDate(fechaInicioDate, formato: "HH:mm") + " " + NSLocalizedString("lbl_horas_resumido", comment: "label para indicar hrs")
            horaVencimiento = Util.transformarFechaNSDate(fechaTerminoDate, formato: "HH:mm") + " " + NSLocalizedString("lbl_horas_resumido", comment: "label para indicar hrs")

            esColapsable = true
        } else {
            fechaActivada = ""
            fechaVencimientoDesplegable = ""
            esColapsable = false
        }

        self.limite = json["limite"].boolValue
        self.horario = json["horario"].stringValue
        self.promocional = esPromo
        self.tipoBolsa = tipoBolsa
        self.idPCRF = json["codigoBolsa"].stringValue
        self.categoria = json["idCategoria"].intValue
        self.muestraConsumo = json["muestraConsumo"].intValue
        self.consumido = Util.transformarKilos(String(json["consumidoBolsa"].intValue), dato2: "")
        self.nombreBolsa = json["nombreComercial"].stringValue
        self.esBasal = esBasal
        self.imagen = json["icono"].stringValue
        self.tipoTematica = tipoTematica
        self.plataforma = json["plataforma"].stringValue.lowercased()

        if json["idCategoria"].intValue == 3 || String(json["tarifa"].intValue) == "0" {
            self.precio = NSLocalizedString("label_bolsa_gratis", comment: "bolsa gratuita")
        } else {
            self.precio = Util.formatearPesos(String(json["tarifa"].intValue))
        }

        if tipoBolsa == Constants.Indicadores.tipoTraficoDatos {
            self.restanteBolsa = Util.transformarKilos(String(json["vigenteBolsa"].intValue), dato2: "")
            self.totalBolsa = Util.transformarKilos(String(json["totalBolsa"].intValue), dato2: "")
        } else if tipoBolsa == Constants.Indicadores.tipoTraficoMinutos {
            self.restanteBolsa = Util.transformarSegundos(String(json["vigenteBolsa"].intValue), dato2: "")
            self.totalBolsa = Util.transformarSegundos(String(json["totalBolsa"].intValue), dato2: "")
        } else if tipoBolsa == Constants.Indicadores.tipoTraficoSMS {
            self.restanteBolsa = Bolsa(datoIn: String(json["vigenteBolsa"].intValue), datoEnKilosIn: String(json["vigenteBolsa"].intValue), metricaIn: "SMS")
            self.totalBolsa = Bolsa(datoIn: json["totalBolsa"].intValue, datoEnKilosIn: json["totalBolsa"].intValue, metricaIn: "SMS")
            validarSMSIlimitado(json: json)
        }

        if self.plataforma == Constants.Indicadores.soloConsumo || self.plataforma == Constants.Indicadores.soloNombre || esBasal {
            self.esColapsable = false
        }
    }

    /// Genera el nombre para bolsa basal de sms ilimitado
    func validarSMSIlimitado(json: JSON) {
        if Globales.datosLlamadas.smsIlimitado {
            self.nombreBolsa = NSLocalizedString("label_sms_ilimitados", comment: "Label para sms ilimitados")
            self.restanteBolsa = Bolsa(datoIn: String(json["consumidoBolsa"].intValue), datoEnKilosIn: String(json["consumidoBolsa"].intValue), metricaIn: "SMS")
        }
    }

    required init(grupo: GrupoBolsaTO, esPromo: Bool, esCabecera: Bool) {
        self.promocional = esPromo
        self.nombreBolsa = grupo.descripcion

        if grupo.tipo == Constants.Indicadores.tipoTraficoDatos {
            self.restanteBolsa = Util.transformarKilos(String(grupo.vigente), dato2: "")
            self.totalBolsa = Util.transformarKilos(String(grupo.vigente), dato2: String(grupo.consumido))
        } else {
            self.restanteBolsa = Util.transformarSegundos(String(grupo.vigente), dato2: "")
            self.totalBolsa = Util.transformarSegundos(String(grupo.vigente), dato2: String(grupo.consumido))
        }

        self.cantidadAgrupacion = grupo.cantidadAgrupacion
        self.tipoBolsa = grupo.tipo
        self.esCabecera = esCabecera
        self.imagen = grupo.imagen
        self.categoria = grupo.categoria
        self.esColapsable = true
        self.limite = true
        self.horario = ""
        self.consumido = Bolsa()
        self.agrupacion = grupo.idAgrupacion
    }

    init(otroOBjeto: BolsaTO) {
        nombreBolsa = otroOBjeto.nombreBolsa
        diaVencimiento = otroOBjeto.diaVencimiento
        restanteBolsa = otroOBjeto.restanteBolsa
        totalBolsa = otroOBjeto.totalBolsa
        totalBolsaString = otroOBjeto.totalBolsaString
        fechaActivada = otroOBjeto.fechaActivada
        fechaVencimientoDesplegable = otroOBjeto.fechaVencimientoDesplegable
        promocional = otroOBjeto.promocional
        tipoBolsa = otroOBjeto.tipoBolsa
        colapsado = otroOBjeto.colapsado
        esColapsable = otroOBjeto.esColapsable
        precio = otroOBjeto.precio
        esBasal = otroOBjeto.esBasal
        idPCRF = otroOBjeto.idPCRF
    }

    func copy() -> BolsaTO {
        return BolsaTO(otroOBjeto: self)
    }

}
