//
//  CategoriaBolsaTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de las categorias de las bolsas.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class CategoriaBolsaTO {
    var idCategoria: Int!
    var nombreCategoria: String!
}
