//
//  DetalleBolsaTO.swift
//  AppWomMobile
//
//  Clase TO con datos del detalle de las bolsas.
//  @author Esteban Pavez A. (TINet).
//

class DetalleBolsaTO {
    var imagenDetalle: String!
    var tituloBolsa: String!
    var datosBolsa: String!

    required init(imagenDetalleIN: String, tituloBolsaIN: String, datosBolsaIN: String) {

        imagenDetalle = imagenDetalleIN
        tituloBolsa = tituloBolsaIN
        datosBolsa = datosBolsaIN
    }

    required init() {

    }
}
