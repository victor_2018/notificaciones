//
//  EncabezadoHomeTO.swift
//  AppWomMobile
//
//  Clase TO con los datos del encabezado de los homes.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class EncabezadoHomeTO {
    var saldo = Int()
    var saldoFormateado = String()
    var totalAPagar = String()
    var inicioConsumo = String()
    var proximoConsumo = String()
    var inicioConsumoDate: Date!
    var proximoConsumoDate: Date!
    var ratePlan = Int()
    var tieneDeuda = false
    var idCliente = String()
    var nombrePlan = String()
    var tipoPlan = String()
    var idFormaPago = String()
    var rut = String()
    var urlFactura = String()
    var contratos = [ContractTO]()
    var esBigBoss = false
    var numeroTelefono = String()

    required init(json: JSON) {
        let numberFormatter = NumberFormatter()
        let formatoFecha = "yyyy-MM-dd'T'hh:mm:ss.AAAZ"

        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = " "
        numberFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!

        let formatoFechaFinal: DateFormatter = DateFormatter()
        formatoFechaFinal.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        formatoFechaFinal.dateFormat = "d/MM/yyyy"

        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        inicioConsumoDate = calendar.startOfDay(for: Util.transformarFechaString(json["consumer"]["fechaInicioCiclo"].stringValue, formato: formatoFecha))
        proximoConsumoDate = calendar.startOfDay(for: Util.transformarFechaString(json["consumer"]["fechaTerCiclo"].stringValue, formato: formatoFecha))

        if Globales.indicadorProblemaFechas {
            inicioConsumo = ""
            proximoConsumo = ""
        } else {
            inicioConsumo = formatoFechaFinal.string(from: inicioConsumoDate)
            proximoConsumo = formatoFechaFinal.string(from: proximoConsumoDate)
        }

        idCliente = json["consumer"]["customerID"].stringValue
        let totalDeuda = json["consumer"]["totalApagar"].stringValue

        if Int(totalDeuda)! > 0 {
            tieneDeuda = true
        }

        totalAPagar = "$\(String(describing: numberFormatter.string(from: Int(totalDeuda)! as NSNumber)))"
        ratePlan = Int(json["consumer"]["ratePlan"].numberValue)
        rut = json["consumer"]["RUT"].stringValue
        numeroTelefono = Globales.numeroTelefono
    }

    required init() {
    }
}
