//
//  CatalogoBolsaTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de una bolsa del catalogo.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class CatalogoBolsaTO {
    var idProducto = ""
    var unidadBolsa = ""
    var vigencia = ""
    var tipoTrafico = ""
    var valorBolsa = ""
    var segmentoProducto = ""
    var categoriaProducto = ""
    var esPromo = ""
    var nombre = ""
    var idPCRF = ""
    var idBSCS = ""
    var fechaActivacion = ""
    var fechaFinalizacion = ""
    var horaActivacion = ""
    var horaFinalizacion = ""
    var descripcion = ""
    var tipoProductoBSCS = ""
    var status = ""
    var familyPlan = ""

    required init(json: JSON) {
        if json["vigencia"] != JSON.null {
            let newDateComponents = NSDateComponents()
            newDateComponents.hour = Int(Util.transformarSegundos(json["vigencia"].stringValue, dato2: "").dato)
            let fechaFinal = NSCalendar.current.date(byAdding: newDateComponents as DateComponents, to: Date())
            idProducto = json["id_prod"].stringValue
            unidadBolsa = json["unit_free"].stringValue
            vigencia = json["vigencia"].stringValue
            tipoTrafico = json["trafficType"].stringValue
            valorBolsa = json["tariff_prod"].stringValue
            segmentoProducto = json["segment_prod"].stringValue
            categoriaProducto = json["category_prod"].stringValue
            esPromo = json["promo"].stringValue
            nombre = json["name_product"].stringValue
            idPCRF = json["id_pcrf_product"].stringValue
            idBSCS = json["id_bscs_prod"].stringValue
            fechaActivacion = Util.transformarFechaNSDate(NSDate() as Date, formato: "d/MM/yy")
            fechaFinalizacion = Util.transformarFechaNSDate(fechaFinal!, formato: "d/MM/yy")
            horaActivacion = Util.transformarFechaNSDate(NSDate() as Date, formato: "HH:mm") + " " + NSLocalizedString("lbl_horas_resumido", comment: "label para indicar hrs")
            horaFinalizacion = Util.transformarFechaNSDate(fechaFinal!, formato: "HH:mm") + " " + NSLocalizedString("lbl_horas_resumido", comment: "label para indicar hrs")
            descripcion = json["desc_prod"].stringValue
            tipoProductoBSCS = json["type_prod_bscs"].stringValue
            status = json["status"].stringValue
            familyPlan = json["family_plan"].stringValue
        }
    }

    required init() {
    }
}
