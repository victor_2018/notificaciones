//
//  BolsaTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de las bolsas del home.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class BolsaHistorialTO {
    var nombreProducto: String!
    var fechaVenta: String!
    var fechaExpiracion: String!
    var estadoBolsa: String!
    var valorTotal: String!
    var detalleBolsa: String!
    var numTransaccion: String!
    var detalleBolsaTitulo1: String!
    var detalleBolsaSubtitulo1: String!
    var detalleBolsaTitulo2: String!
    var detalleBolsaSubtitulo2: String!
    var tipoBolsa: String!
    var tipoTrafico: String!

    required init(json: JSON) {
        let formatoFecha: DateFormatter = DateFormatter()
        formatoFecha.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatoFecha.locale = NSLocale(localeIdentifier: "en_US") as Locale!

        nombreProducto = json["name_prod"].stringValue
        fechaVenta = Util.transformarFechaNSDate(formatoFecha.date(from: json["fecha_venta"].stringValue)!, formato: "d/MM/yy")
        fechaExpiracion = Util.transformarFechaNSDate(formatoFecha.date(from: json["fecha_expiracion"].stringValue)!, formato: "d/MM/yy")

        estadoBolsa = json["estado_bolsa"].stringValue
        valorTotal = json["strtariff_prod"].stringValue
        detalleBolsa = json["strdesc_prod"].stringValue
        numTransaccion = json["transaction_code_buy"].stringValue
        tipoBolsa = json["strcategory_prod"].stringValue
        tipoTrafico = json["strtraffic_type"].stringValue
    }

    required init() {

    }
}
