//
//  ContactoWhatsappTO.swift
//  AppWomMobile
//
//  Clase TO con datos del contacto de whatsapp obtenido de firebase
//  @author Esteban Pavez A. (TINet).
//

class ContactoWhatsappTO {
    var mensaje = ""
    var numero = ""

    required init() {
    }
}
