//
//  Bolsa.swift
//  AppWomMobile
//
//  Clase de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class Bolsa {
    var dato: CGFloat!
    var datoEnKilos: CGFloat!
    var metrica: String!
    var valor: Int!
    var valorFormateado: String!
    var vigencia: String!
    var nombre: String!
    var tipo: String!
    var idProducto: String!
    var idPCRF: String!
    var idBSCS: String!
    var fechaActivacion: String!
    var fechaFinalizacion: String!
    var horaActivacion: String!
    var horaFinalizacion: String!
    var es4G = false
    var descripcion: String!
    var tipoProductoBSCS = ""

    required init(datoIn: CGFloat, datoEnKilosIn: CGFloat, metricaIn: String) {
        self.dato = datoIn
        self.datoEnKilos = datoEnKilosIn
        self.metrica = metricaIn
    }

    required init(datoIn: String, datoEnKilosIn: CGFloat, metricaIn: String) {
        self.dato = CGFloat(Int(datoIn)!)
        self.datoEnKilos = datoEnKilosIn
        self.metrica = metricaIn
    }

    required init(datoIn: String, datoEnKilosIn: String, metricaIn: String) {
        self.dato = CGFloat(Int(datoIn)!)
        self.datoEnKilos = CGFloat(Int(datoEnKilosIn)!)
        self.metrica = metricaIn
    }

    required init(datoIn: Int, datoEnKilosIn: Int, metricaIn: String) {
        self.dato = CGFloat(datoIn)
        self.datoEnKilos = CGFloat(datoEnKilosIn)
        self.metrica = metricaIn
    }

    required init(datoIn: Int, datoEnKilosIn: CGFloat, metricaIn: String) {
        self.dato = CGFloat(datoIn)
        self.datoEnKilos = datoEnKilosIn
        self.metrica = metricaIn
    }

    required init() {
    }
}
