//
//  PaisesTO.swift
//  AppWomMobile
//
//  Clase TO con datos de los paises para Roaming
//  @author Esteban Pavez A. (TINet).
//

class PaisesTO {
    var paisesPreferente = ""
    var paisesEuropa = ""
    var paisesRestoDelMundo = ""

    required init() {
    }
}
