//
//  PromocionesTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de las promociones.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class PromocionesTO {
    var idPromo: String!
    var urlImagenHomeBanner: String!
    var urlImagenDetalle: String!
    var urlImagenListado: String!
    var nombre: String!
    var descripcion1: String!
    var descripcion2: String!
    var prioridad: Int!
    var condicionesPromocion: String!
    var idTransaccion: String!
    var tipoPromo: String!
    var idPCRF: String!
    var idProd: String!

    required init(json: JSON) {
        switch json["type"].stringValue.lowercased() {
        case Constants.Promociones.tipoDatos, Constants.Promociones.tipoSMS, Constants.Promociones.tipoVoz:
            tipoPromo = Constants.Indicadores.tipoPromoCompraBolsa
            break
        case Constants.Promociones.tipoInformativa:
            tipoPromo = Constants.Indicadores.tipoPromoInformativa
            break
        default:
            tipoPromo = ""
        }
        idPromo = json["offerID"].stringValue
        urlImagenHomeBanner = json["offerImage1"].stringValue.replacingOccurrences(of: " ", with: "")
        urlImagenDetalle = json["offerImage2"].stringValue.replacingOccurrences(of: " ", with: "")
        urlImagenListado = json["offerImage3"].stringValue.replacingOccurrences(of: " ", with: "")
        nombre = json["offerShortName"].stringValue
        descripcion1 = json["offerDescription1"].stringValue
        descripcion2 = json["offerDescription2"].stringValue
        prioridad = json["priority"].intValue
        condicionesPromocion = json["offerTermsCond"].stringValue
        idTransaccion = json["idTransaccion"].stringValue
        idPCRF = json["pcrfProdID"].stringValue
        idProd = json["offerImage4"].stringValue
    }

    required init() {
    }
}
