//
//  ReglaValidacionBolsaTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de respuesta para validar bolsas disponibles.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class ReglaValidacionBolsaTO {
    var parametro: String!
    var aplica: String!
    var valor: String!

    required init(json: JSON) {
        parametro = json["parameter"].stringValue
        aplica = json["applied"].stringValue
        valor = json["value"].stringValue
    }

    required init() {
    }
}
