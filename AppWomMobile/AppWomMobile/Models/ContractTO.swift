//
//  ContractTO.swift
//  AppWomMobile
//
//  Clase TO con datos de las lineas del cliente
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class ContractTO {
    var contractId = 0
    var nroCelular = ""
    var ratePlan = 0
    var shDes = ""
    var equipo = ""
    var tecnologia = 0
    var tipoContrato = ""
    var eqSerialNum = ""
    var estado = ""
    var fechaActivacion = Date()
    var pin = ""
    var puk = ""

    required init() {
    }

    required init(json: JSON) {
        let formatoFecha = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        fechaActivacion = Util.transformarFechaString(json["fechaActivacion"].stringValue, formato: formatoFecha)

        nroCelular = json["nroCelular"].stringValue
        ratePlan = json["ratePlan"].intValue
        tipoContrato = json["tipoContrato"].stringValue
    }
}
