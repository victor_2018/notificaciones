//
//  CompraBolsasTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de respuesta al comprar bolsa.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class CompraBolsasTO {
    var codigoTransaccion: String!
    var codigoError: String!
    var descripcionError: String!

    required init(json: JSON) {
        codigoTransaccion = json["transaction_code"].stringValue
        codigoError = json["error_code"].stringValue
        descripcionError = json["err_description"].stringValue
    }

    required init() {

    }
}
