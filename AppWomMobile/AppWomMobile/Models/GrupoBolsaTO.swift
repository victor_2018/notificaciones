//
//  GrupoBolsaTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de los grupos de las bolsas.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class GrupoBolsaTO {

    var idAgrupacion: Int!
    var nombreAgrupacion: String!
    var descripcion: String!
    var tipo: String!
    var totalAgrupacion: Int!
    var vigente: Int!
    var consumido: Int!
    var cantidadAgrupacion: Int!
    var imagen: String!
    var categoria = 0
    var bolsas = [BolsaTO]()

    required init(json: JSON, esPromo: Bool, esBasal: Bool) {
        idAgrupacion = json["idAgrupacion"].intValue
        nombreAgrupacion = json["nombreAgrupacion"].stringValue
        descripcion = json["descripcionAgrupacion"].stringValue
        tipo = json["tipoAgrupacion"].stringValue
        totalAgrupacion = json["totalAgrupacion"].intValue
        vigente = json["vigenteAgrupacion"].intValue
        consumido = json["consumidoAgrupacion"].intValue
        cantidadAgrupacion = json["cantidadAgrupacion"].intValue

        var tipoTematica = false

        if json["red"].stringValue.lowercased() == Constants.Indicadores.tipoTematica {
            tipoTematica = true
        }

        for bolsa in json["bolsas"].array! {
            bolsas.append(BolsaTO(json: bolsa, esPromo: esPromo, tipoBolsa: tipo, esBasal: esBasal, tipoTematica: tipoTematica))
        }

    }

    required init(bolsa: JSON) {
        idAgrupacion = bolsa["idAgrupacion"].intValue
        nombreAgrupacion = bolsa["nombreAgrupacion"].stringValue
        descripcion = bolsa["descripcionAgrupacion"].stringValue
        tipo = bolsa["tipoAgrupacion"].stringValue
        cantidadAgrupacion = 1
        consumido = bolsa["consumidoBolsa"].intValue
        totalAgrupacion = bolsa["totalBolsa"].intValue
        imagen = bolsa["icono"].stringValue
        categoria = bolsa["idCategoria"].intValue
        vigente = bolsa["vigenteBolsa"].intValue
        categoria = bolsa["idCategoria"].intValue
    }

    required init() {
    }
}
