//
//  TokenTO.swift
//  AppWomMobile
//
//  Clase TO con los datos de respuesta del token del login.
//  @author Esteban Pavez A. (TINet).
//

import SwiftyJSON

class TokenTO {
    var token: String!
    var expiracionToken: String!
    var tokenRefresco: String!
    var expiracionTokenRefresco: String!
    var tipo: String!
    var idToken: String!
    var notBeforePolicy: String!
    var estadoSesion: String!
    var IMSI: String!
    var dateExpiracionToken: Date!
    var dateExpiracionTokenRefresco: Date!

    required init(json: JSON) {
        token = json["access_token"].stringValue
        expiracionToken = json["expires_in"].stringValue
        expiracionTokenRefresco = json["refresh_expires_in"].stringValue
        tokenRefresco = json["refresh_token"].stringValue
        tipo = json["token_type"].stringValue
        idToken = json["id_token"].stringValue
        notBeforePolicy = json["not-before-policy"].stringValue
        estadoSesion = json["session_state"].stringValue
    }

    required init() {
    }
}
