//
//  Constants.swift
//  AppWomMobile
//
//  Archivo de constantes globales de la aplicación.
//
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class Constants {

    struct TipoFuente {
        static let ceraBold = "Cera-Bold"
        static let ceraRegular = "Cera-Regular"
        static let ceraLight = "Cera-Light"
    }

    struct TamanioFuente {
        static let tamTitulo = 29
        static let tamTituloIpad = 60
    }

    struct Imagenes {
        static let imgBolsaVigente = "bolsa-vigente"
        static let imgBolsaHistorica = "bolsa-historica"
        static let imgTarjetaSim = "cellphone-sim-card"
        static let imgMinutos = "call-icon"
        static let imgSeccionDatosMoviles = "icono-datos-moviles"
        static let imgSeccionMinutos = "icono-phone"
        static let imgSeccionMensajes = "icono-message"
        static let imgBarraEstado = "barra_bolsas_final"
        static let imgDesplegarBolsas = "arrow-up"
        static let imgPlegarBolsas = "arrow-down"
        static let imgBotonComprar = "btn_comprar_bolsas"
        static let imgBotonBgBolsa = "bg_btn_big"
        static let imgIconoBolsa = "bag_line_purple"
        static let imgTutorial1 = "speaker_icon"
        static let imgTutorial2 = "smartphone_hand"
        static let imgTutorial3 = "trophy_icon"
        static let imgFondoTuto1 = "slide_1"
        static let imgFondoTuto2 = "slide_2"
        static let imgFondoTuto3 = "slide_3"
        static let imgBackgroundTutorial = "giphy"
        static let imgBtnBolsaDato = "datos_icon"
        static let imgBtnBolsaSMS = "mensaje_icon"
        static let imgBtnBolsaVoz = "phone_icon"
        static let imgBtnBolsaMixta = "mix_icon"
        static let imgBtnBolsaTematica = "btn-exclusivas"
        static let imgBtnBolsaRoaming = "roaming_icon"
        static let imgNivelWomer0 = "nivel_0"
        static let imgNivelWomer1 = "nivel_1"
        static let imgNivelWomer2 = "nivel_2"
        static let imgNivelWomer3 = "nivel_3"
        static let imgNivelWomer4 = "nivel_4"
        static let imgNivelWomer5 = "nivel_5"
        static let imgNivelWomerDefault = "profile_picture"
        static let imgBolsaFija = "icono-cell-small"
        static let imgBolsaPromo = "icono-tag-small"
        static let imgBolsa = "icono-bag-small"
        static let imgBtnDeshabilitado = "button-disabled"
        static let imgAvatarDefecto = "avatar_wom"
        static let imgAvatarPerfilDefecto = "avatar_wom"
        static let imgTicketSeleccionado = "ticket_box_on"
        static let imgTicketDeseleccionado = "ticket_box_off"
        static let imgChevronWhiteDown = "chevron-down"
        static let imgChevronWhiteUp = "chevron-up"
        static let imgIconoConDeuda = "exclamacion_pagar"
        static let imgIconoSinDeuda = "check_circle"
        static let imgBtnSinFondoLargo = "btn_bolsa_full"
        static let imgBtnWhiteGrande = "bg_btn_white"
        static let imgWhatsapp = "Whatsapp-icon"
        static let imgLaptop = "icon_laptop"
        static let imgCerrarMorado = "close_icon"
        static let imgMantencion = "icon_mantencion"
        static let imgLogoWom = "logo_wom"
        static let imgMultilinea = "icon_mutilinea_md"
        static let imgCompraBolsaFinalizada = "icon_wom_rocks"
        static let imgErrorGeneral = "icon_ghost"
        static let urlImagenDarkside = "https://1701336841.rsc.cdn77.org/app/error/icon_vadder.png"
        static let urlImagenLimbo = "https://1701336841.rsc.cdn77.org/app/error/icon_wom_rocks.png"
    }

    struct Indicadores {
        static let tipoConsultaLinea = "LINEA"
        static let tipoConsultaRUT = "RUT"
        static let tipoPrepago = "PREPAID"
        static let tipoPostpago = "POSTPAID"
        static let tipoControlado = "HYBRID"
        static var clientName = "WEBAPP"
        static var clientPortal = "PORTAL"
        static var clientType = "PERSONA"
        static var idProd = "BOLSA"
        static var idFormaPagoSaldo = "SALDO"
        static var metodoPagoPrepagoControlado = "SALDO_PREPAGO"
        static var metodoPagoPostpago = "CONTRA_FACTURA"
        static var pagoAplicado = "N"
        static var nivelWomerMaximo = "5"
        static var seccionDatosMoviles = "MOVILES"
        static var seccionDatosMovilesBasales = "MOVILES_BASALES"
        static var seccionDatosMovilesBolsasCompradas = "MOVILES_COMPRADAS"
        static var seccionDatosMovilesBolsasPromocionales = "MOVILES_PROMOCIONALES"
        static var seccionMinutosBasales = "MINUTOS_BASALES"
        static var seccionMinutosBolsasCompradas = "MINUTOS_COMPRADAS"
        static var seccionMinutosPromocionales = "MINUTOS_PROMOCIONALES"
        static var seccionMinutos = "MINUTOS"
        static var seccionSMS = "SMS"
        static var tabHome = 0
        static var tabComprarBolsas = 1
        static var tabNewWomer = 2
        static var tabPromociones = 4
        static var tipoBasal = "BASAL"
        static var tipoBolsaComprada = "COMPRADA"
        static var tipoBolsaPromocional = "PROMO"
        static var tipoBolsaMinutosBasal = "MINUTOS_BASAL"
        static var tipoBolsaMinutosComprada = "MINUTOS_COMPRADA"
        static var tipoBolsaMinutosPromocional = "MINUTOS_PROMO"
        static var tipoBolsaSMS = "SMS_BASAL"
        static var tipoTraficoDatos = "DATOS"
        static var tipoTraficoMinutos = "VOZ"
        static var tipoTraficoSMS = "SMS"
        static var pasoNewWomer5 = "paso5"
        static var recargarHome = "app-1"
        static var recargarCompraBolsas = "app-0"
        static var tipoBolsaTematica = "APP_Tema"
        static var tipoPromoCompraBolsa = "promoCompraBolsa"
        static var tipoPromoInformativa = "promoInformativa"
        static var canalApp = "APP"
        static var margenTiempoBanner = -5
        static var tipoTematica = "tem"
        static var categoriaBasal = 1
        static var categoriaBolsasCompradas = 2
        static var categoriaPromocionales = 3
        static var smsIlimitado = "-1"
        static var bolsa4g = "4G"
        static var margenTiempoCaching = -720
        static var roamingPreferente = "BR0010"
        static var roamingEuropa = "BR0011"
        static var roamingRestoDelMundo = "BR0012"
        static var bolsaActivada = "A"
        static var familyPlanAll = "ALL"
        static var roamingActivado = "S"
        static var soloConsumo = "consumo"
        static var soloVigencia = "vigencia"
        static var consumoIlimitado = CGFloat(-1)
        static var soloNombre = "solo_nombre"
    }

    struct KeyPlist {
        static var numeroTelefono = "numeroTelefono"
        static var IMSI = "IMSI"
        static var imagenPerfil = "pathImagenPerfil"
        static var validacionAvance = "validacionAvanceAppKey"
        static var nombreCliente = "nombreCliente"
        static var apellidoCliente = "apellidoCliente"
        static var listadoPromociones = "promociones"
        static var respuestaServicioValidaVenta = "respuestaServicioValidaVenta"
        static var respuestaServicioUpselling = "respuestaServicioUpselling"
        static var tiempoCachingCompraBolsas = "tiempoCachingCompraBolsas"
    }

    struct Login {
        static var realmFacil = "ifacil"
        static var realmServicios = "servicios"
        static var clientIdIfacil = "ifacil-mobile"
        static var clientIdServicios = "mobile-client"
        static var clientSecretFacil = "413f9fa7-3fe0-43d0-8f96-1222f2e78c39"
        static var clientSecretServicios = "b623cc32-91f0-46d9-bb1c-1406da04cfee"
        static var headerAuthorization = "Bearer"
        static var grantType = "password"
        static var refreshToken = "refresh_token"
        static var limiteErroresToken = 3
        static var errorAutorizacion = 401
        static var numeroDemo = "56949465173"
    }

    struct TipoHome {
        static var homePrepago = "PREPAID"
        static var homeBam = "BAM"
        static var homeBusiness = "HYBRID_BUSINESS"
        static var homePostpagoHibrido = "HYBRID_POSTPAID"
        static var homePostpagoLibre = "POSTPAID_LIBRE"
        static var homePostpagoDatosSMS = "POSTPAID_DATOSSMS"
        static var homePostpagoVozSMS = "POSTPAID_VOZSMS"
        static var homeControladoSimple = "HYBRID_SIMPLE"
        static var homeControladoAntiguo = "HYBRID_ANTIGUO"
    }

    struct Tags {
        static var tagLoader = 101
        static var tabHome = 0
        static var tabCompraBolsas = 1
        static var tabNewWomer = 2
        static var tabAyuda = 3
        static var homeBanner = 15
    }

    struct Servicios {
        static var timeout = Double(30)
    }

    struct URL {
        static var videoInicial = "https://1701336841.rsc.cdn77.org/vid/app_home.mp4"
        static var ayudaPortabilidad = "http://www.wom.cl/portate"
        static var urlRoaming = "https://www.wom.cl/cobertura/roaming-internacional"
    }

    struct Promociones {
        static var tipoDatos = "datos"
        static var tipoRRSS = "redesSociales"
        static var tipoVoz = "voz"
        static var tipoSMS = "sms"
        static var tipoInformativa = "informativa"
    }

    struct Hashmap {
        static var tipoBolsaImagen: [String: String] = ["DATOS": "datos_small",
                                                        "SMS": "sms_small",
                                                        "VOZ": "voz_small",
                                                        "4G": "4g_small",
                                                        "TEMATICA": "tematicas_small",
                                                        "MIXTAS": "mixtas_small",
                                                        "ROAMING": "icon_roaming_small"
                                                        ]
        static var tipoBolsaImagenModal: [String: String] = ["DATOS": "datos_big",
                                                        "SMS": "sms_big",
                                                        "VOZ": "voz_big",
                                                        "4G": "4g_big",
                                                        "TEMATICA": "tematicas_big",
                                                        "MIXTAS": "mixtas_big",
                                                        "ROAMING": "icon_roaming_big"
                                                        ]
        static var textoBasal: [String: String] = ["DATOS": "datos",
                                                             "SMS": "SMS",
                                                             "VOZ": "minutos"
        ]
        static var tipoRoaming: [String: String] = ["BR0010": "Roaming Preferente",
                                                   "BR0011": "Roaming Europa",
                                                   "BR0012": "Roaming Resto del Mundo"
        ]

    }

    struct RRSSIdentificador {
        static var tokenFacebook = "2248597245366307|xnhxvbxa1zp-01kYSaNbUkEqpSg"
        static var appId = "2248597245366307"
    }

    struct Firebase {
        static var rutaWhatsappDarkside = "configuracion_app/whatsapp_darkside"
        static var rutaWhatsappRoaming = "configuracion_app/whatsapp_roaming"
        static var rutaListadoPaisesRoaming = "configuracion_app/bolsas_roaming"
        static var rutaMantencion = "configuracion_app/configuracion_mantencion"
    }
}
