//
//  HomeIpad.swift
//  AppWomMobile
//
//  Controlador de la vista principal de la aplicación.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class HomeIpad: UIViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        // Botón de navegación para volver al Home
        let backItem = UIBarButtonItem()

        backItem.title = NSLocalizedString("boton_atras_navegacion_home", comment: "botonAtras")
        navigationItem.backBarButtonItem = backItem
    }
}
