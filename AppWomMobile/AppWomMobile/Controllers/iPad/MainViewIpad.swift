//
//  MainViewIpad.swift
//  AppWomMobile
//
//  Controlador de vista inicial de la aplicación iPad.
//
//  @author Esteban Pavez A. (TINet).
//


import UIKit

class MainViewIpad: UIViewController, UIPageViewControllerDataSource {
    // Enlace con boton para saltar tutorial en vista
    @IBOutlet weak var saltarButton: UIButton!
    // Enlace con pageControl en vista
    @IBOutlet weak var pageControl: UIPageControl!
    // Enlace con imagen de fondo de boton para saltar tutorial en vista
    @IBOutlet weak var buttonBackground: UIImageView!

    // viewController generado que contiene controladores de slides
    var pageViewController: UIPageViewController!
    // controller de login
    var loginViewController: UIViewController!
    // arreglo de Strings con los titulos de slides de tutoriales
    var tituloViews: NSArray!
    // arreglo de Strings con las descripciones de slides de tutoriales
    var descripcionViews: NSArray!
    // arreglo de Strings con los nombres de las imagenes de los tutoriales
    var imagenesViews: NSArray!
    // Llave para obtener propiedad que guarda la visualizacion del tutorial
    let validacionAvanceAppKey = "validacionAvanceAppKey"
    // Estado de aplicación que ya se revisó tutorial
    let estadoTutorialRevisado = 1

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    /**
     Añade las imagenes y titulos dinamicamente a los slides de tutoriales.
     */
    func mostrarTutorial() {
        //Cambiar al tener diseño final
        self.tituloViews = NSArray(objects: NSLocalizedString("titulo_tutorial_slide1", comment: "tituloSlide1"), NSLocalizedString("titulo_tutorial_slide2", comment: "tituloSlide2"), NSLocalizedString("titulo_tutorial_slide3", comment: "tituloSlide3"))

        self.descripcionViews = NSArray(objects: NSLocalizedString("descripcion_tutorial_slide1", comment: "descripcionTutorialSlide1"), NSLocalizedString("descripcion_tutorial_slide2", comment: "descripcionTutorialSlide2"), NSLocalizedString("descripcion_tutorial_slide3", comment: "descripcionTutorialSlide3"))

        self.imagenesViews = NSArray(objects: NSLocalizedString("nombre_imagen_slide1", comment: "nombre_imagen_slide1"), NSLocalizedString("nombre_imagen_slide2", comment: "nombre_imagen_slide2"), NSLocalizedString("nombre_imagen_slide3", comment: "nombre_imagen_slide3"))
        self.pageControl.currentPage = 0

        self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "TutorialPageViewController_ipad") as! UIPageViewController
        self.pageViewController.dataSource = self

        let startVC = self.mainControllerAtIndex(0) as TutorialContenidoIpad
        let viewControllers = NSArray(object: startVC)

        self.pageViewController.setViewControllers((viewControllers as! [UIViewController]), direction: .forward, animated: true, completion: nil)

        self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height)

        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParentViewController: self)

        self.view.bringSubview(toFront: self.pageControl)
        self.view.bringSubview(toFront: self.saltarButton)
    }

    /**
     Forma vista actual del tutorial

     - parameter index: Índice de donde está parado en tutorial.

     - returns: View Controller de la vista generada dinamicamente
     */
    func mainControllerAtIndex(_ index: Int) -> TutorialContenidoIpad {
        if self.tituloViews.count == 0 || index >= self.tituloViews.count {
            return TutorialContenidoIpad()
        }

        let vc: TutorialContenidoIpad = self.storyboard?.instantiateViewController(withIdentifier: "TutorialContenidoIpad") as! TutorialContenidoIpad

        vc.imagen = self.imagenesViews[index] as! String
        vc.textoTitulo = self.tituloViews[index] as! String
        vc.descrip = self.descripcionViews[index] as! String
        vc.indiceView = index

        return vc
    }

    /**
     Obtiene el índice de la vista que viene anterior a la actual.

     - parameter pageViewController: variable no utilizada pero obligatoria
     - parameter viewController:     controlador con base para tutorial

     - returns: viewController de la vista anterior.
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        let vc = viewController as! TutorialContenidoIpad
        var index = vc.indiceView as Int

        self.pageControl.currentPage = index

        if index == 0 || index == NSNotFound {
            return nil
        }

        index -= 1

        return self.mainControllerAtIndex(index)

    }

    /**
     Obtiene el índice de la vista que viene después a la actual.

     - parameter pageViewController: variable no utilizada
     - parameter viewController:     controlador con base para tutorial

     - returns: viewController de la vista siguiente.
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        let vc = viewController as! TutorialContenidoIpad
        var index = vc.indiceView as Int

        self.pageControl.currentPage = index

        if index == self.tituloViews.count {
            return nil
        }

        index += 1

        if index == self.tituloViews.count {
            return nil
        }

        return self.mainControllerAtIndex(index)
    }
}
