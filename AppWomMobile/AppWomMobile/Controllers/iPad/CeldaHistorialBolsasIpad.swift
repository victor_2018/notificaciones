//
//  CeldaHistorialBolsasIpad.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaHistorialBolsasIpad: UITableViewCell {
    // Enlace con label del titulo de la bolsa
    @IBOutlet weak var nombreProducto: UILabel!
    // Enlace con label de fecha de activación
    @IBOutlet weak var fechaActivacion: UILabel!
    // Enlace con label de la primera parte del label de detalle de la bolsa
    @IBOutlet weak var datoBolsaPrimero: UILabel!
    // Enlace con label de la segunda parte del label de detalle de la bolsa
    @IBOutlet weak var datoBolsaSegundo: UILabel!
    // Enlace con imagen del estado de la bolsa
    @IBOutlet weak var imgEstado: UIImageView!
    // Enlace con imagen de la primera parte del label de detalle de la bolsa
    @IBOutlet weak var imgDatoBolsaPrimero: UIImageView!
    // Enlace con imagen de la segunda parte del label de detalle de la bolsa
    @IBOutlet weak var imgDatoBolsaSegundo: UIImageView!
}
