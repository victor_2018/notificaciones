//
//  HistorialBolsasIpad.swift
//  AppWomMobile
//
//  Controlador en donde se mostrarán las bolsas compradas por el womer.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON

class HistorialBolsasIpad: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Enlace con tabla de bolsas en vista
    @IBOutlet weak var tablaBolsas: UITableView!

    // arreglo de bolsas
    var bolsas = [BolsaHistorialTO]()
    // índice de fila seleccionada
    var indexSeleccionado = Int()
    // controlador del detalle de las bolsas
    //var detalleBolsasViewController = DetalleBolsasIpad()

    override func viewWillAppear(_ animated: Bool) {
        self.tablaBolsas.dataSource = self
        self.tablaBolsas.delegate = self

        // Se reinicializa tabla al volver desde vista siguiente
        self.bolsas.removeAll()
        self.tablaBolsas.reloadData()

        buscarBolsas()
    }

    func buscarBolsas() {
        //Quitar comentarios para usar dummies

        //        HistorialBolsasService.sharedInstance.getHistorialBolsas { (json: JSON) in
        //            if let results = json["recuperaInfoBolsaHistoricoOut"].array {
        //                for entry in results {
        //                    self.items.append(BolsaTO(json: entry))
        //                    print(entry)
        //                }
        //                dispatch_async(dispatch_get_main_queue(),{
        //                    self.tablaBolsas.reloadData()
        //                })
        //            }
        //        }

        self.tablaBolsas.reloadData()
    }

    /**
     Función donde se setea el número de filas en la tabla

     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     sección en tabla

     - returns: valor de filas que se mostrarán
     */
    func tableView(_ tablaBolsas: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bolsas.count
    }

    /**
     Función que estructura las filas de historial de bolsas

     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila

     - returns: celda seleccionada
     */
    func tableView(_ tablaBolsas: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // objeto de celda seleccionada
        let cell = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsas_ipad", for: indexPath) as! CeldaHistorialBolsasIpad
        // bolsa relacionada a la fila
        let bolsa = self.bolsas[indexPath.row]
        let tipoBolsaMixta = "BOLSA_MIXTA"
        let tipoBolsaVigente = "VIGENTE"

        cell.nombreProducto.text = bolsa.nombreProducto
        cell.fechaActivacion.text = NSLocalizedString("pre_fecha_activacion", comment: "texto que precede a fecha activacion") + bolsa.fechaVenta

        cell.datoBolsaPrimero.text = "1 gb."

        if bolsa.tipoBolsa != tipoBolsaMixta {
            cell.datoBolsaSegundo.isHidden = true
            cell.imgDatoBolsaSegundo.isHidden = true
        } else {
            cell.datoBolsaSegundo.text = "50 min"
        }


        if bolsa.estadoBolsa == tipoBolsaVigente {
            cell.imgEstado.image = UIImage(named: Constants.Imagenes.imgBolsaVigente)
        } else {
            cell.imgEstado.image = UIImage(named: Constants.Imagenes.imgBolsaHistorica)
        }

        return cell
    }

    /**
     Código que realiza al seleccionar una fila

     - parameter tableView: tabla de bolsas en vista
     - parameter indexPath: índice seleccionado
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexSeleccionado = indexPath.row
        //detalleBolsasViewController.bolsa = self.bolsas[indexSeleccionado]
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //detalleBolsasViewController = segue.destinationViewController as! DetalleBolsasIpad

        // Botón de navegación para volver a las bolsas
        let backItem = UIBarButtonItem()

        backItem.title = NSLocalizedString("boton_atras_navegacion_historial_bolsas", comment: "boton para volver a bolsas")
        navigationItem.backBarButtonItem = backItem
    }
}
