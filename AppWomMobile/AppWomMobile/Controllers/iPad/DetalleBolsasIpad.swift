//
//  DetalleBolsasIpad.swift
//  AppWomMobile
//
//  Controlador encargado de generar los datos para la tabla de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class DetalleBolsasIpad: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Enlace con label "Detalle" en vista
    @IBOutlet weak var detalleBolsa: UILabel!
    // Enlace con label "Valor total" en vista
    @IBOutlet weak var valorTotal: UILabel!
    // Enlace con label "Nº transaccion" en vista
    @IBOutlet weak var numTransaccion: UILabel!
    // Enlace con label "Estado" en vista
    @IBOutlet weak var fechaActivacion: UILabel!

    // objeto de la bolsa de historial
    var bolsa = BolsaHistorialTO()
    // arreglo que contiene los detalles de la bolsa
    var detallesBolsa = [DetalleBolsaTO]()

    override func viewDidLoad() {
        super.viewDidLoad()

        detalleBolsa.text = bolsa.detalleBolsa
        valorTotal.text = String(bolsa.valorTotal)
        numTransaccion.text = bolsa.numTransaccion
        fechaActivacion.text = bolsa.fechaVenta

        armarListaDetalles()
    }

    /**
     Genera las celdas de detalle de bolsas.
     */
    func armarListaDetalles() {
        self.detallesBolsa.append(DetalleBolsaTO(imagenDetalleIN: Constants.Imagenes.imgTarjetaSim, tituloBolsaIN: bolsa.detalleBolsaTitulo1, datosBolsaIN: bolsa.detalleBolsaSubtitulo1))

        if bolsa.detalleBolsaTitulo2 != "" {
            self.detallesBolsa.append(DetalleBolsaTO(imagenDetalleIN: Constants.Imagenes.imgMinutos, tituloBolsaIN: bolsa.detalleBolsaTitulo2, datosBolsaIN: bolsa.detalleBolsaSubtitulo2))
        }

    }

    /**
     Determina el número de filas que tendrá la tabla

     - parameter tablaBolsas: tabla que se determinará la cantidad de filas
     - parameter section:     sección a que se determinará la fila

     - returns: número de filas
     */
    func tableView(_ tablaBolsas: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detallesBolsa.count
    }

    /**
     Función que arma las celdas en la fila

     - parameter tablaDetalleBolsas: tabla que se generarán las celdas
     - parameter indexPath:          índice que indica la fila que está generando

     - returns: celda generada
     */
    func tableView(_ tablaDetalleBolsas: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // UITableViewCell que se generará en la tabla
        let cell = tablaDetalleBolsas.dequeueReusableCell(withIdentifier: "celdaDetalleBolsa", for: indexPath) as! CeldaHistorialBolsasIpad
        // objeto con los datos del detalle de bolsa
//        let detalle = self.detallesBolsa[indexPath.row]

//        cell.imagenDetalle.image = UIImage(named: detalle.imagenDetalle)
//        cell.tituloBolsa.text = detalle.tituloBolsa
//        cell.datosBolsa.text = detalle.datosBolsa

        return cell
    }
}
