//
//  TutorialContenidoIpad.swift
//  AppWomMobile
//
//  Controlador en donde se cargarán los slides de las vistas de los tutoriales.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class TutorialContenidoIpad: UIViewController {

    // Enlace a imagen en vista
    @IBOutlet weak var imagenView: UIImageView!
    // Enlace a label del titulo en vista
    @IBOutlet weak var tituloLabel: UILabel!
    // Enlace a label de la descripción en vista
    @IBOutlet weak var descripcion: UILabel!


    // Indice de pagina del tutorial
    var indiceView: Int!
    // Titulo del slide
    var textoTitulo: String!
    // Nombre de la imagen de fondo del slide
    var imagen: String!
    // texto con la descripción del slide
    var descrip: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagenView.image = UIImage(named: self.imagen)
        self.tituloLabel.text = self.textoTitulo
        self.descripcion.text = self.descrip

        self.tituloLabel.font = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (Constants.TamanioFuente.tamTituloIpad))

    }
}
