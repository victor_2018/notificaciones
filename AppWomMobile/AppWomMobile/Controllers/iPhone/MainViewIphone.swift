//
//  MainViewIphone.swift
//  AppWomMobile
//
//  Controlador donde se verifica el codigo enviado por sms.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Firebase
import AVFoundation

protocol ProtocoloMainViewIphone: class {
    func reintentarServicio()
}
class MainViewIphone: UIViewController {
    // Estado de aplicación que ya se revisó tutorial
    let estadoTutorialRevisado = 1
    // Tiempo de espera para que se muestre loading en segundos
    let tiempoEsperaLoading = 3

    @IBOutlet weak var imageCargando: UIImageView!
    @IBOutlet weak var logoWOM: UIImageView!

    override func viewDidLayoutSubviews() {
        UtilApp.agregarActivityIndicatorEspecifico(imageIndicador: imageCargando)
    }

    override func viewDidAppear(_ animated: Bool) {
        let videoURL: URL = URL(string: Constants.URL.videoInicial)!

        Globales.avPlayer = AVPlayer(url: videoURL)
        Util.determinarDispositivo(self.view.frame.height)
        cargarFotoPerfil()
        Globales.anchuraDispositivo = self.view.frame.size.width
        Globales.imagenDarkside.downloadedFrom(link: Constants.Imagenes.urlImagenDarkside)
        Globales.imagenLimbo.downloadedFrom(link: Constants.Imagenes.urlImagenLimbo)
        UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
            if retorno {
                self.verificarIngresoApp()
            }
        }
    }

    /// Verifica las properties para abrir la app normalmente o redireccionar al home
    func verificarIngresoApp() {
        if let validacionAvanceAppKeyValue = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.validacionAvance) {
            if validacionAvanceAppKeyValue as? Int == self.estadoTutorialRevisado {
                if let numeroTelefono = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.numeroTelefono) {
                    if let nombreCliente = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.nombreCliente) {
                        if let apellidoCliente = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.apellidoCliente) {
                            if let IMSI = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.IMSI) {
                                Globales.numeroTelefono = Util.decodificarStringCripto(numeroTelefono as! Data, llave: Constants.KeyPlist.numeroTelefono)

                                Globales.nombreCliente = nombreCliente as! String
                                Globales.apellidoCliente = apellidoCliente as! String
                                Globales.IMSI = Util.decodificarStringCripto(IMSI as! Data, llave: Constants.KeyPlist.IMSI)
                                self.verificarBam()
                            }
                        }
                    }
                }
            } else {
                let controladorTutorial = self.storyboard!.instantiateViewController(withIdentifier: "tutorialApp")
                self.present(controladorTutorial, animated: true, completion: nil)
            }
        }
    }

    /**
     Función que llama a los servicios principales
     */
    func verificarBam() {
        UtilServicios.obtenerTipoHome(Globales.numeroTelefono) { retorno in
            if retorno {
                DispatchQueue.main.async(execute: {

                    if Globales.homeSeleccionado == Constants.TipoHome.homeBam {
                        self.abrirHomeBam()
                    } else {
                        self.recuperarDatos()
                    }
                    Logger.log("finaliza servicio obtener home")
                })
            } else {
                self.mostrarErrorServicios()
                Logger.log("error servicio obtener home")
            }
        }
    }

    /**
     Función que llama a los servicios principales
     */
    func recuperarDatos() {
        UtilServicios.obtenerToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, numeroTelefono: Globales.numeroTelefono, password: Globales.IMSI, grantType: Constants.Login.grantType, refreshToken: "", realm: Constants.Login.realmServicios) { retorno in
            if retorno {
                self.llamarServicios()
            } else {
                DispatchQueue.main.async(execute: {
                    self.mostrarErrorServicios()
                    Logger.log("error al obtener token")
                })

            }
        }
    }

    /// Sigue el flujo normal y llama a servicios
    func llamarServicios() {
        UtilServicios.buscarDatosCliente { retorno in
            DispatchQueue.main.async(execute: {
                if retorno && Globales.datosCliente.nombrePlan != nil && Globales.datosCliente.idFormaPago != nil && Globales.datosCliente.tipoPlan != nil {
                    UtilApp.abrirHome(self)
                } else {
                    self.mostrarErrorServicios()
                }
            })
        }
    }
    /**
     Función que abre tabBarController tipo BAM.
     */
    func abrirHomeBam() {
        UtilApp.verificarDatosLogin()
        let controladorHomeBam = storyboard!.instantiateViewController(withIdentifier: "homeBam")
        present(controladorHomeBam, animated: true, completion: nil)
    }

    /**
     Despliega error que invalida ingreso a la aplicación.
     */
    func mostrarErrorServicios() {
        if let controladorError = storyboard!.instantiateViewController(withIdentifier: "errorServiciosPrincipales") as? ErrorServiciosPrincipales {
            controladorError.delegateLauncher = self
            present(controladorError, animated: true, completion: nil)
        }
    }

    /// Carga la imagen de perfil en caso de que se haya seleccionado una previamente
    func cargarFotoPerfil() {
        if let pathImagen = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.imagenPerfil) {
            if pathImagen as! String != "" {
                let rutaImagen = pathImagen as! String
                let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                // Get the Document directory path
                let documentDirectorPath: String = paths[0]
                // Create a new path for the new images folder
                let imagesDirectoryPath = documentDirectorPath + "/ImagenPerfil"
                let data = FileManager.default.contents(atPath: imagesDirectoryPath + rutaImagen)
                Globales.imagenPerfil = DesignHelper.corregirOrientacionImagen(UIImage(data: data!)!)
            }
        }
    }
}

extension MainViewIphone : ProtocoloMainViewIphone {
    /**
     Vuelve a llamar al servicio de compra bolsas.
     */
    func reintentarServicio() {
        llamarServicios()
    }
}

extension String {

    subscript (ind: Int) -> Character {
        return self[index(startIndex, offsetBy: ind)]
    }

    subscript (ind: Int) -> String {
        return String(self[ind] as Character)
    }

    subscript (range: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(startIndex, offsetBy: range.upperBound)
        return self[Range(start ..< end)]
    }
}
