//
//  HomePostpagoIphone.swift
//  AppWomMobile
//
//  Controlador de la vista principal de la aplicación.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Foundation

protocol ProtocoloHomePostpago: class {
    func actualizarAlturaEncabezado(indicadorSeccion: Int, animacionDoble: Bool)
}

class HomePostpagoIphone: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Enlace con tabla de bolsas en vista
    @IBOutlet weak var tablaBolsas: UITableView!
    // Vista loading
    @IBOutlet weak var viewActivityIndicator: UIView!
    // icono wom superior

    // arreglo de bolsas datos moviles
    var bolsasDatos = [BolsaTO]()
    // arreglo de bolsas datos moviles basal
    var bolsasDatosBasales = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales
    var bolsasDatosPromo = [BolsaTO]()
    // arreglo de bolsas datos moviles Agrupada
    var bolsasDatosAgrupada = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales Agrupada
    var bolsasDatosPromoAgrupada = [BolsaTO]()
    // arreglo de bolsas minutos
    var bolsasMinutos = [BolsaTO]()
    // arreglo de bolsas mensajes
    var bolsasMensajes = [BolsaTO]()
    // índice de fila seleccionada
    var indexSeleccionado = Int()
    // Secciones de la tabla
    var secciones = [String]()
    // indice de fila seleccionada
    var selectedIndexPath: IndexPath?
    // indicador de bolsas de datos basales vacia
    var bolsasDatosBasalesVacia = false
    // indicador de bolsas de datos vacia
    var bolsasDatosCompradasVacia = false
    // indicador de bolsas de datos vacia
    var bolsasDatosPromoVacia = false
    // indicador de bolsas de minutos vacia
    var bolsasMinutosVacia = false
    // indicador de bolsas de sms vacia
    var bolsasMensajesVacia = false
    // sección del encabezado
    let seccionEncabezado = 0
    // sección datos móviles
    var seccionDatosMoviles = Int()
    // sección datos móviles
    var seccionBasales = Int()
    // sección datos móviles
    var seccionBolsasCompradas = Int()
    // sección datos móviles
    var seccionBolsasPromo = Int()
    // sección voz
    var seccionMinutos = Int()
    // seccion basal minutos
    var seccionMinutosBasales = Int()
    // seccion basal sms
    var seccionSMSBasal = Int()
    // sección datos móviles
    var seccionSMS = Int()
    // en caso de que no existan bolsas en listados
    let seccionSinBolsas = 1
    // fila de informacion en seccion de datos
    let seccionInformacionDatos = 1
    // fila de la sección que va al final de cada sección
    let seccionPieSeccion = 1
    // indice con fila de informacion en seccion de datos moviles.
    var indiceInformacionDatos = Int()
    // indice con fila de informacion en seccion de datos moviles.
    var indiceMinutosConsumidos = Int()
    // indice con fila de informacion en seccion de datos moviles.
    var indiceSobreconsumo = Int()
    // indice con fila de informacion en seccion de datos moviles.
    var indiceInformacionMinutos = Int()
    // indicador de si el home es de tipo postpago libre
    let esPostpagoLibre = (Globales.homeSeleccionado == Constants.TipoHome.homePostpagoLibre)
    // indica si hay un error de servicio
    var errorServicio = false
    // numero de lineas para celda de informacion datos
    let numeroLineasDatos = 1
    // numero de lineas para celda de pie de pagina
    let numeroLineasPiePagina = 3
    // objeto con seccion de encabezado
    var viewSectionEncabezado = CeldaEncabezadoHomeControladoIphone()
    /// Variable con que se maneja refresco de vista
    var refreshControl: UIRefreshControl!
    // Indicador de tamaño del encabezado
    var indicadorSeccionesEncabezado = 4
    // cuenta las animaciones para restringir la primera, primero es -1 para la primera vez, con 1 se muestra animación y con 0 no
    var contadorAnimacionVistaConDeuda = -1

    override func viewDidLoad() {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refrescarHome), for: UIControlEvents.valueChanged)
        tablaBolsas.addSubview(refreshControl)
        self.tablaBolsas.separatorStyle = UITableViewCellSeparatorStyle.none
        UtilApp.determinarSeccionesPostpago(secciones: &secciones, seccionDatosMoviles: &seccionDatosMoviles, seccionBasales: &seccionBasales, seccionBolsasPromo: &seccionBolsasPromo, seccionBolsasCompradas: &seccionBolsasCompradas, seccionMinutos: &seccionMinutos, seccionMinutosBasales: &seccionMinutosBasales, seccionSMS: &seccionSMS, seccionSMSBasal: &seccionSMSBasal)
    }

    override func viewDidDisappear(_ animated: Bool) {
        contadorAnimacionVistaConDeuda = 1
    }

    /// Función que se llama despues de deslizar la pantalla en el home y que refresca vista
    ///
    /// - Parameter sender: objeto realiza acción
    func refrescarHome(sender: AnyObject) {
        viewSectionEncabezado.guardarEstadoVistas()
        contadorAnimacionVistaConDeuda = 1
        Util.agregarActivityIndicator(self.tabBarController!.view)
        UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
            Util.removerActivityIndicator(self.tabBarController!.view)
            if retorno {
                self.llamarServicios()
            }
        }
        refreshControl.endRefreshing()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tablaBolsas.dataSource = self
        self.tablaBolsas.delegate = self
        self.viewActivityIndicator.isHidden = true
        Globales.homeController = self
        tablaBolsas.setContentOffset(CGPoint.zero, animated: false)
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        if !Globales.modoMultilinea {
            Util.agregarActivityIndicator(self.tabBarController!.view)
            UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
                Util.removerActivityIndicator(self.tabBarController!.view)
                if retorno {
                    self.llamarServicios()
                }
            }
        }
    }

    /// Funcion que detecta scroll en tabla
    ///
    /// - Parameter scrollView: objeto de scroll en tabla
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DesignHelper.bloquearBounceInferior(scrollView: scrollView)
    }

    /**
     Hace llamada a servicios faltantes para cargar home
     */
    func llamarServicios() {
        var serviciosFinalizados = 0
        let serviciosTotales = 2
        var respuestaServicioPrincipal = false

        Util.agregarActivityIndicator(self.tabBarController!.view)
        var servicioPostpago = UtilServicios.consultaConsumo

        if esPostpagoLibre {
            servicioPostpago = UtilServicios.recuperarDatosLlamados
        }

        UtilServicios.obtenerFacturas() { retorno in
            serviciosFinalizados += 1
            if serviciosFinalizados == serviciosTotales {
                serviciosFinalizados = 0
                self.generarTabla(retorno: respuestaServicioPrincipal)
            }
        }

        servicioPostpago { retorno in
            serviciosFinalizados += 1
            respuestaServicioPrincipal = retorno
            if serviciosFinalizados == serviciosTotales {
                serviciosFinalizados = 0
                self.generarTabla(retorno: retorno)
            }
        }
    }

    /// Se genera la tabla del home postpago
    ///
    /// - Parameter retorno: respuesta de servicios
    func generarTabla(retorno: Bool) {
        if retorno {
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_ONLOAD", comment: " marca home onload "))

            // Se reinicializa tabla al volver desde otra vista
            self.errorServicio = false
            self.bolsasDatos.removeAll()
            self.bolsasDatosBasales.removeAll()
            self.bolsasDatosPromo.removeAll()
            self.bolsasDatosAgrupada.removeAll()
            self.bolsasDatosPromoAgrupada.removeAll()
            self.bolsasMinutos.removeAll()
            self.bolsasMensajes.removeAll()
            self.tablaBolsas.reloadData()

            self.generarArreglos()

            self.tablaBolsas.reloadData()
            self.tablaBolsas.separatorStyle = UITableViewCellSeparatorStyle.none

            if Globales.modoCampaignManager {
                UtilServiciosPromos.generarBadgePromos(self.tabBarController!)
                UtilServiciosPromos.desplegarHomeBanner(self, tabBarController: self.tabBarController!)
            }
        } else {
            if Globales.modoCampaignManager {
                self.tabBarController?.tabBar.items?[4].badgeValue = "0"
            }
            self.errorServicio = true
            UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("HOME_ONLOAD", comment: " marca home onload "))
            Util.alertErrorGeneral(self)
            self.tablaBolsas.reloadData()
        }

        Util.removerActivityIndicator(self.tabBarController!.view)
    }

/// Se generan los arreglso de las bolsas de la tabla
    func generarArreglos() {
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasDatosBasales, listadoBolsas: &self.bolsasDatosBasales, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasDatosAgrupada, listadoBolsas: &self.bolsasDatos, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasDatosPromoAgrupada, listadoBolsas: &self.bolsasDatosPromo, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)

        if !Globales.esBusiness {
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasDatosBasales, listadoBolsas: &self.bolsasDatosBasales, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMinutos, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMinutos, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)

            UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
        }

        indiceInformacionDatos = bolsasDatosPromoAgrupada.count
    }

    /**
     Función que determina el número de secciones
     - parameter tablaBolsas: tabla de bolsas en vista.
     - returns: número de celdas.
     */
    func numberOfSections(in tablaBolsas: UITableView) -> Int {
        return self.secciones.count
    }
    /**
     Función donde se setea el número de filas en la tabla
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     sección en tabla
     - returns: valor de filas que se mostrarán
     */
    func tableView(_ tablaBolsas: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case seccionBasales:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosBasales.count, indicadorBolsasVacias: &bolsasDatosBasalesVacia, esconderSinBolsas: true)
        case seccionBolsasCompradas:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosAgrupada.count, indicadorBolsasVacias: &bolsasDatosCompradasVacia, esconderSinBolsas: false) + seccionPieSeccion
        case seccionBolsasPromo:
            if self.bolsasDatosPromoAgrupada.count > 0 {
                bolsasDatosPromoVacia = false
                return self.bolsasDatosPromoAgrupada.count
            } else {
                bolsasDatosPromoVacia = true
                return 0
            }
        case seccionMinutosBasales:
            let cantidadFilasLibre = 3
            let cantidadFilasBusiness = 1
            if esPostpagoLibre {
                return cantidadFilasLibre
            } else if Globales.esBusiness {
                return cantidadFilasBusiness + seccionPieSeccion
            } else {
                return UtilBolsas.determinarNumeroFilas(self.bolsasMinutos.count, indicadorBolsasVacias: &bolsasMinutosVacia, esconderSinBolsas: false) + seccionPieSeccion
            }
        case seccionSMSBasal:
            return UtilBolsas.determinarNumeroFilas(self.bolsasMensajes.count, indicadorBolsasVacias: &bolsasMensajesVacia, esconderSinBolsas: false) + seccionPieSeccion
        default:
            Logger.log("default switch número de filas seccion: \(section)")
        }
        return 0
    }
    /// Función que realiza las acciones al desplegar los header.
    ///
    /// - parameter tableView: tabla de bolsas.
    /// - parameter view:      vista de header.
    /// - parameter section:   número de sección.
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == 0 {
            if Globales.datosCliente.esBigBoss {
                DesignHelper.realizarAnimacionNivelWomer(viewSectionEncabezado.viewNivelWomer)
            }
            DesignHelper.animacionPagarCuenta(contadorAnimacionVistaConDeuda: &contadorAnimacionVistaConDeuda, viewSectionEncabezado: viewSectionEncabezado)
        }
    }
    /**
     Función donde se construye las secciones de la tabla
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     índice de sección en tabla
     - returns: objeto de la sección generada
     */
    func tableView(_ tablaBolsas: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case seccionEncabezado:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaEncabezadoHomeControlado")! as! CeldaEncabezadoHomeControladoIphone
            viewSection.prepararVista()
            viewSection.delegate = self
            viewSectionEncabezado = viewSection
            return viewSection
        case seccionBolsasCompradas, seccionBolsasPromo, seccionBasales:
            let viewSection = UtilBolsas.obtenerCeldaSeccionInterna(tablaBolsas, seccion: section, seccionBasales: seccionBasales, seccionBolsasCompradas: seccionBolsasCompradas, seccionBolsasPromo: seccionBolsasPromo, tipoTrafico: Constants.Indicadores.tipoTraficoDatos)
            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        case seccionMinutosBasales:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSeccionBolsasInternaIphone")! as! CeldaSeccionBolsasInternaIphone
            viewSection.asignarDatos(Constants.Indicadores.tipoBolsaMinutosBasal)
            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        case seccionSMSBasal:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSeccionBolsasInternaIphone")! as! CeldaSeccionBolsasInternaIphone
            viewSection.asignarDatos(Constants.Indicadores.tipoBolsaSMS)
            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        default:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionTabla")! as! CeldaSeccionBolsasHomeIphone

            switch section {
            case seccionDatosMoviles:
                viewSection.asignarDatos(Constants.Indicadores.seccionDatosMoviles)
            case seccionMinutos:
                viewSection.asignarDatos(Constants.Indicadores.seccionMinutos)
            case seccionSMS:
                viewSection.asignarDatos(Constants.Indicadores.seccionSMS)
            default:
                viewSection.asignarDatos("")
            }

            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        }
    }
    /**
     Función que estructura las filas de historial de bolsas
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila
     - returns: celda seleccionada
     */
    func tableView(_ tablaBolsas: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case seccionBasales:
            return UtilHome.obtenerCeldaBolsaHome(indexPath, tablaBolsas: tablaBolsas, listadoBolsas: bolsasDatosBasales, seccion: Constants.Indicadores.seccionDatosMovilesBasales, labelSinBolsas: NSLocalizedString("label_sin_bolsas_datos", comment: "sin bolsas datos"))
        case seccionBolsasPromo, seccionBolsasCompradas:
            return UtilHome.obtenerCeldaDatos(indexPath, tablaBolsas: tablaBolsas, bolsasDatosPromoVacia: bolsasDatosPromoVacia, bolsasDatosCompradasVacia: bolsasDatosCompradasVacia, bolsasDatosBasalesVacia: bolsasDatosBasalesVacia, bolsasDatosPromoAgrupada: bolsasDatosPromoAgrupada, bolsasDatosBasales: bolsasDatosBasales, bolsasDatosAgrupada: bolsasDatosAgrupada, seccionBolsasPromo: seccionBolsasPromo, seccionBasales: seccionBasales, seccionBolsasCompradas: seccionBolsasCompradas)
        case seccionMinutosBasales:
            if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionMinutosBasales, indexPath: indexPath, bolsasAgrupada: bolsasMinutos) {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            } else if Globales.esBusiness {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaRedireccionGestor")! as! CeldaMinutosHomeBusinessIphone
                celda.selectionStyle = UITableViewCellSelectionStyle.none
                return celda
            } else {
                return UtilHome.obtenerCeldaVozPostpago(indexPath, esPostpagoLibre: esPostpagoLibre, tablaBolsas: tablaBolsas, bolsasMinutosVacia: bolsasMinutosVacia, bolsasMinutos: bolsasMinutos)
            }
        case seccionSMSBasal:
            if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionSMSBasal, indexPath: indexPath, bolsasAgrupada: bolsasMensajes) {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            } else if !bolsasMensajesVacia {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasHome", for: indexPath) as! CeldaBolsasHomeIphone
                celda.asignarDatos(bolsasMensajes[indexPath.row], seccion: Constants.Indicadores.seccionSMS, fila: indexPath.row)
                return celda
            } else {
                let celdaSinBolsas = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSinBolsas", for: indexPath) as! CeldaSinBolsas
                celdaSinBolsas.asignarDatos(NSLocalizedString("label_sin_bolsas_SMS", comment: "mensaje sin bolsas sms"))
                return celdaSinBolsas
            }
        default:
            Logger.log("Default switch cellForRowAtIndexPath")
        }
        return UITableViewCell()
    }
    /**
     Función que determina alto de secciones.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:   índice de secciones
     - returns: tamaño secciones
     */
    func tableView(_ tablaBolsas: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var heightEncabezado = CGFloat(390)
        let heightSecciones = CGFloat(70)
        let heightSeccionBasales = CGFloat(55)
        let heightSeccionInterna = CGFloat(80)
        var heightSeccionPagarCuentaAbierta = CGFloat(460)
        var heightSeccionAreaFacturacionAbierta = CGFloat(560)

        if !Globales.datosCliente.esBigBoss {
            heightEncabezado = heightEncabezado - CGFloat(50)
            heightSeccionPagarCuentaAbierta = heightSeccionPagarCuentaAbierta - CGFloat(50)
            heightSeccionAreaFacturacionAbierta = heightSeccionAreaFacturacionAbierta - CGFloat(50)
        }

        switch section {
        case seccionEncabezado:
            if indicadorSeccionesEncabezado == 1 {
                return heightSeccionPagarCuentaAbierta
            } else if indicadorSeccionesEncabezado == 2 {
                return heightSeccionAreaFacturacionAbierta
            } else if indicadorSeccionesEncabezado == 3 {
                return heightEncabezado + CGFloat(Globales.datosCliente.contratos.count * 165)
            } else {
                return heightEncabezado
            }
        case seccionBasales, seccionSMSBasal, seccionMinutosBasales:
            return heightSeccionBasales
        case seccionBolsasCompradas:
            return heightSeccionInterna
        case seccionBolsasPromo:
            if bolsasDatosPromo.count == 0 {
                return 0
            } else {
                return heightSeccionInterna
            }
        default:
            return heightSecciones
        }
    }
    /**
     Función que determina altura de celdas.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de celda
     - returns: tamaño celda
     */
    func tableView(_ tablaBolsas: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var heightSobreconsumo = CGFloat(30)
        var heightMinutosConsumidos = CGFloat(60)
        let alturaCeldaMinutosBusiness = CGFloat(100)
        let heightCompraBolsa = CGFloat(85)
        let alturaFilaPieSeccion = CGFloat(20)
        var listadoBolsasAgrupada = [BolsaTO]()

        if !Globales.showSobreconsumo {
             heightSobreconsumo = CGFloat(0)
             heightMinutosConsumidos = CGFloat(0)
        }

        let validacionPieSeccion = (UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasCompradas, indexPath: indexPath, bolsasAgrupada: bolsasDatosAgrupada) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionMinutosBasales, indexPath: indexPath, bolsasAgrupada: bolsasMinutos) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionSMSBasal, indexPath: indexPath, bolsasAgrupada: bolsasMensajes))

        listadoBolsasAgrupada = seleccionListadoBolsasAgrupadas(indexPath.section)

        if errorServicio || indexPath.section == seccionEncabezado {
            return 0
        } else if validacionPieSeccion {
            return alturaFilaPieSeccion
        } else if bolsasDatosCompradasVacia && indexPath.section == seccionBolsasCompradas {
            return heightCompraBolsa
        } else if indexPath.section == seccionMinutosBasales && Globales.esBusiness {
            return alturaCeldaMinutosBusiness
        } else if indexPath.section == seccionMinutosBasales && indexPath.row == 1 && esPostpagoLibre {
            return heightMinutosConsumidos
        } else if indexPath.section == seccionMinutosBasales && indexPath.row == 2 && esPostpagoLibre {
            return heightSobreconsumo
        } else if UtilBolsas.validarFilaAgrupadaEnLista(indexPath.section, fila: indexPath.row, listaBolsas: listadoBolsasAgrupada) {
            return CGFloat(85)
        } else if listadoBolsasAgrupada.count > 0 && listadoBolsasAgrupada[indexPath.row].colapsado {
            return CeldaBolsasHomeIphone.heightDefecto
        } else if listadoBolsasAgrupada.count > 0 && !listadoBolsasAgrupada[indexPath.row].colapsado {
            return CeldaBolsasHomeIphone.heightExpandida
        }

        return CeldaBolsasHomeIphone.heightDefecto
    }

    /// Busca el listado de bolsas agrupadas
    ///
    /// - Parameter seccion: seccion de la lista
    /// - Returns: listado de bolsa agrupada
    func seleccionListadoBolsasAgrupadas(_ seccion: Int) -> [BolsaTO] {
        var listadoBolsasAgrupada = [BolsaTO]()

        if seccion == seccionBolsasPromo {
            listadoBolsasAgrupada = bolsasDatosPromoAgrupada
        } else if seccion == seccionBasales {
            listadoBolsasAgrupada = bolsasDatosBasales
        } else if seccion == seccionBolsasCompradas {
            listadoBolsasAgrupada = bolsasDatosAgrupada
        } else if seccion == seccionMinutosBasales {
            listadoBolsasAgrupada = bolsasMinutos
        } else if seccion == seccionSMSBasal {
            listadoBolsasAgrupada = bolsasMensajes
        }

        return listadoBolsasAgrupada
    }
    /**
     Desplegar/plegar info bolsas.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila
     */
    func tableView(_ tablaBolsas: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == seccionBolsasPromo {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosPromoAgrupada, listadoBolsas: &bolsasDatosPromo, indexPath: indexPath)
        } else if indexPath.section == seccionBasales {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosBasales, listadoBolsas: &bolsasDatosBasales, indexPath: indexPath)
        } else if indexPath.section == seccionBolsasCompradas && !bolsasDatosCompradasVacia {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosAgrupada, listadoBolsas: &bolsasDatos, indexPath: indexPath)
        } else if indexPath.section == seccionMinutosBasales && !Globales.esBusiness {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMinutos, listadoBolsas: &bolsasMinutos, indexPath: indexPath)
        } else if indexPath.section == seccionSMSBasal {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMensajes, listadoBolsas: &bolsasMensajes, indexPath: indexPath)
        }
    }

    /**
     Función que se ejecuta antes de mostrar celda
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter cell: celda de bolsas en vista
     - parameter indexPath:   índice de celda
     */
    func tableView(_ tablaBolsas: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            if let celdaBolsas = cell as? CeldaBolsasHomeIphone {
                celdaBolsas.watchFrameChanges()
            }
        }
    }
    /**
     Función que se ejecuta despues de mostrar celda
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter cell: celda de bolsas en vista
     - parameter indexPath:   índice de celda
     */
    func tableView(_ tablaBolsas: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            if let celdaBolsas = cell as? CeldaBolsasHomeIphone {
                celdaBolsas.ignoreFrameChanges()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Botón de navegación para volver al Home
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
}

extension HomePostpagoIphone: ProtocoloHomePostpago {
    func actualizarAlturaEncabezado(indicadorSeccion: Int, animacionDoble: Bool) {
        indicadorSeccionesEncabezado = indicadorSeccion
        tablaBolsas.reloadSections([1], with: .none)
        if (indicadorSeccion == 1 && !animacionDoble) || indicadorSeccion == 2 || indicadorSeccion == 3 {
            tablaBolsas.moveSection(0, toSection: 0)
        }
    }
}
