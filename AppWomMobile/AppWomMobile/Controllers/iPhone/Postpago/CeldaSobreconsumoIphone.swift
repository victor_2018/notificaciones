//
//  CeldaSobreconsumoIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda que muestra el sobreconsumo de minutos.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaSobreconsumoIphone: UITableViewCell {
    // minutos que indican el sobre consumo
    @IBOutlet weak var minutosSobreconsumo: UILabel!

    /**
     Se asignan datos a la vista actual.
     - parameter sobreconsumo: cantidad de sobreconsumo.
     */
    func asignarDatos(_ sobreconsumo: String) {
        let minutosLbl = "min"
        minutosSobreconsumo.text = "\(sobreconsumo) \(minutosLbl)"
    }
}
