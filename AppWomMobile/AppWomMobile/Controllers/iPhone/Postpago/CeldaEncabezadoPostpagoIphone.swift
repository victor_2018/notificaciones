//
//  CeldaEncabezadoHomePostpagoIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda del encabezado de los homes hibridos.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel

class CeldaEncabezadoHomePostpagoIphone: UITableViewCell {
    // Enlace a nombre del cliente
    @IBOutlet weak var nombreCliente: UILabel!
    @IBOutlet weak var viewNombre: UIView!
    // Enlace a vista que indica que tiene deuda
    @IBOutlet weak var vistaDeuda: UIView!
    // Enlace a fecha de facturacion inicial
    @IBOutlet weak var fechaFacturacionInicio: UILabel!
    // Enlace a fecha de facturacion final
    @IBOutlet weak var fechaFacturacionFinal: UILabel!
    // Enlace a nombre de plan
    @IBOutlet weak var nombrePlan: UILabel!
    // Enlace a nombre de plan
    @IBOutlet weak var imagenWomer: UIImageView!
    // Enlace a label con nivel de womer
    @IBOutlet weak var nivelWomer: UILabel!
    // vista con fondo de nivel womer
    @IBOutlet weak var viewNivelWomer: UIImageView!
    @IBOutlet weak var numeroTelefono: UILabel!
    /**
     Función para asignar datos a encabezado
    */
    func prepararVista() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        let posicionEspacioVacio = 4
        let heightVistaNombreModificada = CGFloat(113)
        let heightNombreModificado = CGFloat(63)
        let heightNombrePlanModificado = CGFloat(38)
        let heightNumeroTelefonoModificado = CGFloat(63)

        nombrePlan.text = Globales.datosCliente.nombrePlan
        nombreCliente.text = Globales.nombreCliente
        fechaFacturacionInicio.text = Globales.datosCliente.inicioConsumo
        fechaFacturacionFinal.text = Globales.datosCliente.proximoConsumo
        imagenWomer.image = Globales.imagenPerfil
        imagenWomer = DesignHelper.redondearImagen(imagenWomer)
        nivelWomer.text = Globales.nivelWomer

        numeroTelefono.text = "+" + Globales.numeroTelefono
        numeroTelefono.text = UtilApp.insertarCaracterEnString(caracter: " ", texto: numeroTelefono.text!, posicion: posicionEspacioVacio)

        if Globales.nombreCliente.characters.count > UtilApp.obtenerLimiteCaracteresNombre() {
            viewNombre.frame.size.height = heightVistaNombreModificada
            nombreCliente.frame.size.height = heightNombreModificado
            nombrePlan.frame.size.height = heightNombrePlanModificado
            numeroTelefono.frame.size.height = heightNumeroTelefonoModificado
        }

        if Globales.datosCliente.tieneDeuda {
            vistaDeuda.layer.cornerRadius = radioBordeVista
        } else {
            vistaDeuda.frame.size.height = 0
            vistaDeuda.isHidden = true
        }

        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.abrirSeleccionImagen))
        imagenWomer.isUserInteractionEnabled = true
        imagenWomer.addGestureRecognizer(tapGestureRecognizer)
    }

    /// Abre vista para modificar imagen de perfil
    func abrirSeleccionImagen() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        Globales.homeController.navigationItem.backBarButtonItem = backItem

        UtilApp.generarMigaDePan(NSLocalizedString("HOME_MOD_INGRESO_FACIL", comment: "marca boton ingreso facil"))

        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controladorSeleccionImagen = storyboard.instantiateViewController(withIdentifier: "personalizaPerfilRRSS") as! PersonalizaPerfilHomeIphone

        Globales.homeController.navigationController!.pushViewController(controladorSeleccionImagen as UIViewController, animated: true)
    }
    /**
     Acción para redirigir a tab comprar bolsas
     - parameter sender: enlace a boton que abre tab comprar bolsas
     */
    @IBAction func abrirComprarBolsas(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_COMPRA_BOLSA", comment: "marca home boton comprar bolsa"))
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabComprarBolsas
    }
    /**
     Acción para redirigir a tab new womer
     - parameter sender: enlace a boton que abre tab new womer
     */
    @IBAction func abrirNewWomer(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_PERFIL_WOMER", comment: "marca home perfil womer"))
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabNewWomer
    }
    /// Acción para pagar cuentas
    ///
    /// - parameter sender: objeto de botón pagar cuentas
    @IBAction func pagarCuenta(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_PAGO", comment: "marca home boton pagar "))

        var urlPagarCuenta = URLServicios.pagarCuenta.replacingOccurrences(of: "{rut}", with: String(Globales.datosCliente.rut.characters.dropLast()))
        urlPagarCuenta = urlPagarCuenta.replacingOccurrences(of: "{dv}", with: String(Globales.datosCliente.rut.characters.last!))
        let url = URL(string: urlPagarCuenta)!
        UIApplication.shared.openURL(url)
    }

}
