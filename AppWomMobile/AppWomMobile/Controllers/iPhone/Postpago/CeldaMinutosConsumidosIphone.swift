//
//  CeldaMinutosConsumidosIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda que muestra los minutos consumidos entre las lineas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaMinutosConsumidosIphone: UITableViewCell {
    // Texto con información.
    @IBOutlet weak var labelMinutosConsumidos: UILabel!

    /**
     Se asignan datos a la vista actual.
     - parameter minutosConsumidos: numero con minutos consumidos.
     */
    func asignarDatos(_ minutosConsumidos: String) {
        labelMinutosConsumidos.text = "\(NSLocalizedString("minutos_consumidos", comment: "label de minutos consumidos")) \n\(minutosConsumidos) minutos"
    }
}
