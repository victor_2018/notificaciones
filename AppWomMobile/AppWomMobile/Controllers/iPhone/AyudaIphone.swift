//
//  AyudaIphone.swift
//  AppWomMobile
//
//  Controlador donde se muestra pagina externa de centro de ayuda.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class AyudaIphone: UIViewController, UIWebViewDelegate, UIScrollViewDelegate {
    // Enlace a webView donde se cargará página.
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        Util.agregarActivityIndicator(self.tabBarController!.view)

        let url = URL(string: URLServicios.centroDeAyuda)
        let requestObj = URLRequest(url: url!)

        webView.loadRequest(requestObj)
        webView.delegate = self
        webView.scrollView.bounces = false
    }
    /**
     Accion al presionar "X" para cerrar vista
     - parameter sender: boton que realiza acción.
     */
    @IBAction func cerrarVista(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    /**
     Función que realiza acciones al fallar la carga de la página.

     - parameter webView: webView donde se abre la página
     - parameter error: error específico
     */
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Util.removerActivityIndicator(self.tabBarController!.view)
        UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("HOME_MENU_AYUDA", comment: "miga de pan error ayuda"))
        Util.alertErrorGeneral(self)
    }
    /**
     Función que realiza acciones al comenzar la carga de la página.

     - parameter webView: webView donde se abre la página
     */
    func webViewDidStartLoad(_ webView: UIWebView) {
        Logger.log("Webview Cargando...")
    }
    /**
     Función que realiza acciones al terminar la carga de la página.

     - parameter webView: webView donde se abre la página
     */
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Logger.log("Webview terminó de cargar")
        Util.removerActivityIndicator(self.tabBarController!.view)
    }
}
