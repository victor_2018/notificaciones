//
//  PrototipoBolsa.swift
//  AppWomMobile
//
//  Controlador prototipo para bolsa en la seleccion de estas.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class PrototipoBolsa: UIView {
    // titulo de la bolsa
    var tituloBolsa: UILabel = UILabel()
    // vigencia de la bolsa
    var vigenciaBolsa: UILabel = UILabel()
    // valor total de la bolsa
    var valorBolsa: UILabel = UILabel()
    // imagen por defecto de la bolsa
    var imagenBolsa: UIImageView = UIImageView()
    // boton para comprar la bolsa
    var botonComprar: UIButton = UIButton()
    // imagen de fondo
    var imagenBackground: UIImageView = UIImageView()
    // indice de bolsa
    var indiceBolsa = Int()
    // factor dimensional para modificar
    var factorDimensional = CGFloat()
    // Indicador de bolsa tematica
    let tipoTematica = "TEMATICA"

    // delegate para acceder al view padre
    weak var delegate: ProtocoloSeleccionBolsaIphone?

    override init (frame: CGRect) {
        super.init(frame: frame)
        if Globales.isIphonePlus {
            factorDimensional = 0.5
        } else if Globales.isIphone6 {
            factorDimensional = 0.3
        } else {
            factorDimensional = 0
        }
        generarView()
    }

    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError(NSLocalizedString("msj_no_soporta_nscoding", comment: "mensaje que indica que no soporta NSCoding"))
    }
    /**
     Genera vista de bolsa
     */
    func generarView() {
        // margen top de imagen de fondo
        let margenImagenFondo = CGFloat(0)
        // margen top de boton
        let margenBoton = CGFloat(self.frame.height - (37 + 13 * factorDimensional))
        // margen desde la izquierda de boton
        let margenIzqBoton = CGFloat(12)
        // margen de la anchura del boton
        let margenAnchuraBoton = CGFloat(margenIzqBoton * 2)
        // altura del boton
        let alturaBoton = CGFloat(31)

        //Imagen background
        let imagenFondo = UIImage(named: Constants.Imagenes.imgBotonBgBolsa)
        imagenBackground.image = imagenFondo
        imagenBackground.frame = CGRect(x: 0, y: margenImagenFondo, width: self.frame.size.width, height: self.frame.size.height)

        // Boton para comprar bolsa
        botonComprar.setTitle(NSLocalizedString("lbl_compra_bolsa", comment: "label comprar"), for: UIControlState())
        botonComprar.frame = CGRect(x: margenIzqBoton, y: margenBoton, width: self.frame.size.width - margenAnchuraBoton, height: alturaBoton)
        botonComprar.titleLabel!.font = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (16))!
        botonComprar.setBackgroundImage(UIImage(named: Constants.Imagenes.imgBotonComprar), for: UIControlState())
        botonComprar.addTarget(self, action: #selector(PrototipoBolsa.comprarBolsa), for: .touchUpInside)

        asignarDatosLabels()

        self.addSubview(imagenBackground)
        self.addSubview(imagenBolsa)
        self.addSubview(tituloBolsa)
        self.addSubview(vigenciaBolsa)
        self.addSubview(valorBolsa)
        self.addSubview(botonComprar)
    }
    /**
     Funcion que arma los labels de cada bolsa.
     */
    func asignarDatosLabels() {
        // margen top de titulo
        let margenTituloBolsa = CGFloat(45 + 35 * factorDimensional)
        // margen top de vigencia de la bolsa
        let margenVigencia = CGFloat(76 + 51 * factorDimensional)
        // margen top de valor total
        let margenValorTotal = CGFloat(self.frame.height - (76 + 35 * factorDimensional))
        // tamaño del texto del titulo
        let tamanioTextoTitulo = 17 + 18 * factorDimensional
        // tamaño del texto de vigencia
        let tamanioTextoVigencia = 10 + 12 * factorDimensional
        // tamaño del texto del valor total
        let tamanioTextoValorTotal = 20 + 22 * factorDimensional
        // altura de textos más chicos
        let alturatextoTitulo = CGFloat(30 + 20 * factorDimensional)
        // altura texto vigencia
        let alturatextoVigencia = CGFloat(45 + 20 * factorDimensional)
        // altura de textos más grandes
        let alturatextosGrandes = CGFloat(25 + 25 * factorDimensional)

        //Titulo bolsa
        tituloBolsa.frame = CGRect(x: 5, y: margenTituloBolsa, width: self.frame.size.width - 10, height: alturatextoTitulo)
        tituloBolsa.textAlignment = NSTextAlignment.center
        tituloBolsa.font = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (tamanioTextoTitulo))
        tituloBolsa.textColor = DesignHelper.UIColorFromRGB(0x4A4A4A)
        tituloBolsa.adjustsFontSizeToFitWidth = true
        tituloBolsa.numberOfLines = 2

        // Vigencia de la bolsa
        vigenciaBolsa.frame = CGRect(x: 5, y: margenVigencia, width: self.frame.size.width - 10, height: alturatextoVigencia)
        vigenciaBolsa.textAlignment = NSTextAlignment.center
        vigenciaBolsa.font = UIFont (name: Constants.TipoFuente.ceraRegular, size: CGFloat (tamanioTextoVigencia))
        vigenciaBolsa.textColor = DesignHelper.UIColorFromRGB(0x4A4A4A)
        vigenciaBolsa.numberOfLines = 0
        vigenciaBolsa.adjustsFontSizeToFitWidth = true

        // Valor total de bolsa
        valorBolsa.frame = CGRect(x: 0, y: margenValorTotal, width: self.frame.size.width, height: alturatextosGrandes)
        valorBolsa.textAlignment = NSTextAlignment.center
        valorBolsa.font = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (tamanioTextoValorTotal))
        valorBolsa.textColor = DesignHelper.UIColorFromRGB(0x720184)

    }
    /**
     Funcion que redirige a la función del padre para comprar bolsa.
     */
    func comprarBolsa () {
        delegate?.abrirPopupComprarBolsa(indiceBolsa)
    }
    /**
     Funcion que arma el objeto de la bolsa.

     - parameter bolsa: objeto de bolsa.
     */
    func asignarDatos(_ bolsa: Bolsa) {
        // margen top de imagen de bolsa
        let margenImagenBolsa = CGFloat(13 + 13 * factorDimensional)
        // altura de la imagen
        let alturaImagen = CGFloat(27 + 27 * factorDimensional)
        var imagen = UIImage()

        self.tituloBolsa.text = bolsa.nombre
        self.vigenciaBolsa.text = bolsa.descripcion
        self.valorBolsa.text = bolsa.valorFormateado

        if bolsa.es4G {
            imagen = UIImage(named: Constants.Hashmap.tipoBolsaImagen[Constants.Indicadores.bolsa4g]!)!
        } else {
            imagen = UIImage(named: Constants.Hashmap.tipoBolsaImagen[bolsa.tipo]!)!
        }

        imagenBolsa.contentMode = .scaleAspectFit
        imagenBolsa.image = imagen
        imagenBolsa.frame = CGRect(x: 0, y: margenImagenBolsa, width: self.frame.size.width, height: alturaImagen)
    }
}
