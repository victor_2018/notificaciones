//
//  Modal_newomer.swift
//  AppWomMobile
//
//  Created by Juan Tamayo on 01-03-17.
//  Copyright © 2017 Tinet. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase

class PreNewWomerIphone: UIViewController {

    @IBOutlet weak var textoSaberMas: UILabel!
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var btnRecargar: UIButton!
    @IBOutlet weak var textoRecompensaWom: UILabel!
    @IBOutlet weak var textoSiNoRecargas: UILabel!

    var continuar = true

    override func viewDidAppear(_ animated: Bool) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_MENU_PERFIL", comment: "miga validar womer"))

        UtilApp.generarMigaDePan(NSLocalizedString("PERFIL_WOMER_ONLOAD", comment: "marca perfil womer onload"))
        if Globales.homeSeleccionado == Constants.TipoHome.homePrepago {
            Util.agregarActivityIndicator(self.tabBarController!.view)
            self.determinarUltimaRecarga()
        }
    }

    /// Obtiene la última recarga
    func determinarUltimaRecarga() {
        UtilServicios.getPeriodoRecarga { retorno in
            if retorno {
                let today = Date()
                let omeMonthEarly = today.addingTimeInterval(-30 * 24 * 60 * 60)

                switch Globales.diaRecarga.compare(omeMonthEarly) {
                case .orderedAscending     :
                    Logger.log("La fecha de regarga es anterior a 30 días, DESACTIVADO")
                    self.cambiarVistaNoActivado()
                case .orderedDescending    :
                    Logger.log("La fecha de regarga esta dentro de los 30 días, ACTIVADO")
                case .orderedSame          :
                    Logger.log("ACTIVADO")
                }
            } else {
                Logger.log("error en servicio getPeriodoRecarga, DESACTIVADO")
                self.cambiarVistaNoActivado()
            }
            Util.removerActivityIndicator(self.tabBarController!.view)
        }
    }

    /// Cambia a la vista
    func cambiarVistaNoActivado () {
        self.textoSiNoRecargas.isHidden = false
        self.btnRecargar.isHidden = false
        self.btnContinuar.isHidden = true
        self.textoRecompensaWom.isHidden = false
        self.textoSaberMas.isHidden = true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Botón de navegación para volver al Home
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }

    @IBAction func continuar(_ sender: UIButton) {
        UtilApp.generarMigaDePan(NSLocalizedString("PERFIL_WOMER_BTN_CONTINUAR", comment: "marca perfil womer boton continuar"))
    }

    @IBAction func abrirRecarga(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("PERFIL_WOMER_BTN_IR_RECARGAR", comment: "marca perfil womer boton ir a recargar"))
        UtilBolsas.abrirRecargaBrowser(saldoFaltante: "", numeroTelefono: Globales.numeroTelefono)
    }
}
