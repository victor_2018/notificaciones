//
//  ConfirmacionActivacionPromoIphone.swift
//  AppWomMobile
//
//  Controlador de la vista de popup con confirmacion de activacion promo.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class ConfirmacionActivacionPromoIphone: UIViewController {

    // delegate a vista padre
    weak var delegate: UIViewController?

    @IBOutlet weak var labelInformacion: UILabel!

    /// Tag identificador de este controlador en vista padre.
    var tagView = Int()

    override func viewDidLoad() {
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        asignarTextoInformativo()
        DesignHelper.mostrarAnimacionPopUp(self.view)
    }

    /// Asigna texto de información en la activación, según tipo de promo.
    func asignarTextoInformativo() {
        if (delegate as? PromoCompraBolsaIphone) != nil {
            labelInformacion.text = NSLocalizedString("lbl_activacion_promo_bolsas", comment: "texto informativo para promo compra bolsas")
            labelInformacion.sizeToFit()
        }
    }

    @IBAction func cerrarVista(_ sender: AnyObject) {
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // escala inicial de la vista
        let escalaInicialVista = CGFloat(1.3)

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.delegate?.view.viewWithTag(self.tagView)?.transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
            self.delegate?.view.viewWithTag(self.tagView)?.alpha = 0.0
            }, completion: {(finished: Bool)  in
                if finished {
                    self.delegate?.navigationController!.popViewController(animated: true)
                    self.delegate!.navigationItem.setHidesBackButton(false, animated:true)
                }
        })
    }

}
