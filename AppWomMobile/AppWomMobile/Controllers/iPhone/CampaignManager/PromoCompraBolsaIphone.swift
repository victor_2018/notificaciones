//
//  PromoCompraBolsaIphone.swift
//  AppWomMobile
//
//  Controlador de la vista de promociones del tipo compra bolsa.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

protocol ProtocoloPromoCompraBolsaIphone: class {
    func cerrarTerminosCondiciones()
}

class PromoCompraBolsaIphone: UIViewController, UIGestureRecognizerDelegate {

    /// Constraint con altura de vista contenedora
    @IBOutlet weak var alturaViewContenedora: NSLayoutConstraint!
    /// Contenedor Scrollview de la vista
    @IBOutlet weak var scrollview: UIScrollView!
    /// View contenedor de la vista
    @IBOutlet weak var viewContenedora: UIView!
    /// Imagen ubicada en la cabecera de la promoción
    @IBOutlet weak var imagen: UIImageView!
    /// Título de la promoción
    @IBOutlet weak var tituloPromo: UILabel!
    /// Descripción de la promoción
    @IBOutlet weak var descripcionPromo: UILabel!
    /// View que contiene ticket y términos y condiciones
    @IBOutlet weak var viewTerminosCondiciones: UIView!
    /// Imagen de ticket en terminos y condiciones
    @IBOutlet weak var imagenTicket: UIImageView!
    /// Vista que contiene la descripcion y la vista de terminos y condiciones
    @IBOutlet weak var viewContenedoraDescTerm: UIView!
    /// Label de términos y condiciones
    @IBOutlet weak var textoTerminosCondiciones: UILabel!
    /// Label donde se indica que se deben aceptar términos y condiciones
    @IBOutlet weak var textoError: UILabel!

    /// Objeto con datos de promoción seleccionada
    var promoInfo = PromocionesTO()
    /// Booleano que indica si el ticket esta seleccionado o no
    var indicadorTicket = false
    /// Controlador de la vista de términos y condiciones
    var popupTerminosCondiciones = TerminosCondicionesPromosIphone()
    /// Controlador de la vista de activacion de promo
    var popupActivacionPromo = ConfirmacionActivacionPromoIphone()
    /// Datos de la bolsa que se va a comprar
    var datosBolsaPorComprar = CatalogoBolsaTO()
    /// Controlador de la vista de error general
    var popupErrorGeneral = ErrorGeneral()


    override func viewDidLoad() {
        let alturaAdicional = CGFloat(20)

        Util.agregarActivityIndicator(self.tabBarController!.view)

        descripcionPromo.text = promoInfo.descripcion1
        descripcionPromo.sizeToFit()
        tituloPromo.text = promoInfo.nombre
        viewContenedoraDescTerm.frame.size.height = descripcionPromo.frame.size.height + viewTerminosCondiciones.frame.size.height + alturaAdicional

        if promoInfo.condicionesPromocion.isEmpty {
            imagenTicket.isHidden = true
            textoTerminosCondiciones.isHidden = true
        } else {
            // Se agrega la acción a la imágen del ticket de terminos y condiciones para que cambie.
            let tapGestureRecognizerTicket = UITapGestureRecognizer(target:self, action:#selector(self.seleccionarTicket))
            imagenTicket.isUserInteractionEnabled = true
            imagenTicket.addGestureRecognizer(tapGestureRecognizerTicket)

            // se agrega subrayado a "Términos y condiciones".
            let text = (textoTerminosCondiciones.text)!
            let underlineAttriString = NSMutableAttributedString(string: text)
            let range1 = (text as NSString).range(of: NSLocalizedString("lbl_terminos_condiciones_link", comment: "texto de terminos y condiciones clickeable"))
            underlineAttriString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
            textoTerminosCondiciones.attributedText = underlineAttriString

            // Se añade la función al texto "Términos y condiciones" para que abra la vista de estos.
            let tapGestureRecognizerTerminosCondiciones = UITapGestureRecognizer(target:self, action:#selector(self.tapLabelTerminosCondiciones))
            textoTerminosCondiciones.isUserInteractionEnabled = true
            textoTerminosCondiciones.addGestureRecognizer(tapGestureRecognizerTerminosCondiciones)
        }
    }

    override func viewDidAppear(_ animated: Bool) {

        if !promoInfo.urlImagenDetalle.isEmpty {
            let imagenPromo = CIImage(contentsOf: URL(string: promoInfo.urlImagenDetalle)!)

            if imagenPromo != nil {
                imagen.image = UIImage(ciImage: imagenPromo!)
            } else {
                Logger.log("Hubo un problema al cargar imagen")
            }
        }
        Util.removerActivityIndicator(self.tabBarController!.view)
    }

    override func viewDidLayoutSubviews() {
        self.viewContenedora.sizeToFitCustom()
        alturaViewContenedora.constant = self.viewContenedora.frame.size.height

        scrollview.translatesAutoresizingMaskIntoConstraints = true
        scrollview.contentSize = CGSize(width: scrollview.contentSize.width, height: alturaViewContenedora.constant)
    }

    /// Función donde se intercala la selección del ticket de términos y condiciones
    func seleccionarTicket() {
        if indicadorTicket {
            imagenTicket.image = UIImage(named: Constants.Imagenes.imgTicketDeseleccionado)
            indicadorTicket = false
        } else {
            imagenTicket.image = UIImage(named: Constants.Imagenes.imgTicketSeleccionado)
            indicadorTicket = true
            textoError.isHidden = true
        }
    }

    /// Valida que los términos y condiciones esten aceptados
    ///
    /// - Returns: estado en que se encuentran aceptados los terminos
    func validarTerminosCondiciones() -> Bool {
        if indicadorTicket {
            return true
        } else {
            textoError.isHidden = false
            return false
        }
    }


    /// Función donde se valida que se seleccione solo el texto "Términos y condiciones" para abrir controlador
    ///
    /// - Parameter gesture: objeto con datos del reconocedor de gestos
    @IBAction func tapLabelTerminosCondiciones(_ gesture: UITapGestureRecognizer) {
        let text = (textoTerminosCondiciones.text)!
        let termsRange = (text as NSString).range(of: NSLocalizedString("lbl_terminos_condiciones_link", comment: "texto de terminos y condiciones clickeable"))

        if gesture.didTapAttributedTextInLabel(textoTerminosCondiciones, inRange: termsRange) {
            let storyboard = UIStoryboard(name: "Tab5_iphone", bundle: nil)
            popupTerminosCondiciones = storyboard.instantiateViewController(withIdentifier: "terminosCondicionesPromosIphone") as! TerminosCondicionesPromosIphone

            popupTerminosCondiciones.delegate = self
            popupTerminosCondiciones.textoTerminosCondiciones = promoInfo.condicionesPromocion
            popupTerminosCondiciones.idPromo = promoInfo.idPromo

            self.addChildViewController(popupTerminosCondiciones)
            popupTerminosCondiciones.view.frame = self.view.frame
            self.view.addSubview(popupTerminosCondiciones.view)
            popupTerminosCondiciones.didMove(toParentViewController: self)

            self.navigationItem.setHidesBackButton(true, animated: true)
        }
    }

    @IBAction func contratarBolsa(_ sender: AnyObject) {
        if validarTerminosCondiciones() {
            Util.agregarActivityIndicator(self.tabBarController!.view)
            self.cargarCatalogoBolsas() { promo in
                if promo != nil {
                    self.datosBolsaPorComprar = promo!
                    self.comprarBolsa()
                } else {
                    Util.alertErrorGeneral(self)
                    UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("PROMO_COMPRA_BOLSA_ONLOAD", comment: "miga de pan promo compra bolsa"))
                    Util.removerActivityIndicator(self.tabBarController!.view)
                    Logger.log("Bolsa promocional no encontrada en catalogo")
                }
            }
        }
    }

    @IBAction func rechazarPromo(_ sender: AnyObject) {
        UtilServiciosPromos.actualizarFecha(promoInfo.idPromo)
        self.navigationController?.popViewController(animated: true)
    }

    /// Funcion donde obtiene el catalogo de bolsas.
    ///
    /// - parameter onCompletion: retorna indicador de si servicio fue llamado satisfactoriamente o no
    func cargarCatalogoBolsas(_ onCompletion: @escaping (_ promo: CatalogoBolsaTO?) -> ()) {
        var arregloBolsasCatalogo = [String: CatalogoBolsaTO]()
        GetNextelProductCatalog.sharedInstance.getBolsasDisponibles(Constants.Indicadores.clientName,
                                                                    onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {

                                                                        for bolsa in json["listProduct"]["itm_product"].array! {
                                                                            arregloBolsasCatalogo[bolsa["id_prod"].stringValue] = CatalogoBolsaTO(json: bolsa)
                                                                        }

                                                                        DispatchQueue.main.async(execute: {
                                                                            Logger.log("finaliza servicio catalogo")
                                                                            onCompletion(arregloBolsasCatalogo[self.promoInfo.idProd])
                                                                        })
                                                                    } else {
                                                                        DispatchQueue.main.async(execute: {
                                                                            Logger.log("error al cargar catalogo")
                                                                            Util.removerActivityIndicator(self.tabBarController!.view)
                                                                            onCompletion(nil)
                                                                        })
                                                                        }
        })
    }

    /**
     Se arman parametros para enviar a llamada de servicio Post comprar bolsa.

     - returns: cuerpo de json para enviar al servicio
     */
    func armarBodyJsonCompraBolsas() -> [String: AnyObject] {
        var bodyJson = [String: AnyObject]()
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let fechaFormateada = dateFormatter.string(from: date)

        bodyJson["client_name"] = Constants.Indicadores.clientName as AnyObject
        bodyJson["phone_number"] = Globales.numeroTelefono as AnyObject
        bodyJson["customer_id"] = "" as AnyObject
        bodyJson["client_type"] = Constants.Indicadores.clientType as AnyObject
        bodyJson["segment_prod"] = Globales.datosCliente.tipoPlan as AnyObject
        bodyJson["rate_plan"] = Globales.datosCliente.ratePlan as AnyObject
        bodyJson["category_prod"] = Constants.Indicadores.idProd as AnyObject
        bodyJson["id_prod"] = datosBolsaPorComprar.idProducto as AnyObject
        bodyJson["id_pcrf_prod"] = datosBolsaPorComprar.idPCRF as AnyObject
        bodyJson["id_bscs_prod"] = datosBolsaPorComprar.idBSCS as AnyObject
        bodyJson["date"] = fechaFormateada as AnyObject

        if Globales.datosCliente.idFormaPago == Constants.Indicadores.idFormaPagoSaldo {
            bodyJson["metodo_pago"] = Constants.Indicadores.metodoPagoPrepagoControlado as AnyObject
        } else {
            bodyJson["metodo_pago"] = Constants.Indicadores.metodoPagoPostpago as AnyObject
        }

        bodyJson["pago_aplicado"] = Constants.Indicadores.pagoAplicado as AnyObject
        bodyJson["transaction_code"] = "" as AnyObject

        return bodyJson
    }

    /**
     Función que realiza la llamada al servicio para comprar bolsas
     */
    func comprarBolsa() {
        var bodyJson = [String: AnyObject]()
        bodyJson = armarBodyJsonCompraBolsas()

        SetNextelActiveProduct.sharedInstance.comprarBolsa(bodyJson, onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
            let respuestaCompraBolsas = CompraBolsasTO(json: json)
            let codigoValido = "0"

            if respuestaCompraBolsas.codigoError == codigoValido && !respuestaCompraBolsas.codigoTransaccion.isEmpty {

                DispatchQueue.main.async(execute: {
                    UtilServiciosPromos.activarPromo(promocion: self.promoInfo) { retorno in
                        Util.removerActivityIndicator(self.tabBarController!.view)
                        if retorno {
                            Globales.confirmacionPromocion = true
                            UtilServiciosPromos.eliminarPromo(self.promoInfo.idPromo)
                            self.abrirPopupConfirmacionActivacion()
                        } else {
                            UtilApp.abrirPopupErrorServicio(textoError: NSLocalizedString("lbl_error_activacion_promo", comment: "mensaje de error al fallar servicio activacion promocion"), controller: self)
                        }
                    }
                })

            } else {
                DispatchQueue.main.async(execute: {
                    Util.removerActivityIndicator(self.tabBarController!.view)
                    UtilApp.abrirPopupErrorServicio(textoError: NSLocalizedString("lbl_error_servicio_compra_bolsa", comment: "mensaje de error al fallar servicio compra bolsa"), controller: self)
                })
            }
        } else {
            DispatchQueue.main.async(execute: {
                Util.removerActivityIndicator(self.tabBarController!.view)
                UtilApp.abrirPopupErrorServicio(textoError: NSLocalizedString("lbl_error_servicio_compra_bolsa", comment: "mensaje de error al fallar servicio compra bolsa"), controller: self)
                Logger.log("ERROR al comprar bolsa")
            })
            }
        })
    }

    /// Abre la vista de la confirmación de la activación de la promoción
    func abrirPopupConfirmacionActivacion() {
        // identificador de la vista de la confirmación
        let tagVista = 1
        let storyboard = UIStoryboard(name: "Tab5_iphone", bundle: nil)
        popupActivacionPromo = storyboard.instantiateViewController(withIdentifier: "confirmacionActivacionPromoIphone") as! ConfirmacionActivacionPromoIphone

        popupActivacionPromo.delegate = self

        popupActivacionPromo.view.tag = tagVista
        popupActivacionPromo.tagView = tagVista

        self.addChildViewController(popupActivacionPromo)
        popupActivacionPromo.view.frame = self.view.frame
        self.view.addSubview(popupActivacionPromo.view)
        popupActivacionPromo.didMove(toParentViewController: self)

        self.navigationItem.setHidesBackButton(true, animated: true)
    }
}

extension PromoCompraBolsaIphone : ProtocoloPromoCompraBolsaIphone {

    /// Función que cierra lal vista de terminos y condiciones.
    func cerrarTerminosCondiciones() {
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // escala inicial de la vista
        let escalaInicialVista = CGFloat(1.3)

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.popupTerminosCondiciones.view.transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
            self.popupTerminosCondiciones.view.alpha = 0.0
            }, completion: {(finished: Bool)  in
                if finished {
                    self.popupTerminosCondiciones.view.removeFromSuperview()
                    self.navigationItem.setHidesBackButton(false, animated:true)
                }
        })
    }
}
