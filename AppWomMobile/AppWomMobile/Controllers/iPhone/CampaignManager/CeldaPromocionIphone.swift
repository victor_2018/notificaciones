//
//  CeldaPromocionIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda de promociones.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaPromocionIphone: UITableViewCell {
    // imagen de la promocion
    @IBOutlet weak var imagen: UIImageView!
    // texto inferior en la celda de la promocion
    @IBOutlet weak var descripcion: UILabel!
}
