//
//  HomeBannerIphone.swift
//  AppWomMobile
//
//  Controlador de la vista de popup con home banner.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class HomeBannerIphone: UIViewController {

    /// Enlace a imagen de banner
    @IBOutlet weak var imagenBanner: UIImageView!
    /// Enlace a boton con cantidad de notificaciones
    @IBOutlet weak var cantidadNotificaciones: UIButton!
    /// Enlace a botón para ver todas las promociones
    @IBOutlet weak var btnVerPromociones: UIButton!

    // delegate a vista padre
    weak var delegate: UIViewController?

    /// Tag identificador de este controlador en vista padre.
    var tagView = Int()
    /// Indicador de si se debe mostrar home banner o no
    var mostrarBanner = false

    override func viewDidLayoutSubviews() {
        if !mostrarBanner {
            self.delegate?.tabBarController!.view.viewWithTag(Constants.Tags.homeBanner)!.removeFromSuperview()
        }
    }

    override func viewDidLoad() {
        if validarTiempoBanner() {
            mostrarBanner = true
            // transparencia de fondo
            let transparenciaVista = CGFloat(0.5)

            btnVerPromociones.titleLabel!.adjustsFontSizeToFitWidth = true
            btnVerPromociones.titleLabel!.minimumScaleFactor = 0.5

            let tapGestureRecognizerImagen = UITapGestureRecognizer(target:self, action:#selector(self.verDetalle))
            imagenBanner.isUserInteractionEnabled = true
            imagenBanner.addGestureRecognizer(tapGestureRecognizerImagen)

            imagenBanner.image = Globales.imagenPromoPrincipal
            cantidadNotificaciones.setTitle(String(Globales.promociones.count), for: UIControlState())
            self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
            DesignHelper.mostrarAnimacionPopUp(self.view)

            UtilServiciosPromos.actualizarFecha(Globales.promociones[0].idPromo)
        } else {
            mostrarBanner = false
        }
    }

    /// Se valida que el tiempo del home banner sea valido para ser mostrado
    ///
    /// - Returns: indicador de si se debe mostrar o no
    func validarTiempoBanner() -> Bool {

        if let listaPromociones = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.listadoPromociones) {
            var promociones = listaPromociones as! Array<String>
            if promociones.count > 0 {
                var newDateComponents = DateComponents()
                newDateComponents.minute = Constants.Indicadores.margenTiempoBanner
                let fechaActual = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: Date(), options: NSCalendar.Options.init(rawValue: 0))

                let fechaPromo = Util.transformarFechaString(promociones[0].components(separatedBy: ",")[1], formato: "yyyy-MM-dd'T'HH:mm:ss")

                let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.minute, from: fechaPromo, to: fechaActual!, options: NSCalendar.Options.init(rawValue: 0))

                Logger.log("fecha Promo: \(fechaPromo) fecha Actual: \(String(describing: fechaActual)) min dif: \(String(describing: components.minute))")

                if components.minute! > 0 {
                    Logger.log("Mostrando banner")
                    return true
                } else {
                    Logger.log("No es tiempo aún para mostrar banner")
                    return false
                }
            } else {
                inicializarPromociones()
            }
        } else {
            inicializarPromociones()
        }
        return true
    }

    /// Inicializa el valor de la promo en el arreglo de propiedades
    func inicializarPromociones() {
        var promociones = Array<String>()

        if Globales.promociones.count > 0 {
            promociones.append("\(Globales.promociones[0].idPromo),\(Util.transformarFechaNSDate(Date(), formato: "yyyy-MM-dd'T'hh:mm:ss")),N")
        }

        PlistManager.sharedInstance.addNewItemWithKey(Constants.KeyPlist.listadoPromociones, value: promociones as AnyObject)
    }


    @IBAction func cerrarVista(_ sender: AnyObject) {
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // escala inicial de la vista
        let escalaInicialVista = CGFloat(1.3)

        UtilServiciosPromos.actualizarFecha(Globales.promociones[0].idPromo)

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.delegate?.tabBarController!.view.viewWithTag(Constants.Tags.homeBanner)?.transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
            self.delegate?.tabBarController!.view.viewWithTag(Constants.Tags.homeBanner)?.alpha = 0.0
            }, completion: {(finished: Bool)  in
                if finished {
                    self.delegate?.tabBarController!.view.viewWithTag(Constants.Tags.homeBanner)!.removeFromSuperview()
                }
        })
    }

    @IBAction func verDetalle(_ sender: AnyObject) {
        let promoController: UIViewController

        cerrarVista(" " as AnyObject)
        let storyboard = UIStoryboard(name: "Tab5_iphone", bundle: nil)
        if Globales.promociones[0].tipoPromo == Constants.Indicadores.tipoPromoCompraBolsa {
            promoController = storyboard.instantiateViewController(withIdentifier: "promoCompraBolsaIphone") as! PromoCompraBolsaIphone
            (promoController as! PromoCompraBolsaIphone).promoInfo = Globales.promociones[0]

        } else {
            promoController = storyboard.instantiateViewController(withIdentifier: "promoInformativaIphone") as! PromoInformativaIphone
            (promoController as! PromoInformativaIphone).promoInfo = Globales.promociones[0]
        }

        // Botón de navegación para volver a las bolsas
        let backItem = UIBarButtonItem()
        backItem.title = ""

        self.delegate?.navigationItem.backBarButtonItem = backItem
        self.delegate?.navigationController!.pushViewController(promoController as UIViewController, animated: true)
    }

    @IBAction func verTodasMisPromociones(_ sender: AnyObject) {
        cerrarVista(" " as AnyObject)
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabPromociones
    }

}
