//
//  PromoInformativaIphone.swift
//  AppWomMobile
//
//  Controlador de la vista de promociones del tipo informativa.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class PromoInformativaIphone: UIViewController {

    @IBOutlet weak var alturaViewContenedora: NSLayoutConstraint!
    @IBOutlet weak var viewDescripcion: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var tituloPromo: UILabel!
    @IBOutlet weak var descripcionPromo: UILabel!
    @IBOutlet weak var viewContenedora: UIView!

    /// Objeto con datos de promoción seleccionada
    var promoInfo = PromocionesTO()
    /// Controlador de la vista de activacion de promo
    var popupActivacionPromo = ConfirmacionActivacionPromoIphone()

    override func viewDidLoad() {
        let alturaAdicional = CGFloat(65)

        Util.agregarActivityIndicator(self.tabBarController!.view)

        descripcionPromo.text = promoInfo.descripcion1

        descripcionPromo.sizeToFit()
        tituloPromo.text = promoInfo.nombre
        viewDescripcion.frame.size.height = descripcionPromo.frame.size.height + alturaAdicional

    }

    override func viewDidAppear(_ animated: Bool) {

        if !promoInfo.urlImagenDetalle.isEmpty {
            let imagenPromo = CIImage(contentsOf: URL(string: promoInfo.urlImagenDetalle)!)
            if imagenPromo != nil {
                imagen.image = UIImage(ciImage: imagenPromo!)
            } else {
                Logger.log("Hubo un problema al cargar imagen")
            }
        }
        Util.removerActivityIndicator(self.tabBarController!.view)
    }

    override func viewDidLayoutSubviews() {
        self.viewContenedora.sizeToFitCustom()
        alturaViewContenedora.constant = self.viewContenedora.frame.size.height

        scrollview.translatesAutoresizingMaskIntoConstraints = true
        scrollview.contentSize = CGSize(width: scrollview.contentSize.width, height: alturaViewContenedora.constant)
    }

    @IBAction func verDetalle(_ sender: AnyObject) {
        if promoInfo.descripcion2 != "" {
            let url = URL(string: promoInfo.descripcion2)!
            UIApplication.shared.openURL(url)
            self.activarPromo()
        } else {
            UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("PROMO_INFORMATIVA_ONLOAD", comment: "miga de pan promo informativa"))
            Util.alertErrorGeneral(self)
        }
    }

    @IBAction func rechazarPromo(_ sender: AnyObject) {
        UtilServiciosPromos.actualizarFecha(promoInfo.idPromo)
        self.navigationController?.popViewController(animated: true)
    }

    /**
     Función que realiza la llamada al servicio para comprar bolsas
     */
    func activarPromo() {

        UtilServiciosPromos.activarPromo(promocion: promoInfo) { retorno in
            Util.removerActivityIndicator(self.tabBarController!.view)
            Globales.confirmacionPromocion = true
            UtilServiciosPromos.eliminarPromo(self.promoInfo.idPromo)
            self.navigationController?.popViewController(animated: true)
        }
    }

}
