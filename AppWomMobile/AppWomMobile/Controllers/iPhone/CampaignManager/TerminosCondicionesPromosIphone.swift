//
//  TerminosCondicionesPromosIphone.swift
//  AppWomMobile
//
//  Controlador de la vista de los términos y condiciones de las promociones.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class TerminosCondicionesPromosIphone: UIViewController {

    // delegate a vista padre
    weak var delegate: ProtocoloPromoCompraBolsaIphone?

    var textoTerminosCondiciones = String()
    var idPromo = String()

    @IBOutlet weak var txtViewTerminosCondiciones: UITextView!

    override func viewDidLoad() {
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        if idPromo == "201"{
            txtViewTerminosCondiciones.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        } else {
            txtViewTerminosCondiciones.text = textoTerminosCondiciones
        }

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        DesignHelper.mostrarAnimacionPopUp(self.view)
    }

    @IBAction func cerrarVista(_ sender: AnyObject) {
        delegate!.cerrarTerminosCondiciones()
    }
}
