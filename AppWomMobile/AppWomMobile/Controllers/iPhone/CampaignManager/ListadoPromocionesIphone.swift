//
//  ListadoPromocionesIphone.swift
//  AppWomMobile
//
//  Controlador de la vista del listado de promociones.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit

class ListadoPromocionesIphone: UIViewController, UITableViewDataSource, UITableViewDelegate {

    /// Tableview de la tabla de promociones
    @IBOutlet weak var tablaPromociones: UITableView!

    override func viewDidAppear(_ animated: Bool) {
        if Globales.confirmacionPromocion {
            Globales.confirmacionPromocion = false
            Util.agregarActivityIndicator(self.tabBarController!.view)

            UtilServicios.obtenerPromociones() { retorno in
                Util.removerActivityIndicator(self.tabBarController!.view)
                if retorno {
                    self.tablaPromociones.reloadData()
                    self.tabBarController?.tabBar.items?[4].badgeValue = String(Globales.promociones.count)
                } else {
                    Util.alertErrorGeneral(self)
                    UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("LISTADO_PROMOCIONES_ONLOAD", comment: "miga de pan listado promociones"))
                }
            }
        }
    }

    /// Se retorna número de secciones en la tabla
    ///
    /// - Parameter tableView: tabla de promociones
    /// - Returns: cantidad de secciones
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    /// Se retorna número de filas por sección en la tabla
    ///
    /// - Parameters:
    ///   - tableView: tabla de promociones
    ///   - section: sección que se está verificando
    /// - Returns: cantidad de filas
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let filaSinPromociones = 1
        if Globales.promociones.count > 0 {
            return Globales.promociones.count
        } else {
            return filaSinPromociones
        }
    }

    /// Se genera la vista para el encabezado de la tabla de promociones
    ///
    /// - Parameters:
    ///   - tableView: tabla de promociones
    ///   - section: sección que se verifica
    /// - Returns: vista de la sección
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewSection = tablaPromociones.dequeueReusableCell(withIdentifier: "celdaCabeceraPromociones")! as! CeldaEncabezadoPromocionesIphone
        return viewSection
    }

    /// Se genera la vista para las promociones
    ///
    /// - Parameters:
    ///   - tableView: tabla de promociones
    ///   - indexPath: objeto de indice con la fila
    /// - Returns: celda
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if Globales.promociones.count > 0 {
            let celda = tablaPromociones.dequeueReusableCell(withIdentifier: "celdaPromocionIphone", for: indexPath) as! CeldaPromocionIphone

            celda.descripcion.text = Globales.promociones[indexPath.row].nombre
            celda.imagen.image = Globales.listadoImagenesPromos[Globales.promociones[indexPath.row].idPromo]

            return celda
        } else {
            let celda = tablaPromociones.dequeueReusableCell(withIdentifier: "celdaSinPromosIphone", for: indexPath) as! CeldaSinPromosIphone
            return celda
        }
    }

    /// Acciones que se realizan al seleccionar una fila
    ///
    /// - Parameters:
    ///   - tableView: tabla de promociones
    ///   - indexPath: objeto de indice con la fila
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promoController: UIViewController
        let storyboard = UIStoryboard(name: "Tab5_iphone", bundle: nil)
        if Globales.promociones.count > 0 {
            if Globales.promociones[indexPath.row].tipoPromo == Constants.Indicadores.tipoPromoCompraBolsa {
                promoController = storyboard.instantiateViewController(withIdentifier: "promoCompraBolsaIphone") as! PromoCompraBolsaIphone
                (promoController as! PromoCompraBolsaIphone).promoInfo = Globales.promociones[indexPath.row]

            } else {
                promoController = storyboard.instantiateViewController(withIdentifier: "promoInformativaIphone") as! PromoInformativaIphone
                (promoController as! PromoInformativaIphone).promoInfo = Globales.promociones[indexPath.row]
            }

            // Botón de navegación para volver a las bolsas
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem

            self.navigationController!.pushViewController(promoController as UIViewController, animated: true)
        }
    }

    /// Altura de las secciones
    ///
    /// - Parameters:
    ///   - tableView: tabla de promociones
    ///   - section: seccion que se verifica
    /// - Returns: altura de la seccion
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(225)
    }

    /// Altura de las filas
    ///
    /// - Parameters:
    ///   - tableView: tabla de promociones
    ///   - indexPath: objeto de indice con la fila
    /// - Returns: altura de fila
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(260)
    }
}
