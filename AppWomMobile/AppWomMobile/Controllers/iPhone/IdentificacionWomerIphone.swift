//
//  IdentificacionWomerIphone.swift
//  AppWomMobile
//
//  Controlador del login de la aplicación.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class IdentificacionWomerIphone: UIViewController, UITextFieldDelegate {
    // Enlace a textfield de nombre
    @IBOutlet weak var txtfieldNombre: UITextField!
    // Enlace a textfield de apellido
    @IBOutlet weak var txtfieldApellido: UITextField!
    // Enlace a imagen con icono de error en textfield nombre
    @IBOutlet weak var iconoErrorNombre: UIImageView!
    // Enlace a imagen con icono de error en textfield apellido
    @IBOutlet weak var iconoErrorApellido: UIImageView!
    // Enlace a label con informacion de error en textfield nombre
    @IBOutlet weak var labelErrorNombre: UILabel!
    // Enlace a label con informacion de error en textfield apellido
    @IBOutlet weak var labelErrorApellido: UILabel!
    // Enlace a linea inferior textfield nombre
    @IBOutlet weak var viewLineaNombre: UIView!
    // Enlace a linea inferior textfield apellido
    @IBOutlet weak var viewLineaApellido: UIView!

    // Altura que se resta a movimiento de vista al abrir teclado
    var movimientoAdicional = CGFloat(85)
    // booleando que indica si el teclado se encuentra escondido o no
    var tecladoEscondido = true

    override func viewDidLoad() {
        super.viewDidLoad()
        let sizeFuente = CGFloat(14)

        txtfieldNombre.delegate = self
        txtfieldApellido.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.hideKeyboardWhenTappedAround()

        addDoneButtonOnKeyboard()
        txtfieldNombre.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("label_nombre", comment: "label en textfield nombre"),
                                                                  attributes:[NSForegroundColorAttributeName: DesignHelper.UIColorFromRGB(0x3A1F4F), NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraLight, size: sizeFuente)!])
        txtfieldApellido.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("label_apellido", comment: "label en textfield apellido"),
                                                                    attributes:[NSForegroundColorAttributeName: DesignHelper.UIColorFromRGB(0x3A1F4F), NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraLight, size: sizeFuente)!])

        if !Globales.isIphone5 {
            movimientoAdicional = 0
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

    }

    /**
     Función que se activa al presionar boton home de dispositivo.

     - parameter notification: notificacion de reproductor.
     */
    func willResignActive(_ notification: Notification) {
        self.view.endEditing(true)
    }
    /**
     Función que abre la vista para ingresar numero telefonico.

     - parameter sender: datos del boton para continuar navegacion.
     */
    @IBAction func abrirIngresoNumero(_ sender: AnyObject) {
        validarApellido()
        if validarNombre() && validarApellido() {
            Globales.nombreCliente = txtfieldNombre.text!

            let resultController = storyboard!.instantiateViewController(withIdentifier: "loginIphone")
            present(resultController, animated: true, completion: nil)
        }
    }
    /**
     Función que valida campo de nombre.

     - returns: validacion del campo.
     */
    func validarNombre() -> Bool {
        if txtfieldNombre.text == "" {
            iconoErrorNombre.isHidden = false
            labelErrorNombre.isHidden = false
            viewLineaNombre.backgroundColor = UIColor.red
            return false
        } else {
            iconoErrorNombre.isHidden = true
            labelErrorNombre.isHidden = true
            viewLineaNombre.backgroundColor = DesignHelper.UIColorFromRGB(0x3A1F4F)

        }
        return true
    }
    /**
     Función que valida campo de apellido.

     - returns: validacion del campo.
     */
    func validarApellido() -> Bool {
        if txtfieldApellido.text == "" {
            iconoErrorApellido.isHidden = false
            labelErrorApellido.isHidden = false
            viewLineaApellido.backgroundColor = UIColor.red
            return false
        } else {
            iconoErrorApellido.isHidden = true
            labelErrorApellido.isHidden = true
            viewLineaApellido.backgroundColor = DesignHelper.UIColorFromRGB(0x3A1F4F)
        }

        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtfieldNombre:
            if txtfieldNombre.text == "" {
                iconoErrorNombre.isHidden = true
                labelErrorNombre.isHidden = true
                viewLineaNombre.backgroundColor = DesignHelper.UIColorFromRGB(0x3A1F4F)
            }
        case txtfieldApellido:
            if txtfieldApellido.text == "" {
                iconoErrorApellido.isHidden = true
                labelErrorApellido.isHidden = true
                viewLineaApellido.backgroundColor = DesignHelper.UIColorFromRGB(0x3A1F4F)
            }
        default:
            Logger.log("DEFAULT TEXTFIELD DID END EDITING")
        }
    }
    /**
     Función que realiza acciones al mostrar teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillShow(_ notification: Notification) {
        if tecladoEscondido {
            tecladoEscondido = false
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.view.frame.origin.y -= keyboardSize.height - movimientoAdicional
            }
        }
    }
    /**
     Función que realiza acciones al esconder teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillHide(_ notification: Notification) {
        if !tecladoEscondido {
            tecladoEscondido = true
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    /**
     Funcion para abrir vista donde se ingresa el código validador.
     */
    func abrirVistaIngresoCodigo() {
        let resultController = storyboard!.instantiateViewController(withIdentifier: "loginIphone")
        present(resultController, animated: true, completion: nil)
    }

    /**
     Función que se llama al interactuar con el textfield.

     - parameter textField: textfield donde se escribe el codigo.
     - parameter rango: rango de textfield
     - parameter string: string a validar.
     - returns: indicador de si será modificado el string.
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn rango: NSRange, replacementString string: String) -> Bool {
        let maximoCaracteres = 20
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = (strcmp(char, "\\b") == -92)
        let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ ")
        let validacionNumeros = (string.rangeOfCharacter(from: set) != nil || isBackSpace)

        if (textField.text?.characters.count < maximoCaracteres || string.characters.count == 0) && validacionNumeros {
            return true
        } else {
            return false
        }
    }

    /**
     Función que agrega el botón aceptar en el toolbar del teclado.
     */
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("boton_aceptar", comment: "boton aceptar de toolbar"), style: UIBarButtonItemStyle.done, target: self, action: #selector(IdentificacionWomerIphone.abrirIngresoNumero))

        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)

        doneToolbar.items = items
        doneToolbar.sizeToFit()
        doneToolbar.barTintColor = UIColor.white

        self.txtfieldNombre.inputAccessoryView = doneToolbar
        self.txtfieldApellido.inputAccessoryView = doneToolbar
    }
}
