//
//  ExcepcionesWOMIphone.swift
//  AppWomMobile
//
//  Controller para controlar excepciones de la aplicación.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class ExcepcionesWOMIphone: UIViewController {

    /// Enlace a Imagen superior de vista
    @IBOutlet weak var imgError: UIImageView!
    /// Enlace a Label con informacion de error
    @IBOutlet weak var lblInfoError: UILabel!
    /// Enlace a vista con el boton con icono
    @IBOutlet weak var viewBtn: UIView!
    /// Enlace a Label de vista con boton e icono
    @IBOutlet weak var lblBtn: UILabel!
    /// Enlace a Imagen de vista con boton e icono
    @IBOutlet weak var imgIconoBtn: UIImageView!
    /// Enlace a Vista boton inferior
    @IBOutlet weak var viewBtnVolver: UIView!

    /// Indicador de error Darkside
    var esErrorDarkside = false
    /// Indicador de error Limbo
    var esErrorLimbo = false
    /// Indicador de error Mantencion
    var enMantencion = false
    /// Indicador de error General
    var esErrorGeneral = false
    /// Imagen enviada desde controlador padre
    var imagenError = UIImageView()
    /// Factor para modificar vista en iphone 5
    var factorIphone5 = 3
    /// Indicador de error mostrado dentro de una vista
    var errorComoVista = false
    /// Controlador padre
    weak var delegate = UIViewController()

    override func viewDidLoad() {
        viewBtn.layer.cornerRadius = 6
        viewBtn.layer.borderWidth = 2.5
        viewBtn.layer.borderColor = DesignHelper.UIColorFromRGB(0xBA007C).cgColor

        viewBtnVolver.layer.cornerRadius = 6
        viewBtnVolver.layer.borderWidth = 2.5
        viewBtnVolver.layer.borderColor = DesignHelper.UIColorFromRGB(0xBA007C).cgColor

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(self.cerrarVista))
        viewBtnVolver.isUserInteractionEnabled = true
        viewBtnVolver.addGestureRecognizer(tapViewBoton)

        if !Globales.isIphone5 {
            factorIphone5 = 0
        }

        if esErrorDarkside {
            errorDarkside()
        } else if esErrorLimbo {
            errorLimbo()
        } else if enMantencion {
            errorMantencion()
        } else if esErrorGeneral {
            errorGeneral()
        }
    }

    /// Abre web en caso de que no existan numeros de whatsapp activados
    func abrirWebWOM() {
        if let url = URL(string: Constants.URL.ayudaPortabilidad),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    // abre app de whatsapp
    func abrirWhatsapp() {
        UtilCompraBolsas.abrirWhatsapp(numero: Globales.contactoWhatsappDarkside.numero, mensaje: Globales.contactoWhatsappDarkside.mensaje)
    }

    /// Se genera vista para error Darkside
    func errorDarkside() {
        lblInfoError.text = NSLocalizedString("label_error_darkside", comment: "mensaje darkside")
        lblInfoError.numberOfLines = 5
        imgError.image = Globales.imagenDarkside.image

        DesignHelper.generarVistaBoton(view: viewBtnVolver, textoBoton: NSLocalizedString("boton_volver", comment: "boton volver").uppercased(), sizeFuente: CGFloat(16))
        viewBtnVolver.center.x = self.view.center.x

        if Globales.contactoWhatsappDarkside.numero != "" {
            Logger.log("Numero whatsapp obtenido satisfactoriamente")

            imgIconoBtn.image = UIImage(named: Constants.Imagenes.imgWhatsapp)

            let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(abrirWhatsapp))
            viewBtn.isUserInteractionEnabled = true
            viewBtn.addGestureRecognizer(tapViewBoton)

            lblBtn.text = NSLocalizedString("label_boton_darkside_whatsapp", comment: "mensaje boton darkside")
        } else {
            Logger.log("Numero whatsapp obtenido insatisfactoriamente")
            imgIconoBtn.image = UIImage(named: Constants.Imagenes.imgLaptop)
            imgIconoBtn.frame.origin.x = imgIconoBtn.frame.origin.x + CGFloat(20)
            lblBtn.text = NSLocalizedString("label_boton_darkside_sin_whatsapp", comment: "mensaje boton darkside sin whatsapp")
            lblBtn.frame.origin.x = lblBtn.frame.origin.x + CGFloat(22)

            let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(self.abrirWebWOM))
            viewBtn.isUserInteractionEnabled = true
            viewBtn.addGestureRecognizer(tapViewBoton)
        }
    }

    /// Cierra vista de error
    func cerrarVista() {
        if errorComoVista {
            delegate?.navigationController!.popViewController(animated: false)
        } else {
            if (self.delegate as? PopupCompraBolsasMultilinea) != nil {
                (self.delegate as? PopupCompraBolsasMultilinea)?.recargarServicios()
            } else if (self.delegate as? LoginIphone) != nil {
                (self.delegate as? LoginIphone)?.verificarBam()
            }
            dismiss(animated: true, completion: nil)
        }
    }

    /// Genera vista de error Limbo
    func errorLimbo() {
        imgIconoBtn.isHidden = true
        lblBtn.isHidden = true
        viewBtnVolver.isHidden = true

        imgError.image = Globales.imagenLimbo.image

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(self.cerrarVista))
        viewBtn.isUserInteractionEnabled = true
        viewBtn.addGestureRecognizer(tapViewBoton)
        viewBtn.frame.size.height = CGFloat(43)
        viewBtn.frame.size.width = CGFloat(160)
        viewBtn.frame.origin.y = viewBtn.frame.origin.y + CGFloat(20)

        DesignHelper.generarVistaBoton(view: viewBtn, textoBoton: NSLocalizedString("boton_aceptar", comment: "boton aceptar").uppercased(), sizeFuente: CGFloat(16))
        viewBtn.center.x = self.view.center.x

        lblInfoError.attributedText = DesignHelper.modificarAtributosTexto(NSLocalizedString("label_error_limbo", comment: "mensaje limbo"), sizeTextoModificado: 30 - factorIphone5, sizeFuente: 18 - factorIphone5, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: NSLocalizedString("label_error_limbo_bienvenido", comment: "mensaje bienvenido"), colorFuente: UIColor.white, espaciado: CGFloat(1.5))
        lblInfoError.textAlignment = .center
    }

    /// Genera vista para error de mantención
    func errorMantencion() {
        imgIconoBtn.isHidden = true
        lblBtn.isHidden = true
        viewBtnVolver.isHidden = true

        imgError.image = UIImage(named: Constants.Imagenes.imgMantencion)

        lblInfoError.attributedText = DesignHelper.modificarAtributosTexto(NSLocalizedString("label_mantencion_inferior", comment: "mensaje mantencion"), sizeTextoModificado: 30 - factorIphone5, sizeFuente: 18 - factorIphone5, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: NSLocalizedString("label_mantencion_titulo", comment: "mensaje mantencion titulo"), colorFuente: UIColor.white, espaciado: CGFloat(1.5))
        lblInfoError.textAlignment = .center

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(self.verificarEstadoMantencion))

        self.viewBtn.isUserInteractionEnabled = true
        self.viewBtn.addGestureRecognizer(tapViewBoton)

        viewBtn.frame.size.height = CGFloat(43)
        viewBtn.frame.size.width = CGFloat(160)
        viewBtn.frame.origin.y = viewBtn.frame.origin.y + CGFloat(20)

        DesignHelper.generarVistaBoton(view: viewBtn, textoBoton: NSLocalizedString("boton_reintentar", comment: "boton reintentar").uppercased(), sizeFuente: CGFloat(16))
        viewBtn.center.x = self.view.center.x
    }

    /// Verifica el estado de los servicios en mantencion
    func verificarEstadoMantencion() {
        Util.agregarActivityIndicator(self.view)
        UtilApp.validarEstadoServicios(controller: delegate!, abrirVista: false) { retorno in
            DispatchQueue.main.async(execute: {
                Util.removerActivityIndicator(self.view)
                if retorno {
                    if (self.delegate as? MainViewIphone) != nil {
                        (self.delegate as? MainViewIphone)?.verificarIngresoApp()
                    } else if (self.delegate as? SeleccionTipoBolsaIphone) != nil {
                        (self.delegate as? SeleccionTipoBolsaIphone)?.validarMantencion()
                    }
                    self.cerrarVista()
                }

            })
        }
    }

    /// Genera vista para error de mantención
    func errorGeneral() {
        delegate?.navigationController?.setNavigationBarHidden(true, animated: false)
        imgIconoBtn.isHidden = true
        lblBtn.isHidden = true
        viewBtnVolver.isHidden = true

        imgError.image = UIImage(named: Constants.Imagenes.imgErrorGeneral)

        lblInfoError.text = NSLocalizedString("label_error_general", comment: "mensaje error general")
        lblInfoError.numberOfLines = 6

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(cerrarVista))
        self.viewBtn.isUserInteractionEnabled = true
        self.viewBtn.addGestureRecognizer(tapViewBoton)

        viewBtn.frame.size.height = CGFloat(43)
        viewBtn.frame.size.width = CGFloat(160)
        viewBtn.frame.origin.y = viewBtn.frame.origin.y + CGFloat(20)

        DesignHelper.generarVistaBoton(view: viewBtn, textoBoton: NSLocalizedString("boton_reintentar", comment: "boton reintentar").uppercased(), sizeFuente: CGFloat(16))
        viewBtn.center.x = self.view.center.x
    }
}
