//
//  RecargaIphone.swift
//  AppWomMobile
//
//  Controlador de popup para recargar saldo.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON

class RecargaIphone: UIViewController, UIWebViewDelegate, UIScrollViewDelegate {

    /// Vista contenedora de webview
    @IBOutlet weak var viewContenido: UIView!
    /// Enlace a webview de la vista
    @IBOutlet weak var webview: UIWebView!
    /// Botón para cerrar vista al estar en vista de webpay
    @IBOutlet weak var btnCerrar: UIButton!

    var saldoFaltante = String()

    override func viewDidLoad() {
        Util.agregarActivityIndicator(self.view)

        webview.delegate = self
        webview.scrollView.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        cargarWebView()
    }

    /// Carga webview con pagina de recargar
    func cargarWebView() {
        let numeroCortado = String(Globales.numeroTelefono.characters.dropFirst(2))

        var stringURLRecargar = URLServicios.recargarSaldo.replacingOccurrences(of: "{numero}", with: numeroCortado)
        stringURLRecargar = stringURLRecargar.replacingOccurrences(of: "{saldo}", with: String(Globales.datosCliente.saldo))

        if Globales.tabBarSeleccionado.selectedIndex == Constants.Indicadores.tabComprarBolsas {
            stringURLRecargar = stringURLRecargar.replacingOccurrences(of: "{version}", with: Constants.Indicadores.recargarCompraBolsas)
            stringURLRecargar = stringURLRecargar + "&montorecarga=\(saldoFaltante)"
        } else {
            stringURLRecargar = stringURLRecargar.replacingOccurrences(of: "{version}", with: Constants.Indicadores.recargarHome)
        }

        let urlRecarga = URL(string: stringURLRecargar)

        let requestObj = URLRequest(url: urlRecarga!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
        webview.loadRequest(requestObj)
    }

    /**
     Función para cerrar la página desde la cruz en la página.

     - parameter webView: webView donde se abre la página.
     - parameter request: request con url de redirección.
     - parameter navigationType: tipo de navegación.
     - returns: booleano que indica si se realiza la acción de redirección en request.
     */
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = String(describing: request.url!)
        let alturaCerradoVista = CGFloat(30)

        if url.range(of: "salir=true") != nil || url == URLServicios.urlCerrarRecarga {
            self.dismiss(animated: true, completion: nil)
            return false
        } else if url == URLServicios.urlDesplegarCerrarVista {
            self.webview.frame.origin.y = alturaCerradoVista
            self.btnCerrar.isHidden = false
        } else if url == URLServicios.urlOcultarCerrarVista {
            self.webview.frame.origin.y = 0
            self.btnCerrar.isHidden = true
        }

        return true
    }

    /**
     Función que realiza acciones al comenzar la carga de la página.

     - parameter webView: webView donde se abre la página
     */
    func webViewDidStartLoad(_ webView: UIWebView) {
        Logger.log("Webview Cargando...")
    }
    /**
     Función que realiza acciones al terminar la carga de la página.

     - parameter webView: webView donde se abre la página
     */
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Logger.log("Webview terminó de cargar")
        Util.removerActivityIndicator(self.view)
    }
    /**
     Función que realiza acciones al fallar la carga de la página.

     - parameter webView: webView donde se abre la página
     - parameter error: error específico
     */
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Util.removerActivityIndicator(self.view)
        Logger.log("Error al cargar webview")
    }

    @IBAction func cerrarVista(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }


}
