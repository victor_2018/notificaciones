//
//  TutorialAppIphone.swift
//  AppWomMobile
//
//  Controlador del tutorial de la aplicación.
//
//  @author Esteban Pavez A. (TINet).
//


import UIKit
import AVFoundation
import Firebase


class TutorialAppIphone: UIViewController, UIPageViewControllerDataSource {

    // Enlace con boton para saltar tutorial en vista
    @IBOutlet weak var saltarButton: UIButton!
    // Enlace con pageControl en vista
    @IBOutlet weak var pageControl: UIPageControl!
    // Enlace con imagen de fondo de boton para saltar tutorial en vista
    @IBOutlet weak var buttonBackground: UIImageView!

    // viewController generado que contiene controladores de slides
    var pageViewController: UIPageViewController!
    // arreglo de Strings con los titulos de slides de tutoriales
    var pageTitles: NSArray!
    // arreglo de Strings con las descripciones de slides de tutoriales
    var pageDescript: NSArray!
    // arreglo de Strings con los nombres de las imagenes de los tutoriales
    var imagenesCentrales: NSArray!
    // Layer de reproductor de video de fondo
    var avPlayerLayer: AVPlayerLayer!
    // Variable que indica si el video está detenido o no.
    var paused: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_PASO1_ONLOAD", comment: " marca de ingreso a paso 1 oload"))
        pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)

        mostrarVideoTutorial()
        playVideo()

        NotificationCenter.default.addObserver(self, selector: #selector(self.willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

    }

    @IBAction func Empezar(_ sender: UIButton) {
        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_PASO1", comment: " marca de ingreso a paso 1"))
    }
    /**
     Función que se activa al presionar boton home de dispositivo.

     - parameter notification: notificacion de reproductor.
     */
    func willResignActive(_ notification: Notification) {
        stopVideo()
    }
    /**
     Función que se activa al volver del home del dispositivo

     - parameter notification: notificacion de reproductor.
     */
    func willEnterForeground(_ notification: Notification) {
        playVideo()
    }

    /**
     Función para generar video de fondo de tutorial.
     */
    func mostrarVideoTutorial() {
        avPlayerLayer = AVPlayerLayer(player: Globales.avPlayer)//avPlayer)
        avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        Globales.avPlayer.volume = 0
        Globales.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none

        avPlayerLayer.frame = self.view.frame
        view.layer.insertSublayer(avPlayerLayer, at: 0)

        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(TutorialAppIphone.playerItemDidReachEnd(_:)),
                                                         name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                         object: Globales.avPlayer.currentItem)
    }

    /**
     Función para indicar al reproductor que no se detenga.

     - parameter notification: notificacion de reproductor.
     */
    func playerItemDidReachEnd(_ notification: Notification) {
        let reproductor: AVPlayerItem = notification.object as! AVPlayerItem
        reproductor.seek(to: kCMTimeZero)
    }
    override func viewDidLayoutSubviews() {
        let limiteSubViews = 4
        if self.view.subviews.count == limiteSubViews {
            mostrarTutorial()
        }
    }
    /**
     Empieza reproducción de video de fondo.
     */
    func playVideo() {
        Globales.avPlayer.play()
        paused = false
    }
    /**
     Detiene reproducción de video de fondo.
     */
    func stopVideo() {
        Globales.avPlayer.pause()
        paused = true
    }

    /**
     Añade las imagenes y titulos dinamicamente a los slides de tutoriales.
     */
    func mostrarTutorial() {
        self.pageTitles = NSArray(objects: NSLocalizedString("titulo_tutorial_slide1", comment: "tituloSlide1"), NSLocalizedString("titulo_tutorial_slide2", comment: "tituloSlide2"), NSLocalizedString("titulo_tutorial_slide3", comment: "tituloSlide3"))

        self.pageDescript = NSArray(objects: NSLocalizedString("descripcion_tutorial_slide1", comment: "descripcionTutorialSlide1"), NSLocalizedString("descripcion_tutorial_slide2", comment: "descripcionTutorialSlide2"), NSLocalizedString("descripcion_tutorial_slide3", comment: "descripcionTutorialSlide3"))

        self.imagenesCentrales = NSArray(objects: Constants.Imagenes.imgTutorial1, Constants.Imagenes.imgTutorial2, Constants.Imagenes.imgTutorial3)

        self.pageControl.currentPage = 0

        self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "TutorialController") as! UIPageViewController
        self.pageViewController.dataSource = self

        let startVC = self.mainControllerAtIndex(0) as TutorialContenidoIphone
        let viewControllers = NSArray(object: startVC)

        self.pageViewController.setViewControllers((viewControllers as! [UIViewController]), direction: .forward, animated: true, completion: nil)

        self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height)

        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMove(toParentViewController: self)

        self.view.bringSubview(toFront: self.pageControl)
        self.view.bringSubview(toFront: self.saltarButton)
    }

    /**
     Forma vista actual del tutorial

     - parameter index: Índice de donde está parado en tutorial.

     - returns: View Controller de la vista generada dinamicamente
     */
    func mainControllerAtIndex(_ index: Int) -> TutorialContenidoIphone {
        if (self.pageTitles.count == 0) || (index >= self.pageTitles.count) {
            return TutorialContenidoIphone()
        }

        let vc: TutorialContenidoIphone = self.storyboard?.instantiateViewController(withIdentifier: "TutorialContenidoIphone") as! TutorialContenidoIphone

        vc.imagenCentralIn = self.imagenesCentrales[index] as! String
        vc.tituloTextoIn = self.pageTitles[index] as! String
        vc.descripIn = self.pageDescript[index] as! String
        vc.pageIndex = index

        return vc
    }

    /**
     Obtiene el índice de la vista que viene anterior a la actual.

     - parameter pageViewController: variable no utilizada pero obligatoria
     - parameter viewController:     controlador con base para tutorial

     - returns: viewController de la vista anterior.
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        let vc = viewController as! TutorialContenidoIphone
        var index = vc.pageIndex as Int

        self.pageControl.currentPage = index

        if index == 0 || index == NSNotFound {
            return nil
        }

        index -= 1

        return self.mainControllerAtIndex(index)

    }

    /**
     Obtiene el índice de la vista que viene después a la actual.

     - parameter pageViewController: variable no utilizada
     - parameter viewController:     controlador con base para tutorial

     - returns: viewController de la vista siguiente.
     */
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        let vc = viewController as! TutorialContenidoIphone
        var index = vc.pageIndex as Int

        self.pageControl.currentPage = index

        if index == self.pageTitles.count {
            return nil
        }

        index += 1

        if index == self.pageTitles.count {
            return nil
        }

        return self.mainControllerAtIndex(index)
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}
