//
//  LoginIphone.swift
//  AppWomMobile
//
//  Controlador del login de la aplicación.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class LoginIphone: UIViewController, UITextFieldDelegate {
    // campo de texto donde se ingresa el número de teléfono.
    @IBOutlet weak var txtFieldNumeroTelefono: UITextField!
    // enlace a imagen de ícono de error
    @IBOutlet weak var iconoErrorNumero: UIImageView!
    // label con mensaje de error
    @IBOutlet weak var labelErrorNumero: UILabel!
    // linea inferior al textfield
    @IBOutlet weak var viewLineaTextfield: UIView!
    // arreglo con posibles números de telefono prepago
    var arregloNumeros = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_PASO2_ONLOAD", comment: " marca de ingreso a paso 2 onload"))

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.hideKeyboardWhenTappedAround()
        txtFieldNumeroTelefono.delegate = self
        addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(self.willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

    }
    /**
     Función que se activa al presionar boton home de dispositivo.

     - parameter notification: notificacion de reproductor.
     */
    func willResignActive(_ notification: Notification) {
        self.view.endEditing(true)
    }

    /**
     Función que realiza acciones al mostrar teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height
        }

    }
    /**
     Función que realiza acciones al esconder teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillHide(_ notification: Notification) {
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    /**
     Función que se llama al aparecer el teclado del telefono.

     - parameter textField: textfield donde se escribe el codigo.
     - parameter rango: rango de textfield
     - parameter string: string que se ingresa.
     - returns: booleano indicando si el string se reemplazará o no.
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn rango: NSRange, replacementString string: String) -> Bool {
        let maximoCaracteres = 9

        if textField.text?.characters.count < maximoCaracteres {
            return true
        } else if string.characters.count == 0 {
            return true
        } else {
            return false
        }
    }
    /**
     Acción al presionar botón "solicitar codigo".

     - parameter sender: identificador de elemento que envía la accion.
     */
    @IBAction func solicitarCodigo(_ sender: AnyObject) {
        let numeroTelefono = txtFieldNumeroTelefono.text
        let prefijo = "56"

        if validarNumero(prefijo + numeroTelefono!) {
            UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_PASO2", comment: " marca de ingreso a paso 2"))
            Globales.numeroTelefono = prefijo + numeroTelefono!
            verificarBam()
        } else {
            iconoErrorNumero.isHidden = false
            labelErrorNumero.isHidden = false
            viewLineaTextfield.backgroundColor = DesignHelper.UIColorFromRGB(0xF5A623)
            self.view.endEditing(true)
        }
    }
    /**
     Funcion que valida número ingresado.
     - parameter numero: número para validar.
     - returns: booleano que indica si pasó validación.
     */
    func validarNumero(_ numero: String) -> Bool {
        if numero.characters.count < 11 {
            return false
        } else {
            return true
        }
    }
    /**
     Muestra indicadores de error en la vista
     */
    func mostrarError() {
        iconoErrorNumero.isHidden = false
        labelErrorNumero.isHidden = false
        viewLineaTextfield.backgroundColor = DesignHelper.UIColorFromRGB(0xF5A623)
    }
    /**
     Esconde indicadores de error en la vista
     */
    func esconderError() {
        iconoErrorNumero.isHidden = true
        labelErrorNumero.isHidden = true
        viewLineaTextfield.backgroundColor = UIColor.white
    }
    /**
     Funcion para abrir vista donde se ingresa el código validador.
     */
    func abrirVistaIngresoCodigo() {
        if Globales.modoTest {
            let resultController = self.storyboard!.instantiateViewController(withIdentifier: "insertarCodigoLogin")
            self.present(resultController, animated: true, completion: nil)
        } else {
            //   Se llama a servicio para obtener token
            UtilServicios.obtenerToken(Constants.Login.clientIdIfacil, clientSecret: Constants.Login.clientSecretFacil, numeroTelefono: Globales.numeroTelefono, password: Globales.numeroTelefono, grantType: Constants.Login.grantType, refreshToken: "", realm: Constants.Login.realmFacil) { retorno in
                DispatchQueue.main.async(execute: {
                    if retorno {
                        // Una vez obtenido token se envia sms a womer con el siguiente servicio
                        UtilServicios.solicitarCodigoVerificador(Globales.numeroTelefono) { retorno in
                            DispatchQueue.main.async(execute: {
                                Util.removerActivityIndicator(self.view)
                                if retorno {
                                    let resultController = self.storyboard!.instantiateViewController(withIdentifier: "insertarCodigoLogin")
                                    self.present(resultController, animated: true, completion: nil)
                                } else {
                                    Util.alertErrorGeneral(self)
                                    UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_PASO2", comment: "miga de pan ingreso facil"))
                                }
                            })
                        }
                    } else if Globales.esErrorLimbo {

                        let resultController = self.storyboard!.instantiateViewController(withIdentifier: "excepcionesWom") as! ExcepcionesWOMIphone
                        resultController.esErrorLimbo = true
                        self.present(resultController, animated: true, completion: nil)
                        Logger.log("Error Limbo")

                        Util.removerActivityIndicator(self.view)
                    } else {
                        UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_PASO2", comment: "miga de pan ingreso facil"))
                        Util.alertErrorGeneral(self)
                        Util.removerActivityIndicator(self.view)
                        Logger.log("Error en token general")
                    }
                })
            }
        }
    }

    /**
     Función que llama a los servicios principales
     */
    func verificarBam() {
        self.view.endEditing(true)
        Util.agregarActivityIndicator(self.view)

        UtilServicios.obtenerTipoHome(Globales.numeroTelefono) { retorno in
            DispatchQueue.main.async(execute: {
                if retorno {
                    if Globales.homeSeleccionado == Constants.TipoHome.homeBam {
                        self.abrirHomeBam()
                    } else {
                        self.abrirVistaIngresoCodigo()
                    }
                    Logger.log("finaliza servicio obtener home")
                } else {
                    UtilServicios.obtenerTipoHome("") { retorno in
                        DispatchQueue.main.async(execute: {
                            if retorno {
                                Logger.log("Error Darkside")
                                //Se obtiene contacto de whatsapp
                                UtilFirebase.obtenerContactosWhatsappDarkside { retorno in
                                    if retorno {
                                        let resultController = self.storyboard!.instantiateViewController(withIdentifier: "excepcionesWom") as! ExcepcionesWOMIphone
                                        resultController.esErrorDarkside = true
                                        self.present(resultController, animated: true, completion: nil)
                                    } else {
                                        UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_PASO2", comment: "miga de pan ingreso facil"))
                                        Util.alertErrorGeneral(self)
                                    }
                                    Util.removerActivityIndicator(self.view)
                                }
                            } else {
                                Logger.log("Liferay se encuentra caido")
                                Util.removerActivityIndicator(self.view)
                                Util.alertErrorGeneral(self)
                                UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_PASO2", comment: "miga de pan ingreso facil"))
                            }

                        })
                    }
                }
            })
        }
    }
    /**
     Función que abre tabBarController tipo BAM.
     */
    func abrirHomeBam() {
        UtilApp.verificarDatosLogin()
        let controladorHomeBam = storyboard!.instantiateViewController(withIdentifier: "homeBam")
        present(controladorHomeBam, animated: true, completion: nil)
    }
    /**
     Función que el numero se encuentre en el arreglo de numeros permitidos.

     - parameter arregloNumeros: arreglo de números indicado.
     - parameter numero: número que se deseas validar.
     - returns: booleano que indica si se encuentra o no el número
     */
    func validarNumeroEnArreglo(_ arregloNumeros: [String], numero: String) -> Bool {
        for index in 0...arregloNumeros.count - 1 {
            if numero == arregloNumeros[index] {
                return true
            }
        }
        return false
    }
    /**
     Función que agrega el botón aceptar en el toolbar del teclado.
     */
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("boton_aceptar", comment: "boton aceptar de toolbar"), style: UIBarButtonItemStyle.done, target: self, action: #selector(LoginIphone.solicitarCodigo))

        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)

        doneToolbar.items = items
        doneToolbar.sizeToFit()
        doneToolbar.barTintColor = UIColor.white

        self.txtFieldNumeroTelefono.inputAccessoryView = doneToolbar
    }
}
