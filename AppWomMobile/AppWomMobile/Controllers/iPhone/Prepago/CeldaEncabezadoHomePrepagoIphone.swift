//
//  CeldaEncabezadoHomePrepagoIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda del encabezado del home prepago.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel

class CeldaEncabezadoHomePrepagoIphone: UITableViewCell {
    // Enlace de botón para inciar new womer
    @IBOutlet weak var botonPerfil: UIButton!
    // Enlace de label con saldo del cliente
    @IBOutlet weak var saldoCliente: UILabel!
    // Enlace a nombre del cliente
    @IBOutlet weak var nombreCliente: UILabel!
    // Enlace a imagen de womer
    @IBOutlet weak var imagenWomer: UIImageView!
    // Enlace a nombre de plan
    @IBOutlet weak var nombrePlan: UILabel!
    // vista donde se muestra el nombre en una o dos lineas
    @IBOutlet weak var viewNombre: UIView!
    // Enlace a numero de telefono en vista
    @IBOutlet weak var numeroTelefono: UILabel!
    /**
     Acción para redirigir a tab comprar bolsas
     - parameter sender: enlace a boton que abre tab comprar bolsas
    */

    @IBAction func comprarBolsas(_ sender: AnyObject) {
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabComprarBolsas
    }
    /**
     Acción para redirigir a tab new womer
     - parameter sender: enlace a boton que abre new womer
     */
    @IBAction func abrirNewWomer(_ sender: AnyObject) {
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabNewWomer
    }

    /**
     Función para asignar datos a encabezado
     */
    func prepararVista() {
        let posicionEspacioVacio = 4

        Globales.nombreCliente = Globales.nombreCliente
        nombreCliente.text = Globales.nombreCliente
        saldoCliente.text = Globales.datosCliente.saldoFormateado
        imagenWomer.image = Globales.imagenPerfil
        imagenWomer = DesignHelper.redondearImagen(imagenWomer)

        numeroTelefono.text = "+" + Globales.numeroTelefono
        numeroTelefono.text = UtilApp.insertarCaracterEnString(caracter: " ", texto: numeroTelefono.text!, posicion: posicionEspacioVacio)

        // Para los casos de simple y prepago se comportan de igual forma, mostrando "Prepago"
        if Globales.homeSeleccionado != Constants.TipoHome.homePrepago {
            nombrePlan.text = Globales.datosCliente.nombrePlan
        }

        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.abrirSeleccionImagen))
        imagenWomer.isUserInteractionEnabled = true
        imagenWomer.addGestureRecognizer(tapGestureRecognizer)
    }

    /// Abre vista para modificar imagen de perfil
    func abrirSeleccionImagen() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        Globales.homeController.navigationItem.backBarButtonItem = backItem

        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controladorSeleccionImagen = storyboard.instantiateViewController(withIdentifier: "personalizaPerfilRRSS") as! PersonalizaPerfilHomeIphone

        Globales.homeController.navigationController!.pushViewController(controladorSeleccionImagen as UIViewController, animated: true)
    }
    /**
     Acción para abrir pagina recarga saldo
     - parameter sender: enlace a boton que abre pagina para recargar saldo
     */
    @IBAction func abrirRecargaSaldo(_ sender: AnyObject) {
        UtilBolsas.abrirRecargaBrowser(saldoFaltante: "", numeroTelefono: Globales.numeroTelefono)
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_RECARGA", comment: "marca home boton recarga"))
    }
}
