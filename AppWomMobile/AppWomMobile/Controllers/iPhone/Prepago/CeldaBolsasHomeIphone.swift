//
//  CeldaBolsasHomeIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CeldaBolsasHomeIphone: UITableViewCell {
    // imagen del item
    @IBOutlet weak var imagen: UIImageView!
    // imagen indicador promocional
    @IBOutlet weak var imagenPromocion: UIImageView!
    // datos de bolsa restantes
    @IBOutlet weak var restanteBolsa: MarqueeLabel!
    // dias vencimiento bolsa
    @IBOutlet weak var fechaVencimiento: UILabel!
    // nombre de la bolsa
    @IBOutlet weak var nombreBolsa: MarqueeLabel!
    // boton desplegable
    @IBOutlet weak var btnDesplegable: UIImageView!
    // label "vence" de bolsa
    @IBOutlet weak var labelVence: UILabel!
    // view contenedor de view
    @IBOutlet weak var viewDesplegable: UIView!
    // fecha activacion de bolsa
    @IBOutlet weak var fechaActivada: UILabel!
    // fecha vencimiento de bolsa
    @IBOutlet weak var fechaVencimientoDesplegable: UILabel!
    // valor total bolsa
    @IBOutlet weak var valorTotal: UILabel!
    // View que contiene los datos de la celda
    @IBOutlet weak var viewContenedora: UIView!
    // cantidad de bolsas agrupadas
    @IBOutlet weak var cantidadBolsas: UILabel!
    // label de vigencia de basales
    @IBOutlet weak var vigenciaBasal: UILabel!
    // label con la hora de activación de la bolsa
    @IBOutlet weak var horaActivacion: UILabel!
    // label con la hora de finalización de la bolsa
    @IBOutlet weak var horaFinalizacion: UILabel!

    // imagen que se anima por el background
    var bgImage: UIImageView!
    // porcentaje consumido de bolsa
    var avance: CGFloat!
    // observador de frames
    var isObserving = false
    // indicador de si esta desplegado
    var estaDesplegado = false
    // valor de celda expandida
    var anchoMaxNombreBolsa = CGFloat()
    var anchoMaxRestanteBolsa = CGFloat()

    class var heightExpandida: CGFloat {
        get {
            return 149
        }
    }
    // valor de celda plegada
    class var heightDefecto: CGFloat {
        get {
            return 85
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        var cellFrame = self.contentView.frame
        cellFrame.size.height = CeldaBolsasHomeIphone.heightDefecto
        cellFrame.size.width = Globales.anchuraDispositivo

        self.bgImage = UIImageView(frame: cellFrame)
        self.bgImage.image = UIImage(named: Constants.Imagenes.imgBarraEstado)
        self.bgImage.frame.origin.x = 0

        determinarAnchuraLabel()
    }

    /// determina anchura maxima de labels del nombre de la bolsa y el restante
    func determinarAnchuraLabel() {
        if Globales.isIphone5 {
            anchoMaxNombreBolsa = CGFloat(176)
            anchoMaxRestanteBolsa = CGFloat(185)
        } else if Globales.isIphone6 {
            anchoMaxNombreBolsa = CGFloat(206)
            anchoMaxRestanteBolsa = CGFloat(217)
        } else {
            anchoMaxNombreBolsa = CGFloat(228)
            anchoMaxRestanteBolsa = CGFloat(240)
        }
    }
    /**
     Oculta vista desplegable si el tamaño del frame es menor a lo determinado en expandida.
     */
    func checkHeight() {
        viewDesplegable.isHidden = (frame.size.height < CeldaBolsasHomeIphone.heightExpandida)
    }
    /**
     Agrega observador.
     */
    func watchFrameChanges() {
        if !isObserving {
            addObserver(self, forKeyPath: "frame", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.initial], context: nil)
            isObserving = true
        }
    }
    /**
     Remueve observador.
     */
    func ignoreFrameChanges() {
        if isObserving {
            removeObserver(self, forKeyPath: "frame")
            isObserving = false
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "frame" {
            checkHeight()
        }
    }
    /**
     Asigna los datos a la celda.
     - parameter bolsa: porcentaje de avance de carga en background
     - parameter seccion: seccion que se encuentra la bolsa
     - parameter fila: indice de fila en sección
     */
    func asignarDatos(_ bolsa: BolsaTO, seccion: String, fila: Int) {

        determinarDatosRestantes(seccion, bolsa: bolsa)
        determinarImagenBolsa(bolsa)
        determinarSeccionVencimiento(seccion, bolsa: bolsa)

        self.layoutMargins = UIEdgeInsets.zero
        self.nombreBolsa.attributedText = Util.modificarAtributosTexto(bolsa.nombreBolsa, tamFuente: 15, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: "ILIMITADO", textoSecundarioModificado: "", colorFuente: DesignHelper.UIColorFromRGB(0xBA007C))
        self.valorTotal.text = bolsa.precio
        self.nombreBolsa.type = .continuous
        self.nombreBolsa.animationCurve = .linear
        self.restanteBolsa.type = .continuous
        self.restanteBolsa.animationCurve = .linear

        if Globales.indicadorProblemaFechas {
            self.fechaActivada.text = ""
            self.fechaVencimientoDesplegable.text = ""
            self.horaActivacion.text = ""
            self.horaFinalizacion.text = ""
        } else {
            self.fechaActivada.text = bolsa.fechaActivada
            self.fechaVencimientoDesplegable.text = bolsa.fechaVencimientoDesplegable
            self.horaActivacion.text = bolsa.horaActivada
            self.horaFinalizacion.text = bolsa.horaVencimiento
        }

        if nombreBolsa.intrinsicContentSize.width >= anchoMaxNombreBolsa {
            nombreBolsa.restartLabel()
            nombreBolsa.text?.append("      ")
        }

        if restanteBolsa.intrinsicContentSize.width >= anchoMaxRestanteBolsa {
            restanteBolsa.restartLabel()
            restanteBolsa.text?.append("     ")
        }

        if bolsa.colapsado {
            self.btnDesplegable.image = UIImage(named: Constants.Imagenes.imgPlegarBolsas)
        } else {
            self.btnDesplegable.image = UIImage(named: Constants.Imagenes.imgDesplegarBolsas)
        }

        self.imagenPromocion.isHidden = !bolsa.promocional
        self.isUserInteractionEnabled = bolsa.esColapsable
        self.btnDesplegable.isHidden = !bolsa.esColapsable

        if bolsa.esBasal == true && bolsa.tipoBolsa != nil {
            self.vigenciaBasal.text = UtilHome.validarVencimientoBolsasBasales(bolsa: bolsa)

            if seccion == Constants.Indicadores.seccionSMS && Globales.datosLlamadas.smsIlimitado {
                self.vigenciaBasal.isHidden = true
            } else {
                self.vigenciaBasal.isHidden = false
            }
            self.labelVence.isHidden = true
            self.fechaVencimiento.isHidden = true
            self.btnDesplegable.isHidden = true
        }

        aplicarAnimacion(bolsa, bolsaKey: seccion + String(fila))
        UtilHome.verificarIndicadorPlataforma(bolsa: bolsa, restanteBolsa: restanteBolsa, labelVence: vigenciaBasal)

        self.selectionStyle = UITableViewCellSelectionStyle.none
    }


    /// Se determina lo que se muestra en la seccion de la bolsa del vencimiento de esta
    ///
    /// - parameter seccion: seccion donde esta ubicada la bolsa
    /// - Parameter bolsa: objeto de bolsa
    func determinarSeccionVencimiento(_ seccion: String, bolsa: BolsaTO) {
        if bolsa.diaVencimiento != nil {
            self.labelVence.isHidden = false
            self.fechaVencimiento.isHidden = false
            asignarDiaVencimientoBolsa(seccion, bolsa: bolsa)
        } else {
            self.labelVence.isHidden = true
            self.fechaVencimiento.isHidden = true
        }

        if bolsa.cantidadAgrupacion > 1 {
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: "\(bolsa.cantidadAgrupacion) \(NSLocalizedString("label_agrupacion_bolsas", comment: "label que muestra el texto despues del numero de bolsas"))", attributes: underlineAttribute)
            self.cantidadBolsas.attributedText = underlineAttributedString
            self.labelVence.isHidden = true
            self.fechaVencimiento.isHidden = true
        } else {
            self.cantidadBolsas.isHidden = true
        }
        if Globales.homeSeleccionado == Constants.TipoHome.homePostpagoLibre && bolsa.esBasal {
            self.labelVence.isHidden = true
        } else if bolsa.esBasal {
            self.labelVence.isHidden = false
        }
    }

    /// Le aplica la animación al fondo de la fila
    ///
    /// - parameter bolsa: objeto con datos de la bolsa
    /// - parameter bolsaKey: llave que identifica a la bolsa en la lista.
    func aplicarAnimacion(_ bolsa: BolsaTO, bolsaKey: String) {
        let total = bolsa.totalBolsa.datoEnKilos
        var restante = bolsa.restanteBolsa.datoEnKilos

        if restante < 0 {
            restante = (restante! * -6)
        }
        var porcentajeRestante = CGFloat()
        // duracion de la animacion
        let duracionAnimacion = Double(1)
        var ajusteAnimacion = CGFloat(0.1)

        if total == 0 {
            porcentajeRestante = 0
        } else {
            porcentajeRestante = restante! / total!
        }

        if porcentajeRestante == 1 {
            ajusteAnimacion = 0
        } else if (porcentajeRestante - ajusteAnimacion) <= 0 {
            ajusteAnimacion = porcentajeRestante
        }

        if Globales.arregloAnimacionBolsas[bolsaKey] == nil {
            Globales.arregloAnimacionBolsas[bolsaKey] = true
            self.viewContenedora.insertSubview(self.bgImage, at: 0)
            self.viewContenedora.sendSubview(toBack: self.bgImage)

            UIView.animate(withDuration: duracionAnimacion, delay: 0, options: .curveLinear, animations: {
                self.bgImage.frame.origin.x = Globales.anchuraDispositivo
                }, completion: { _ in
                    UIView.animate(withDuration: duracionAnimacion, delay: 0, options: .curveLinear, animations: {
                        self.bgImage.frame.origin.x = Globales.anchuraDispositivo * porcentajeRestante
                        }, completion: {  _ in
                            if porcentajeRestante == 0 {
                                self.viewContenedora.backgroundColor = DesignHelper.UIColorFromRGB(0xF0F0F0)
                                self.bgImage.removeFromSuperview()
                            }
                    })
            })
        } else {
            if porcentajeRestante == 0 {
                self.viewContenedora.backgroundColor = DesignHelper.UIColorFromRGB(0xF0F0F0)
                self.bgImage.removeFromSuperview()
            } else {
                self.viewContenedora.backgroundColor = UIColor.white
                self.viewContenedora.insertSubview(self.bgImage, at: 0)
                self.viewContenedora.sendSubview(toBack: self.bgImage)
                self.bgImage.frame.origin.x = Globales.anchuraDispositivo * porcentajeRestante
            }
        }
    }
    /**
     Determina que imagen debe ir en la celda de la bolsa.
     - parameter bolsa: objeto bolsa con datos de esta.
     */
    func determinarImagenBolsa(_ bolsa: BolsaTO) {
        self.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        if bolsa.promocional == true {
            self.imagen.image = UIImage(named: Constants.Imagenes.imgBolsaPromo)
        } else if bolsa.esBasal == true {
            self.imagen.image = UIImage(named: Constants.Imagenes.imgBolsaFija)
        } else {
            self.imagen.image = UIImage(named: Constants.Imagenes.imgBolsa)
        }
    }
    /**
     Función que asigna a la celda el valor de los datos (MB/min/sms) restantes.
     - parameter seccion:   sección en que se encuentra celda.
     - parameter bolsa:   bolsa con los datos de ella.
     */
    func determinarDatosRestantes(_ seccion: String, bolsa: BolsaTO) {
        switch seccion {
        case Constants.Indicadores.seccionDatosMovilesBasales, Constants.Indicadores.seccionDatosMovilesBolsasCompradas, Constants.Indicadores.seccionDatosMovilesBolsasPromocionales:
            if !bolsa.limite {
                if Int(bolsa.horario) > 0 || bolsa.muestraConsumo == 0 {
                    self.restanteBolsa.text = NSLocalizedString("label_consumo_ilimitado", comment: "texto consumo ilimitado")
                    self.restanteBolsa.textColor = DesignHelper.UIColorFromRGB(0xBA007C)
                }
            } else {
                self.restanteBolsa.text = "Te quedan \(Util.verificarCerosEnDecimales(String(format: "%.2f", bolsa.restanteBolsa.dato))) \(bolsa.restanteBolsa.metrica!)  "
            }
        case Constants.Indicadores.seccionMinutos:
            if Globales.homeSeleccionado == Constants.TipoHome.homePostpagoLibre && Globales.datosLlamadas.minutosDisponibles == -1 {
                self.restanteBolsa.text = "Has consumido \(Int(bolsa.restanteBolsa.dato)) \(bolsa.restanteBolsa.metrica!)  "
            } else if bolsa.restanteBolsa.dato >= 1 {
                self.restanteBolsa.text = "Te quedan \(Int(bolsa.restanteBolsa.dato)) \(bolsa.restanteBolsa.metrica!)  "
            } else {
                self.restanteBolsa.text = "Te quedan 0 \(bolsa.restanteBolsa.metrica!)  "
            }

            if bolsa.restanteBolsa.datoEnKilos == Constants.Indicadores.consumoIlimitado {
                self.restanteBolsa.text = NSLocalizedString("label_consumo_ilimitado", comment: "texto consumo ilimitado")
                self.restanteBolsa.textColor = DesignHelper.UIColorFromRGB(0xBA007C)
            }
        case Constants.Indicadores.seccionSMS:
            if Globales.datosLlamadas.smsIlimitado && bolsa.esBasal {
                self.restanteBolsa.text = "Has enviado \(Int(bolsa.restanteBolsa.dato)) \(bolsa.restanteBolsa.metrica!)  "
            } else {
                self.restanteBolsa.text = "Te quedan \(Int(bolsa.restanteBolsa.dato)) \(bolsa.restanteBolsa.metrica!)  "
            }
        default:
            Logger.log("default switch tabla datos")
        }
    }
    /**
     Función que estructura las filas de historial de bolsas
     - parameter seccion: seccion donde esta ubicada la bolsa
     - parameter bolsa: bolsa con datos para asignar
     */
    func asignarDiaVencimientoBolsa(_ seccion: String, bolsa: BolsaTO) {

        var diaMes = ""
        var esMes = false
        let topeMensajeMeses = 90
        let topeDiasMes = 30
        var periodo = ""
        let limiteMesesIlimitada = 72

        if bolsa.diaVencimiento != nil {
            let diasVenc = Int(bolsa.diaVencimiento)
            if diasVenc > topeMensajeMeses {
                diaMes = String(Int(bolsa.diaVencimiento)! / topeDiasMes )
                esMes = true
            } else {
                diaMes = bolsa.diaVencimiento
            }
        }

        if esMes {
            periodo = NSLocalizedString("lbl_meses", comment: "label para meses")
        } else {
            periodo = NSLocalizedString("lbl_dias", comment: "label para dias")
        }

        if Globales.indicadorProblemaFechas {
            self.fechaVencimiento.text = ""
            self.labelVence.text = ""
        } else if Int(bolsa.diaVencimiento) < 1 {
            self.fechaVencimiento.text = NSLocalizedString("label_dias_vencimiento", comment: "label indica que hoy vence la bolsa")
            self.labelVence.text = NSLocalizedString("label_vence", comment: "label que cambia de vence a vence")
        } else if Int(bolsa.diaVencimiento) == 1 {
            self.fechaVencimiento.text = " \(diaMes) \(periodo)"
            self.labelVence.text = NSLocalizedString("label_vence_en", comment: "label que cambia de vence a vence")
        } else if Int(diaMes)! > limiteMesesIlimitada && esMes {
            self.labelVence.text = NSLocalizedString("label_vence_nunca", comment: "label que despliega vigencia para siempre")
        } else {
            self.fechaVencimiento.text = "\(diaMes) \(periodo)"
            self.labelVence.text = NSLocalizedString("label_vence_en", comment: "label que cambia de vence a vence en")
        }
    }
    /**
     Función que realiza el despliegue de la celda escondida.

     - parameter selectedIndexPath: NSIndexPath con celda que estaba seleccionada.
     - parameter indexPath: NSIndexPath con celda que se seleccionó.
     - parameter tablaDatos: tabla con los datos de la celda seleccionada.
     - parameter tablaDatosAnterior: tabla con los datos de la celda seleccionada previamente.
     - returns tablaActual: tabla con datos relacionada a celda seleccionada.
     - returns tablaPrevia: tabla con datos relacionada a celda seleccionada previamente.
     - returns indiceAnterior: indice de la celda seleccionada previamente.
     - returns indexPaths: arreglo de indices de las celdas seleccionadas.
     - returns existenIndices: booleano indicador para el refresco de la tabla de datos.
     */
    static func desplegarCelda(_ selectedIndexPath: inout IndexPath?, indexPath: IndexPath, tablaDatos: [BolsaTO], tablaDatosAnterior: [BolsaTO]) -> (tablaActual: [BolsaTO], tablaPrevia: [BolsaTO], indiceAnterior: IndexPath?, indexPaths: Array<IndexPath>, existenIndices: Bool) {

        let previousIndexPath = selectedIndexPath

        if indexPath == selectedIndexPath {
            tablaDatos[indexPath.row].colapsado = true
            selectedIndexPath = nil
        } else {
            if selectedIndexPath == nil {
                tablaDatos[indexPath.row].colapsado = false
            } else {
                tablaDatos[indexPath.row].colapsado = false
                tablaDatosAnterior[previousIndexPath!.row].colapsado = true
            }
            selectedIndexPath = indexPath
        }

        var indexPaths: Array<IndexPath> = []
        if let previous = previousIndexPath {
            indexPaths += [previous]
        }
        if let current = selectedIndexPath {
            indexPaths += [current]
        }

        return (tablaDatos, tablaDatosAnterior, previousIndexPath, indexPaths, indexPaths.count > 0)
    }
}
