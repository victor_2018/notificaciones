//
//  PopupComprarBolsaIphone.swift
//  AppWomMobile
//
//  Controlador de popup para comprar bolsas.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class PopupComprarBolsaIphone: UIViewController {
    // vista con el contenido del pop-up
    @IBOutlet weak var viewContenido: UIView!
    // delegate a la vista padre
    @IBOutlet weak var nombreBolsa: UILabel!
    // enlace a label de vigencia de la bolsa
    @IBOutlet weak var vigenciaBolsa: UILabel!
    // enlace a label del valor total
    @IBOutlet weak var valorTotal: UILabel!
    // enlace a label de fecha activacion de bolsa
    @IBOutlet weak var fechaActivacion: UILabel!
    // enlace a label de fecha termino de la bolsa
    @IBOutlet weak var fechaTermino: UILabel!
    // enlace a label del saldo actual
    @IBOutlet weak var horaActivacion: UILabel!
    // Hora de término de bolsa tematica
    @IBOutlet weak var horaTermino: UILabel!
    // Saldo actual del cliente
    @IBOutlet weak var saldoActual: UILabel!
    // enlace a label del saldo necesario para comprar la bolsa
    @IBOutlet weak var saldoNecesario: UILabel!
    // enlace a label de saldo faltante para comprar bolsa
    @IBOutlet weak var saldoFaltante: UILabel!
    // enlace a view que indica saldo insuficiente
    @IBOutlet weak var viewSaldoInsuficiente: UIView!
    // enlace a view con mensaje que se descontará de la proxima factura la bolsa
    @IBOutlet weak var viewMensajePostpagoControlado: UIView!
    // enlace a label del gasto total en la bolsa
    @IBOutlet weak var labelGastoTotal: UILabel!
    // enlace a label del saldo faltante/final al comprar bolsa
    @IBOutlet weak var labelSaldoFaltanteFinal: UILabel!
    // enlace a boton para comprar/recargar
    @IBOutlet weak var botonComprarRecargarBolsa: UIButton!
    // enlace a constraint de altura de imagen de bolsa
    @IBOutlet weak var heightImagenBolsa: NSLayoutConstraint!
    /// imagen superior de la bolsa
    @IBOutlet weak var imagenBolsa: UIImageView!
    /// Enlace a boton para abrir detalle de paises
    @IBOutlet weak var btnAbrirPaises: UIButton!
    /// Enlace a imagen que cierra popup
    @IBOutlet weak var imgBtnCerrar: UIImageView!
    /// Enlace a vista inferior en multilinea
    @IBOutlet weak var viewInferiorMultilinea: UIView!
    /// Enlace a label con el numero de la linea hija
    @IBOutlet weak var lblNumeroMultilinea: UILabel!
    /// Enlace a label con el saldo actual de la linea hija
    @IBOutlet weak var lblSaldoActual: UILabel!
    /// Enlace a label de mensaje cuando el cliente no tiene saldo
    @IBOutlet weak var lblSinSaldo: UILabel!

    // objeto con la bolsa seleccionada
    var bolsa = Bolsa()
    // indicador de si falta saldo para comprar bolsa
    var faltaSaldo = false
    // indica si se vió por primera vez la vista o no
    var indicadorVista = false
    /// Indicador si es roaming
    var esRoaming = false
    // delegate a vista padre
    weak var delegate: ProtocoloSeleccionBolsaIphone?
    /// Controlador de la vista de error general
    var popupErrorGeneral = ErrorGeneral()
    /// Controlador de popup multilinea
    var controllerPopupMultilinea = PopupCompraBolsasMultilinea()
    /// Datos de la linea
    var cliente = EncabezadoHomeTO()
    /// Indicador de flujo multilinea
    var esMultilinea = false

    override func viewDidLoad() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        Globales.montoBolsa = self.bolsa.valor
        Globales.nombreBolsa = self.bolsa.nombre
        Globales.tipoBolsa = self.bolsa.tipo

        if !esMultilinea {
            cliente = Globales.datosCliente
        }

        Analytics.logEvent("event_wom", parameters: [
            "linea": self.cliente.numeroTelefono,//numero de telefono
            "segmento": self.cliente.tipoPlan,
            "canal":"app",
            "fecha": Globales.fechaHoy,
            "funcionalidad": "COMPRA_BOLSA_PASO1",
            "dispositivo": "IOS",
            "tipo_bolsa": self.bolsa.tipo,
            "nombre_bolsa":self.bolsa.nombre,
            "monto_bolsa":self.bolsa.valor
            ])

        Analytics.logEvent("event_wom", parameters: [
            "linea": self.cliente.numeroTelefono,//numero de telefono
            "segmento": self.cliente.tipoPlan,
            "canal":"app",
            "fecha": Globales.fechaHoy,
            "funcionalidad": "COMPRA_BOLSA_PASO2_ONLOAD",
            "dispositivo": "IOS",
            "tipo_bolsa": self.bolsa.tipo,
            "nombre_bolsa":self.bolsa.nombre,
            "monto_bolsa":self.bolsa.valor
            ])

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista
        viewSaldoInsuficiente.layer.borderColor = DesignHelper.UIColorFromRGB(0xFAEBCC).cgColor
        viewSaldoInsuficiente.layer.borderWidth = 1
        viewMensajePostpagoControlado.layer.borderColor = DesignHelper.UIColorFromRGB(0xDDDDDD).cgColor
        viewMensajePostpagoControlado.layer.borderWidth = 1

        if bolsa.es4G {
            self.imagenBolsa.image = UIImage(named: Constants.Hashmap.tipoBolsaImagenModal[Constants.Indicadores.bolsa4g]!)
        } else {
            self.imagenBolsa.image = UIImage(named: Constants.Hashmap.tipoBolsaImagenModal[bolsa.tipo]!)
        }

        self.mostrarAnimacion()

        if [Constants.Indicadores.roamingPreferente, Constants.Indicadores.roamingEuropa].contains(bolsa.tipoProductoBSCS) {
            btnAbrirPaises.isHidden = false
            esRoaming = true
        } else {
            btnAbrirPaises.isHidden = true
            esRoaming = false
        }

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(cerrarVista(_:)))
        imgBtnCerrar.isUserInteractionEnabled = true
        imgBtnCerrar.addGestureRecognizer(tapViewBoton)

        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }


    override func viewWillAppear(_ animated: Bool) {
        self.asignarDatos(bolsa)
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    /**
     Función que se activa al volver del home del dispositivo

     - parameter notification: notificacion de reproductor.
     */
    func willEnterForeground(_ notification: Notification) {
        if Globales.tabBarSeleccionado.selectedIndex == Constants.Indicadores.tabComprarBolsas && self.tabBarController != nil {
            Util.agregarActivityIndicator(Globales.tabBarSeleccionado.view)
            self.obtenerValorSaldo(false)
        }
    }

    /// Genera la vista con las modificaciones para multilinea
    func generarVistaMultilinea() {
        viewInferiorMultilinea.isHidden = false
        UtilMultilinea.generarLabelTelefonoPopupCompraBolsa(labelNumeroTelefono: lblNumeroMultilinea, numeroTelefono: cliente.numeroTelefono, texto: NSLocalizedString("lbl_telefono_comprabolsa_multilinea", comment: "label con telefono compra bolsa multilinea"))
        botonComprarRecargarBolsa.setBackgroundImage(UIImage(named: Constants.Imagenes.imgBtnWhiteGrande), for: .normal)
        botonComprarRecargarBolsa.setTitle(NSLocalizedString("lbl_compra_bolsa_multilinea", comment: "texto para boton comprar bolsa"), for: UIControlState())
        botonComprarRecargarBolsa.setTitleColor(DesignHelper.UIColorFromRGB(0xBA007C), for: .normal)

        if cliente.tipoPlan != Constants.Indicadores.tipoPostpago {
            lblSaldoActual.text = NSLocalizedString("lbl_saldo_actual_multilinea", comment: "label sobre saldo actual")
            labelGastoTotal.text = NSLocalizedString("lbl_necesita_actual_multilinea", comment: "label que indica cuando dinero se necesita para comprar bolsa en multilinea")
            if cliente.saldo - bolsa.valor < 0 {
                viewSaldoInsuficiente.isHidden = false
                labelSaldoFaltanteFinal.text = NSLocalizedString("lbl_falta_actual_multilinea", comment: "label que indica lo que falta para comprar la bolsa")
                botonComprarRecargarBolsa.setTitle(NSLocalizedString("btn_recargar_saldo_multilinea", comment: "texto para boton recargar saldo"), for: UIControlState())

                lblSinSaldo.attributedText = Util.modificarAtributosTexto(NSLocalizedString("lbl_falta_saldo_multilinea", comment: "label sin saldo multilinea"), tamFuente: 16, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: NSLocalizedString("lbl_falta_saldo_oops", comment: "label sin saldo multilinea"), textoSecundarioModificado: NSLocalizedString("lbl_falta_saldo_recarga_ahora_multilinea", comment: "label sin saldo multilinea"), colorFuente: DesignHelper.UIColorFromRGB(0x8A6D3B))

                UtilMultilinea.generarLabelTelefonoPopupCompraBolsa(labelNumeroTelefono: lblNumeroMultilinea, numeroTelefono: cliente.numeroTelefono, texto: NSLocalizedString("lbl_recargaremos_multilinea", comment: "label con telefono compra bolsa multilinea sin saldo"))
            } else {
                labelSaldoFaltanteFinal.text = NSLocalizedString("lbl_queda_actual_multilinea", comment: "label que indica saldo final con que se quedará el womer")
            }
        }
    }

    /**
     Accion para comprar la bolsa

     - parameter sender: boton que realiza la accion.
     */
    @IBAction func comprarBolsa(_ sender: AnyObject) {
        Analytics.logEvent("event_wom", parameters: [
            "linea": self.cliente.numeroTelefono,//numero de telefono
            "segmento": self.cliente.tipoPlan,
            "canal":"app",
            "fecha": Globales.fechaHoy,
            "funcionalidad": "COMPRA_BOLSA_PASO2_BTN_COMPRAR",
            "dispositivo": "IOS",
            "tipo_bolsa": Globales.tipoBolsa,
            "nombre_bolsa":Globales.nombreBolsa,
            "monto_bolsa":Globales.montoBolsa
            ])

        if faltaSaldo {
            UtilBolsas.abrirRecargaBrowser(saldoFaltante: String(bolsa.valor - self.cliente.saldo), numeroTelefono: self.cliente.numeroTelefono)
        } else if esRoaming {
            comprarBolsaRoaming()
        } else {
            comprarBolsaNacional()
        }
    }
    /**
     Muestra la animación de la vista del pop-up
     */
    func mostrarAnimacion() {
        // escala con la animación inicial
        let escalaAnimacionInicial = CGFloat(1.3)
        // escala con la animación final
        let escalaAnimacionFinal = CGFloat(1.0)
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // transparencia inicial de la vista
        let transparenciaInicial = CGFloat(0.0)
        // transparencia final de la vista
        let transparenciaFinal = CGFloat(1.0)

        self.view.transform = CGAffineTransform(scaleX: escalaAnimacionInicial, y: escalaAnimacionInicial)
        self.view.alpha = transparenciaInicial

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.view.alpha = transparenciaFinal
            self.view.transform = CGAffineTransform(scaleX: escalaAnimacionFinal, y: escalaAnimacionFinal)
            }, completion: nil)
    }
    /**
     Asigna los datos a la vista de la bolsa

     - parameter bolsa: objeto donde vienen los datos para pintar pantalla.
     */
    func asignarDatos(_ bolsa: Bolsa) {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = " "
        numberFormatter.locale = Locale(identifier: "es_CL")

        nombreBolsa.text = bolsa.nombre
        valorTotal.text = bolsa.valorFormateado
        saldoActual.text = self.cliente.saldoFormateado
        saldoNecesario.text = bolsa.valorFormateado
        vigenciaBolsa.text = bolsa.descripcion

        if self.cliente.tipoPlan == Constants.Indicadores.tipoPostpago {
            viewMensajePostpagoControlado.isHidden = false
            faltaSaldo = false
            horaActivacion.text = ""
            horaTermino.text = ""
            fechaActivacion.text = bolsa.fechaActivacion
            fechaTermino.text = bolsa.fechaFinalizacion
        } else if self.cliente.saldo - bolsa.valor < 0 {
            viewSaldoInsuficiente.isHidden = false
            saldoFaltante.text = "$\(String(describing: numberFormatter.string(from: Int(bolsa.valor - self.cliente.saldo) as NSNumber)!))"
            labelGastoTotal.text = NSLocalizedString("lbl_necesitas", comment: "label que indica cuando dinero se necesita para comprar bolsa")
            labelSaldoFaltanteFinal.text = NSLocalizedString("lbl_faltante", comment: "label que indica lo que falta para comprar la bolsa")
            botonComprarRecargarBolsa.setTitle(NSLocalizedString("btn_recargar_saldo", comment: "texto para boton recargar saldo"), for: UIControlState())
            faltaSaldo = true
            horaActivacion.text = ""
            horaTermino.text = ""
            lblSinSaldo.attributedText = Util.modificarAtributosTexto(NSLocalizedString("lbl_falta_saldo", comment: "label sin saldo"), tamFuente: 16, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: NSLocalizedString("lbl_falta_saldo_oops", comment: "label oops sin saldo multilinea"), textoSecundarioModificado: NSLocalizedString("lbl_falta_saldo_recarga_ahora", comment: "label recargar ahora sin saldo"), colorFuente: DesignHelper.UIColorFromRGB(0x8A6D3B))
        } else {
            viewSaldoInsuficiente.isHidden = true
            saldoFaltante.text = "$\(String(describing: numberFormatter.string(from: Int(self.cliente.saldo - bolsa.valor) as NSNumber)!))"
            labelGastoTotal.text = NSLocalizedString("lbl_gastas", comment: "label que indica cuanto se va a gastar para comprar la bolsa")
            labelSaldoFaltanteFinal.text = NSLocalizedString("lbl_saldo_final", comment: "label que indica saldo final con que se quedará el womer")
            fechaActivacion.text = bolsa.fechaActivacion
            fechaTermino.text = bolsa.fechaFinalizacion
            faltaSaldo = false
        }
    }
    /**
     Setea el objeto de bolsa seleccionado.

     - parameter bolsaIn: bolsa que se envia desde vista padre.
     */
    func setBolsa(_ bolsaIn: Bolsa) {
        self.bolsa = bolsaIn
    }
    /**
     Se arman parametros para enviar a llamada de servicio Post comprar bolsa.

     - returns: cuerpo de json para enviar al servicio
     */
    func armarBodyJsonCompraBolsas() -> [String: AnyObject] {
        var bodyJson = [String: AnyObject]()
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let fechaFormateada = dateFormatter.string(from: date)

        bodyJson["client_name"] = Constants.Indicadores.clientName as AnyObject
        bodyJson["phone_number"] = self.cliente.numeroTelefono as AnyObject
        bodyJson["customer_id"] = "" as AnyObject
        bodyJson["client_type"] = Constants.Indicadores.clientType as AnyObject
        bodyJson["segment_prod"] = self.cliente.tipoPlan as AnyObject
        bodyJson["rate_plan"] = self.cliente.ratePlan as AnyObject
        bodyJson["category_prod"] = Constants.Indicadores.idProd as AnyObject
        bodyJson["id_prod"] = bolsa.idProducto as AnyObject
        bodyJson["id_pcrf_prod"] = bolsa.idPCRF as AnyObject
        bodyJson["id_bscs_prod"] = bolsa.idBSCS as AnyObject
        bodyJson["date"] = fechaFormateada as AnyObject

        if self.cliente.idFormaPago == Constants.Indicadores.idFormaPagoSaldo {
            bodyJson["metodo_pago"] = Constants.Indicadores.metodoPagoPrepagoControlado as AnyObject
        } else {
            bodyJson["metodo_pago"] = Constants.Indicadores.metodoPagoPostpago as AnyObject
        }

        bodyJson["pago_aplicado"] = Constants.Indicadores.pagoAplicado as AnyObject
        bodyJson["transaction_code"] = "" as AnyObject

        return bodyJson
    }
    /**
     Función que realiza la llamada al servicio para comprar bolsas
     */
    func comprarBolsaNacional() {
        var bodyJson = [String: AnyObject]()
        bodyJson = armarBodyJsonCompraBolsas()

        Util.agregarActivityIndicator(Globales.tabBarSeleccionado.view)

        SetNextelActiveProduct.sharedInstance.comprarBolsa(bodyJson, onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {
            let respuestaCompraBolsas = CompraBolsasTO(json: json)
            let codigoValido = "0"

            if respuestaCompraBolsas.codigoError == codigoValido && !respuestaCompraBolsas.codigoTransaccion.isEmpty {

                DispatchQueue.main.async(execute: {
                    if self.cliente.tipoPlan != Constants.Indicadores.tipoPostpago {
                        self.obtenerValorSaldo(true)
                    } else {
                        Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                        self.delegate?.abrirPopupMensajeComprarBolsa()
                    }
                })

            } else {
                DispatchQueue.main.async(execute: {
                    Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                    self.abrirPopupErrorServicio(textoError: "")
                })
            }
        } else {
            DispatchQueue.main.async(execute: {
                Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                self.abrirPopupErrorServicio(textoError: "")
                Logger.log("ERROR al comprar bolsa")
            })
            }
        })
    }

    /// Función donde se llama al servicio que trae los datos del cliente.
    ///
    /// - Parameter continuarFlujo: booleano que indica si se debe continuar a pantalla final o no
    func obtenerValorSaldo(_ continuarFlujo: Bool) {
        let valorPositivo = "Y"
        GetSearchBalanceIcc.sharedInstance.getBalance(self.cliente.numeroTelefono, detalle: valorPositivo,
                                                      onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {

                                                        if json["error_code"] != JSON.null && json["error_code"].stringValue == "0" && Int(json["totalBalance"].stringValue) != nil {

                                                            self.cliente.saldo = Int(json["totalBalance"].stringValue)!
                                                            self.cliente.saldoFormateado = Util.formatearPesos(json["totalBalance"].stringValue)
                                                            DispatchQueue.main.async(execute: {
                                                                Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                                                                if continuarFlujo {
                                                                    self.delegate?.abrirPopupMensajeComprarBolsa()
                                                                } else {
                                                                    self.asignarDatos(self.bolsa)
                                                                }
                                                            })
                                                        } else {
                                                            DispatchQueue.main.async(execute: {
                                                                Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                                                                Logger.log("error de servicio obtener valor saldo")
                                                                UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("COMPRA_BOLSA_PASO2_ONLOAD", comment: "miga de pan paso 2 compra bolsa"))
                                                                Util.alertErrorGeneral(self)
                                                            })
                                                        }
                                                      } else {
                                                        DispatchQueue.main.async(execute: {
                                                            Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                                                            Logger.log("error de servicio obtener valor saldo")
                                                            UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("COMPRA_BOLSA_PASO2_ONLOAD", comment: "miga de pan paso 2 compra bolsa"))
                                                            Util.alertErrorGeneral(self)
                                                        })
                                                        }
        })
    }

    /// Realiza la compra de la bolsa roaming
    func comprarBolsaRoaming() {
        var bodyJson = [String: AnyObject]()
        bodyJson["EN_PHONE_NUMBER"] = self.cliente.numeroTelefono as AnyObject
        bodyJson["EN_ID_PROD"] = bolsa.idProducto as AnyObject

        Util.agregarActivityIndicator(Globales.tabBarSeleccionado.view)

        DBAVentaBolsasR.sharedInstance.comprarBolsaRoaming(bodyJson, onCompletion: {  (json: JSON) in
            DispatchQueue.main.async(execute: {
                if json.dictionaryObject != nil {
                    if json["SN_MENS_RETORNO"].stringValue.uppercased() == "OK" {
                        self.delegate?.abrirConfirmacionCompraRoaming()
                    } else {
                        Logger.log("ERROR compra bolsas roaming: \(json["SN_MENS_RETORNO"].stringValue)")
                        self.abrirPopupErrorServicio(textoError: "")
                    }
                } else {
                    Logger.log("ERROR Compra bolsas roaming")
                    self.abrirPopupErrorServicio(textoError: "")
                }
                Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
            })

        })
    }

    /// Abre la vista de la confirmación de la activación de la promoción
    func abrirPopupErrorServicio(textoError: String) {
        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        popupErrorGeneral = storyboard.instantiateViewController(withIdentifier: "errorGeneral") as! ErrorGeneral

        popupErrorGeneral.delegate = self

        popupErrorGeneral.textoError = textoError

        self.addChildViewController(popupErrorGeneral)
        popupErrorGeneral.view.frame = self.view.frame
        self.view.addSubview(popupErrorGeneral.view)
        popupErrorGeneral.didMove(toParentViewController: self)

        if esMultilinea {
            UtilMultilinea.ajustarPopupInterno(esMultilinea: esMultilinea, popupController: popupErrorGeneral, controllerPopupMultilinea: controllerPopupMultilinea)
        }

        self.navigationItem.setHidesBackButton(true, animated: true)
    }

    /// Cierra el popup
    ///
    /// - Parameter sender: objeto que envia accion
    @IBAction func cerrarVista(_ sender: Any) {
        delegate?.cerrarPopup()
    }

    /// Abre el popup con el listado de paises roaming
    ///
    /// - Parameter sender: objeto que envia accion
    @IBAction func abrirListadoPaises(_ sender: Any) {
        delegate?.flipAListadoPaises(tipoBolsa: bolsa.tipoProductoBSCS)
    }
}
