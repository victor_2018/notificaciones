//
//  PopupMensajeComprarBolsaIphone.swift
//  AppWomMobile
//
//  Controlador de popup de mensaje confirmando compra de bolsa.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import Firebase

class PopupMensajeComprarBolsaIphone: UIViewController {
    // vista del contenido del pop-up
    @IBOutlet weak var viewContenido: UIView!
    // enlace a altura de imagen meme
    @IBOutlet weak var alturaImagen: NSLayoutConstraint!
    /// Enlace a label con el titulo del popup
    @IBOutlet weak var lblTitulo: UILabel!
    /// Enlace a imagen superior del popup
    @IBOutlet weak var imgCompra: UIImageView!
    /// Enlace a label con texto de informacion superior
    @IBOutlet weak var lblInfoSuperior: UILabel!
    /// Enlace a label con texto de informacion medio
    @IBOutlet weak var lblInfoMedio: UILabel!
    /// Enlace a label con texto de informacion inferior
    @IBOutlet weak var lblInfoInferior: UILabel!

    /// Datos del cliente
    var cliente = EncabezadoHomeTO()
    /// Indicador de si se encuentra en flujo multilinea
    var esMultilinea = false
    /// controlador de popup multilinea
    var controllerPopupMultilinea = PopupCompraBolsasMultilinea()

    // delegate a vista padre
    weak var delegate: ProtocoloSeleccionBolsaIphone?

    override func viewDidLoad() {

        UtilApp.generarMigaDePan(NSLocalizedString("COMPRA_BOLSA_PASO3_ONLOAD", comment: " marca compra bolsa paso 3 onload "))

        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista

        DesignHelper.mostrarAnimacionPopUp(self.view)
    }

    /// Genera textos y boton para multilinea
    func generarVistaMultilinea() {
        if esMultilinea {
            lblTitulo.text = NSLocalizedString("lbl_titulo_bolsa_comprada_multilinea", comment: "label titulo compra bolsa multilinea")
            imgCompra.image = UIImage(named: Constants.Imagenes.imgCompraBolsaFinalizada)
            imgCompra.frame.origin.y = imgCompra.frame.origin.y + 20
            lblInfoSuperior.isHidden = true
            generarLabelNumero()
            lblInfoInferior.text = NSLocalizedString("lbl_info_inferior_bolsa_comprada_multilinea", comment: "label info medio compra bolsa multilinea")
            lblInfoInferior.font = lblInfoInferior.font.withSize(15)
        }
    }

    /// Genera el texto con el numero de la linea
    func generarLabelNumero() {
        /// Posicion que se agrega al espacio vacio del numero
        let posicionEspacioVacio = 3

        let numeroConEspaciado = UtilApp.insertarCaracterEnString(caracter: " ", texto: cliente.numeroTelefono, posicion: posicionEspacioVacio)
        let textoLinea = NSLocalizedString("lbl_info_medio_bolsa_comprada_multilinea", comment: "label info medio compra bolsa multilinea").replacingOccurrences(of: "{linea}", with: numeroConEspaciado)

        lblInfoMedio.attributedText = DesignHelper.modificarAtributosTexto(textoLinea, sizeTextoModificado: 18, sizeFuente: 18, tipoFuente: Constants.TipoFuente.ceraRegular, textoModificado: "+\(numeroConEspaciado).", colorFuente: DesignHelper.UIColorFromRGB(0x720184), espaciado: 1.5)
        lblInfoMedio.textAlignment = .center
    }

    /**
     Acción de botón para cerrar el pop-up.

     - parameter sender: Boton que realiza accion.
     */
    @IBAction func cerrarPopup(_ sender: AnyObject) {

        UtilApp.generarMigaDePan(NSLocalizedString("COMPRA_BOLSA_PASO3_BTN_OK", comment: "marca compra bolsa paso 3 boton ok"))

        if esMultilinea {
            controllerPopupMultilinea.cerrarPopup()
        } else {
            removerAnimacion()
            delegate?.recuperarBotonBack()
        }
    }

    /**
     Remueve la vista con animación.
     */
    func removerAnimacion() {
        // escala con la animación inicial
        let escalaAnimacionInicial = CGFloat(1.3)
        // transparencia inicial de la vista
        let transparenciaInicial = CGFloat(0.0)
        // duracion de la animacion
        let duracionAnimacion = 0.25

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.view.transform = CGAffineTransform(scaleX: escalaAnimacionInicial, y: escalaAnimacionInicial)
            self.view.alpha = transparenciaInicial
            }, completion: { (finished: Bool)  in
                if finished {
                    self.delegate?.cerrarSeleccionTipoBolsas()
                }
        })
    }
}
