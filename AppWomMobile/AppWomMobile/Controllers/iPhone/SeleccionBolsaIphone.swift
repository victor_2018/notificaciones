//
//  SeleccionBolsaIphone.swift
//  AppWomMobile
//
//  Controlador en donde se selecciona las distintas bolsas dentro del tipo seleccionado.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import DropDown
import Firebase
protocol ProtocoloSeleccionBolsaIphone: class {
    func abrirPopupComprarBolsa(_ indice: Int)
    func abrirPopupMensajeComprarBolsa()
    func recuperarBotonBack()
    func cerrarSeleccionTipoBolsas()
    func flipAListadoPaises(tipoBolsa: String)
    func cerrarPopup()
    func abrirConfirmacionCompraRoaming()
}

class SeleccionBolsaIphone: UIViewController {
    // vista de scroll principal
    @IBOutlet weak var scrollView: UIScrollView!
    // vista del contenido donde estarán las bolsas
    @IBOutlet weak var viewContenido: UIView!
    // boton de combobox
    @IBOutlet weak var botonPicker: UIButton!
    // Constraint de altura de combo box
    @IBOutlet weak var heightBotonPicker: NSLayoutConstraint!
    /// Enlace a vista del boton que activa roaming
    @IBOutlet weak var viewBtnActivarRoaming: UIView!
    /// Enlace a label para activar roaming
    @IBOutlet weak var lblActivarRoaming: UILabel!
    /// Enlace a imagen del icono en activar roaming
    @IBOutlet weak var imgIconoActivarRoaming: UIImageView!
    /// Enlace a vista del boton para reintentar servicios
    @IBOutlet weak var viewBtnReintentar: UIView!
    /// Enlace a vista para activar roaming
    @IBOutlet weak var viewActivarRoaming: UIView!
    /// Enlace a vista con multilineas
    @IBOutlet weak var viewMultilineas: UIView!
    /// Enlace a boton para cerrar popup en flujo multilinea
    @IBOutlet weak var btnCerrarPopup: UIButton!
    /// Enlace a label con el numero de telefono en flujo multilinea
    @IBOutlet weak var lblNumeroTelefono: UILabel!
    /// Enlace a label con el saldo en flujo multilinea
    @IBOutlet weak var lblSaldo: UILabel!
    /// Enlace a imagen del logo de wom
    @IBOutlet weak var logoWom: UIImageView!

    // delegate para acceder al view padre
    weak var delegate: ProtocoloSeleccionTipoBolsaIphone?

    // instancia de clase para utilizar combo-box
    let dropDown = DropDown()
    // variable que indica la bolsa que se seleccionó en la vista previa
    var bolsaSeleccionada = 0
    // instancia de controlador de pop-up para comprar bolsas
    var popupController = PopupComprarBolsaIphone()
    // instancia de controlador de pop-up de mensaje confirmando compra de bolsa
    var popupMensajeController = PopupMensajeComprarBolsaIphone()
    // popup con listado de paises roaming
    var popupListadoPaises = ListadoPaisesRoaming()
    // arreglo con posibles bolsas para comprar
    var pickerData = [String]()
    // Numero total de bolsas
    var totalBolsas = [Bolsa]()
    // vista donde iran bolsas
    var vistaContenedoraBolsas = UIView()
    // indicador de si esta mostrando vista trasera con la animacion de flip
    var showingBack = false
    /// Vista frontal en animacion flip
    var frontView = UIView()
    /// Vista trasera en animacion flip
    var backView = UIView()
    /// Datos del cliente
    var cliente = EncabezadoHomeTO()
    /// Indicador de flujo multilinea
    var esMultilinea = false
    /// Indicador de cliente postpago en multilinea
    var esPostpagoMultilinea = false
    /// Controlador de popup multilinea
    weak var controllerPopupMultilinea = PopupCompraBolsasMultilinea()

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerData.removeAll()
        scrollView.contentSize = self.view.frame.size
        generarArregloOpciones()
        botonPicker.setTitle(pickerData[bolsaSeleccionada], for: UIControlState())

        UtilApp.generarMigaDePan(NSLocalizedString("COMPRA_BOLSA_HOME_ " + pickerData[bolsaSeleccionada], comment: " marca compra bolsa home"))
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        setupDropDown()
    }
    override func viewDidAppear(_ animated: Bool) {
        if esMultilinea {
            UtilApp.validarEstadoServicios(controller: controllerPopupMultilinea!, abrirVista: true) { retorno in}
        } else {
            UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in}
        }

        self.selectedOption(bolsaSeleccionada)
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /// Modifica el contenido y altura de la vista superior en multilinea
    func modificarVistaMultilinea() {
        esPostpagoMultilinea = (cliente.tipoPlan.uppercased() == Constants.Indicadores.tipoPostpago)
        UtilMultilinea.generarLabelTelefono(labelNumeroTelefono: lblNumeroTelefono, numeroTelefono: cliente.numeroTelefono)
        if self.esPostpagoMultilinea {
            viewMultilineas.frame.size.height = viewMultilineas.frame.size.height * 0.7
            logoWom.isHidden = true
        } else {
            UtilMultilinea.generarLabelSaldo(labelSaldo: lblSaldo, saldo: cliente.saldoFormateado)
        }
    }

    /// Funcion que verifica si se encuentra en la seccion roaming
    ///
    /// - Returns: indicador si esta en la seccion roaming
    func esRoaming() -> Bool {
        if pickerData[bolsaSeleccionada] == NSLocalizedString("tipo_bolsa_roaming", comment: "texto roaming") {
            return true
        } else {
            return false
        }
    }

    /// Abre la seccion de roaming, tanto bolsas como para activar servicio
    func abrirSeccionRoaming() {
        vistaContenedoraBolsas.isHidden = true
        Util.agregarActivityIndicator(Globales.tabBarSeleccionado.view)
        UtilCompraBolsas.validarServicioRoaming(cliente.numeroTelefono) { retorno in
            if retorno {
                UtilApp.generarMigaDePan(NSLocalizedString("ROAMING_ACTIVADO", comment: "miga de pan para servicio roaming activado"))
                UtilFirebase.obtenerListadoPaisesRoaming() { retorno in
                    self.viewActivarRoaming.isHidden = true
                    self.vistaContenedoraBolsas.isHidden = false
                    Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                }
            } else {
                UtilApp.generarMigaDePan(NSLocalizedString("ROAMING_DESACTIVADO", comment: "miga de pan para servicio roaming desactivado"))
                UtilFirebase.obtenerContactosWhatsappRoaming() { retorno in
                    self.generarVistaSinRoaming()
                    self.viewActivarRoaming.isHidden = false
                    Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
                }
            }
        }
    }

    // abre app de whatsapp
    func abrirWhatsapp() {
        var numeroTelefono = String()

        if esMultilinea {
            numeroTelefono = cliente.numeroTelefono
        } else {
            numeroTelefono = Globales.numeroTelefono
        }

        UtilApp.generarMigaDePan(NSLocalizedString("BTN_ROAMING_WHATSAPP", comment: "miga de pan para btn whatsapp"))
        var mensaje = Globales.contactoWhatsappRoaming.mensaje
        mensaje = mensaje.replacingOccurrences(of: "{1}", with: Globales.nombreCliente + " " + Globales.apellidoCliente)
        mensaje = mensaje.replacingOccurrences(of: "{2}", with: Globales.datosCliente.rut)
        mensaje = mensaje.replacingOccurrences(of: "{3}", with: numeroTelefono)

        UtilCompraBolsas.abrirWhatsapp(numero: Globales.contactoWhatsappRoaming.numero, mensaje: mensaje)
    }

    /// inicializa la vista de roaming
    func generarVistaSinRoaming() {
        DesignHelper.generarVistaBoton(view: viewBtnReintentar, textoBoton: NSLocalizedString("boton_reintentar", comment: "boton reintentar").uppercased(), sizeFuente: CGFloat(16))
        DesignHelper.generarVistaBoton(view: viewBtnActivarRoaming, textoBoton: "", sizeFuente: CGFloat(16))

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(abrirSeccionRoaming))
        viewBtnReintentar.isUserInteractionEnabled = true
        viewBtnReintentar.addGestureRecognizer(tapViewBoton)

        if Globales.contactoWhatsappRoaming.numero != "" {
            Logger.log("Numero whatsapp obtenido satisfactoriamente")

            imgIconoActivarRoaming.image = UIImage(named: Constants.Imagenes.imgWhatsapp)

            let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(abrirWhatsapp))
            viewBtnActivarRoaming.isUserInteractionEnabled = true
            viewBtnActivarRoaming.addGestureRecognizer(tapViewBoton)

            lblActivarRoaming.text = NSLocalizedString("activar_roaming_con_whatsapp", comment: "mensaje boton roaming")
        } else {
            Logger.log("Numero whatsapp obtenido insatisfactoriamente")
            imgIconoActivarRoaming.image = UIImage(named: Constants.Imagenes.imgLaptop)
            imgIconoActivarRoaming.frame.origin.x = imgIconoActivarRoaming.frame.origin.x
            lblActivarRoaming.text = NSLocalizedString("activar_roaming_sin_whatsapp", comment: "mensaje boton roaming sin whatsapp")
            lblActivarRoaming.frame.origin.x = lblActivarRoaming.frame.origin.x

            let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(self.abrirWebWOM))
            viewBtnActivarRoaming.isUserInteractionEnabled = true
            viewBtnActivarRoaming.addGestureRecognizer(tapViewBoton)
        }
    }

    /// Abre web en caso de que no existan numeros de whatsapp activados
    func abrirWebWOM() {
        UtilApp.generarMigaDePan(NSLocalizedString("BTN_ROAMING_WEB", comment: "miga de pan para btn web"))
        if let url = URL(string: Constants.URL.urlRoaming),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }

    /// Genera el combobox de tipos de bolsas
    func generarArregloOpciones() {
        pickerData = (delegate?.obtenerArregloBolsasLabels())!
    }
    /**
     funcion para setear configuraciones al desplegable del combo-box
     */
    func setupDropDown() {
        dropDown.anchorView = botonPicker
        dropDown.dataSource = pickerData

        dropDown.bottomOffset = CGPoint(x: 0, y: heightBotonPicker.constant - 5)
        dropDown.textFont = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (15))!
        dropDown.textColor = DesignHelper.UIColorFromRGB(0x720184)

        dropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedOption(index)
        }
    }
    /**
     funcion para setear en combo-box el seleccionado y cargar bolsas

     - parameter row: fila seleccionada
     */
    func selectedOption(_ row: Int) {
        if dropDown.indexForSelectedRow == nil || self.bolsaSeleccionada != row {

            if dropDown.indexForSelectedRow != nil {
                self.bolsaSeleccionada = dropDown.indexForSelectedRow!
            }

            // altura adicional para el combo-box
            let alturaAdicionalCombo = CGFloat(200)
            botonPicker.setTitle("\(pickerData[row])", for: UIControlState())

            vistaContenedoraBolsas.removeFromSuperview()
            vistaContenedoraBolsas = showOptions(row, columnas: 2)

            let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: vistaContenedoraBolsas.frame.height + alturaAdicionalCombo)
            scrollView.contentSize = frame.size
            scrollView.addSubview(vistaContenedoraBolsas)

            if esRoaming() {
                abrirSeccionRoaming()
            } else {
                dropDown.selectRow(at: bolsaSeleccionada)
                self.vistaContenedoraBolsas.isHidden = false
                self.viewActivarRoaming.isHidden = true
            }
        }
    }
    /**
     funcion donde se generan cajas de bolsas

     - parameter indiceSeleccionado: indice de la bolsa seleccionada.
     - parameter columnas: número de columnas en que se muestran las bolsas.
     - returns: vista contenedora de bolsas.
     */
    func showOptions(_ indiceSeleccionado: Int, columnas: Int) -> UIView {

        totalBolsas = (delegate?.obtenerArregloBolsas(pickerData[indiceSeleccionado]))!
        // altura total de la vista de bolsas
        var heightTotalVista = CGFloat(0)
        var modificadorMultilinea = CGFloat(9)
        var factorAltura = CGFloat(1.7)

        if !esMultilinea {
            modificadorMultilinea = 0
            factorAltura = CGFloat(1.45)
        }

        vistaContenedoraBolsas = UIView()

        if totalBolsas.count != 0 {
            // ancho de vista completa
            let widthVista = self.view.frame.width - modificadorMultilinea
            // espacio en el eje X
            let espacioEjeX = CGFloat(18)
            // espacio en el eje Y
            let espacioEjeY = CGFloat(18)
            // ancho de caja unica de bolsa
            let widthCajaBolsa = ( widthVista - (espacioEjeX * CGFloat(columnas + 1)) ) / CGFloat(columnas)
            // altura de caja unica de bolsa
            let heightCajaBolsa = widthCajaBolsa * factorAltura// Alto de cada opcion.
            // fila en que se esta ubicada la bolsa actual
            var filaUbicacionBolsa = CGFloat(1)
            // posicion donde se posicionará la bolsa
            var posicionEjeY = CGFloat(0)

            if esRoaming() {
                posicionEjeY = CGFloat(50)
                generarSeccionRoaming(ajustarView: false, posicionEjeY: &posicionEjeY, filaUbicacionBolsa: &filaUbicacionBolsa, indiceBolsa: 0)
            }

            var bolsaAnterior = totalBolsas[0]

            for index in 0...(totalBolsas.count - 1) {
                if esRoaming() && bolsaAnterior.tipoProductoBSCS != totalBolsas[index].tipoProductoBSCS {
                    bolsaAnterior = totalBolsas[index]
                    generarSeccionRoaming(ajustarView: true, posicionEjeY: &posicionEjeY, filaUbicacionBolsa: &filaUbicacionBolsa, indiceBolsa: index)
                }

                // posicion del eje x de bolsa
                var posicionEjeX = (espacioEjeX * filaUbicacionBolsa) + (widthCajaBolsa * (filaUbicacionBolsa - 1))
                    + 2

                if index % 2 != 0 {
                    posicionEjeX += 5
                }

                // vista con los datos de la bolsa
                let bolsa = PrototipoBolsa(frame: CGRect(x: posicionEjeX, y: posicionEjeY, width: widthCajaBolsa, height: heightCajaBolsa))

                bolsa.delegate = self
                bolsa.asignarDatos(totalBolsas[index])
                bolsa.indiceBolsa = index

                // Animacion
                bolsa.frame.origin.y += 20
                bolsa.alpha = CGFloat(0)
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    bolsa.frame.origin.y = posicionEjeY
                    bolsa.alpha = CGFloat(100)
                    }, completion: nil)

                vistaContenedoraBolsas.addSubview(bolsa)

                if Int(filaUbicacionBolsa) == columnas {
                    posicionEjeY = (posicionEjeY + espacioEjeY) + heightCajaBolsa
                    heightTotalVista = posicionEjeY
                    filaUbicacionBolsa = 1
                } else {
                    heightTotalVista = posicionEjeY + heightCajaBolsa
                    filaUbicacionBolsa += 1
                }
            }
        }
        // margen desde arriba de la vista contenedora de las bolsas
        let margenTopVistaContenedora = CGFloat(230)
        // altura adicional para la vista contenedora
        let alturaAdicionalVistaContenedora = CGFloat(60)

        vistaContenedoraBolsas.frame = CGRect(x: 0, y: margenTopVistaContenedora, width: self.view.frame.width, height: heightTotalVista + alturaAdicionalVistaContenedora)

        return vistaContenedoraBolsas
    }

    /// Abre el pop up de listado de paises
    ///
    /// - Parameter sender: Objeto de boton
    func abrirListadoPaises(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        let popupListadoPaises = storyboard.instantiateViewController(withIdentifier: "listadoPaisesRoaming") as! ListadoPaisesRoaming
        popupListadoPaises.delegate = self
        if (sender as? String) == nil {
            var vista = UIView()
            vista = sender.view

            var labelRoaming = UILabel()
            labelRoaming = vista.superview?.viewWithTag(1) as! UILabel
            popupListadoPaises.modoListado = true
            popupListadoPaises.tituloRoaming = labelRoaming.text!
        } else {
            var tipoRoaming = String()
            tipoRoaming = sender as! String
            popupListadoPaises.modoListado = false
            popupListadoPaises.tituloRoaming = Constants.Hashmap.tipoRoaming[tipoRoaming]!
        }

        self.addChildViewController(popupListadoPaises)
        popupListadoPaises.view.frame = self.view.frame
        self.view.addSubview(popupListadoPaises.view)
        popupListadoPaises.didMove(toParentViewController: self)

        if esMultilinea {
            UtilMultilinea.ajustarPopupInterno(esMultilinea: esMultilinea, popupController: popupListadoPaises, controllerPopupMultilinea: controllerPopupMultilinea!)
        }

        self.navigationItem.setHidesBackButton(true, animated: true)
    }

    /// Genera seccion de roaming con seccion paises
    ///
    /// - Parameters:
    ///   - ajustarView: indicador si se debe ajustar la vista
    ///   - posicionEjeY: posicion del eje y en donde se dbe ubicar la vista
    ///   - filaUbicacionBolsa: ubicacion de la vista en que se encuentra
    func generarSeccionRoaming(ajustarView: Bool, posicionEjeY: inout CGFloat, filaUbicacionBolsa: inout CGFloat, indiceBolsa: Int) {
        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "seccionBolsaRoaming") as! SeccionBolsaRoaming
        let tituloSeccion = controller.view.viewWithTag(1) as! UILabel
        let btnVerPaises = controller.view.viewWithTag(2)!
        let ajusteBotonPaisesMultilinea = CGFloat(35)

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(abrirListadoPaises))
        btnVerPaises.isUserInteractionEnabled = true
        btnVerPaises.addGestureRecognizer(tapViewBoton)

        switch totalBolsas[indiceBolsa].tipoProductoBSCS {
        case Constants.Indicadores.roamingPreferente:
            tituloSeccion.text = NSLocalizedString("titulo_seccion_roaming_preferente", comment: "titulo seccion preferente")
        case Constants.Indicadores.roamingEuropa:
            tituloSeccion.text = NSLocalizedString("titulo_seccion_roaming_europa", comment: "titulo seccion europa")
        case Constants.Indicadores.roamingRestoDelMundo:
            tituloSeccion.text = NSLocalizedString("titulo_seccion_roaming_resto_mundo", comment: "titulo seccion resto del mundo")
            btnVerPaises.isHidden = true
        default:
            Logger.log()
        }

        if esMultilinea {
            btnVerPaises.frame.origin.x = btnVerPaises.frame.origin.x - ajusteBotonPaisesMultilinea
            if Globales.isIphone5 {
                tituloSeccion.font = tituloSeccion.font.withSize(16)
            }
        }

        let viewSeccion = controller.view
        let viewFinal = UIView(frame: (viewSeccion?.frame)!)
        viewSeccion?.frame.size.height = CGFloat(45)
        viewFinal.addSubview(viewSeccion!)

        if ajustarView {
            viewFinal.frame.origin.y = posicionEjeY
            posicionEjeY = posicionEjeY + CGFloat(50)
            filaUbicacionBolsa = 1
        }

        vistaContenedoraBolsas.addSubview(viewFinal)
    }
    /**
     Acción al seleccionar el combo-box.

     - parameter sender: elemento que envia la accion.
     */
    @IBAction func seleccionarCombo(_ sender: AnyObject) {
        dropDown.show()
    }

    /// Accion de cerrar popup en multilinea
    ///
    /// - Parameter sender: objeto que envia accion
    @IBAction func btnCerrarPopupMultilinea(_ sender: Any) {
        controllerPopupMultilinea?.cerrarPopup()
    }
    /**
     Función que cierra pop-up de comprar bolsa con animación.

     - parameter mostrarBack: indicador para mostrar boton back en navegacion.
     */
    func cerrarPopupComprarBolsa(_ mostrarBack: Bool) {
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // escala inicial de la vista
        let escalaInicialVista = CGFloat(1.3)
        // cantidad de subviews al tener error general
        let popupErrorGeneral = 5
        // indice de subview de error general
        let indiceErrorGeneral = 4

        Analytics.logEvent("event_wom", parameters: [
            "linea": Globales.numeroTelefono,//numero de telefono
            "segmento": Globales.datosCliente.tipoPlan,
            "canal":"app",
            "fecha": Globales.fechaHoy,
            "funcionalidad": "COMPRA_BOLSA_PASO2_BTN_CERRAR",
            "dispositivo": "IOS",
            "tipo_bolsa": Globales.tipoBolsa,
            "nombre_bolsa":Globales.nombreBolsa,
            "monto_bolsa":Globales.montoBolsa
            ])


        if self.popupController.view.subviews.count == popupErrorGeneral {
            UIView.animate(withDuration: duracionAnimacion, animations: {
                self.popupController.view.subviews[indiceErrorGeneral].transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
                self.popupController.view.subviews[indiceErrorGeneral].alpha = 0.0
                }, completion: {(finished: Bool)  in
                    if finished {
                        self.popupController.view.subviews[indiceErrorGeneral].removeFromSuperview()
                    }
            })
        } else {
            UIView.animate(withDuration: duracionAnimacion, animations: {
                self.popupController.view.transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
                self.popupController.view.alpha = 0.0

                if self.esRoaming() {
                    self.popupListadoPaises.view.transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
                    self.popupListadoPaises.view.alpha = 0.0
                }
                }, completion: {(finished: Bool)  in
                    if finished {
                        self.popupController.willMove(toParentViewController: nil)
                        self.popupController.view.removeFromSuperview()
                        self.popupController.removeFromParentViewController()
                        if self.esRoaming() {
                            self.popupListadoPaises.willMove(toParentViewController: nil)
                            self.popupListadoPaises.view.removeFromSuperview()
                            self.popupListadoPaises.removeFromParentViewController()
                        }
                        if mostrarBack {
                            self.navigationItem.setHidesBackButton(false, animated:true)
                        }
                    }
            })
        }
    }
}

extension SeleccionBolsaIphone : ProtocoloSeleccionBolsaIphone {
    /**
     Abre el pop-up para comprar bolsa seleccionada.

     - parameter indice: Indice de bolsa que se abrirá.
     */
    func abrirPopupComprarBolsa(_ indice: Int) {
        showingBack = false
        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        if esRoaming() {
            popupListadoPaises = storyboard.instantiateViewController(withIdentifier: "listadoPaisesRoaming") as! ListadoPaisesRoaming

            popupListadoPaises.delegate = self
            popupListadoPaises.modoListado = false
            popupListadoPaises.tituloRoaming = Constants.Hashmap.tipoRoaming[totalBolsas[indice].tipoProductoBSCS]!

            self.addChildViewController(popupListadoPaises)
            popupListadoPaises.view.frame = self.view.frame
            self.view.addSubview(popupListadoPaises.view)
            popupListadoPaises.didMove(toParentViewController: self)

            if esMultilinea {
                UtilMultilinea.ajustarPopupInterno(esMultilinea: esMultilinea, popupController: popupListadoPaises, controllerPopupMultilinea: controllerPopupMultilinea!)
            }

            backView = popupListadoPaises.viewContenido
        }

        popupController = storyboard.instantiateViewController(withIdentifier: "popUpComprarBolsa") as! PopupComprarBolsaIphone

        popupController.delegate = self
        popupController.setBolsa(totalBolsas[indice])
        popupController.cliente = cliente
        popupController.esMultilinea = esMultilinea

        self.addChildViewController(popupController)
        popupController.view.frame = self.view.frame
        self.view.addSubview(popupController.view)
        popupController.didMove(toParentViewController: self)

        if esMultilinea {
            popupController.controllerPopupMultilinea = controllerPopupMultilinea!
            popupController.generarVistaMultilinea()
            UtilMultilinea.ajustarPopupInterno(esMultilinea: esMultilinea, popupController: popupController, controllerPopupMultilinea: controllerPopupMultilinea!)
        }

        if esRoaming() {
            popupController.view.backgroundColor = UIColor.clear
            frontView = popupController.viewContenido
        }

        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    /**
     Abre pop-up con mensaje de confirmación de compra de bolsa.
     */
    func abrirPopupMensajeComprarBolsa() {
        cerrarPopupComprarBolsa(false)

        popupMensajeController = UIStoryboard(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "popUpMensajeComprarBolsa") as! PopupMensajeComprarBolsaIphone
        popupMensajeController.delegate = self

        if esMultilinea {
            popupMensajeController.controllerPopupMultilinea = controllerPopupMultilinea!
            popupMensajeController.cliente = cliente
            popupMensajeController.esMultilinea = esMultilinea
        }

        self.addChildViewController(popupMensajeController)
        popupMensajeController.view.frame = self.view.frame
        self.view.addSubview(popupMensajeController.view)
        popupMensajeController.didMove(toParentViewController: self)

        popupMensajeController.generarVistaMultilinea()

        if esMultilinea {
            UtilMultilinea.ajustarPopupInterno(esMultilinea: esMultilinea, popupController: popupMensajeController, controllerPopupMultilinea: controllerPopupMultilinea!)
        }

        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    /**
     Reaparece el botón de la navegación para atrás.
     */
    func recuperarBotonBack() {
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    /**
     Cierra la actual vista.
    */
    func cerrarSeleccionTipoBolsas() {
        navigationController!.popViewController(animated: true)
        delegate?.actualizarIndicadorRecargaServicios()
    }

    /// Realiza cambio de vistas por medio de animacion flip
    ///
    /// - Parameter tipoBolsa: tipo bolsa roaming
    func flipAListadoPaises(tipoBolsa: String) {
        var transitionOptions: UIViewAnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]

        if showingBack {
            transitionOptions = [.transitionFlipFromRight, .showHideTransitionViews]
        } else {
            transitionOptions = [.transitionFlipFromLeft, .showHideTransitionViews]
        }

        let toView = showingBack ? frontView : backView
        let fromView = showingBack ? backView : frontView

        UIView.transition(with: fromView, duration: 0.5, options: transitionOptions, animations: {
            fromView.isHidden = true
        }, completion: { _ in
            if self.showingBack {
                self.popupController.view.isHidden = true
            }
        })

        UIView.transition(with: toView, duration: 0.5, options: transitionOptions, animations: {
            self.popupController.view.isHidden = false
            toView.isHidden = false
        })

        showingBack = !showingBack
    }


    /// Abre pop up con confirmacion de compra bolsa roaming
    func abrirConfirmacionCompraRoaming() {
        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        let popupConfirmacionRoaming = storyboard.instantiateViewController(withIdentifier: "confirmacionCompraBolsaRoaming") as! ConfirmacionCompraBolsaRoaming

        popupConfirmacionRoaming.delegate = self
        popupConfirmacionRoaming.controllerPopupMultilinea = controllerPopupMultilinea!
        popupConfirmacionRoaming.esMultilinea = esMultilinea

        self.addChildViewController(popupConfirmacionRoaming)
        popupConfirmacionRoaming.view.frame = self.view.frame
        self.view.addSubview(popupConfirmacionRoaming.view)
        popupConfirmacionRoaming.didMove(toParentViewController: self)

        if esMultilinea {
            UtilMultilinea.ajustarPopupInterno(esMultilinea: esMultilinea, popupController: popupConfirmacionRoaming, controllerPopupMultilinea: controllerPopupMultilinea!)
        }

        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    /**
     Acción para cerrar popup para comprar bolsa.

     - parameter sender: elemento que envia la accion.
     */
    func cerrarPopup() {
        cerrarPopupComprarBolsa(true)
    }
}
