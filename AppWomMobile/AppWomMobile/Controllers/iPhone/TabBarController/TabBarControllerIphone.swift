//
//  TabBarControllerIphone.swift
//  AppWomMobile
//
//  Clase controlador Tab de home Prepago.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import Firebase

class TabBarControllerIphone: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        self.delegate = self
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case Constants.Tags.tabHome:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_MENU_CONSUMO", comment: "marca home consumo"))
        case Constants.Tags.tabCompraBolsas:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_MENU_BOLSAS", comment: "marca menu bolsas"))
        case Constants.Tags.tabNewWomer:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_MENU_PERFIL", comment: "marca menu perfil"))
        case Constants.Tags.tabAyuda:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_MENU_AYUDA", comment: "marca home menu ayuda"))
        default:
            Logger.log("default seleccion tab")
        }
    }
}
