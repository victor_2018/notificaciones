//
//  ErrorGeneral.swift
//  AppWomMobile
//
//  Controlador de error general en la aplicación.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class ErrorGeneral: UIViewController {
    // vista de contenido de popup
    @IBOutlet weak var viewContenido: UIView!
    // texto informativo del error
    @IBOutlet weak var texto: UILabel!
    // boton para reintentar acción
    @IBOutlet weak var btnReintentar: UIButton!

    // duracion de la animacion
    let duracionAnimacion = 0.25
    // escala inicial de la vista
    let escalaInicialVista = CGFloat(1.3)
    var textoError = String()
    // delegate a controlador que arroja error
    weak var delegate: UIViewController?

    override func viewDidLoad() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista

        if (delegate as? PromoCompraBolsaIphone) != nil || (delegate as? PromoInformativaIphone) != nil {
            btnReintentar.isHidden = true
        }
        if textoError != "" {
            texto.text = textoError
        }
        mostrarAnimacion()
    }

    /**
     Muestra la animación de la vista del pop-up
     */
    func mostrarAnimacion() {
        // escala con la animación inicial
        let escalaAnimacionInicial = CGFloat(1.3)
        // escala con la animación final
        let escalaAnimacionFinal = CGFloat(1.0)
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // transparencia inicial de la vista
        let transparenciaInicial = CGFloat(0.0)
        // transparencia final de la vista
        let transparenciaFinal = CGFloat(1.0)

        self.view.transform = CGAffineTransform(scaleX: escalaAnimacionInicial, y: escalaAnimacionInicial)
        self.view.alpha = transparenciaInicial

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.view.alpha = transparenciaFinal
            self.view.transform = CGAffineTransform(scaleX: escalaAnimacionFinal, y: escalaAnimacionFinal)
        }, completion: {(finished: Bool)  in
        })
    }

    /**
     Función que cierra pop-up de error general.
     */
    func cerrarVista() {
        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.view.transform = CGAffineTransform(scaleX: self.escalaInicialVista, y: self.escalaInicialVista)
            self.view.alpha = 0.0
            }, completion: {(finished: Bool)  in
                if finished {
                    self.willMove(toParentViewController: nil)
                    self.view.removeFromSuperview()
                    self.removeFromParentViewController()
                }
        })
    }
    /**
     Acción de boton Reintentar.
     - parameter sender: referencia al boton reintentar.
     */
    @IBAction func reintentarAccion(_ sender: AnyObject) {
        cerrarVista()
        if (delegate as? PopupComprarBolsaIphone) != nil {
            (delegate as! PopupComprarBolsaIphone).comprarBolsa("" as AnyObject)
        }
    }
    /**
     Acción de boton Volver.
     - parameter sender: referencia al boton volver.
     */
    @IBAction func cerrarVistaVolver(_ sender: AnyObject) {
        cerrarVista()
    }
}
