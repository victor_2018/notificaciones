//
//  InicioNewWomerIphone.swift
//  AppWomMobile
//
//  Controlador donde se muestra web view de new womer.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import WebKit

class InicioNewWomerIphone: UIViewController, UIWebViewDelegate, UIScrollViewDelegate {
    // Enlace a webView donde se cargará página.
    @IBOutlet weak var webView: UIWebView!
    // Enlace a webView externo donde se cargarán páginas externas a new womer.
    @IBOutlet weak var webViewExterno: UIWebView!
    // Enlace a vista contenedora del webView.
    @IBOutlet weak var viewContenedora: UIView!

    // puente que se crea entre nativo y new womer.
    var bridge: SwiftWebViewBridge!
    // booleando que indica si el teclado se encuentra escondido o no
    var tecladoEscondido = true
    // url donde se guarda la vista actual en new womer
    var urlNavegacionActual = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        Util.agregarActivityIndicator(self.view)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0

        let url = URL(string: "\(URLServicios.newWomer)\(Globales.numeroTelefono)")

        let requestObj = URLRequest(url: url!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)

        webView.delegate = self

        bridge = SwiftWebViewBridge.bridge(webView, defaultHandler: { mensaje, responseCallback in
            let mensajeRecibido = String(describing: mensaje)
            Logger.log("Mensaje recibido desde JS: \(mensajeRecibido)")

            UtilApp.validarToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, password: Globales.IMSI, realm: Constants.Login.realmServicios, onCompletion: {
                retorno in
                if retorno {
                    responseCallback(["msg": Globales.tokenApp.token])
                } else {
                    Logger.log("error carga Token")
                    responseCallback(["msg": ""])
                }
            })
        })
        webView.allowsInlineMediaPlayback = true
        webView.loadRequest(requestObj)
        webView.scrollView.delegate = self
        webView.scrollView.bounces = false
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.scrollView.isScrollEnabled = false

        webViewExterno.scrollView.delegate = self
        webViewExterno.scrollView.bounces = false
        webViewExterno.scrollView.showsVerticalScrollIndicator = false

    }

    /**
     Función que realiza acciones al mostrar teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillShow(_ notification: Notification) {
        let movimientoSuperior = CGFloat(85)
        if tecladoEscondido {
            tecladoEscondido = false
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.view.frame.origin.y -= keyboardSize.height - movimientoSuperior
            }
        }
    }
    /**
     Función que realiza acciones al esconder teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillHide(_ notification: Notification) {
        if !tecladoEscondido {
            tecladoEscondido = true
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }

    /**
     Función para cerrar la página desde la cruz en la página.

     - parameter webView: webView donde se abre la página.
     - parameter request: request con url de redirección.
     - parameter navigationType: tipo de navegación.
     - returns: booleano que indica si se realiza la acción de redirección en request.
     */
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        var url = String(describing: request.url!)

        print("\nLink request: \(url)\n")

        if url.range(of: "ios-log") != nil {
            url = url.replacingOccurrences(of: "%20", with: " ")
            url = url.replacingOccurrences(of: "%23", with: " ")
            url = url.replacingOccurrences(of: "%22", with: " ")
            print("\nLOG: \(url)\n\n")
            return false
        }

        if url.range(of: "ret=login&logger_id") != nil {
            let requestObj = URLRequest(url: URL(string: urlNavegacionActual)!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)

            webView.loadRequest(requestObj)
            return false
        }

        if url.range(of: "twitter.com/intent/tweet") != nil || url.range(of: "twitter.com/intent/follow") != nil {
            let requestObj = URLRequest(url: URL(string: url)!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)

            webViewExterno.isHidden = false
            webViewExterno.loadRequest(requestObj)
            return false
        }

        if url.range(of: "salir.html") != nil {
            self.dismiss(animated: true, completion: nil)
            return false
        } else if url.range(of: "/staticxx.facebook.com") != nil || url.range(of: "www.facebook.com/") != nil || url.range(of: "platform.twitter.com") != nil || url.range(of: "m.facebook.com/") != nil || url.range(of: "api.twitter.com/oauth/") != nil {
            print("^\n|\nAbriendo en interno request\n")
            return true
        } else if url.range(of: "api.twitter.com/login/error") != nil {
            return false
        } else if !(url.range(of: "newwomer/") != nil && url.range(of: "referer") == nil) {
            print("^\n|\nAbriendo en externo request\n")
            let urlNueva = URL(string: url)!
            UIApplication.shared.openURL(urlNueva)
            return false
        }

        urlNavegacionActual = url
        return true
    }

    /**
     Función que realiza acciones al comenzar la carga de la página.

     - parameter webView: webView donde se abre la página
     */
    func webViewDidStartLoad(_ webView: UIWebView) {
        Logger.log("Webview Cargando...")
    }
    /**
     Función que realiza acciones al terminar la carga de la página.

     - parameter webView: webView donde se abre la página
     */
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Logger.log("Webview terminó de cargar")
        Util.removerActivityIndicator(self.view)
    }
    /**
     Función que realiza acciones al fallar la carga de la página.

     - parameter webView: webView donde se abre la página
     - parameter error: error específico
     */
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Util.removerActivityIndicator(self.view)
        Logger.log("Error al cargar webview \(error.localizedDescription)")
        UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("PERFIL_WOMER_WEBVIEW_ONLOAD", comment: "miga de pan web view perfil womer"))
        Util.alertErrorGeneral(self)
    }

    /**
     Función para evitar que realice zoom dentro del webview
     - parameter scrollView: scrollview dentro del webview
     - returns: nil para evitar zoom
     */
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    /**
     Accion al presionar "X" para cerrar vista
     - parameter sender: boton que realiza accion
     */
    @IBAction func cerrarVista(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
