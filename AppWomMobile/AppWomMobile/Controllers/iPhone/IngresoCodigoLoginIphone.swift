//
//  IngresoCodigoLoginIphone.swift
//  AppWomMobile
//
//  Controlador donde se ingresa el codigo enviado por sms.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}



class IngresoCodigoLoginIphone: UIViewController, UITextFieldDelegate {
    // Enlace a textfield donde se ingresa el código
    @IBOutlet weak var txtfieldCodigo: UITextField!
    // Enlace a label con mensaje de error
    @IBOutlet weak var mensajeError: UILabel!
    // Enlace a label con icono de error
    @IBOutlet weak var iconoError: UIImageView!
    // Enlace a view de linea separadora textfiel
    @IBOutlet weak var lineaError: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_PASO3_ONLOAD", comment: " marca de ingreso a paso 3 onload"))

        txtfieldCodigo.delegate = self

        addDoneButtonOnKeyboard()
        // esconde teclado al presionar fuera de él.
        self.hideKeyboardWhenTappedAround()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        txtfieldCodigo.addTarget(self, action: #selector(IngresoCodigoLoginIphone.textFieldDidChange), for: .editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(self.willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
    }
    /**
     Función que se activa al presionar boton home de dispositivo.

     - parameter notification: notificacion de reproductor.
     */
    func willResignActive(_ notification: Notification) {
        self.view.endEditing(true)
    }
    /**
     Función que se llama al realizar un cambio en el textfield del codigo.
     */
    func textFieldDidChange () {
        let attributedString = NSMutableAttributedString(string: txtfieldCodigo.text!)
        attributedString.addAttribute(NSKernAttributeName, value: 7, range: NSRange(location: 0, length: txtfieldCodigo.text!.characters.count))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSRange(location: 0, length: txtfieldCodigo.text!.characters.count))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: Constants.TipoFuente.ceraBold, size: 16)!, range: NSRange(location: 0, length: txtfieldCodigo.text!.characters.count))

        txtfieldCodigo.attributedText = attributedString
    }
    /**
     Función que se llama al aparecer el teclado del telefono.

     - parameter notification: notificacion.
     */
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height
        }
    }
    /**
     Función que se llama al aparecer el teclado del telefono.

     - parameter notification: notificacion.
     */
    func keyboardWillHide(_ notification: Notification) {
        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    /**
     Función que se llama al aparecer el teclado del telefono.

     - parameter textField: textfield donde se escribe el codigo.
     - parameter rango: rango de textfield.
     - parameter string: texto a validar.
     - returns: booleano que indica si se debe realizar el cambio.
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn rango: NSRange, replacementString string: String) -> Bool {
        let maximoCaracteres = 4

        if txtfieldCodigo.text?.characters.count < maximoCaracteres || string.characters.count == 0 {
            return true
        } else {
            return false
        }
    }
    /**
     Función que agrega el botón aceptar en el toolbar del teclado.
     */
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("boton_aceptar", comment: "boton aceptar de toolbar"), style: UIBarButtonItemStyle.done, target: self, action: #selector(IngresoCodigoLoginIphone.abrirVerificarCodigo))

        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)

        doneToolbar.items = items
        doneToolbar.sizeToFit()
        doneToolbar.barTintColor = UIColor.white

        self.txtfieldCodigo.inputAccessoryView = doneToolbar
    }
    /**
     Funcion para abrir vista donde se ingresa el código validador.
     */
    func abrirVistaValidarCodigo() {
        self.view.endEditing(true)
        self.esconderError()

        if Globales.modoTest {
            let resultController = self.storyboard!.instantiateViewController(withIdentifier: "validarCodigo")
            self.present(resultController, animated: true, completion: nil)
        } else {
            Util.agregarActivityIndicator(self.view)
            UtilServicios.obtenerIMSI(txtfieldCodigo.text!, onCompletion: { retorno, error in
                if retorno {
                    UtilServicios.obtenerToken(Constants.Login.clientIdServicios, clientSecret: Constants.Login.clientSecretServicios, numeroTelefono: Globales.numeroTelefono, password: Globales.IMSI, grantType: Constants.Login.grantType, refreshToken: "", realm: Constants.Login.realmServicios) { retorno in
                        DispatchQueue.main.async(execute: {
                            if retorno {
                                let resultController = self.storyboard!.instantiateViewController(withIdentifier: "validarCodigo")
                                self.present(resultController, animated: true, completion: nil)
                            } else {
                                Util.removerActivityIndicator(self.view)
                                Util.alertErrorGeneral(self)
                                UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_PASO3_ONLOAD", comment: " marca de ingreso a paso 3 onload"))
                            }
                        })
                    }
                } else if error != 0 {
                    Util.removerActivityIndicator(self.view)
                    self.mostrarError()
                } else {
                    Util.removerActivityIndicator(self.view)
                    Util.alertErrorGeneral(self)
                    UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_PASO3_ONLOAD", comment: " marca de ingreso a paso 3 onload"))

                }
            })
        }

    }
    /**
     Accion al apretar boton "Verificar"
     - parameter sender: boton que envia la accion.
     */
    @IBAction func abrirVerificarCodigo(_ sender: AnyObject) {

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_VERIFICACION_SMS", comment: " marca de verificacion sms"))

        if validarCodigo(txtfieldCodigo.text!) {
            esconderError()
            abrirVistaValidarCodigo()
        } else {
            mostrarError()
        }
    }

    /// Acción al presionar botón reenviar, que solicite el codigo de verificación.
    ///
    /// - Parameter sender: objeto de boton que envia la accion
    @IBAction func reenviarCodigo(_ sender: AnyObject) {

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_REENVIO_SMS", comment: " marca de verificacion sms"))

        self.view.endEditing(true)
        Util.agregarActivityIndicator(self.view)
        txtfieldCodigo.text = ""

        // Una vez obtenido token se envia sms a womer con el siguiente servicio
        UtilServicios.solicitarCodigoVerificador(Globales.numeroTelefono) { retorno in
            if retorno {
                let alert = UIAlertController(title: "Reenvio de SMS", message: "El código de verificación ha sido enviado a tu teléfono", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                Util.removerActivityIndicator(self.view)
            } else {
                Util.removerActivityIndicator(self.view)
            }
        }
    }

    /**
     Funcion que valida código ingresado.
     - parameter codigo: código para validar.
     - returns: booleano que indica si pasó validación.
     */
    func validarCodigo(_ codigo: String) -> Bool {
        if codigo.characters.count < 4 {
            return false
        } else {
            return true
        }
    }
    /**
     Muestra indicadores de error en la vista
     */
    func mostrarError() {
        mensajeError.isHidden = false
        iconoError.isHidden = false
        lineaError.backgroundColor = DesignHelper.UIColorFromRGB(0xF5A623)
    }

    /**
     Esconde indicadores de error en la vista
     */
    func esconderError() {
        mensajeError.isHidden = true
        iconoError.isHidden = true
        lineaError.backgroundColor = UIColor.white
    }
}
