//
//  CustomTextFieldPadding.swift
//  AppWomMobile
//
//  Controlador de clase textfield con codigo con separaciones.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CustomTextFieldPadding: UITextField {
    var padding = CGFloat(25)

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + padding, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + padding, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height)
    }
}
