//
//  PersonalizaPerfilHomeIphone.swift
//  AppWomMobile
//
//  Created by Tinet on 22-02-17.
//  Copyright © 2017 Tinet. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import FacebookLogin
import FacebookCore
import FBSDKShareKit
import Accounts
import Firebase

// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}


class PersonalizaPerfilHomeIphone: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FBSDKLoginButtonDelegate {
    // Enlace a textfield de nombre
    @IBOutlet weak var txtfieldNombre: UITextField!
    // Enlace a textfield de apellido
    @IBOutlet weak var txtfieldApellido: UITextField!
    /// Enlace a imagen de perfil
    @IBOutlet weak var imagenPerfil: UIImageView!
    /// Boton que realiza la conexión a facebook
    @IBOutlet weak var btnConectarFacebook: UIButton!
    /// Boton que realiza la desconexión a facebook
    @IBOutlet weak var btnDesconectarFacebook: UIButton!
    @IBOutlet weak var seguidoresFacebook: UILabel!
    @IBOutlet weak var seguidoresTwitter: UILabel!
    @IBOutlet weak var btnFacebookLike: UIButton!
    @IBOutlet weak var btnSeguirTwitter: UIButton!
    @IBOutlet weak var btnSeguidoTwitter: UIButton!
    @IBOutlet weak var btnDesconectarTwitter: UIButton!
    @IBOutlet weak var btnConectarTwitter: UIButton!

    @IBOutlet weak var vistaContenedora: UIView!
    // Altura que se resta a movimiento de vista al abrir teclado
    var movimientoAdicional = CGFloat(85)
    // booleano que indica si el teclado se encuentra escondido o no
    var tecladoEscondido = true
    let likeButton: FBSDKLikeControl = FBSDKLikeControl()
    let loginButtonFace = FBSDKLoginButton()
    let client = TWTRAPIClient()
    var twUrl: URL = URL(string: "twitter://user?screen_name=SeemuApps")!
    var twUrlWeb: URL = URL(string: "https://twitter.com/womchile")!
    var fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
    var idFBWOM = String()
    var follow = false
    // Llave para acceder a la ruta de la imagen del perfil guardada
    let imagenPerfilKey = "pathImagenPerfil"
    // Ruta donde se guardan las imágenes
    var rutaDirectorioImagenes: String!

    override func viewDidDisappear(_ animated: Bool) {
        Globales.nombreCliente = txtfieldNombre.text!
        Globales.apellidoCliente = txtfieldApellido.text!

        if PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.validacionAvance) != nil {
            PlistManager.sharedInstance.saveValue(Globales.nombreCliente as AnyObject, forKey: Constants.KeyPlist.nombreCliente)
            PlistManager.sharedInstance.saveValue(Globales.apellidoCliente as AnyObject, forKey: Constants.KeyPlist.apellidoCliente)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)
        imagenPerfil.image = Globales.imagenPerfil
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("HOME_PERSONALIZA_PERFIL_ONLOAD", comment: "marca home personaliza perfil onload"))

        let sizeFuente = CGFloat(14)

        if Globales.imagenPerfilDefecto == Globales.imagenPerfil {
            Globales.imagenPerfil = UIImage(named: Constants.Imagenes.imgAvatarPerfilDefecto)
        }

        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)

        txtfieldNombre.text = Globales.nombreCliente
        txtfieldApellido.text = Globales.apellidoCliente
        txtfieldNombre.delegate = self
        txtfieldApellido.delegate = self

        self.hideKeyboardWhenTappedAround()

        addDoneButtonOnKeyboard()

        txtfieldNombre.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("label_nombre", comment: "label en textfield nombre"), attributes:[NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraLight, size: sizeFuente)!])
        txtfieldApellido.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("label_apellido", comment: "label en textfield apellido"), attributes:[NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraLight, size: sizeFuente)!])

        if !Globales.isIphone5 {
            movimientoAdicional = 0
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

        if validarSesionTwitter() {
            verificarSeguidorTwitter()
        }
        obtenerSeguidoresTwitter()
        validarSesionFacebook()
        agregarBotonFacebook()
        obtenerSeguidoresFacebook()
    }

    /// Verifica si cliente es seguidor de Twitter WOM
    func verificarSeguidorTwitter() {
        //si hay session en twitter ir a preguntar por si es seguidor
        if Globales.loginTwitter {
            self.esSeguidor { retorno in
                DispatchQueue.main.async(execute: {
                    if retorno {
                        self.btnSeguidoTwitter.isHidden = false
                        self.btnSeguirTwitter.isHidden = true
                        self.follow = true
                    } else {
                        // llamar a seguir en twiter
                        self.follow = false
                        self.btnSeguidoTwitter.isHidden = true
                        self.btnSeguirTwitter.isHidden = false
                    }
                })
            }
        }
    }

    /// Obtiene la cantidad de seguidores de Twitter WOM
    func obtenerSeguidoresTwitter() {
        // trae los seguidores de womchile twitter
        let urlConnectionTwitter = "https://api.twitter.com/1.1/users/show.json?screen_name=womchile"
        let params = NSMutableDictionary()
        var clientError: NSError?
        let request = client.urlRequest(withMethod: "GET", url: urlConnectionTwitter, parameters: params as? [AnyHashable: Any], error: &clientError)

        if Twitter.sharedInstance().sessionStore.session() != nil {
            let userID = Twitter.sharedInstance().sessionStore.session()!.userID
            let client = TWTRAPIClient(userID: userID)

            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError == nil {
                    let seguidores = UtilApp.obtenerSeguidoresTwitter(data!)
                    self.seguidoresTwitter.text = Util.formateaFollow(seguidores as NSNumber)
                } else {
                    Logger.log("Error: \(connectionError ?? "" as! Error)")
                }
            }
        } else {
            let client = TWTRAPIClient()

            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError == nil {

                    let seguidores = UtilApp.obtenerSeguidoresTwitter(data!)
                    self.seguidoresTwitter.text = Util.formateaFollow(seguidores as NSNumber)

                    let url = URL(string: "https://api.twitter.com")!
                    let cookies = HTTPCookieStorage.shared.cookies(for: url)!
                    for cookie: HTTPCookie in cookies {
                        HTTPCookieStorage.shared.deleteCookie(cookie)
                    }

                    if client.userID != nil {
                        Twitter.sharedInstance().sessionStore.logOutUserID(client.userID!)
                    }
                } else {
                    Logger.log("Error: \(connectionError ?? "" as! Error)")
                }
            }
        }
    }

    /// Agrega boton de facebook nativo
    func agregarBotonFacebook() {
        likeButton.objectID = "https://www.facebook.com/womchile"
        likeButton.isHidden = true
        likeButton.likeControlStyle = .boxCount
        self.view.addSubview(likeButton)
    }

    /// Obtiene cantidad de seguidores de facebook de WOM
    func obtenerSeguidoresFacebook() {

        if FBSDKAccessToken.current() != nil {
            FBSDKGraphRequest(graphPath: "/womchile", parameters: ["fields": "id, name, fan_count"]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    let fbDetails = result as! NSDictionary

                    self.idFBWOM = fbDetails.value(forKey: "id") as! String
                    self.seguidoresFacebook.text = Util.formateaFollow(fbDetails.value(forKey: "fan_count") as! NSNumber)
                    self.obtenerEstadoLike()
                } else {
                    if self.tabBarController != nil {
                        Util.removerActivityIndicator(self.tabBarController!.view)
                    }
                    Logger.log("Error al obtener fans de wom")
                }
            })
        } else {
            FBSDKAccessToken.setCurrent(FBSDKAccessToken.init(tokenString: Constants.RRSSIdentificador.tokenFacebook, permissions: ["email", "user_likes", "user_friends", "public_profile"], declinedPermissions: [], appID: Constants.RRSSIdentificador.appId, userID: "", expirationDate: nil, refreshDate: nil))

            if FBSDKAccessToken.current() != nil {
                FBSDKGraphRequest(graphPath: "/womchile", parameters: ["fields": "id, name, fan_count"]).start(completionHandler: { (connection, result, error) -> Void in
                    if error == nil {
                        let fbDetails = result as! NSDictionary

                        self.idFBWOM = fbDetails.value(forKey: "id") as! String
                        self.seguidoresFacebook.text = Util.formateaFollow(fbDetails.value(forKey: "fan_count") as! NSNumber)

                        FBSDKAccessToken.setCurrent(nil)
                        self.obtenerEstadoLike()
                    } else {
                        Logger.log("Error al obtener fans de wom")
                    }
                    FBSDKAccessToken.setCurrent(nil)
                    if self.tabBarController != nil {
                        Util.removerActivityIndicator(self.tabBarController!.view)
                    }
                })
            } else {
                FBSDKAccessToken.setCurrent(nil)
                Util.removerActivityIndicator(self.tabBarController!.view)
                Logger.log("Error al obtener fans de wom")
            }

        }

    }


    /// Obtiene el estado del "me gusta" del usuario
    func obtenerEstadoLike() {
        if FBSDKAccessToken.current() != nil {
            let request = FBSDKGraphRequest(graphPath: "/me/likes/" + idFBWOM, parameters: ["fields": "name"])
            request?.start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    let fbDetails = result as! NSDictionary

                    let myBoard: NSArray = fbDetails.value(forKey:"data") as! NSArray
                    if myBoard.count > 0 {
                        self.btnFacebookLike.setBackgroundImage(UIImage(named: "fb_liked"), for: UIControlState())
                    } else {
                        self.btnFacebookLike.setBackgroundImage(UIImage(named: "fb_like"), for: UIControlState())
                    }
                } else {
                    Logger.log("Error al obtener likes")
                }
                if self.tabBarController != nil {
                    Util.removerActivityIndicator(self.tabBarController!.view)
                }
            })
        } else {
            Util.removerActivityIndicator(self.tabBarController!.view)
            self.btnFacebookLike.setBackgroundImage(UIImage(named: "fb_like"), for: UIControlState())
            Logger.log("token no valido")
        }
    }

    /// Función autogenerada para abrir imágenes de celular
    ///
    /// - Parameters:
    ///   - picker: controller de seleccionador
    ///   - info: información de la selección en imágen
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagenSeleccionada = info[UIImagePickerControllerOriginalImage] as? UIImage {
            Globales.imagenPerfilTemporal = imagenSeleccionada

            dismiss(animated: true, completion: nil)

            let verificacionImagenPerfil = storyboard!.instantiateViewController(withIdentifier: "verificarImagenPerfil") as! VerificacionImagenPerfilIphone

            present(verificacionImagenPerfil, animated: true, completion: nil)

        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    /**
     Función que se activa al presionar boton home de dispositivo.

     - parameter notification: notificacion de reproductor.
     */
    func willResignActive(_ notification: Notification) {
        self.view.endEditing(true)
    }

    /**
     Función que se llama al interactuar con el textfield.

     - parameter textField: textfield donde se escribe el codigo.
     - parameter rango: rango de textfield
     - parameter string: string a validar.
     - returns: indicador de si será modificado el string.
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn rango: NSRange, replacementString string: String) -> Bool {
        let maximoCaracteres = 20
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = (strcmp(char, "\\b") == -92)
        let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ ")
        let validacionNumeros = (string.rangeOfCharacter(from: set) != nil || isBackSpace)

        if (textField.text?.characters.count < maximoCaracteres || string.characters.count == 0) && validacionNumeros {
            return true
        } else {
            return false
        }
    }

    /**
     Función que agrega el botón aceptar en el toolbar del teclado.
     */
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("boton_aceptar", comment: "boton aceptar de toolbar"), style: UIBarButtonItemStyle.done, target: self, action: #selector(IdentificacionWomerIphone.dismissKeyboard))

        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)

        doneToolbar.items = items
        doneToolbar.sizeToFit()
        doneToolbar.barTintColor = UIColor.white

        self.txtfieldNombre.inputAccessoryView = doneToolbar
        self.txtfieldApellido.inputAccessoryView = doneToolbar
    }

    /// Realiza conexion con facebook
    ///
    /// - Parameter sender: objeto de boton
    @IBAction func conectarAFacebook(_ sender: AnyObject) {
        Util.agregarActivityIndicator(self.tabBarController!.view)

        fbLoginManager.logIn(withReadPermissions: ["email"], handler: { (result, error) -> Void in

            if error == nil {
                self.btnConectarFacebook.isHidden = true
                self.btnDesconectarFacebook.isHidden = false
                // eliminar si se sacan seguidores en onLoad
                self.obtenerSeguidoresFacebook()
            } else {
                Logger.log("Hubo un error al loguear con facebook")
                Util.removerActivityIndicator(self.tabBarController!.view)
            }

        })
    }

    /// Conecta a twitter
    ///
    /// - Parameter sender: objeto de boton
    @IBAction func conectarATwitter(_ sender: AnyObject) {
        Util.agregarActivityIndicator(self.tabBarController!.view)

        Twitter.sharedInstance().logIn {
            (session, error) -> Void in
            if session != nil {
                Globales.loginTwitter = true

                self.btnConectarTwitter.isHidden = true
                self.btnDesconectarTwitter.isHidden = false
                self.verificarSeguidorTwitter()
                // eliminar si se sacan seguidores en onLoad
                self.obtenerSeguidoresTwitter()
            } else {
                Logger.log("error: \(error!.localizedDescription)")
            }
            Util.removerActivityIndicator(self.tabBarController!.view)
        }
    }

    @IBAction func seguirTwitter(_ sender: AnyObject) {
        Util.agregarActivityIndicator(self.tabBarController!.view)
        Twitter.sharedInstance().logIn {
            (session, error) -> Void in
            if session != nil {
                self.esSeguidor { retorno in
                    DispatchQueue.main.async(execute: {
                        var urlConnectionTwitter = String()
                        if retorno {
                            urlConnectionTwitter = "https://api.twitter.com/1.1/friendships/destroy.json?screen_name=womchile"
                        } else {
                            urlConnectionTwitter = "https://api.twitter.com/1.1/friendships/create.json?screen_name=womchile&follow=true"
                        }
                        self.seguirWomTwitter(urlConnectionTwitter)
                    })
                }
            } else {
                Util.removerActivityIndicator(self.tabBarController!.view)
                Logger.log("error autentication")
            }
        }
    }

    func esSeguidor (_ onCompletion: @escaping (_ retorno: Bool) -> ()) {

        Util.agregarActivityIndicator(self.tabBarController!.view)

        var esSeguidor = false
        if Twitter.sharedInstance().sessionStore.session() != nil {
            let userID = Twitter.sharedInstance().sessionStore.session()!.userID
            let client = TWTRAPIClient(userID: userID)
            let urlConnectionTwitter = "https://api.twitter.com/1.1/friendships/lookup.json?screen_name=womchile"
            let params = NSMutableDictionary()
            var clientError: NSError?
            let request = client.urlRequest(withMethod: "GET", url: urlConnectionTwitter, parameters: (params as! [AnyHashable: Any]), error: &clientError)
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError == nil {

                    let data = data,
                    jsonString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    if jsonString!.lowercased.range(of: "following") != nil {
                        esSeguidor = true
                        self.follow = true
                    } else {
                        esSeguidor = false
                        self.follow = false
                    }

                    DispatchQueue.main.async(execute: {
                        onCompletion(esSeguidor)
                        Util.removerActivityIndicator(self.tabBarController!.view)

                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Logger.log("Error: \(connectionError ?? "" as! Error)")
                        onCompletion(esSeguidor)
                        Util.removerActivityIndicator(self.tabBarController!.view)

                    })
                }
            }

        } else {
            Logger.log("No hay sesion de twitter")
            Util.removerActivityIndicator(self.tabBarController!.view)
        }
    }


    /// Se llama al servicio para seguir en Twitter
    ///
    /// - Parameter urlConnectionTwitter: url para conectarse a twitter de wom
    func seguirWomTwitter (_ urlConnectionTwitter: String) {
        // llama a servicio de seguir
        self.btnSeguidoTwitter.isHidden = true
        self.btnSeguirTwitter.isHidden = false
        if Twitter.sharedInstance().sessionStore.session() != nil {
            let userID = Twitter.sharedInstance().sessionStore.session()!.userID
            let client = TWTRAPIClient(userID: userID)
            let params = NSMutableDictionary()
            var clientError: NSError?
            let request = client.urlRequest(withMethod: "POST", url: urlConnectionTwitter, parameters: (params as! [AnyHashable: Any]), error: &clientError)
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError == nil {

                    if self.follow {
                        //accion dejar de seguir
                        self.btnSeguidoTwitter.isHidden = true
                        self.btnSeguirTwitter.isHidden = false
                    } else {
                        //accion de seguir
                        self.btnSeguidoTwitter.isHidden = false
                        self.btnSeguirTwitter.isHidden = true
                    }
                } else {
                    Logger.log("Error: \(connectionError ?? "" as! Error)")
                }
                Util.removerActivityIndicator(self.tabBarController!.view)
            }

        } else {
            Logger.log("No hay sesion de twitter")
                Util.removerActivityIndicator(self.tabBarController!.view)
            }
        }

        /// Valida que exista sesión en facebook
        ///
        /// - Returns: indica si existe sesion o no
        func validarSesionTwitter() -> Bool {
            if Twitter.sharedInstance().sessionStore.session() != nil {
                self.btnConectarTwitter.isHidden = true
                self.btnDesconectarTwitter.isHidden = false
                return true
            } else {
                self.btnConectarTwitter.isHidden = false
                self.btnDesconectarTwitter.isHidden = true
                return false
            }
        }

        /// Valida que exista sesión en facebook
        func validarSesionFacebook() {
            if FBSDKAccessToken.current() != nil {
                self.btnConectarFacebook.isHidden = true
                self.btnDesconectarFacebook.isHidden = false
            } else {
                self.btnConectarFacebook.isHidden = false
                self.btnDesconectarFacebook.isHidden = true
            }
        }

        func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
            Logger.log("login descripcion: " + result.description)
            Logger.log("error: " + error.localizedDescription)
        }

        func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
            Logger.log("Logout de facebook")
        }


        @IBAction func elegirImagen(_ sender: AnyObject) {
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_PERSONALIZA_PERFIL_SUBIR_FOTO", comment: "marca home personaliza perfil subir foto"))

            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary

            present(imagePicker, animated: true, completion: nil)
        }

        @IBAction func likeFacebook(_ sender: AnyObject) {
            if FBSDKAccessToken.current() == nil {
                conectarAFacebook("" as AnyObject)
            } else {
                /// Se busca el boton dentro de las subvistas del boton de facebook
                for boton: AnyObject in likeButton.subviews[0].subviews {
                    if boton is UIButton {
                        boton.sendActions(for: .touchUpInside)
                    }
                }
            }
        }

        @IBAction func logOutFacebook(_ sender: AnyObject) {
            let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logOut()
            self.btnConectarFacebook.isHidden = false
            self.btnDesconectarFacebook.isHidden = true
            self.btnFacebookLike.setBackgroundImage(UIImage(named: "fb_like"), for: UIControlState())
        }

        @IBAction func logOutTwitter(_ sender: AnyObject) {
            let url = URL(string: "https://api.twitter.com")!
            let cookies = HTTPCookieStorage.shared.cookies(for: url)!
            for cookie: HTTPCookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }

            let store = Twitter.sharedInstance().sessionStore
            let userID = store.session()!.userID
            store.logOutUserID(userID)

            self.btnDesconectarTwitter.isHidden = true
            self.btnConectarTwitter.isHidden = false
            self.btnSeguidoTwitter.isHidden = true
            self.btnSeguirTwitter.isHidden = false
        }
}
