//
//  VerificadorCodigoLoginIphone.swift
//  AppWomMobile
//
//  Controlador donde se verifica el codigo enviado por sms.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Firebase

protocol ProtocoloVerificadorCodigoLoginIphone: class {
    func reintentarServicio()
}
class VerificadorCodigoLoginIphone: UIViewController {

    @IBOutlet weak var activityIndicator: UIImageView!

    override func viewDidLoad() {
        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_PASO4_ONLOAD", comment: "marca de ingreso facil paso 4 "))

        UtilApp.agregarActivityIndicatorEspecifico(imageIndicador: activityIndicator)
        llamarServicios()
    }

    /**
     Función que llama a los servicios principales
    */
    func llamarServicios() {
        UtilServicios.buscarDatosCliente { retorno in
            DispatchQueue.main.async(execute: {
                if retorno && Globales.datosCliente.nombrePlan != nil && Globales.datosCliente.idFormaPago != nil && Globales.datosCliente.tipoPlan != nil {
                    self.abrirTabBar()
                } else {
                    self.mostrarErrorServicios()
                }
            })
        }
    }

    /**
     Despliega error que invalida ingreso a la aplicación.
    */
    func mostrarErrorServicios() {
        if let controladorError = storyboard!.instantiateViewController(withIdentifier: "errorServiciosPrincipales") as? ErrorServiciosPrincipales {
            controladorError.delegate = self
            present(controladorError, animated: true, completion: nil)
        }
    }

    /**
     Función que abre tabBarController según el tipo de linea.
     */
    func abrirTabBar() {
        Logger.log("Home seleccionado: \(Globales.homeSeleccionado)")

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_CODIGO_VALIDO", comment: " marca de ingreso facil valida codigo"))

        switch Globales.homeSeleccionado {
        case Constants.TipoHome.homeBam:
            abrirHomeBam()
        case Constants.TipoHome.homeBusiness:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homePrepago:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homeControladoAntiguo:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homeControladoSimple:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homePostpagoHibrido:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homePostpagoLibre:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homePostpagoVozSMS:
            abrirSeleccionImagenPerfil()
        case Constants.TipoHome.homePostpagoDatosSMS:
            abrirSeleccionImagenPerfil()
        default:
            Logger.log("Home seleccionado no existente")
            mostrarErrorServicios()
        }
    }
    /**
     Función que abre tabBarController tipo controlado.
     */
    func abrirSeleccionImagenPerfil() {
        let controladorTabControlado = storyboard!.instantiateViewController(withIdentifier: "personalizarPerfilFaceTwitt")
        present(controladorTabControlado, animated: true, completion: nil)

    }

    /**
     Función que abre tabBarController tipo BAM.
     */
    func abrirHomeBam() {
        UtilApp.verificarDatosLogin()
        let controladorHomeBam = storyboard!.instantiateViewController(withIdentifier: "homeBam")
        present(controladorHomeBam, animated: true, completion: nil)
    }
}

extension VerificadorCodigoLoginIphone : ProtocoloVerificadorCodigoLoginIphone {
    /**
     Vuelve a llamar al servicio de compra bolsas.
     */
    func reintentarServicio() {
        llamarServicios()
    }
}
