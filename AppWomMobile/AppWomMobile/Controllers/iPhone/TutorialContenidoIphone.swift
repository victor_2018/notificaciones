//
//  TutorialContenidoIphone.swift
//  AppWomMobile
//
//  Controlador en donde se cargarán los slides de las vistas de los tutoriales.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class TutorialContenidoIphone: UIViewController {

    // Enlace a label del titulo en vista
    @IBOutlet weak var tituloLabel: UILabel!
    // Enlace a label de la descripción en vista
    @IBOutlet weak var descrip1: UILabel!
    // Imagen que va en el centro de la vista.
    @IBOutlet weak var imagenCentral: UIImageView!

    // Indice de pagina del tutorial
    var pageIndex: Int!
    // Titulo del slide
    var tituloTextoIn: String!
    // Nombre de la imagen de fondo del slide
    var imagenViewIn: String!
    // texto con la descripción del slide
    var descripIn: String!
    // nombre de la imagen central
    var imagenCentralIn: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.imagenCentral.image = UIImage(named: self.imagenCentralIn)
        self.tituloLabel.text = self.tituloTextoIn
        self.descrip1.text = self.descripIn

        self.tituloLabel.font = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (Constants.TamanioFuente.tamTitulo))
        if Globales.arregloValidacionSlidesTutorial[pageIndex] {
            self.imagenCentral.isHidden = false
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        if !Globales.arregloValidacionSlidesTutorial[pageIndex] {
            Globales.arregloValidacionSlidesTutorial[pageIndex] = true
            self.imagenCentral.isHidden = false
            self.imagenCentral.transform = CGAffineTransform(scaleX: 0, y: 0)

            UIView.animate(withDuration: 0.5, animations: {
                self.imagenCentral.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion: nil)
        }
    }
}
