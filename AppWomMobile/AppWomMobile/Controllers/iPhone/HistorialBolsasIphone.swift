//
//  HistorialBolsasIphone.swift
//  AppWomMobile
//
//  Controlador en donde se mostrarán las bolsas compradas por el womer.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Firebase

class HistorialBolsasIphone: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Enlace con tabla de bolsas en vista
    @IBOutlet weak var tablaBolsas: UITableView!
    // Activity indicator de carga de bolsas
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    // Vista que se muestra al no haber bolsas
    @IBOutlet weak var vistaSinBolsas: UIView!

    // arreglo de bolsas
    var bolsas = [BolsaHistorialTO]()
    // índice de fila seleccionada
    var indexSeleccionado = Int()
    // indicador de la sección en donde se ingresó al historial
    var seccionSeleccionada = String()
    // tipo de trafico ingresado
    var tipoTrafico = String()
    // atraso que se aplica a las primeras bolsas mostradas en el historial
    var delayBolsas = Double(0)
    // arreglo donde se identifica si se animó la fila indicada
    var filasAnimadas = [Bool]()
    // indicador de animación realizada a primeras bolsas mostradas
    var animacionRealizada = false
    // total de bolsas mostradas en la pantalla
    var limiteBolsasEnPantalla = Int()

    override func viewWillAppear(_ animated: Bool) {
        UtilApp.generarMigaDePan(NSLocalizedString("HISTORIAL_BOLSA_ONLOAD", comment: "marca hitorial de bolsa onload"))

        self.tablaBolsas.dataSource = self
        self.tablaBolsas.delegate = self
        self.tablaBolsas.backgroundColor = UIColor.clear
        self.vistaSinBolsas.isHidden = true
        // Se reinicializa tabla al volver desde vista siguiente
        self.bolsas.removeAll()
        self.tablaBolsas.reloadData()
        tablaBolsas.insertRows(at: tablaBolsas.indexPathsForVisibleRows!, with: UITableViewRowAnimation.bottom)
        self.tablaBolsas.setContentOffset(CGPoint.zero, animated: false)
        transformarIndicadoresSecciones()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        buscarBolsas()
    }

    override func viewDidLoad() {
        if Globales.isIphone5 {
            limiteBolsasEnPantalla = 1
        } else if Globales.isIphone6 {
            limiteBolsasEnPantalla = 2
        } else {
            limiteBolsasEnPantalla = 3
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        UtilApp.generarMigaDePan(NSLocalizedString("HISTORIAL_BOLSA_BTN_ATRAS", comment: "marca historial bolsa atras"))
    }

    /// Transforma string del nombre de la sección al tipo de tráfico
    func transformarIndicadoresSecciones() {

        switch seccionSeleccionada {
        case Constants.Indicadores.seccionDatosMoviles:
            tipoTrafico = Constants.Indicadores.tipoTraficoDatos
        case Constants.Indicadores.seccionMinutos:
            tipoTrafico = Constants.Indicadores.tipoTraficoMinutos
        case Constants.Indicadores.seccionSMS:
            tipoTrafico = Constants.Indicadores.tipoTraficoSMS
        default:
            Logger.log("default switch transformarIndicadoresSecciones")
        }
    }

    /**
     Llamada a servicio para buscar las bolsas históricas.
     */
    func buscarBolsas() {
        Util.agregarActivityIndicator(self.tabBarController!.view)

        var newDateComponents = DateComponents()
        newDateComponents.month = -6
        let fechaAnterior = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: Date(), options: NSCalendar.Options.init(rawValue: 0))
        let bolsaHistoricaEstado = "HISTORICO"

        HistorialBolsasService.sharedInstance.getHistorialBolsas(Util.transformarFechaNSDate(fechaAnterior!, formato: "yyyy-MM-dd"), fechaHasta: Util.transformarFechaNSDate(Date(), formato: "yyyy-MM-dd"), numeroCelular: Globales.numeroTelefono,
                                                                 onCompletion: { (json: JSON) in
                                                                    if json != JSON.null {
                                                                        if let results = json["return"]["recuperaInfoBolsaHistoricoOut"].array {
                                                                            if results.count == 0 {
                                                                                self.vistaSinBolsas.isHidden = false
                                                                            } else {
                                                                                for entry in results {
                                                                                    let bolsaHistorica = BolsaHistorialTO(json: entry)
                                                                                    if bolsaHistorica.estadoBolsa == bolsaHistoricaEstado {
                                                                                        self.filtrarBolsas(BolsaHistorialTO(json: entry))
                                                                                    }
                                                                                }
                                                                            }
                                                                            DispatchQueue.main.async(execute: {
                                                                                Util.removerActivityIndicator(self.tabBarController!.view)
                                                                                if self.bolsas.count == 0 {
                                                                                    self.vistaSinBolsas.isHidden = false
                                                                                }
                                                                                self.tablaBolsas.reloadData()
                                                                            })
                                                                        } else {
                                                                            DispatchQueue.main.async(execute: {
                                                                                Util.removerActivityIndicator(self.tabBarController!.view)
                                                                                self.vistaSinBolsas.isHidden = false
                                                                            })
                                                                        }
                                                                    } else {
                                                                        DispatchQueue.main.async(execute: {
                                                                            Logger.log("Error en servicio historial de bolsas")
                                                                        UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("HISTORIAL_BOLSA_ONLOAD", comment: "marca hitorial de bolsa onload"))
                                                                            Util.alertErrorGeneral(self)
                                                                            Util.removerActivityIndicator(self.tabBarController!.view)
                                                                        })
                                                                    }
        })

        self.tablaBolsas.reloadData()
    }

    /// Filtro de bolsas según la sección seleccionada
    ///
    /// - parameter bolsa: bolsa que se obtiene del servicio de historial de bolsas
    func filtrarBolsas(_ bolsa: BolsaHistorialTO) {
        let tipoBolsaMixta = "BOLSA_MIXTA"

        if (tipoTrafico == Constants.Indicadores.tipoTraficoDatos && bolsa.tipoBolsa == tipoBolsaMixta) || tipoTrafico == bolsa.tipoTrafico {
            self.bolsas.append(bolsa)
            filasAnimadas.append(false)
        }
    }
    /**
     Función que determina altura de secciones.
     - parameter tableView: tabla de bolsas en vista
     - parameter section:   índice sección
     - returns: tamaño sección
     */
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let alturaSeccion = CGFloat(246)
        if section == 0 {
            return alturaSeccion
        } else {
            return 0
        }
    }

    /**
     Función que determina altura de celdas.

     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de celda

     - returns: tamaño celda
     */
    func tableView(_ tablaBolsas: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let alturaCelda = CGFloat(105)
        if indexPath.section != 0 {
            return alturaCelda
        } else {
            return 0
        }
    }
    /**
     Función que forma la estructura del encabezado.

     - parameter tablaBolsas: tabla de bolsas en vista.
     - parameter section:   índice de sección.

     - returns: objeto de sección
     */
    func tableView(_ tablaBolsas: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaEncabezadoHistorialBolsas")! as! CeldaEncabezadoHistorialIphone
            asignarTitulo(viewSection)
            return viewSection
        } else {
            return UIView()
        }
    }

    func asignarTitulo(_ seccion: CeldaEncabezadoHistorialIphone) {
        switch seccionSeleccionada {
        case Constants.Indicadores.seccionDatosMoviles:
            seccion.tituloVista.text = NSLocalizedString("titulo_historial_bolsas_datos", comment: "titulo historial de bolsas datos")
        case Constants.Indicadores.seccionMinutos:
            seccion.tituloVista.text = NSLocalizedString("titulo_historial_bolsas_minutos", comment: "titulo historial de bolsas minutos")
        case Constants.Indicadores.seccionSMS:
            seccion.tituloVista.text = NSLocalizedString("titulo_historial_bolsas_sms", comment: "titulo historial de bolsas sms")
        default:
            Logger.log("default switch transformarIndicadoresSecciones")
        }
    }
    /**
     Función que determina el número de secciones.

     - parameter tablaBolsas: tabla de bolsas en vista.

     - returns: número de celdas.
     */
    func numberOfSections(in tablaBolsas: UITableView) -> Int {
        return 2
    }

    /**
     Función donde se setea el número de filas en la tabla

     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     sección en tabla

     - returns: valor de filas que se mostrarán
     */
    func tableView(_ tablaBolsas: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bolsas.count
    }

    /**
     Función que estructura las filas de historial de bolsas

     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila

     - returns: celda seleccionada
     */
    func tableView(_ tablaBolsas: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != 0 {
            let cell = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaHistorialBolsas", for: indexPath) as! CeldaHistorialBolsasIphone

            if !filasAnimadas[indexPath.row] {
                cell.frame.origin.y += 200
                UIView.animate(withDuration: 0.5, delay: delayBolsas, options: [], animations: {
                    cell.frame.origin.y = 0
                    }, completion: nil)
            }

            if !animacionRealizada {
                delayBolsas += 0.2
            }

            if indexPath.row > limiteBolsasEnPantalla {
                delayBolsas = 0
                animacionRealizada = true
            }

            filasAnimadas[indexPath.row] = true

            let bolsa = self.bolsas[indexPath.row]
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.currencySymbol = " "
            numberFormatter.locale = Locale(identifier: "es_CL")

            cell.fechaActivacion.text = bolsa.fechaVenta
            cell.fechaCulminacion.text = bolsa.fechaExpiracion
            cell.nombreBolsa.text = bolsa.nombreProducto
            cell.valorTotal.text = "$\(numberFormatter.string(from: Int(bolsa.valorTotal)! as NSNumber) ?? "0")"

            cell.layoutMargins = UIEdgeInsets.zero
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = UITableViewCellSelectionStyle.none

            return cell
        } else {
            return UITableViewCell()
        }
    }

    @IBAction func abrirComprarBolsas(_ sender: AnyObject) {
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabComprarBolsas
    }

    @IBAction func volverHome(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
}
