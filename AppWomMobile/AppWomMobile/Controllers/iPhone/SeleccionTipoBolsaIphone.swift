//
//  SeleccionTipoBolsaIphone.swift
//  AppWomMobile
//
//  Controlador en donde se selecciona el tipo de bolsa.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Firebase

protocol ProtocoloSeleccionTipoBolsaIphone: class {
    func obtenerArregloBolsas(_ titulo: String) -> [Bolsa]
    func obtenerArregloBolsasLabels() -> [String]
    func abrirSeleccionBolsas(_ indice: Int)
    func mostrarBotonesBolsas() -> UIView
    func actualizarIndicadorRecargaServicios()
}

class SeleccionTipoBolsaIphone: UIViewController {
    // scrollView de fondo
    @IBOutlet weak var scrollView: UIScrollView!
    // view que muestra mensaje cuando no hay bolsas para comprar
    @IBOutlet weak var viewSinBolsas: UIView!
    // vista que contiene los botones con los tipos de bolsas
    @IBOutlet weak var vistaContenedoraBotonesBolsas: UIView!
    /// Enlace a label con el numero de la linea flujo multilinea
    @IBOutlet weak var lblNumeroMultilinea: UILabel!
    /// Enlace a label con el saldo de la linea flujo multilinea
    @IBOutlet weak var lblSaldoMultilinea: UILabel!
    /// Enlace a vista superior con los datos de multilinea
    @IBOutlet weak var viewSuperiorMultilinea: UIView!
    /// Enlace a boton cerrar de popup multilinea
    @IBOutlet weak var btnCerrar: UIButton!
    /// Enlace a imagen con el logo de wom
    @IBOutlet weak var logoWom: UIImageView!

    // objeto de controlador de vista de la seleccion de bolsa
    var seleccionBolsaController = SeleccionBolsaIphone()
    // tipo de bolsa de datos
    let tipoDatos = "DATOS"
    // tipo de bolsa SMS
    let tipoSMS = "SMS"
    // tipo de bolsa voz
    let tipoVoz = "VOZ"
    // tipo de bolsa mixta
    let tipoMixtas = "MIXTAS"
    // tipo de bolsa tematica
    let tipoTematicas = "TEMATICA"
    // tipo de bolsa tematica
    let tipoRoaming = "ROAMING"
    // contador de servicios upSelling que han finalizado
    var contadorServiciosFinalizados = 0
    // total de bolsas que estan disponibles
    var totalTipoBolsas = 6
    // arreglo con las bolsas totales que existen en WOM
    var arregloBolsasCatalogo = [String: CatalogoBolsaTO]()
    // arreglo de bolsas de datos disponibles
    var arregloBolsasDatos = [BolsaDisponibleTO]()
    // arreglo de bolsas de sms disponibles
    var arregloBolsasSMS = [BolsaDisponibleTO]()
    // arreglo de bolsas de voz disponibles
    var arregloBolsasVoz = [BolsaDisponibleTO]()
    // arreglo de bolsas mixtas disponibles
    var arregloBolsasMixtas = [BolsaDisponibleTO]()
    // arreglo de bolsas tematicas disponibles
    var arregloBolsasTematicas = [BolsaDisponibleTO]()
    // arreglo de bolsas tematicas disponibles
    var arregloBolsasRoaming = [BolsaDisponibleTO]()
    // arreglo final de bolsas de datos
    var arregloBolsasDatosFinal = [Bolsa]()
    // arreglo final de bolsas SMS
    var arregloBolsasSMSFinal = [Bolsa]()
    // arreglo final de bolsas de voz
    var arregloBolsasVozFinal = [Bolsa]()
    // arreglo final de bolsas mixtas
    var arregloBolsasMixtasFinal = [Bolsa]()
    // arreglo final de bolsas tematicas
    var arregloBolsasTematicasFinal = [Bolsa]()
    // arreglo final de bolsas tematicas
    var arregloBolsasRoamingFinal = [Bolsa]()
    // arreglo con nombres de bolsas
    var arregloBolsasLabels = [String]()
    /// Indicador de flujo multilinea
    var esMultilinea = false
    /// Datos del cliente
    var cliente = EncabezadoHomeTO()
    /// Indicador de cliente postpago multilinea
    var esPostpagoMultilinea = Bool()
    /// Controlador de popup multilinea
    weak var controllerPopupMultilinea = PopupCompraBolsasMultilinea()
    // Vista que contiene botones de bolsas
    var vistaContenedoraBolsas = UIView()
    // Indica si se debe recargar los servicios sin usar cache
    var recargarServicios = false
    // Altura modificada para vista superior en multilinea para postpagos
    var alturaVistaSuperiorPostpagoMultilinea = CGFloat()

    override func viewDidLoad() {
        self.validarProperties(tipoBolsa: self.tipoDatos)
        self.validarProperties(tipoBolsa: self.tipoSMS)
        self.validarProperties(tipoBolsa: self.tipoVoz)
        self.validarProperties(tipoBolsa: self.tipoMixtas)
        self.validarProperties(tipoBolsa: self.tipoTematicas)
        alturaVistaSuperiorPostpagoMultilinea = self.viewSuperiorMultilinea.frame.size.height * 0.7
    }

    override func viewWillLayoutSubviews() {
        if esMultilinea {
            UtilApp.generarMigaDePan(NSLocalizedString("COMPRA_BOLSA_ONLOAD", comment: "marca compra bolsa"))

            limpiarControlador()
            validarMantencion()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        if !esMultilinea {
            UtilApp.generarMigaDePan(NSLocalizedString("COMPRA_BOLSA_ONLOAD", comment: "marca compra bolsa"))

            limpiarControlador()
            validarMantencion()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.automaticallyAdjustsScrollViewInsets = false
    }

    /// Valida estado de mantencion de servicios
    func validarMantencion() {
        Util.agregarActivityIndicator(self.tabBarController!.view)
        self.esPostpagoMultilinea = (self.cliente.tipoPlan.uppercased() == Constants.Indicadores.tipoPostpago)
        if esMultilinea && esPostpagoMultilinea {
            self.viewSuperiorMultilinea.frame.size.height = alturaVistaSuperiorPostpagoMultilinea
            self.logoWom.isHidden = true
        } else {
            self.logoWom.isHidden = false
        }
        self.viewSinBolsas.isHidden = true

        if self.esMultilinea {
            UtilApp.validarEstadoServicios(controller: controllerPopupMultilinea!, abrirVista: true) { retorno in}
            UtilMultilinea.buscarDatosPlan(telefono: self.cliente.numeroTelefono, esPostpago: self.esPostpagoMultilinea) { retorno, datosCliente in
                if retorno {
                    self.cliente.saldo = datosCliente.saldo
                    self.cliente.saldoFormateado = datosCliente.saldoFormateado
                    self.cliente.nombrePlan = datosCliente.nombrePlan
                    self.cliente.idFormaPago = datosCliente.idFormaPago
                    self.cliente.tipoPlan = datosCliente.tipoPlan
                    self.cargarCatalogoBolsas()
                } else {
                    self.viewSinBolsas.isHidden = false
                    Util.removerActivityIndicator(self.tabBarController!.view)
                    Util.alertErrorGeneral(self.controllerPopupMultilinea!)
                    UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("COMPRA_BOLSA_ONLOAD", comment: "miga de pan compra bolsa onload"))
                }
            }
        } else {
            UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in}
            self.cliente = Globales.datosCliente
            self.cargarCatalogoBolsas()
        }
    }
    /**
     Función que remueve todos los elementos de las listas cargadas anteriormente.
     */
    func limpiarControlador() {
        self.contadorServiciosFinalizados = 0
        arregloBolsasCatalogo.removeAll()
        arregloBolsasDatos.removeAll()
        arregloBolsasSMS.removeAll()
        arregloBolsasVoz.removeAll()
        arregloBolsasMixtas.removeAll()
        arregloBolsasTematicas.removeAll()
        arregloBolsasDatosFinal.removeAll()
        arregloBolsasSMSFinal.removeAll()
        arregloBolsasVozFinal.removeAll()
        arregloBolsasMixtasFinal.removeAll()
        arregloBolsasTematicasFinal.removeAll()
        arregloBolsasLabels.removeAll()
        arregloBolsasRoamingFinal.removeAll()
        vistaContenedoraBolsas.removeFromSuperview()
    }
    override func viewDidDisappear(_ animated: Bool) {
        for view in self.vistaContenedoraBolsas.subviews {
            view.removeFromSuperview()
        }
        self.contadorServiciosFinalizados = 0
        vistaContenedoraBolsas.removeFromSuperview()
        self.viewSinBolsas.isHidden = true
    }
    /**
     Funcion donde obtiene el catalogo de bolsas.
     */
    func cargarCatalogoBolsas() {
        GetNextelProductCatalog.sharedInstance.getBolsasDisponibles(Constants.Indicadores.clientName,
                                                                    onCompletion: {  (json: JSON) in
                                                                        if json.dictionaryObject != nil {

                                                                        for bolsa in json["listProduct"]["itm_product"].array! {
                                                                            self.arregloBolsasCatalogo[bolsa["id_prod"].stringValue] = CatalogoBolsaTO(json: bolsa)
                                                                        }

                                                                        DispatchQueue.main.async(execute: {
                                                                            UtilCompraBolsas.generarArregloBolsasRoaming(arregloBolsasCatalogo: self.arregloBolsasCatalogo, arregloBolsasRoaming: &self.arregloBolsasRoamingFinal, cliente: self.cliente)
                                                                            self.contadorServiciosFinalizados +=  1

                                                                            self.validarBolsasDisponibles(tipoBolsa: self.tipoDatos, arregloBolsasDisponibles: self.arregloBolsasDatos) { retorno, listado in
                                                                                if retorno {
                                                                                    self.arregloBolsasDatos = listado
                                                                                }
                                                                            }

                                                                            self.validarBolsasDisponibles(tipoBolsa: self.tipoSMS, arregloBolsasDisponibles: self.arregloBolsasSMS) { retorno, listado in
                                                                                if retorno {
                                                                                    self.arregloBolsasSMS = listado
                                                                                }
                                                                            }

                                                                            self.validarBolsasDisponibles(tipoBolsa: self.tipoVoz, arregloBolsasDisponibles: self.arregloBolsasVoz) { retorno, listado in
                                                                                if retorno {
                                                                                    self.arregloBolsasVoz = listado
                                                                                }
                                                                            }

                                                                            self.validarBolsasDisponibles(tipoBolsa: self.tipoMixtas, arregloBolsasDisponibles: self.arregloBolsasMixtas) { retorno, listado in
                                                                                if retorno {
                                                                                    self.arregloBolsasMixtas = listado
                                                                                }
                                                                            }

                                                                            self.validarBolsasDisponibles(tipoBolsa: self.tipoTematicas, arregloBolsasDisponibles: self.arregloBolsasTematicas) { retorno, listado in
                                                                                if retorno {
                                                                                    self.arregloBolsasTematicas = listado
                                                                                }
                                                                            }

                                                                            Logger.log("finaliza servicio catalogo")
                                                                        })
                                                                    } else {
                                                                        DispatchQueue.main.async(execute: {
                                                                            Util.removerActivityIndicator(self.tabBarController!.view)
                                                                            self.viewSinBolsas.isHidden = false
                                                                        UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("COMPRA_BOLSA_ONLOAD", comment: "miga de pan compra bolsa onload"))
                                                                            Util.alertErrorGeneral(self)
                                                                            Logger.log("error al cargar catalogo")

                                                                        })
                                                                        }
        })
    }

    /// Valida el estado de las properties para caching
    ///
    /// - Parameter tipoBolsa: tipo de bolsa a validar
    func validarProperties(tipoBolsa: String) {
        if PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.respuestaServicioValidaVenta + tipoBolsa) != nil {
            Logger.log("property valida venta encontrada para: \(tipoBolsa)")
        } else {
            Logger.log("property valida venta no encontrada para: \(tipoBolsa)")
            PlistManager.sharedInstance.addNewItemWithKey(Constants.KeyPlist.respuestaServicioValidaVenta + tipoBolsa, value: "" as AnyObject)
        }

        if PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.respuestaServicioUpselling + tipoBolsa) != nil {
            Logger.log("property Upselling encontrada para: \(tipoBolsa)")
        } else {
            Logger.log("property Upselling no encontrada para: \(tipoBolsa)")
            PlistManager.sharedInstance.addNewItemWithKey(Constants.KeyPlist.respuestaServicioUpselling + tipoBolsa, value: "" as AnyObject)
        }
    }

    /**
     Función que obtiene las reglas de validacion para bolsas.

     - parameter tipoBolsa: tipo de bolsa que se esta obteniendo
     - parameter arregloBolsasDisponibles: arreglo de bolsas que se generará
     */
    func validarBolsasDisponibles(tipoBolsa: String, arregloBolsasDisponibles: [BolsaDisponibleTO], onCompletion: @escaping (_ retorno: Bool, _ listado: [BolsaDisponibleTO]) -> () ) {

        var jsonBody = [String: String]()

        jsonBody["client_name"] = Constants.Indicadores.clientName
        jsonBody["phone_number"] = self.cliente.numeroTelefono
        jsonBody["segment_prod"] = self.cliente.tipoPlan
        jsonBody["rate_plan"] = String(self.cliente.ratePlan)
        jsonBody["trafic_type"] = tipoBolsa

        if let respuesta = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.respuestaServicioValidaVenta + tipoBolsa) {
            let jsonCaching: JSON = JSON(parseJSON: respuesta as! String)

            if !esMultilinea && jsonCaching != "" && jsonCaching != JSON.null && !UtilCompraBolsas.validarTiempoCaching(recargarServicios: recargarServicios) {
                Logger.log("Caching ValidaVenta: \(tipoBolsa)")
                self.validarReglasVenta(json: jsonCaching, tipoBolsa: tipoBolsa, arregloBolsasDisponibles: arregloBolsasDisponibles) { retorno, listado in
                    onCompletion(retorno, listado)
                }
            } else {
                Logger.log("Obteniendo validacion venta de servicio con: \(tipoBolsa)")
                GetValidaVentaBolsas.sharedInstance.validarBolsa(jsonBody, onCompletion: {  (json: JSON) in

                    if !self.esMultilinea {
                        PlistManager.sharedInstance.saveValue(json.rawString() as AnyObject, forKey: Constants.KeyPlist.respuestaServicioValidaVenta + tipoBolsa)
                        Logger.log("Guardando datos validaVenta en cache: \(tipoBolsa)")
                    }

                    self.validarReglasVenta(json: json, tipoBolsa: tipoBolsa, arregloBolsasDisponibles: arregloBolsasDisponibles) { retorno, listado in
                        onCompletion(retorno, listado)
                    }
                })
            }
        } else {
            PlistManager.sharedInstance.addNewItemWithKey(Constants.KeyPlist.respuestaServicioValidaVenta + tipoBolsa, value: "" as AnyObject)
            self.validarBolsasDisponibles(tipoBolsa: tipoBolsa, arregloBolsasDisponibles: arregloBolsasDisponibles) { retorno, listado in
                onCompletion(retorno, listado)
            }
        }
    }

    /// Valida las reglas en base a lo retornado por el servicio ValidaVena
    ///
    /// - Parameters:
    ///   - json: json del servicio
    ///   - tipoBolsa: tipo de bolsa que se esta validando
    ///   - arregloBolsasDisponibles: arreglo del tipo de bolsa asociado
    ///   - onCompletion: retorno: si paso bien las reglas, listado: listado de bolsas asociado
    func validarReglasVenta(json: JSON, tipoBolsa: String, arregloBolsasDisponibles: [BolsaDisponibleTO], onCompletion: @escaping (_ retorno: Bool, _ listado: [BolsaDisponibleTO]) -> () ) {
        if let results = json["listaResults"]["item_result"].array {
            var reglasValidaciones = [ReglaValidacionBolsaTO]()
            var objCantBolsas = ReglaValidacionBolsaTO()
            var objLimiteCompra = ReglaValidacionBolsaTO()
            var objEstadoCliente = ReglaValidacionBolsaTO()

            for entry in results {
                reglasValidaciones.append(ReglaValidacionBolsaTO(json: entry))
            }

            UtilCompraBolsas.asignarObjetosReglas(reglasValidaciones, objCantBolsas: &objCantBolsas, objLimiteCompra: &objLimiteCompra, objEstadoCliente: &objEstadoCliente)

            if UtilCompraBolsas.validarReglaUno(objCantBolsas, objLimiteCompra: objLimiteCompra, objEstadoCliente: objEstadoCliente) {
                Logger.log("Validación bolsas \(tipoBolsa): Regla 1")
                self.contadorServiciosFinalizados += 1
            } else if UtilCompraBolsas.validarReglaDos(objCantBolsas, objLimiteCompra: objLimiteCompra, objEstadoCliente: objEstadoCliente) {

                if objCantBolsas.valor != nil && Int(objCantBolsas.valor)! <= 0 {
                    Logger.log("Validación bolsas \(tipoBolsa): Regla 2")
                    self.contadorServiciosFinalizados +=  1
                } else {
                    Logger.log("Validación bolsas \(tipoBolsa): Regla 3")
                    self.cargarBolsasDisponibles(tipoBolsa, arregloBolsasDisponibles: arregloBolsasDisponibles, limiteBolsa: objLimiteCompra.valor) { retorno, listado in
                        if retorno {
                            onCompletion(true, listado)
                        } else {
                            onCompletion(false, listado)
                        }
                    }
                }
            } else {
                Logger.log("Validación bolsas \(tipoBolsa): Regla 4")
                self.cargarBolsasDisponibles(tipoBolsa, arregloBolsasDisponibles: arregloBolsasDisponibles, limiteBolsa: "") { retorno, listado in
                    if retorno {
                        onCompletion(true, listado)
                    } else {
                        onCompletion(false, listado)
                    }
                }
            }
        } else {
            self.contadorServiciosFinalizados +=  1
            Logger.log("ERROR SERVICIO VALIDAR BOLSAS: \(tipoBolsa)")

            DispatchQueue.main.async(execute: {
                self.validarServiciosLlamados()
            })
        }
    }
    /**
     Función que obtiene bolsas disponibles para comprar.

     - parameter tipoBolsa: tipo de bolsa que se esta obteniendo
     - parameter arregloBolsasDisponibles: arreglo de bolsas que se generará
     - parameter limiteBolsa: limite de valor de bolsa que se puede comprar
     */
    func cargarBolsasDisponibles(_ tipoBolsa: String, arregloBolsasDisponibles: [BolsaDisponibleTO], limiteBolsa: String, onCompletion: @escaping (_ retorno: Bool, _ listado: [BolsaDisponibleTO]) -> () ) {
        var arregloBolsasDisponiblesIn = arregloBolsasDisponibles

        if let respuesta = PlistManager.sharedInstance.getValueForKey(Constants.KeyPlist.respuestaServicioUpselling + tipoBolsa) {
            let jsonCaching: JSON = JSON(parseJSON: respuesta as! String)
            if !esMultilinea && jsonCaching != "" && jsonCaching != JSON.null && !UtilCompraBolsas.validarTiempoCaching(recargarServicios: recargarServicios) {
                Logger.log("Caching Upselling: \(tipoBolsa)")
                generarArreglosOferta(json: jsonCaching, tipoBolsa: tipoBolsa, arregloBolsasDisponibles: &arregloBolsasDisponiblesIn, limiteBolsa: limiteBolsa) { retorno, listado in
                    onCompletion(retorno, listado)
                }
            } else {
                GetUpsellingCompraBolsas.sharedInstance.getBolsasDisponibles(self.cliente.numeroTelefono, ratePlan: String(self.cliente.ratePlan), segmentProd: self.cliente.tipoPlan, clientName: Constants.Indicadores.clientName, trafficType: tipoBolsa,
                                                                             onCompletion: {  (json: JSON) in
                                                                                if !self.esMultilinea {
                                                                                    PlistManager.sharedInstance.saveValue(json.rawString() as AnyObject, forKey: Constants.KeyPlist.respuestaServicioUpselling + tipoBolsa)
                                                                                    Logger.log("Guardando datos upselling en cache: \(tipoBolsa)")
                                                                                }

                                                                                self.generarArreglosOferta(json: json, tipoBolsa: tipoBolsa, arregloBolsasDisponibles: &arregloBolsasDisponiblesIn, limiteBolsa: limiteBolsa) { retorno, listado in
                                                                                    onCompletion(retorno, listado)
                                                                                }
                })
            }
        } else {
            PlistManager.sharedInstance.addNewItemWithKey(Constants.KeyPlist.respuestaServicioUpselling + tipoBolsa, value: "" as AnyObject)
            self.cargarBolsasDisponibles(tipoBolsa, arregloBolsasDisponibles: arregloBolsasDisponibles, limiteBolsa: limiteBolsa) { retorno, listado in
                onCompletion(retorno, listado)
            }
        }
    }

    /// Genera arreglos con la oferta obtenida del servicio de Upselling
    ///
    /// - Parameters:
    ///   - json: json obtenido del servicio
    ///   - tipoBolsa: tipo de bolsa que se esta buscando oferta
    ///   - arregloBolsasDisponibles: listado de bolsas asociado al tipo de oferta
    ///   - limiteBolsa: limite de valor de bolsa que se puede comprar
    ///   - onCompletion: retorno: si paso bien las reglas, listado: listado de bolsas asociado
    func generarArreglosOferta(json: JSON, tipoBolsa: String, arregloBolsasDisponibles: inout [BolsaDisponibleTO], limiteBolsa: String, onCompletion: @escaping (_ retorno: Bool, _ listado: [BolsaDisponibleTO]) -> ()) {

        if json.dictionaryObject != nil {

            if json["listaBundles"] != JSON.null {
                for bolsa in json["listaBundles"]["item_bundle"].array! {
                    arregloBolsasDisponibles.append(BolsaDisponibleTO(json: bolsa))
                }
                UtilCompraBolsas.generarArreglosFinales(arregloBolsasDisponibles, tipoBolsa: tipoBolsa, limiteBolsaIn: limiteBolsa, arregloBolsasCatalogo: arregloBolsasCatalogo, arregloBolsasDatosFinal: &arregloBolsasDatosFinal, arregloBolsasVozFinal: &arregloBolsasVozFinal, arregloBolsasSMSFinal: &arregloBolsasSMSFinal, arregloBolsasMixtasFinal: &arregloBolsasMixtasFinal, arregloBolsasTematicasFinal: &arregloBolsasTematicasFinal)
                DispatchQueue.main.async(execute: {
                    self.contadorServiciosFinalizados +=  1
                    self.validarServiciosLlamados()
                })
            } else {

                DispatchQueue.main.async(execute: {
                    self.contadorServiciosFinalizados +=  1
                    Logger.log("ERROR UpSelling: \(tipoBolsa) descripcion: \(json["errorDescription"])")
                    self.validarServiciosLlamados()
                })
            }
        } else {
            DispatchQueue.main.async(execute: {
                self.contadorServiciosFinalizados +=  1
                Logger.log("ERROR UpSelling: \(tipoBolsa)")
                self.validarServiciosLlamados()
            })
        }
    }

    /// Valida que todos los servicios asincronos hayan sido llamados
    func validarServiciosLlamados() {
        Logger.log("Servicios finalizados: \(self.contadorServiciosFinalizados)")
        if self.contadorServiciosFinalizados == self.totalTipoBolsas {
            self.contadorServiciosFinalizados = 0
            Logger.log("Finalizando llamada servicios compra bolsas")
            Util.removerActivityIndicator(Globales.tabBarSeleccionado.view)
            self.vistaContenedoraBotonesBolsas.addSubview(self.mostrarBotonesBolsas())

            if esMultilinea {
                if !esPostpagoMultilinea {
                    UtilMultilinea.generarLabelSaldo(labelSaldo: lblSaldoMultilinea, saldo: cliente.saldoFormateado)
                }
            } else {
                // Se actualiza fecha para cuando se recarguen servicios por compra de bolsa
                if recargarServicios {
                    PlistManager.sharedInstance.saveValue(Date() as AnyObject, forKey: Constants.KeyPlist.tiempoCachingCompraBolsas)
                    Logger.log("Actualizando fecha de caching por compra de bolsa")
                }
                recargarServicios = false
                // Se va a actualizar fecha en caso de que pase la validacion de la fecha guardada
                if UtilCompraBolsas.validarTiempoCaching(recargarServicios: recargarServicios) {
                    PlistManager.sharedInstance.saveValue(Date() as AnyObject, forKey: Constants.KeyPlist.tiempoCachingCompraBolsas)
                    Logger.log("Actualizando fecha de caching por fecha caducada")
                }
            }
        }
    }
}

extension SeleccionTipoBolsaIphone : ProtocoloSeleccionTipoBolsaIphone {
    /**
     Función que obtiene arreglo de bolsas final.

     - parameter titulo: titulo de la bolsa que se desea
     - returns: arreglo de bolsas
     */
    func obtenerArregloBolsas(_ titulo: String) -> [Bolsa] {
        switch titulo {
        case NSLocalizedString("tipo_bolsa_datos", comment: "texto datos"):
            return arregloBolsasDatosFinal
        case NSLocalizedString("tipo_bolsa_voz", comment: "texto voz"):
            return arregloBolsasVozFinal
        case NSLocalizedString("tipo_bolsa_sms", comment: "texto sms"):
            return arregloBolsasSMSFinal
        case NSLocalizedString("tipo_bolsa_mixtas", comment: "texto mixtas"):
            return arregloBolsasMixtasFinal
        case NSLocalizedString("tipo_bolsa_tematicas", comment: "texto tematicas"):
            return arregloBolsasTematicasFinal
        case NSLocalizedString("tipo_bolsa_roaming", comment: "texto roaming"):
            return arregloBolsasRoamingFinal
        default:
            return [Bolsa]()
        }
    }
    /**
     Función que arma arreglo de titulos de las bolsas

     - returns: arreglo de titulos de bolsas
     */
    func obtenerArregloBolsasLabels() -> [String] {
        var arregloBolsasLabels = [String]()
        if arregloBolsasDatosFinal.count > 0 {
            arregloBolsasLabels.append(NSLocalizedString("tipo_bolsa_datos", comment: "texto datos"))
        }
        if arregloBolsasVozFinal.count > 0 {
            arregloBolsasLabels.append(NSLocalizedString("tipo_bolsa_voz", comment: "texto voz"))
        }
        if arregloBolsasSMSFinal.count > 0 {
            arregloBolsasLabels.append(NSLocalizedString("tipo_bolsa_sms", comment: "texto sms"))
        }
        if arregloBolsasMixtasFinal.count > 0 {
            arregloBolsasLabels.append(NSLocalizedString("tipo_bolsa_mixtas", comment: "texto mixtas"))
        }
        if arregloBolsasTematicasFinal.count > 0 {
            arregloBolsasLabels.append(NSLocalizedString("tipo_bolsa_tematicas", comment: "texto tematicas"))
        }
        if arregloBolsasRoamingFinal.count > 0 {
            arregloBolsasLabels.append(NSLocalizedString("tipo_bolsa_roaming", comment: "texto roaming"))
        }

        return arregloBolsasLabels
    }

    /// Abre la pantalla siguiente para listar bolsas
    ///
    /// - Parameter indice: indice de boton seleccionado
    func abrirSeleccionBolsas(_ indice: Int) {
        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        seleccionBolsaController = storyboard.instantiateViewController(withIdentifier: "seleccionBolsas") as! SeleccionBolsaIphone

        seleccionBolsaController.bolsaSeleccionada = indice
        seleccionBolsaController.delegate = self
        self.arregloBolsasLabels = obtenerArregloBolsasLabels()
        // Botón de navegación para volver a las bolsas
        let backItem = UIBarButtonItem()

        backItem.title = ""
        navigationItem.backBarButtonItem = backItem

        seleccionBolsaController.cliente = cliente

        if esMultilinea {
            seleccionBolsaController.view.frame.size.height = (controllerPopupMultilinea?.viewContenido.frame.size.height)!
            seleccionBolsaController.view.frame.size.width = (controllerPopupMultilinea?.viewContenido.frame.size.width)!

            seleccionBolsaController.esMultilinea = true
            seleccionBolsaController.viewMultilineas.isHidden = false

            controllerPopupMultilinea?.viewContenido.addSubview(seleccionBolsaController.view)

            seleccionBolsaController.modificarVistaMultilinea()

            seleccionBolsaController.controllerPopupMultilinea = controllerPopupMultilinea
        } else {
            self.navigationController!.pushViewController(seleccionBolsaController as UIViewController, animated: true)
        }
    }

    /**
     funcion donde se generan cajas de bolsas
     - returns: vista de bolsa
     */
    func mostrarBotonesBolsas() -> UIView {
        let columnas = 3
        // altura total de la vista de bolsas
        var heightTotalVista = CGFloat(0)
        var totalBolsas = obtenerArregloBolsasLabels()

        if totalBolsas.count != 0 {
            // ancho de vista completa
            let widthVista = self.view.frame.width
            // espacio en el eje X
            let espacioEjeX = CGFloat(10)
            // espacio en el eje Y
            let espacioEjeY = CGFloat(10)
            // ancho de caja unica de bolsa
            let widthCajaBolsa = ( widthVista - (espacioEjeX * CGFloat(columnas + 1)) ) / CGFloat(columnas)
            // altura de caja unica de bolsa
            let heightCajaBolsa = widthCajaBolsa
            // fila en que se esta ubicada la bolsa actual
            var filaUbicacionBolsa = CGFloat(1)
            // posicion donde se posicionará la bolsa
            var posicionEjeY = CGFloat(0)

            for index in 0...(totalBolsas.count - 1) {
                // posicion del eje x de bolsa
                let posicionEjeX = (espacioEjeX * filaUbicacionBolsa) + (widthCajaBolsa * (filaUbicacionBolsa - 1))
                    + 2

                // vista con los datos de la bolsa
                let bolsa = PrototipoBotonBolsa(frame: CGRect(x: posicionEjeX, y: posicionEjeY, width: widthCajaBolsa, height: heightCajaBolsa))

                bolsa.delegate = self
                bolsa.asignarDatos(totalBolsas[index], nombreImagen: UtilCompraBolsas.obtenerNombreIcono(totalBolsas[index]), indice: index)

                vistaContenedoraBolsas.addSubview(bolsa)

                if Int(filaUbicacionBolsa) == columnas {
                    posicionEjeY = (posicionEjeY + espacioEjeY) + heightCajaBolsa
                    heightTotalVista = posicionEjeY
                    filaUbicacionBolsa = 1
                } else {
                    heightTotalVista = posicionEjeY + heightCajaBolsa
                    filaUbicacionBolsa += 1
                }
            }

            // margen desde arriba de la vista contenedora de las bolsas
            let margenTopVistaContenedora = CGFloat(60)
            // altura adicional para la vista contenedora
            let alturaAdicionalVistaContenedora = CGFloat(0)

            vistaContenedoraBolsas.frame = CGRect(x: 0, y: margenTopVistaContenedora, width: self.view.frame.width, height: heightTotalVista + alturaAdicionalVistaContenedora)
        } else {
            self.viewSinBolsas.isHidden = false
        }

        return vistaContenedoraBolsas
    }

    /// Actualiza el booleano para recarar servicios en caching
    func actualizarIndicadorRecargaServicios() {
        recargarServicios = true
    }
}
