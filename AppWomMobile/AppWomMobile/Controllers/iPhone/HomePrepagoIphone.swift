//
//  HomePrepagoIphone.swift
//  AppWomMobile
//
//  Controlador de la vista principal de la aplicación.
//  @author Esteban Pavez A. (TINet).
//

import Foundation
import UIKit
import SwiftyJSON

class HomePrepagoIphone: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Enlace con tabla de bolsas en vista
    @IBOutlet weak var tablaBolsas: UITableView!
    @IBOutlet weak var viewActivityIndicator: UIView!

    // sección del encabezado
    let seccionEncabezado = 0
    // sección datos móviles
    let seccionDatosMoviles = 1
    // sección datos móviles
    let seccionBolsasPromo = 2
    // sección datos móviles
    let seccionBolsasCompradas = 3
    // sección minutos
    let seccionMinutos = 4
    // sección bolsas minutos promocionales
    let seccionBolsasMinutosPromo = 5
    // sección bolsas minutos compradas
    let seccionBolsasMinutosCompradas = 6
    // sección minutos
    let seccionSMS = 7
    // sección pie de pagina
    let seccionPiePagina = 8
    // arreglo de bolsas datos moviles
    var bolsasDatos = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales
    var bolsasDatosPromo = [BolsaTO]()
    // arreglo de bolsas datos moviles Agrupada
    var bolsasDatosAgrupada = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales Agrupada
    var bolsasDatosPromoAgrupada = [BolsaTO]()
    // arreglo de bolsas minutos
    var bolsasMinutos = [BolsaTO]()
    // arreglo de bolsas minutos
    var bolsasMinutosAgrupada = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales
    var bolsasMinutosPromo = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales Agrupada
    var bolsasMinutosPromoAgrupada = [BolsaTO]()
    // arreglo de bolsas mensajes
    var bolsasMensajes = [BolsaTO]()
    // indicador de bolsas de datos vacia
    var bolsasDatosCompradasVacia = false
    // indicador de bolsas de datos vacia
    var bolsasDatosPromoVacia = false
    // indicador de bolsas de minutos vacia
    var bolsasMinutosCompradasVacia = false
    // indicador de bolsas de minutos vacia
    var bolsasMinutosPromoVacia = false
    // indicador de bolsas de sms vacia
    var bolsasMensajesVacia = false
    // índice de fila seleccionada
    var indexSeleccionado = Int()
    // Secciones de la tabla
    let secciones = ["cabecera", "seccion datos moviles", "bolsas compradas", "bolsas promocionales", "seccion Minutos", "bolsas minutos compradas", "bolsas minutos promocionales"]
    // alto de secciones
    let altoSeccion = CGFloat(70)
    // indice de fila seleccionada
    var selectedIndexPath: IndexPath?
    // objeto con datos de cliente
    var datosCliente = EncabezadoHomeTO()
    // objeto con seccion de encabezado
    var viewSectionEncabezado = CeldaEncabezadoHomePrepagoIphone()
    /// Variable con que se maneja refresco de vista
    var refreshControl: UIRefreshControl!
    // Seccion gris al final de las secciones
    let seccionPieSeccion = 1

    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refrescarHome), for: UIControlEvents.valueChanged)
        tablaBolsas.addSubview(refreshControl)
    }

    /// Función que se llama despues de deslizar la pantalla en el home y que refresca vista
    ///
    /// - Parameter sender: objeto realiza acción
    func refrescarHome(sender: AnyObject) {
        Util.agregarActivityIndicator(self.tabBarController!.view)
        UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
            Util.removerActivityIndicator(self.tabBarController!.view)
            if retorno {
                self.llamarServicios()
            }
        }
        refreshControl.endRefreshing()
    }

    /// Funcion que detecta scroll en tabla
    ///
    /// - Parameter scrollView: objeto de scroll en tabla
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DesignHelper.bloquearBounceInferior(scrollView: scrollView)
    }

    override func viewDidAppear(_ animated: Bool) {
        self.viewActivityIndicator.isHidden = true
        self.tablaBolsas.dataSource = self
        self.tablaBolsas.delegate = self
        Globales.homeController = self
        tablaBolsas.setContentOffset(.zero, animated: false)
        Util.agregarActivityIndicator(self.tabBarController!.view)
        UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
            Util.removerActivityIndicator(self.tabBarController!.view)
            if retorno {
                self.llamarServicios()
            }
        }
    }
    /**
     Función que se activa al volver del home del dispositivo

     - parameter notification: notificacion de reproductor.
     */
    func willEnterForeground(_ notification: Notification) {
        if Globales.tabBarSeleccionado.selectedIndex == Constants.Indicadores.tabHome {
            llamarServicios()
        }
    }

    /// Hace llamada a servicios faltantes para cargar home
    func llamarServicios() {
        Util.agregarActivityIndicator(self.tabBarController!.view)
        UtilServicios.obtenerValorSaldo { retorno  in
            if retorno {
                UtilApp.generarMigaDePan(NSLocalizedString("HOME_ONLOAD", comment: " marca home onload "))

                // Se reinicializa tabla al volver desde otra vista
                self.bolsasDatos.removeAll()
                self.bolsasDatosPromo.removeAll()
                self.bolsasDatosAgrupada.removeAll()
                self.bolsasDatosPromoAgrupada.removeAll()
                self.bolsasMinutos.removeAll()
                self.bolsasMinutosPromo.removeAll()
                self.bolsasMinutosAgrupada.removeAll()
                self.bolsasMinutosPromoAgrupada.removeAll()
                self.bolsasMensajes.removeAll()
                self.tablaBolsas.reloadData()
                self.generarArreglos()
                self.tablaBolsas.reloadData()
                self.tablaBolsas.separatorStyle = UITableViewCellSeparatorStyle.none

                if Globales.modoCampaignManager {
                    UtilServiciosPromos.generarBadgePromos(self.tabBarController!)
                    UtilServiciosPromos.desplegarHomeBanner(self, tabBarController: self.tabBarController!)
                }
            } else {
                if Globales.modoCampaignManager {
                    self.tabBarController?.tabBar.items?[4].badgeValue = "0"
                }
                UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("HOME_ONLOAD", comment: " marca home onload "))
                Util.alertErrorGeneral(self)
                self.tablaBolsas.reloadData()
            }

            Util.removerActivityIndicator(self.tabBarController!.view)

        }
    }
    /// Se generan los arreglso de las bolsas de la tabla
    func generarArreglos() {
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasDatosAgrupada, listadoBolsas: &self.bolsasDatos, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasDatosPromoAgrupada, listadoBolsas: &self.bolsasDatosPromo, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)

        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMinutosAgrupada, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMinutosPromoAgrupada, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)

        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
    }
    /**
     Función que determina el número de secciones
     - parameter tablaBolsas: tabla de bolsas en vista.
     - returns: número de celdas.
     */
    func numberOfSections(in tablaBolsas: UITableView) -> Int {
        return self.secciones.count
    }
    /**
     Función donde se setea el número de filas en la tabla
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     sección en tabla
     - returns: valor de filas que se mostrarán
     */
    func tableView(_ tablaBolsas: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case seccionBolsasCompradas:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosAgrupada.count, indicadorBolsasVacias: &bolsasDatosCompradasVacia, esconderSinBolsas: false) + seccionPieSeccion
        case seccionBolsasPromo:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosPromoAgrupada.count, indicadorBolsasVacias: &bolsasDatosPromoVacia, esconderSinBolsas: true)
        case seccionBolsasMinutosCompradas:
            return UtilBolsas.determinarNumeroFilas(self.bolsasMinutosAgrupada.count, indicadorBolsasVacias: &bolsasMinutosCompradasVacia, esconderSinBolsas: false) + seccionPieSeccion
        case seccionBolsasMinutosPromo:
            return UtilBolsas.determinarNumeroFilas(self.bolsasMinutosPromoAgrupada.count, indicadorBolsasVacias: &bolsasMinutosPromoVacia, esconderSinBolsas: true)
        default:
            Logger.log("default switch número de filas seccion: \(section)")
        }
        return 0
    }
    /**
     Función donde se construye las secciones de la tabla
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     índice de sección en tabla
     - returns: objeto de la sección generada
     */
    func tableView(_ tablaBolsas: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaEncabezadoHomePrepago")! as! CeldaEncabezadoHomePrepagoIphone
            viewSectionEncabezado = viewSection
            viewSection.prepararVista()
            return viewSection
        } else if [seccionBolsasCompradas, seccionBolsasPromo, seccionBolsasMinutosPromo, seccionBolsasMinutosCompradas].contains(section) {
            let viewSection = obtenerCeldaSeccionInterna(section)

            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        } else {
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionTabla")! as! CeldaSeccionBolsasHomeIphone

            switch section {
            case seccionDatosMoviles:
                viewSection.asignarDatos(Constants.Indicadores.seccionDatosMoviles)
            case seccionMinutos:
                viewSection.asignarDatos(Constants.Indicadores.seccionMinutos)

            default:
                viewSection.asignarDatos("")
            }

            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        }
    }
    /// Funcion que crea objeto de seccion interna
    ///
    /// - Parameter seccion: indice de seccion
    /// - Returns: objeto de seccion
    func obtenerCeldaSeccionInterna(_ seccion: Int) -> CeldaSeccionBolsasInternaIphone {
        let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSeccionBolsasInternaIphone")! as! CeldaSeccionBolsasInternaIphone

        switch seccion {
        case seccionBolsasCompradas, seccionBolsasMinutosCompradas:
            viewSection.asignarDatos(Constants.Indicadores.tipoBolsaComprada)
        case seccionBolsasPromo:
            viewSection.asignarDatos(Constants.Indicadores.tipoBolsaPromocional)
        case seccionBolsasMinutosPromo:
            viewSection.asignarDatos(Constants.Indicadores.tipoBolsaMinutosPromocional)
        default:
            Logger.log()
        }
        return viewSection
    }
    /**
     Función que estructura las filas de historial de bolsas
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila
     - returns: celda seleccionada
     */
    func tableView(_ tablaBolsas: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case seccionBolsasCompradas:
            if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasCompradas, indexPath: indexPath, bolsasAgrupada: bolsasDatosAgrupada) {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            } else {
                return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionDatosMovilesBolsasCompradas, indexPath: indexPath, indicadorBolsasVacia: bolsasDatosCompradasVacia, listadoBolsas: bolsasDatosAgrupada, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_datos", comment: "mensaje sin bolsas de datos"))
            }
        case seccionBolsasPromo:
            return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionDatosMovilesBolsasPromocionales, indexPath: indexPath, indicadorBolsasVacia: bolsasDatosPromoVacia, listadoBolsas: bolsasDatosPromoAgrupada, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_datos_promo", comment: "mensaje sin bolsas de datos"))
        case seccionBolsasMinutosCompradas:
            if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosCompradas, indexPath: indexPath, bolsasAgrupada: bolsasMinutosAgrupada) {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            } else {
                return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionMinutosBolsasCompradas, indexPath: indexPath, indicadorBolsasVacia: bolsasMinutosCompradasVacia, listadoBolsas: bolsasMinutosAgrupada, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_voz", comment: "mensaje sin bolsas de datos"))
            }
        case seccionBolsasMinutosPromo:
            if !bolsasMinutosPromoVacia {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasPromocionalesHome", for: indexPath) as! CeldaBolsaPromocionalesHomeIphone
                celda.asignarDatos(bolsasMinutosPromoAgrupada[indexPath.row], seccion: Constants.Indicadores.seccionMinutosPromocionales, fila: indexPath.row)
                return celda
            }
        case seccionSMS:
            return UtilHome.obtenerCeldaSMS(indexPath, tablaBolsas: tablaBolsas, bolsasMensajes: bolsasMensajes, bolsasMensajesVacia: bolsasMensajesVacia)
        default:
            Logger.log("Default switch cellForRowAtIndexPath")
        }
        return UITableViewCell()
    }
    /**
     Función que determina alto de secciones.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:   índice de secciones
     - returns: tamaño secciones
     */
    func tableView(_ tablaBolsas: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let heightEncabezado = CGFloat(245)
        let heightSecciones = CGFloat(70)
        let heightPiePagina = CGFloat(20)
        let heightSeccionInterna = CGFloat(80)

        if section == seccionBolsasMinutosPromo && bolsasMinutosPromo.isEmpty && bolsasMinutosPromoAgrupada.isEmpty {
            return 0
        } else if section == seccionBolsasPromo && bolsasDatosPromo.isEmpty && bolsasDatosPromoAgrupada.isEmpty {
            return 0
        }

        switch section {
        case seccionEncabezado:
            return heightEncabezado
        case seccionPiePagina:
            return heightPiePagina
        case seccionBolsasCompradas, seccionBolsasPromo, seccionBolsasMinutosCompradas, seccionBolsasMinutosPromo:
            return heightSeccionInterna
        default:
            return heightSecciones
        }
    }
    /**
     Función que determina altura de celdas.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de celda
     - returns: tamaño celda
     */
    func tableView(_ tablaBolsas: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heightCompraBolsa = CGFloat(85)
        let heightPieSeccion = CGFloat(20)

        let validacionFilaPieSeccion = (UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasCompradas, indexPath: indexPath, bolsasAgrupada: bolsasDatosAgrupada) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosCompradas, indexPath: indexPath, bolsasAgrupada: bolsasMinutosAgrupada))

        var listadoBolsasAgrupada = [BolsaTO]()
        if indexPath.section == seccionBolsasPromo {
            listadoBolsasAgrupada = bolsasDatosPromoAgrupada
        } else if indexPath.section == seccionBolsasCompradas {
            listadoBolsasAgrupada = bolsasDatosAgrupada
        } else if indexPath.section == seccionBolsasMinutosPromo {
            listadoBolsasAgrupada = bolsasMinutosPromoAgrupada
        } else if indexPath.section == seccionBolsasMinutosCompradas {
            listadoBolsasAgrupada = bolsasMinutosAgrupada
        } else if indexPath.section == seccionSMS {
            listadoBolsasAgrupada = bolsasMensajes
        }

        if indexPath.section == 0 {
            return 0
        } else if validacionFilaPieSeccion {
            return heightPieSeccion
        } else if bolsasDatosCompradasVacia && indexPath.section == seccionBolsasCompradas || bolsasMinutosCompradasVacia && indexPath.section == seccionBolsasMinutosCompradas {
            return heightCompraBolsa
        } else if UtilBolsas.validarFilaAgrupadaEnLista(indexPath.section, fila: indexPath.row, listaBolsas: listadoBolsasAgrupada) {
            return CeldaBolsasHomeIphone.heightDefecto
        } else if listadoBolsasAgrupada.count > 0 && listadoBolsasAgrupada[indexPath.row].colapsado {
            return CeldaBolsasHomeIphone.heightDefecto
        } else if listadoBolsasAgrupada.count > 0 && !listadoBolsasAgrupada[indexPath.row].colapsado {
            return CeldaBolsasHomeIphone.heightExpandida
        }
        return CeldaBolsasHomeIphone.heightDefecto
    }
    /**
     Desplegar/plegar info bolsas.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila
     */
    func tableView(_ tablaBolsas: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.section == seccionBolsasPromo {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosPromoAgrupada, listadoBolsas: &bolsasDatosPromo, indexPath: indexPath)
        } else if indexPath.section == seccionBolsasCompradas && !bolsasDatosCompradasVacia {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosAgrupada, listadoBolsas: &bolsasDatos, indexPath: indexPath)
        } else if indexPath.section == seccionBolsasMinutosPromo {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMinutosPromoAgrupada, listadoBolsas: &bolsasMinutosPromo, indexPath: indexPath)
        } else if indexPath.section == seccionBolsasMinutosCompradas && !bolsasMinutosCompradasVacia {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMinutosAgrupada, listadoBolsas: &bolsasMinutos, indexPath: indexPath)
        } else if indexPath.section == seccionSMS {
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMensajes, listadoBolsas: &bolsasMensajes, indexPath: indexPath)
        }
    }
    /**
     Obtiene la tabla de datos según el indicador.
     - parameter indicadorSeccion: indicador en string de seccion.
     - returns: arreglo solicitado.
     */
    func obtenerTablaDatos(_ indicadorSeccion: Int?) -> [BolsaTO] {
        switch indicadorSeccion {
        case self.seccionBolsasCompradas?:
            return self.bolsasDatos
        case self.seccionBolsasPromo?:
            return self.bolsasDatosPromoAgrupada
        case self.seccionBolsasMinutosCompradas?:
            return self.bolsasMinutos
        case self.seccionBolsasMinutosPromo?:
            return self.bolsasMinutosPromoAgrupada
        case self.seccionSMS?:
            return self.bolsasMensajes
        default:
            return [BolsaTO]()
        }
    }
    /**
     Función que se ejecuta antes de mostrar celda
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter cell: celda de bolsas en vista
     - parameter indexPath:   índice de celda
     */
    func tableView(_ tablaBolsas: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            if let celdaBolsas = cell as? CeldaBolsasHomeIphone {
                celdaBolsas.watchFrameChanges()
            }
        }
    }
    /**
     Función que se ejecuta despues de mostrar celda
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter cell: celda de bolsas en vista
     - parameter indexPath:   índice de celda
     */
    func tableView(_ tablaBolsas: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0 {
            if let celdaBolsas = cell as? CeldaBolsasHomeIphone {
                celdaBolsas.ignoreFrameChanges()
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Botón de navegación para volver al Home
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
}
