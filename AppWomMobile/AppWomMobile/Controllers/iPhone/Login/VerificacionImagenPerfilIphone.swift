//
//  VerificacionImagenPerfilIphone.swift
//  AppWomMobile
//
//  Controlador en donde se verifica la imagen del perfil de la app.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

protocol ProtocoloVerificacionImagenPerfilIphone: class {
    func cerrarVista()
}
class VerificacionImagenPerfilIphone: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // Enlace a imageView donde se muestra imágen de perfil
    @IBOutlet weak var imagenPerfil: UIImageView!
    // variable con que se abre el seleccionador de imágenes
    let imagePicker = UIImagePickerController()
    // Llave para acceder a la ruta de la imagen del perfil guardada
    let imagenPerfilKey = "pathImagenPerfil"
    // Ruta donde se guardan las imágenes
    var rutaDirectorioImagenes: String!
    // imagen seleccionada desde la galería
    var imagenPerfilSeleccionada: UIImage?
    // delegate a vista padre
    @IBOutlet weak var labelInfoImagenGuardada: UILabel!
    weak var delegate: ProtocoloSeleccionImagenPerfilIphone?

    override func viewDidLoad() {
        super.viewDidLoad()
        imagenPerfil.image = Globales.imagenPerfilTemporal
        imagenPerfilSeleccionada = nil
        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)
        imagePicker.delegate = self
        labelInfoImagenGuardada.attributedText = Util.modificarAtributosTexto(NSLocalizedString("label_info_imagen_guardada", comment: "texto informativo de imagen guardada"), tamFuente: 13, tipoFuente: Constants.TipoFuente.ceraLight, textoModificado: "NO", textoSecundarioModificado: "", colorFuente: DesignHelper.UIColorFromRGB(0xFFFFFF))
        generarCarpetaImagenes()
    }

    /// Función que detecta cuando el usuario elige una imágen de la galería
    ///
    /// - parameter picker: controlador del seleccionador
    /// - parameter info:   arreglo de hashtag con la imagen seleccionada
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagenSeleccionada = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagenPerfil.image = imagenSeleccionada
            imagenPerfilSeleccionada = imagenSeleccionada

            imagenPerfil.layer.borderWidth = 5
            imagenPerfil.layer.masksToBounds = false
            imagenPerfil.layer.borderColor = UIColor.white.cgColor
            imagenPerfil.layer.cornerRadius = imagenPerfil.frame.height/2
            imagenPerfil.clipsToBounds = true

        }

        dismiss(animated: true, completion: nil)
    }

    /// Función con acción de botón para guardar cambios
    ///
    /// - parameter sender: objeto de botón
    @IBAction func guardarCambiosImagen(_ sender: AnyObject) {
        var imagePath = Date().description
        imagePath = imagePath.replacingOccurrences(of: " ", with: "")

        PlistManager.sharedInstance.saveValue("/\(imagePath).png" as AnyObject, forKey: imagenPerfilKey)

        imagePath = rutaDirectorioImagenes + "/\(imagePath).png"
        if imagenPerfilSeleccionada != nil {
            let data = UIImagePNGRepresentation(DesignHelper.corregirOrientacionImagen(imagenPerfilSeleccionada!))
            FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
            Globales.imagenPerfil = imagenPerfilSeleccionada
        } else {
            let data = UIImagePNGRepresentation(DesignHelper.corregirOrientacionImagen(Globales.imagenPerfilTemporal!))
            FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
            Globales.imagenPerfil = Globales.imagenPerfilTemporal
        }

        abrirPopupConfirmacionImagenGuardada()
    }

    /// Función que genera la carpeta en donde se guardará la imágen seleccionada en caso de no haberla creado ya
    func generarCarpetaImagenes() {
        var objcBool: ObjCBool = true
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // Crea la ruta del directorio previo a donde se ingresan las imágenes
        let documentDirectorPath: String = paths[0]
        // Crea la ruta del directorio de imágenes
        rutaDirectorioImagenes = documentDirectorPath + "/ImagenPerfil"

        let isExist = FileManager.default.fileExists(atPath: rutaDirectorioImagenes, isDirectory: &objcBool)
        // Si no existe, se crea
        if isExist == false {
            do {
                try FileManager.default.createDirectory(atPath: rutaDirectorioImagenes, withIntermediateDirectories: true, attributes: nil)
            } catch {
                Logger.log("Hubo un error al generar el directorio de las imágenes")
            }
        }
    }

    /// Abre el popup para continuar al home
    func abrirPopupConfirmacionImagenGuardada() {
        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let popupMensajeController = storyboard.instantiateViewController(withIdentifier: "imagenGuardada") as! InfoImagenGuardadaIphone
        popupMensajeController.delegate = self

        self.addChildViewController(popupMensajeController)
        popupMensajeController.view.frame = self.view.frame
        self.view.addSubview(popupMensajeController.view)
        popupMensajeController.didMove(toParentViewController: self)

    }

    /// Función con acción de botón para elegir una nueva imágen de la galería
    ///
    /// - parameter sender: objeto de botón
    @IBAction func elegirOtraImagen(_ sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary

        present(imagePicker, animated: true, completion: nil)
    }

    /// Función con acción de botón cancelar que redirige al home
    ///
    /// - parameter sender: objeto de botón
    @IBAction func irAHome(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
}
extension VerificacionImagenPerfilIphone : ProtocoloVerificacionImagenPerfilIphone {
    func cerrarVista() {
        dismiss(animated: true, completion: nil)
    }
}
