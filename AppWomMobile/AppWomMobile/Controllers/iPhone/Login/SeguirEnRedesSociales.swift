//
//  SeguirEnRedesSociales.swift
//  AppWomMobile
//
//  Created by Juan Tamayo on 01-02-17.
//  Copyright © 2017 Tinet. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit
import FacebookLogin
import FacebookCore
import FBSDKShareKit
import Firebase


import SwiftyJSON

class SeguirEnRedesSociales: UIViewController, UINavigationControllerDelegate, URLSessionDelegate {
    @IBOutlet weak var comenzarButt: UIButton!
    @IBOutlet weak var twiterButt: UIButton!
    @IBOutlet weak var seguir: UIButton!
    @IBOutlet weak var siguiendo: UIButton!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var omitir: UIButton!
    // Boton donde se le hace me gusta a wom
    @IBOutlet weak var btnFacebookLike: UIButton!
    // Label donde indica los seguidores actuales de facebook de wom
    @IBOutlet weak var seguidoresFacebook: UILabel!
    // Llave para acceder a la ruta de la imagen del perfil guardada
    let imagenPerfilKey = "pathImagenPerfil"
    // Ruta donde se guardan las imágenes
    var rutaDirectorioImagenes: String!
    var follow = false

    let likeButton: FBSDKLikeControl = FBSDKLikeControl()
    let client = TWTRAPIClient()
    var twUrl: URL = URL(string: "twitter://user?screen_name=womchile")!
    var twUrlWeb: URL = URL(string: "https://twitter.com/womchile")!
    var idFBWOM = String()

    override func viewWillAppear(_ animated: Bool) {
        obtenerEstadoLike()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_RRSS_PASO2_ONLOAD", comment: " marca de ingreso RRSS paso 2 onload"))

        //si hay session en twitter ir a preguntar por si es seguidor
        if Globales.loginTwitter {
            self.comenzarButt.isHidden = false
            self.omitir.isHidden = true
            self.esSeguidor { retorno in
                DispatchQueue.main.async(execute: {
                    if retorno {
                        self.siguiendo.isHidden = false
                        self.seguir.isHidden = true
                        self.follow = true
                    } else {
                        // llamar a seguir en twiter
                        self.follow = false
                        self.siguiendo.isHidden = true
                        self.seguir.isHidden = false
                    }
                })
            }
        } else if FBSDKAccessToken.current() != nil {
            self.comenzarButt.isHidden = false
            self.omitir.isHidden = true
        }

        obtenerSeguidoresTwitter()
        obtenerSeguidoresFacebook()

        likeButton.objectID = "https://www.facebook.com/womchile"
        likeButton.isHidden = true
        likeButton.likeControlStyle = .boxCount
        self.view.addSubview(likeButton)
    }

    func obtenerSeguidoresTwitter() {
        // trae los seguidores de womchile twitter
        let urlConnectionTwitter = "https://api.twitter.com/1.1/users/show.json?screen_name=womchile"
        let params = NSMutableDictionary()
        var clientError: NSError?
        let request = client.urlRequest(withMethod: "GET", url: urlConnectionTwitter, parameters: params as! [AnyHashable: Any], error: &clientError)

        if Twitter.sharedInstance().sessionStore.session() != nil {
            let userID = Twitter.sharedInstance().sessionStore.session()!.userID
            let client = TWTRAPIClient(userID: userID)

            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError == nil {
                    let seguidores = UtilApp.obtenerSeguidoresTwitter(data!)
                    self.followers.text = Util.formateaFollow(seguidores as NSNumber)
                } else {
                    Logger.log("Error: \(connectionError ?? "" as! Error)")
                }
            }
        } else {
            let client = TWTRAPIClient()

            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                if connectionError == nil {

                    let seguidores = UtilApp.obtenerSeguidoresTwitter(data!)
                    self.followers.text = Util.formateaFollow(seguidores as NSNumber)

                    let url = URL(string: "https://api.twitter.com")!
                    let cookies = HTTPCookieStorage.shared.cookies(for: url)!
                    for cookie: HTTPCookie in cookies {
                        HTTPCookieStorage.shared.deleteCookie(cookie)
                    }

                    if client.userID != nil {
                        Twitter.sharedInstance().sessionStore.logOutUserID(client.userID!)
                    }
                } else {
                    Logger.log("Error: \(connectionError ?? "" as! Error)")
                }
            }
        }
    }

    /// Obtiene cantidad de seguidores de facebook de WOM
    func obtenerSeguidoresFacebook() {
        Util.agregarActivityIndicator(self.view)

        if FBSDKAccessToken.current() != nil {
            let request = FBSDKGraphRequest(graphPath: "/womchile", parameters: ["fields": "id, name, fan_count"])
            request?.start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    let fbDetails = result as! NSDictionary
                    self.idFBWOM = fbDetails.value(forKey: "id") as! String
                    self.seguidoresFacebook.text = Util.formateaFollow(fbDetails.value(forKey: "fan_count") as! NSNumber)
                    self.obtenerEstadoLike()
                } else {
                    Util.removerActivityIndicator(self.view)
                    Logger.log("Error al obtener fans de wom")
                }
            })
        } else {
            // Se crea un token al no haber sesion para obtener seguidores
            FBSDKAccessToken.setCurrent(FBSDKAccessToken.init(tokenString: Constants.RRSSIdentificador.tokenFacebook, permissions: ["email", "user_likes", "user_friends", "public_profile"], declinedPermissions: [], appID: Constants.RRSSIdentificador.appId, userID: "", expirationDate: nil, refreshDate: nil))

            if FBSDKAccessToken.current() != nil {
                let request = FBSDKGraphRequest(graphPath: "/womchile", parameters: ["fields": "id, name, fan_count"])
                request?.start(completionHandler: { (connection, result, error) -> Void in
                    if error == nil {
                        let fbDetails = result as! NSDictionary
                        self.idFBWOM = fbDetails.value(forKey: "id") as! String
                        self.seguidoresFacebook.text = Util.formateaFollow(fbDetails.value(forKey: "fan_count") as! NSNumber)

                        FBSDKAccessToken.setCurrent(nil)

                        self.obtenerEstadoLike()
                    } else {
                        Logger.log("Error al obtener fans de wom")
                    }
                    FBSDKAccessToken.setCurrent(nil)
                    Util.removerActivityIndicator(self.view)
                })
            } else {
                FBSDKAccessToken.setCurrent(nil)
                Util.removerActivityIndicator(self.view)
                Logger.log("Error al obtener fans de wom")
            }

        }
    }


    /// Obtiene el estado del "me gusta" del usuario
    func obtenerEstadoLike() {
        if FBSDKAccessToken.current() != nil {
            let request = FBSDKGraphRequest(graphPath: "/me/likes/" + idFBWOM, parameters: ["fields": "name"])
            request?.start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    let fbDetails = result as! NSDictionary
                    let myBoard: NSArray = fbDetails.value(forKey: "data") as! NSArray
                    if myBoard.count > 0 {
                        self.btnFacebookLike.setBackgroundImage(UIImage(named: "fb_liked"), for: UIControlState())
                    } else {
                        self.btnFacebookLike.setBackgroundImage(UIImage(named: "fb_like"), for: UIControlState())
                    }
                } else {
                    Logger.log("Error al obtener likes")
                }
                Util.removerActivityIndicator(self.view)
            })
        } else {
            Util.removerActivityIndicator(self.view)
        }
    }

    @IBAction func comenzar(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_BTN_COMENZAR", comment: " marca de ingreso facil boton comenzar"))

        UtilApp.abrirHome(self)
    }

    func esSeguidor (_ onCompletion: @escaping (_ retorno: Bool) -> ()) {

        Util.agregarActivityIndicator(self.view)

        var esSeguidor = false
        if Twitter.sharedInstance().sessionStore.session() != nil {
            let userID = Twitter.sharedInstance().sessionStore.session()!.userID
                let client = TWTRAPIClient(userID: userID)
                let urlConnectionTwitter = "https://api.twitter.com/1.1/friendships/lookup.json?screen_name=womchile"
                let params = NSMutableDictionary()
                var clientError: NSError?
                let request = client.urlRequest(withMethod: "GET", url: urlConnectionTwitter, parameters: params as! [AnyHashable: Any], error: &clientError)
                client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                    if connectionError == nil {

                        let data = data,
                        jsonString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        if jsonString!.lowercased.range(of: "following") != nil {
                            esSeguidor = true
                            self.follow = true
                        } else {
                            esSeguidor = false
                            self.follow = false
                        }

                        DispatchQueue.main.async(execute: {
                            onCompletion(esSeguidor)
                            Util.removerActivityIndicator(self.view)

                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            Logger.log("Error: \(connectionError ?? "" as! Error)")
                            onCompletion(esSeguidor)
                            Util.removerActivityIndicator(self.view)

                        })

                    }
                }
        } else {
            Util.removerActivityIndicator(self.view)
        }
    }

    @IBAction func seguirTwitter(_ sender: AnyObject) {

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_TW_MG", comment: " marca de ingreso facil Twitter seguir"))

        Util.agregarActivityIndicator(self.view)
        Twitter.sharedInstance().logIn {
            (session, error) -> Void in
            if session != nil && Twitter.sharedInstance().sessionStore.session() != nil {
                let userID = Twitter.sharedInstance().sessionStore.session()!.userID
                    let client = TWTRAPIClient(userID: userID)
                    let urlConnectionTwitter = "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true"
                    let params = ["include_email": "true", "skip_status": "true"]
                    var clientError: NSError?
                    let request = client.urlRequest(withMethod: "GET", url: urlConnectionTwitter, parameters: params as [AnyHashable: Any], error: &clientError)
                    client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                        if connectionError == nil {
                            Globales.nombreCliente = UtilApp.obtenerNombreTwitter(data!)
                        } else {
                            Logger.log("Error: \(connectionError ?? "" as! Error)")
                        }
                    }

                let twitterProfileUrl = "https://twitter.com/\(session!.userName)/profile_image?size=original"
                let url = URL(string:twitterProfileUrl)
                let data = try? Data(contentsOf: url!)
                if data!.count > 1 {
                    Globales.imagenPerfil = UIImage(data:data!)
                    self.generarCarpetaImagenes()
                    self.guardaImagen()
                }
                Globales.nombreCliente = "\(session!.userName)"

                self.esSeguidor { retorno in
                    DispatchQueue.main.async(execute: {
                        var urlConnectionTwitter = "https://api.twitter.com/1.1/friendships/create.json?screen_name=womchile&follow=true"
                        if retorno {
                            self.siguiendo.isHidden = false
                            self.seguir.isHidden = true
                            urlConnectionTwitter = "https://api.twitter.com/1.1/friendships/destroy.json?screen_name=womchile"
                        } else {
                            // llamar a seguir en twiter
                            self.siguiendo.isHidden = true
                            self.seguir.isHidden = false
                        }
                        self.seguirWomTwitter(urlConnectionTwitter)
                    })
                }
            } else {
                Util.removerActivityIndicator(self.view)
                Logger.log("error autentication")
            }
        }
    }


    func seguirWomTwitter(_ urlConnectionTwitter: String) {
        // llama a servicio de seguir
        self.siguiendo.isHidden = true
        self.seguir.isHidden = false
        if Twitter.sharedInstance().sessionStore.session() != nil {
            let userID = Twitter.sharedInstance().sessionStore.session()!.userID
                let client = TWTRAPIClient(userID: userID)
                let params = NSMutableDictionary()
                var clientError: NSError?
                let request = client.urlRequest(withMethod: "POST", url: urlConnectionTwitter, parameters: params as! [AnyHashable: Any], error: &clientError)
                client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                    if connectionError == nil {
                        self.comenzarButt.isHidden = false
                        self.omitir.isHidden = true
                        if self.follow {
                            //accion dejar de seguir
                            self.siguiendo.isHidden = true
                            self.seguir.isHidden = false
                        } else {
                            //accion de seguir
                            self.siguiendo.isHidden = false
                            self.seguir.isHidden = true
                        }
                    } else {
                        Logger.log("Error: \(connectionError ?? "" as! Error)")
                    }
                    Util.removerActivityIndicator(self.view)
                }
        } else {
            Logger.log("No hay sesion de twitter")
            Util.removerActivityIndicator(self.view)
        }
    }

    /// Guarda imagen en el dispositivo
    func guardaImagen() {
        var imagePath = String(describing: NSDate())
        imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        PlistManager.sharedInstance.saveValue("/\(imagePath).png" as AnyObject, forKey: imagenPerfilKey)
        imagePath = rutaDirectorioImagenes + "/\(imagePath).png"
        let data = UIImagePNGRepresentation(DesignHelper.corregirOrientacionImagen(Globales.imagenPerfil!))
        FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
    }

    /// Genera la carpeta donde se guardarán las imagenes
    func generarCarpetaImagenes() {
        var objcBool: ObjCBool = true
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // Crea la ruta del directorio previo a donde se ingresan las imágenes
        let documentDirectorPath: String = paths[0]
        // Crea la ruta del directorio de imágenes
        rutaDirectorioImagenes = documentDirectorPath + "/ImagenPerfil"

        let isExist = FileManager.default.fileExists(atPath: rutaDirectorioImagenes, isDirectory: &objcBool)
        // Si no existe, se crea
        if isExist == false {
            do {
                try FileManager.default.createDirectory(atPath: rutaDirectorioImagenes, withIntermediateDirectories: true, attributes: nil)
            } catch {
                Logger.log("Hubo un error al generar el directorio de las imágenes")
            }
        }
    }

    @IBAction func omitir(_ sender: AnyObject) {
        let resultController = self.storyboard!.instantiateViewController(withIdentifier: "seleccionImagenNombre")
        self.present(resultController, animated: true, completion: nil)
    }

    @IBAction func likeFacebook(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_FB_MG", comment: " marca de ingreso Facebook me gusta"))

        /// Se busca el boton dentro de las subvistas del boton de facebook
        for boton: AnyObject in likeButton.subviews[0].subviews {
            if boton is UIButton {
                boton.sendActions(for: .touchUpInside)
            }
        }
    }
}
