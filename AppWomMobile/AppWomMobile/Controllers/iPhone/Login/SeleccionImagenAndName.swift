//
//  SeleccionImagenAndName.swift
//  AppWomMobile
//
//  Created by Juan Tamayo on 07-02-17.
//  Copyright © 2017 Tinet. All rights reserved.
//

import UIKit
import Firebase
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


protocol ProtocoloSeleccionImagenAndName: class {
    func cerrarVista()
}
class SeleccionImagenAndName: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // Enlace a textfield de nombre
    @IBOutlet weak var txtfieldNombre: UITextField!
    // Enlace a textfield de apellido
    @IBOutlet weak var txtfieldApellido: UITextField!
    // Enlace a imagen con icono de error en textfield nombre
    @IBOutlet weak var iconoErrorNombre: UIImageView!
    // Enlace a imagen con icono de error en textfield apellido
    @IBOutlet weak var iconoErrorApellido: UIImageView!
    // Enlace a label con informacion de error en textfield nombre
    @IBOutlet weak var labelErrorNombre: UILabel!
    // Enlace a label con informacion de error en textfield apellido
    @IBOutlet weak var labelErrorApellido: UILabel!
    // Enlace a linea inferior textfield nombre
    @IBOutlet weak var viewLineaNombre: UIView!
    // Enlace a linea inferior textfield apellido
    @IBOutlet weak var viewLineaApellido: UIView!
    @IBOutlet weak var imagenPerfil: UIImageView!

    var imagenPerfilSeleccionada: UIImage?


    // Altura que se resta a movimiento de vista al abrir teclado
    var movimientoAdicional = CGFloat(85)
    // booleano que indica si el teclado se encuentra escondido o no
    var tecladoEscondido = true

    override func viewWillAppear(_ animated: Bool) {
        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)
        imagenPerfil.image = Globales.imagenPerfil
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_RRSS_PASO3_ONLOAD", comment: " marca de ingreso facil RRSS paso 3 onload"))

        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)

        let sizeFuente = CGFloat(14)
        txtfieldNombre.delegate = self
        txtfieldApellido.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.hideKeyboardWhenTappedAround()

        addDoneButtonOnKeyboard()
        txtfieldNombre.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("label_nombre", comment: "label en textfield nombre"), attributes:[NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraLight, size: sizeFuente)!])
        txtfieldApellido.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("label_apellido", comment: "label en textfield apellido"), attributes:[NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont (name: Constants.TipoFuente.ceraLight, size: sizeFuente)!])

        if !Globales.isIphone5 {
            movimientoAdicional = 0
        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.willResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagenSeleccionada = info[UIImagePickerControllerOriginalImage] as? UIImage {
            Globales.imagenPerfilTemporal = imagenSeleccionada

            dismiss(animated: true, completion: nil)

            let verificacionImagenPerfil = storyboard!.instantiateViewController(withIdentifier: "verificarImagenPerfil") as! VerificacionImagenPerfilIphone

            present(verificacionImagenPerfil, animated: true, completion: nil)

        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func elegirImagen(_ sender: AnyObject) {

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_RRSS_SUBIR_FOTO", comment: " marca de ingreso facil RRSS subir foto"))

        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary

        present(imagePicker, animated: true, completion: nil)
    }


    /**
     Función que se activa al presionar boton home de dispositivo.

     - parameter notification: notificacion de reproductor.
     */
    func willResignActive(_ notification: Notification) {
        self.view.endEditing(true)
    }


    /**
     Función que abre la vista para ingresar numero telefonico.

     - parameter sender: datos del boton para continuar navegacion.
     */
    @IBAction func abrirIngresoNumero(_ sender: AnyObject) {
        validarApellido()
        if validarNombre() && validarApellido() {
            Globales.nombreCliente = txtfieldNombre.text!
            Globales.apellidoCliente = txtfieldApellido.text!
            if Globales.enHome {
                cerrarVista()
            } else {
                UtilApp.abrirHome(self)
            }
        }
    }
    /**
     Función que valida campo de nombre.

     - returns: validacion del campo.
     */
    func validarNombre() -> Bool {
        if txtfieldNombre.text == "" {
            iconoErrorNombre.isHidden = false
            labelErrorNombre.isHidden = false
            viewLineaNombre.backgroundColor = DesignHelper.UIColorFromRGB(0xF5A623)
            return false
        } else {
            iconoErrorNombre.isHidden = true
            labelErrorNombre.isHidden = true
            viewLineaNombre.backgroundColor = UIColor.white

        }
        return true
    }
    /**
     Función que valida campo de apellido.

     - returns: validacion del campo.
     */
    func validarApellido() -> Bool {
        if txtfieldApellido.text == "" {
            iconoErrorApellido.isHidden = false
            labelErrorApellido.isHidden = false
            viewLineaApellido.backgroundColor = DesignHelper.UIColorFromRGB(0xF5A623)
            return false
        } else {
            iconoErrorApellido.isHidden = true
            labelErrorApellido.isHidden = true
            viewLineaApellido.backgroundColor = UIColor.white
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtfieldNombre:
            if txtfieldNombre.text == "" {
                iconoErrorNombre.isHidden = true
                labelErrorNombre.isHidden = true
                viewLineaNombre.backgroundColor = UIColor.white

            }
        case txtfieldApellido:
            if txtfieldApellido.text == "" {
                iconoErrorApellido.isHidden = true
                labelErrorApellido.isHidden = true
                viewLineaApellido.backgroundColor = UIColor.white

            }
        default:
            Logger.log("DEFAULT TEXTFIELD DID END EDITING")
        }
    }
    /**
     Función que realiza acciones al mostrar teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillShow(_ notification: Notification) {
        if tecladoEscondido {
            tecladoEscondido = false
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.view.frame.origin.y -= keyboardSize.height - movimientoAdicional
            }
        }
    }
    /**
     Función que realiza acciones al esconder teclado.

     - parameter notification: notificacion.
     */
    func keyboardWillHide(_ notification: Notification) {
        if !tecladoEscondido {
            tecladoEscondido = true
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    /**
     Funcion para abrir vista donde se ingresa el código validador.
     */
    func abrirVistaIngresoCodigo() {

        if Globales.enHome {
            cerrarVista()
        } else {
            UtilApp.abrirHome(self)
        }
    }

    /**
     Función que se llama al interactuar con el textfield.

     - parameter textField: textfield donde se escribe el codigo.
     - parameter rango: rango de textfield
     - parameter string: string a validar.
     - returns: indicador de si será modificado el string.
     */
    func textField(_ textField: UITextField, shouldChangeCharactersIn rango: NSRange, replacementString string: String) -> Bool {
        let maximoCaracteres = 20
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = (strcmp(char, "\\b") == -92)
        let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ ")
        let validacionNumeros = (string.rangeOfCharacter(from: set) != nil || isBackSpace)

        if (textField.text?.characters.count < maximoCaracteres || string.characters.count == 0) && validacionNumeros {
            return true
        } else {
            return false
        }
    }

    /**
     Función que agrega el botón aceptar en el toolbar del teclado.
     */
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)

        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        doneToolbar.barTintColor = UIColor.white

        self.txtfieldNombre.inputAccessoryView = doneToolbar
        self.txtfieldApellido.inputAccessoryView = doneToolbar
    }
    @IBAction func irAHome(_ sender: AnyObject) {
        if Globales.enHome {
            print("home")
        } else {
            UtilApp.abrirHome(self)
        }
    }

}

extension SeleccionImagenAndName : ProtocoloSeleccionImagenAndName {
    /**
     Vuelve a llamar al servicio de compra bolsas.
     */
    func cerrarVista() {
        irAHome("" as AnyObject)
    }
}
