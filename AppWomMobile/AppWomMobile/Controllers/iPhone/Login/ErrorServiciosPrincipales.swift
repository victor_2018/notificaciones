//
//  ErrorServiciosPrincipales.swift
//  AppWomMobile
//
//  Controlador de la vista del error principal de la aplicación por falla de servicios.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class ErrorServiciosPrincipales: UIViewController {
    weak var delegate: ProtocoloVerificadorCodigoLoginIphone?
    weak var delegateLauncher: ProtocoloMainViewIphone?

    @IBAction func reintentarServicio(_ sender: AnyObject) {
        cerrarVista()
        if delegate == nil {
            delegateLauncher?.reintentarServicio()
        } else {
            delegate?.reintentarServicio()
        }
    }
    /**
     Función que cierra pop-up de error general.
     */
    func cerrarVista() {
        self.dismiss(animated: true, completion: nil)
    }
}
