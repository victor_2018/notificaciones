//
//  PersonalizaFaceTwit.swift
//  AppWomMobile
//
//  Created by Juan Tamayo on 30-01-17.
//  Copyright © 2017 Tinet. All rights reserved.
//
import UIKit
import FBSDKLoginKit
import TwitterKit
import FacebookLogin
import FacebookCore
import FBSDKShareKit
import Accounts
import Firebase

class PersonalizaFaceTwit: UIViewController, UINavigationControllerDelegate, FBSDKLoginButtonDelegate {

    @IBOutlet weak var nombreFace: UILabel!
    @IBOutlet weak var imgFaceIn: UIImageView!
    @IBOutlet weak var lowerTextDes: UILabel!
    @IBOutlet weak var btnConecTwitter: UIButton!
    @IBOutlet weak var nombreTwitter: UILabel!
    @IBOutlet weak var buttonTwitterCheck: UIButton!
    @IBOutlet weak var omitir: UIButton!
    @IBOutlet weak var btnConecFacebook: UIButton!
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var botonFace: FBSDKLoginButton!
    // Llave para acceder a la ruta de la imagen del perfil guardada
    let imagenPerfilKey = "pathImagenPerfil"
    // Ruta donde se guardan las imágenes
    var rutaDirectorioImagenes: String!

    let loginButtonFace = FBSDKLoginButton()
    let client = TWTRAPIClient()
    var twUrl: URL = URL(string: "twitter://user?screen_name=SeemuApps")!
    var twUrlWeb: URL = URL(string: "https://twitter.com/womchile")!

    override func viewDidLoad() {
        super.viewDidLoad()

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_RRSS_PASO1_ONLOAD", comment: " marca de ingreso facil RRSS paso 1 onload"))

        // Se resetea sesión de facebook en el caso de no haber llegado al Home
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        // Se resetea imagen guardada por facebook
        Globales.imagenPerfil = Globales.imagenPerfilDefecto
    }

    @IBAction func twitterLogin(_ sender: AnyObject) {
        Util.agregarActivityIndicator(self.view)

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_CONNECT_TW", comment: " marca de ingreso facil Twitter"))

        Twitter.sharedInstance().logIn {
            (session, error) -> Void in
            if session != nil {

                let userID = Twitter.sharedInstance().sessionStore.session()!.userID
                let client = TWTRAPIClient(userID: userID)
                let urlConnectionTwitter = "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true"
                let params = ["include_email": "true", "skip_status": "true"]
                var clientError: NSError?

                let request = client.urlRequest(withMethod: "GET", url: urlConnectionTwitter, parameters: params as [AnyHashable: Any], error: &clientError)
                client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                    if connectionError == nil {
                        UtilApp.generarMigaDePan(NSLocalizedString("miga_login_twitter", comment: "miga de pan al conectar twitter"))
                        self.nombreTwitter.text = UtilApp.obtenerNombreTwitter(data!)
                    } else {
                        Logger.log("Error: \(connectionError ?? "" as! Error)")
                    }
                }

                Globales.loginTwitter = true

                self.btnContinuar.isHidden = false
                self.btnConecTwitter.isHidden = true
                self.buttonTwitterCheck.isHidden = false
                self.omitir.isHidden = true

                let twitterProfileUrl = "https://twitter.com/\(session!.userName)/profile_image?size=original"
                let url = URL(string: twitterProfileUrl)
                let data = try? Data(contentsOf: url!)

                if data!.count > 1 {
                    Globales.imagenPerfil = UIImage(data: data!)
                    self.generarCarpetaImagenes()
                    self.guardaImagen()
                }
            } else {
                Util.alertErrorGeneral(self)
                UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("INGRESO_FACIL_CONNECT_TW", comment: "miga de pan twitter"))
                Logger.log("error: \(error!.localizedDescription)")
            }
            Util.removerActivityIndicator(self.view)
        }
    }

    @IBAction func btnFBLoginPressed(_ sender: AnyObject) {
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        Util.agregarActivityIndicator(self.view)

        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_CONNECT_FB", comment: " marca de ingreso facil Facebook"))

        fbLoginManager.logIn(withReadPermissions: ["email", "user_likes", "user_friends", "public_profile"], handler: { (result, error) -> Void in

            if error == nil {
                let fbloginresult: FBSDKLoginManagerLoginResult = result!

                if !fbloginresult.isCancelled && fbloginresult.grantedPermissions.contains("email") {
                    self.returnUserData()
                }
            }
        })
    }

    func returnUserData() {
        if FBSDKAccessToken.current() != nil {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    self.btnContinuar.isHidden = false
                    self.btnConecFacebook.isHidden = true
                    self.imgFaceIn.isHidden = false

                    let fbDetails = result as! NSDictionary

                    let userID: String = fbDetails.value(forKey: "id") as! String

                    let facebookProfileUrl = "http://graph.facebook.com/\(userID)/picture?type=large"
                    let url = URL(string:facebookProfileUrl)
                    let data = try? Data(contentsOf: url!)

                    if data!.count > 1 {
                        Globales.imagenPerfil = UIImage(data:data!)
                        self.generarCarpetaImagenes()
                        self.guardaImagen()
                    }

                    let strFirstName: String = (fbDetails.object(forKey: "first_name") as? String)!
                    let strLastName: String = (fbDetails.object(forKey: "last_name") as? String)!

                    self.nombreFace.text = strFirstName + " " + strLastName
                    Globales.nombreCliente = strFirstName
                    Globales.apellidoCliente = strLastName
                    self.omitir.isHidden = true
                }
                Util.removerActivityIndicator(self.view)
            })
        } else {
            self.btnConecFacebook.isHidden = false
            self.imgFaceIn.isHidden = true
            self.btnContinuar.isHidden = true
            self.omitir.isHidden = false
            Util.removerActivityIndicator(self.view)
        }
    }


    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        Logger.log("login descripcion: " + result.description)
        Logger.log("error:  \(error.localizedDescription)")

    }

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        Logger.log("Logout de facebook")
    }

    @IBAction func continuar(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_BTN_CONTINUAR", comment: " marca de ingreso boton continuar"))


        let resultController = self.storyboard!.instantiateViewController(withIdentifier: "seguirRedesSociales")
        self.present(resultController, animated: true, completion: nil)
    }

    @IBAction func omitir(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("INGRESO_FACIL_BTN_OMITIR", comment: " marca de ingreso boton omitir"))

        let resultController = self.storyboard!.instantiateViewController(withIdentifier: "seguirRedesSociales")
        self.present(resultController, animated: true, completion: nil)
    }

    /// Guarda imagen en el dispositivo
    func guardaImagen() {
        var imagePath = String(describing: Date.description(Date()))
        imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        PlistManager.sharedInstance.saveValue("/\(imagePath).png" as AnyObject, forKey: imagenPerfilKey)
        imagePath = rutaDirectorioImagenes + "/\(imagePath).png"
        let data = UIImagePNGRepresentation(DesignHelper.corregirOrientacionImagen(Globales.imagenPerfil!))
        FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
    }

    /// Genera la carpeta donde se guardarán las imagenes
    func generarCarpetaImagenes() {
        var objcBool: ObjCBool = true
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // Crea la ruta del directorio previo a donde se ingresan las imágenes
        let documentDirectorPath: String = paths[0]
        // Crea la ruta del directorio de imágenes
        rutaDirectorioImagenes = documentDirectorPath + "/ImagenPerfil"

        let isExist = FileManager.default.fileExists(atPath: rutaDirectorioImagenes, isDirectory: &objcBool)
        // Si no existe, se crea
        if isExist == false {
            do {
                try FileManager.default.createDirectory(atPath: rutaDirectorioImagenes, withIntermediateDirectories: true, attributes: nil)
            } catch {
                Logger.log("Hubo un error al generar el directorio de las imágenes")
            }
        }
    }

    func cerrarVista() {
        navigationController!.popViewController(animated: false)
    }
}
