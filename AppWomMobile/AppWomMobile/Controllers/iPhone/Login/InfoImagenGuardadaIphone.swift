//
//  infoImagenGuardada.swift
//  AppWomMobile
//
//  Controlador donde se indica que la imagen de perfil ha sido guardada.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON

class InfoImagenGuardadaIphone: UIViewController {
    // Enlace a vista contenedora de pop up
    @IBOutlet weak var viewContenido: UIView!

    weak var delegate: ProtocoloVerificacionImagenPerfilIphone?

    override func viewDidLoad() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista

        DesignHelper.mostrarAnimacionPopUp(self.view)
    }

    @IBAction func irAHome(_ sender: AnyObject) {
        self.delegate?.cerrarVista()
    }
}
