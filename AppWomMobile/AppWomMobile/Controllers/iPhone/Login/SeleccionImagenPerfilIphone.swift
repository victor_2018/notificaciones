//
//  SeleccionImagenPerfilIphone.swift
//  AppWomMobile
//
//  Controlador en donde se selecciona la imagen del perfil de la app.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

protocol ProtocoloSeleccionImagenPerfilIphone: class {
    func cerrarVista()
}
class SeleccionImagenPerfilIphone: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // Enlace a imageView donde se muestra la imágen del perfil
    @IBOutlet weak var imagenPerfil: UIImageView!
    // variable para mostrar galería de fotos
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        imagenPerfil.image = Globales.imagenPerfil
        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)
        imagePicker.delegate = self
    }

    /// Función con acción de botón que abre galería de fotos
    ///
    /// - parameter sender: objeto de botón
    @IBAction func seleccionarImagen(_ sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary

        present(imagePicker, animated: true, completion: nil)
    }

    /// Función que detecta cuando el usuario elige una imágen de la galería
    ///
    /// - parameter picker: controlador del seleccionador
    /// - parameter info:   arreglo de hashtag con la imagen seleccionada
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagenSeleccionada = info[UIImagePickerControllerOriginalImage] as? UIImage {
            Globales.imagenPerfilTemporal = imagenSeleccionada

            dismiss(animated: true, completion: nil)

            let verificacionImagenPerfil = storyboard!.instantiateViewController(withIdentifier: "verificarImagenPerfil") as! VerificacionImagenPerfilIphone
            verificacionImagenPerfil.delegate = self
            present(verificacionImagenPerfil, animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    /// Función con acción de botón cancelar que redirige al home
    ///
    /// - parameter sender: objeto de botón
    @IBAction func irAHome(_ sender: AnyObject) {
        if Globales.enHome {
            cerrarVista()
        } else {
            UtilApp.abrirHome(self)
        }
    }
}
extension SeleccionImagenPerfilIphone : ProtocoloSeleccionImagenPerfilIphone {
    /**
     Vuelve a llamar al servicio de compra bolsas.
     */
    func cerrarVista() {
        //self.dismissViewControllerAnimated(true, completion: nil)
        navigationController!.popViewController(animated: false)
    }
}
