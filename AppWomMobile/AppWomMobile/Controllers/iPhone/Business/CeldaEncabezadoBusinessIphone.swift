//
//  CeldaEncabezadoBusinessIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda del encabezado del home business.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel
import Firebase

class CeldaEncabezadoBusinessIphone: UITableViewCell {
    // Enlace a nombre del cliente
    @IBOutlet weak var nombreCliente: UILabel!
    // vista con nombre y boton de new womer
    @IBOutlet weak var viewNombre: UIView!
    // Enlace a vista que indica que tiene deuda
    @IBOutlet weak var vistaDeuda: UIView!
    // Enlace a fecha de facturacion inicial
    @IBOutlet weak var fechaFacturacionInicio: UILabel!
    // Enlace a fecha de facturacion final
    @IBOutlet weak var fechaFacturacionFinal: UILabel!
    // Enlace a nombre de plan
    @IBOutlet weak var nombrePlan: BottomAlignedLabel!
    // Enlace a nombre de plan
    @IBOutlet weak var imagenWomer: UIImageView!
    // Enlace a label con nivel de womer
    @IBOutlet weak var nivelWomer: UILabel!
    // vista de fondo que contiene el nivel de womer
    @IBOutlet weak var viewNivelWomer: UIImageView!

    /**
     Función para asignar datos a encabezado
     */
    func prepararVista() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)

        nombreCliente.text = Globales.nombreCliente
        fechaFacturacionInicio.text = Globales.datosCliente.inicioConsumo
        fechaFacturacionFinal.text = Globales.datosCliente.proximoConsumo
        nombrePlan.text = Globales.datosCliente.nombrePlan
        imagenWomer.image = Globales.imagenPerfil
        imagenWomer = DesignHelper.redondearImagen(imagenWomer)
        nivelWomer.text = Globales.nivelWomer

        if Globales.nombreCliente.characters.count > UtilApp.obtenerLimiteCaracteresNombre() {
            viewNombre.frame.size.height = CGFloat(113)
            nombreCliente.frame.size.height = CGFloat(63)
            nombrePlan.frame.size.height = CGFloat(65)
        } else {
            nombrePlan.frame.size.height = CGFloat(50)
        }

        if Globales.datosCliente.tieneDeuda {
            vistaDeuda.layer.cornerRadius = radioBordeVista
        } else {
            vistaDeuda.frame.size.height = 0
            vistaDeuda.isHidden = true
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.abrirSeleccionImagen))
        imagenWomer.isUserInteractionEnabled = true
        imagenWomer.addGestureRecognizer(tapGestureRecognizer)
    }

    /// Abre vista para modificar imagen de perfil
    func abrirSeleccionImagen() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        Globales.homeController.navigationItem.backBarButtonItem = backItem


        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controladorSeleccionImagen = storyboard.instantiateViewController(withIdentifier: "personalizaPerfilRRSS") as! PersonalizaPerfilHomeIphone

        Globales.homeController.navigationController!.pushViewController(controladorSeleccionImagen as UIViewController, animated: true)
    }
    /**
     Acción para redirigir a tab comprar bolsas
     - parameter sender: enlace a boton que abre tab comprar bolsas
     */
    @IBAction func abrirComprarBolsas(_ sender: AnyObject) {

        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabComprarBolsas
    }

    /**
     Acción para redirigir a tab new womer
     - parameter sender: enlace a boton que abre tab new womer
     */
    @IBAction func abrirNewWomer(_ sender: AnyObject) {
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabNewWomer
    }
    /// Acción para pagar cuentas
    ///
    /// - parameter sender: objeto de botón pagar cuentas
    @IBAction func pagarCuenta(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_PAGO", comment: "marca home boton pago "))

        var urlPagarCuenta = URLServicios.pagarCuenta.replacingOccurrences(of: "{rut}", with: String(Globales.datosCliente.rut.characters.dropLast()))

        urlPagarCuenta = urlPagarCuenta.replacingOccurrences(of: "{dv}", with: String(Globales.datosCliente.rut.characters.last!))

        let url = URL(string: urlPagarCuenta)!

        UIApplication.shared.openURL(url)
    }
}
