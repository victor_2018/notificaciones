//
//  CeldaMinutosHomeBusinessIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda del encabezado del home business.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaMinutosHomeBusinessIphone: UITableViewCell {

    /// Abre el gestor corporativo
    ///
    /// - Parameter sender: objeto de botón que realiza acción
    @IBAction func abrirGestor(_ sender: AnyObject) {
        let url = URL(string: URLServicios.gestor)!
        UIApplication.shared.openURL(url)
    }
}
