//
//  NewWomerIphone.swift
//  AppWomMobile
//
//  Controlador donde se muestra información de new womer.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import iCarousel
import Firebase

class NewWomerIphone: UIViewController, iCarouselDataSource, iCarouselDelegate {
    // Enlace a label de nombre del cliente
    @IBOutlet weak var nombreCliente: UILabel!
    // Enlace a imagen del perfil de new womer
    @IBOutlet weak var imagenPerfil: UIImageView!
    // Enlace a label de nivel womer
    @IBOutlet weak var labelNivelWomer: UILabel!
    // Enlace a vista contenedora de carrusel
    @IBOutlet var carousel: iCarousel!
    // vista que contiene el nombre del cliente y su nivel womer
    @IBOutlet weak var vistaNombreNivelWomer: UIView!
    // arreglo de items en el carrusel
    var itemsCarrusel: [Int] = []
    // nivel inicial de new womer
    let nivelInicial = "0"


    override func viewDidAppear(_ animated: Bool) {
        UtilApp.generarMigaDePan(NSLocalizedString("PERFIL_WOMER_NVL_ONLOAD", comment: "marca perfil womer nivel onload"))

        let marcaNivel = "PERFIL_WOMER_NVL "+self.nivelInicial+"_BTN_COMENZAR"

        UtilApp.generarMigaDePan(NSLocalizedString(marcaNivel, comment: "marca boton ingreso facil"))

        nombreCliente.text = Globales.nombreCliente
        imagenPerfil.image = Globales.imagenPerfil
        imagenPerfil = DesignHelper.redondearImagen(imagenPerfil)
        //Se resetea este valor antes de llamar al servicio de nuevo
        Globales.nivelWomer = self.nivelInicial
        obtenerInfoNewWomer()
    }

    override func viewDidLoad() {
        self.recuperarBotonBack()
        carousel.type = .rotary
        carousel.bounces = false

        if Globales.nombreCliente.characters.count > obtenerLimiteCaracteresNombre() {
            vistaNombreNivelWomer.frame.size.height = CGFloat(90)
            nombreCliente.frame.size.height = CGFloat(62)
        }
    }

    /// Retorna el limite de caracteres en una linea para el nombre del cliente.
    ///
    /// - Returns: limite caracteres del nombre
    func obtenerLimiteCaracteresNombre() -> Int {
        let limiteCaracteresIphone5 = 13
        let limiteCaracteresIphone6 = 16
        let limiteCaracteresIphonePlus = 17

        if Globales.isIphone5 {
            return limiteCaracteresIphone5
        } else if Globales.isIphone6 {
            return limiteCaracteresIphone6
        } else {
            return limiteCaracteresIphonePlus
        }
    }

    /**
     Función donde se llama al servicio que trae los datos del cliente.
     */
    func obtenerInfoNewWomer() {
        Util.agregarActivityIndicator(self.tabBarController!.view)
        NewWomerInfo.sharedInstance.getInfoNewWomer(Globales.numeroTelefono,
                                                    onCompletion: {  (json: JSON) in if json.dictionaryObject != nil {

                                                        let arregloInfoNewWomer = json["data"].stringValue.components(separatedBy: ",")

                                                        Globales.nivelWomer = arregloInfoNewWomer[3][9]

                                                        DispatchQueue.main.async(execute: {
                                                            Util.removerActivityIndicator(self.tabBarController!.view)
                                                            var nivelWomer = Int()
                                                            if Int(Globales.nivelWomer)! - 1 < 0 {
                                                                nivelWomer = Int(self.nivelInicial)!
                                                            } else {
                                                                nivelWomer = Int(Globales.nivelWomer)! - 1
                                                            }
                                                            self.labelNivelWomer.text = "Womer Nivel \(nivelWomer)"
                                                            // se le resta a 1 por el indice empezar en 0
                                                            self.carousel.currentItemIndex = nivelWomer
                                                            self.carousel.reloadData()
                                                            //UtilApp.generarMigaDePan(NSLocalizedString("miga_perfil_womer_load", comment: "miga de pan al cargar perfil womer"))
                                                        })
                                                    } else {
                                                        DispatchQueue.main.async(execute: {
                                                            Globales.nivelWomer = self.nivelInicial
                                                            self.carousel.reloadData()
                                                            Util.removerActivityIndicator(self.tabBarController!.view)
                                                            Logger.log("error de servicio obtener info new womer")
                                                        })
                                                        }})
    }

    @IBAction func empezarNewWomer(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "inicioNewWomer") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // nivel maximo de womer 4 (+ 1) por indice empezar en 0
        let nivelMaximoWomer = 4

        for index in 0 ... nivelMaximoWomer {
            itemsCarrusel.append(index)
        }
    }

    /// Determina la cantidad de items en el carrusel.
    ///
    /// - parameter carousel: Objeto de carrusel
    ///
    /// - returns: cantidad de items en carrusel
    func numberOfItems(in carousel: iCarousel) -> Int {
        return itemsCarrusel.count
    }

    /// Determina la vista con que se dibuja un item en el carrusel
    ///
    /// - parameter carousel: objeto del carrusel
    /// - parameter index:    indice de ubicación de item en el carrusel
    /// - parameter view:     view reusing
    ///
    /// - returns: retorna vista con item de carrusel
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {

        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "itemWomer") as UIViewController
        let btnEmpezar = controller.view.viewWithTag(1) as! UIButton
        let nivel = controller.view.viewWithTag(2) as! UILabel
        let textoRecompensa = controller.view.viewWithTag(3) as! UILabel

        // Nivel Womer (index + 1 por indice empezar en 0)
        nivel.text = "\(itemsCarrusel[index] + 1)"
        if Globales.nivelWomer != "" {
            if (Globales.nivelWomer == nivelInicial && index == 0) || (Int(Globales.nivelWomer)! - 1) == index {
                btnEmpezar.addTarget(self, action: #selector(empezarNewWomer), for:.allTouchEvents)
            } else if index < Int(Globales.nivelWomer)! {
                btnEmpezar.isUserInteractionEnabled = false
                btnEmpezar.setBackgroundImage(UIImage(named: ""), for: UIControlState())
                btnEmpezar.setTitleColor(UIColor.gray, for: UIControlState())
                btnEmpezar.setTitle(NSLocalizedString("lbl_nivel_finalizado", comment: "texto nivel new womer terminado"), for: UIControlState())
            } else {
                btnEmpezar.isUserInteractionEnabled = false
                btnEmpezar.setBackgroundImage(UIImage(named: ""), for: UIControlState())
                btnEmpezar.setTitleColor(UIColor.gray, for: UIControlState())
                btnEmpezar.setTitle(NSLocalizedString("lbl_nivel_pendiente", comment: "texto nivel new womer terminado"), for: UIControlState())
            }
        }

        let bolsaSize = determinarValorBolsa(itemsCarrusel[index])

        textoRecompensa.text = "\(NSLocalizedString("lbl_regalo_bolsa", comment: "texto regalo bolsa")) \(bolsaSize)"

        return controller.view
    }

    /// Determina los megas que se regalan al pasar de nivel en new womer.
    ///
    /// - Parameter indiceCarrusel: indice que se está pintando
    /// - Returns: valor de la bolsa con su métrica
    func determinarValorBolsa(_ indiceCarrusel: Int) -> String {
        let indiceNivelMaximo = 4
        let mbPrimerasPostpago = "200 MB"
        let mbFinalPostpago = "1 GB"
        let mbPrimerasPrepago = "100 MB"
        let mbFinalPrepago = "500 MB"

        if Globales.datosCliente.tipoPlan == Constants.Indicadores.tipoPrepago {
            if indiceCarrusel < indiceNivelMaximo {
                return mbPrimerasPrepago
            } else {
                return mbFinalPrepago
            }
        } else {
            if indiceCarrusel < indiceNivelMaximo {
                return mbPrimerasPostpago
            } else {
                return mbFinalPostpago
            }
        }
    }

    /// Determina la posición de las cajas del carrusel
    ///
    /// - parameter carousel: objeto de la caja del carrusel
    /// - parameter option:   opcion iCarousel
    /// - parameter value:    posición carrusel por defecto
    ///
    /// - returns: valor de posición
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == .spacing {
            return value / 1.2
        }
        if option == .visibleItems {
            return 3
        }
        if option == .arc {
            return CGFloat(M_PI)
        }
        if option == .wrap {
            return 0.0
        }
        return value
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Botón de navegación para volver al Home
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
    /**
     Reaparece el botón de la navegación para atrás.
     */
    func recuperarBotonBack() {
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
}
