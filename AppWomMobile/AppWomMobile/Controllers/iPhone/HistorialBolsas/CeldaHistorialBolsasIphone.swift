//
//  CeldaHistorialBolsasIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaHistorialBolsasIphone: UITableViewCell {

    @IBOutlet weak var nombreBolsa: UILabel!
    @IBOutlet weak var fechaActivacion: UILabel!
    @IBOutlet weak var fechaCulminacion: UILabel!
    @IBOutlet weak var valorTotal: UILabel!
}
