//
//  CeldaEncabezadoHistorialIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda del encabezado del historial.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaEncabezadoHistorialIphone: UITableViewCell {

    @IBOutlet weak var tituloVista: UILabel!
}
