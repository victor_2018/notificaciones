//
//  CeldaEncabezadoHomeControladoIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda del encabezado del home controlado.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel

class CeldaEncabezadoHomeControladoIphone: UITableViewCell {
    // Label de Deuda a pagar
    @IBOutlet weak var deudaAPagar: UILabel!
    // Label de saldo actual
    @IBOutlet weak var saldoActual: UILabel!
    // Label de inicio del consumo
    @IBOutlet weak var inicioConsumo: UILabel!
    // Label de proxima facturacion
    @IBOutlet weak var proximaFacturacion: UILabel!
    // Label del nombre del plan
    @IBOutlet weak var nombrePlan: UILabel!
    // Label del nombre del cliente
    @IBOutlet weak var nombreCliente: UILabel!
    // Enlace a imagen de womer
    @IBOutlet weak var imagenWomer: UIImageView!
    // vista de fondo que contiene el nivel de womer
    @IBOutlet weak var viewNivelWomer: UIImageView!
    // vista que contiene nombre y botón de new womer
    @IBOutlet weak var viewNombre: UIView!
    // Label con "tu saldo"
    @IBOutlet weak var labelTuSaldo: UILabel!
    // Enlace a numero de telefono en vista
    @IBOutlet weak var numeroTelefono: UILabel!
    // Enlace a vista donde indica si tiene o no una cuenta por pagar
    @IBOutlet weak var viewPagarCuenta: UIView!
    // Enlace a vista con informacion de ciclo de facturacion
    @IBOutlet weak var viewAreaFacturacion: UIView!
    // Enlace a label del reinicio del ciclo
    @IBOutlet weak var lblReinicioCiclo: UILabel!
    // Enlace a label de la fecha de reinicio del ciclo
    @IBOutlet weak var lblFechaReinicioCiclo: UILabel!
    // Enlace a imagen de chevron de area de facturacion
    @IBOutlet weak var imgChevronAreaFacturacion: UIImageView!
    // Enlace a imagen de chevron de vista pagar cuenta
    @IBOutlet weak var imgChevronPagarCuenta: UIImageView!
    // Enlace a imagen del icono en la vista de pagar cuenta
    @IBOutlet weak var imgIconoPagarCuenta: UIImageView!
    // Enlace a boton donde se ve la ultima boleta pagada
    @IBOutlet weak var btnVerUltimaBoletaPagada: UIButton!
    // Enlace a vista que contiene botones de compra bolsa y recargar
    @IBOutlet weak var viewFooter: UIView!
    // Enlace a label de pagar cuenta
    @IBOutlet weak var lblPagarCuenta: UILabel!
    // Enlace a botón para pagar cuenta
    @IBOutlet weak var btnPagarAhora: UIButton!
    // Enlace a botón para ver la ultima boleta
    @IBOutlet weak var btnVerBoleta: UIButton!
    // Enlace a label con informacion de reinicio de consumo
    @IBOutlet weak var lblInfoReinicioConsumo: UILabel!
    // Enlace a botón para comprar bolsas
    @IBOutlet weak var btnComprarBolsas: UIButton!
    // Enlace a botón para recargar saldo
    @IBOutlet weak var btnRecargar: UIButton!
    // Booleano que indica si la vista de pagar cuenta se encuentra desplegada
    @IBOutlet weak var viewContenedora: UIView!
    /// boton que abre multilineas
    @IBOutlet weak var btnMultiLineas: UIButton!
    /// label inferior a boton multilineas
    @IBOutlet weak var lblMultiLineas: UILabel!
    /// vista que contiene las lineas hijas
    @IBOutlet weak var viewMultilineas: UIView!
    /// vista del area del perfil
    @IBOutlet weak var viewPerfil: UIView!
    /// vista que contiene a boton para pagar cuenta
    @IBOutlet weak var viewContenedoraPagarCuenta: UIView!
    /// vista para cerrar multilinea
    @IBOutlet weak var viewCerrarMultilinea: UIView!
    // icono wom superior
    @IBOutlet weak var logoWom: UIImageView!

    /// Indicador de vista pagar cuenta abierta
    var vistaPagarCuentaAbierta = false
    // Booleano que indica si la vista de area de facturacion se encuentra desplegada
    var vistaAreaFacturacionAbierta = false
    // Booleano que indica si la vista de multilinea se encuentra desplegada
    var vistaMultilineaAbierta = false
    // delegate para conectarse con home de postpago y controlado
    weak var delegate: UIViewController?

    // Indicador de vista pagar cuenta desplegada
    let seccionVistaPagarCuentaAbierta = 1
    // Indicador de vista area facturacion desplegada
    let seccionAreaFacturacionAbierta = 2
    // Indicador de vista multilinea desplegada
    let seccionMultilineaAbierta = 3
    // Indicador de vistas cerradas
    let seccionesEscondidas = 4
    /// altura del area de facturacion al estar abierta
    var heightAreaFacturacionAbierta = CGFloat(560)
    /// altura de vista de pagar cuenta al estar abierta
    var heightPagarCuentaAbierta = CGFloat(460)
    /// altura de seccion con todo cerrado
    let heightSeccionInicial = CGFloat(390)
    /// altura de que se separa el area de perfil al haber bigboss
    var factorBigBoss = CGFloat(50)
    // calcula de altura de vista multilinea
    let factorMultilineaAbierto = CGFloat(Globales.datosCliente.contratos.count * 167)

    /// Se inicializa la vista
    func prepararVista() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        /// Indice de posicion de espacio de numero telefono
        let posicionEspacioVacio = 4

        generarVistaMultilinea()
        generarVistaGuardada()

        nombreCliente.text = Globales.nombreCliente
        nombrePlan.text = String(Globales.datosCliente.nombrePlan)
        inicioConsumo.text = Globales.datosCliente.inicioConsumo
        proximaFacturacion.text = Globales.datosCliente.proximoConsumo
        imagenWomer.image = Globales.imagenPerfil
        imagenWomer = DesignHelper.redondearImagen(imagenWomer)
        numeroTelefono.text = "+" + Globales.numeroTelefono
        numeroTelefono.text = UtilApp.insertarCaracterEnString(caracter: " ", texto: numeroTelefono.text!, posicion: posicionEspacioVacio)
        viewPagarCuenta.layer.cornerRadius = radioBordeVista

        if Globales.datosCliente.tipoPlan != Constants.Indicadores.tipoPostpago {
            saldoActual.text = Globales.datosCliente.saldoFormateado
        } else {
            btnComprarBolsas.frame.size.width = CGFloat(340)
            btnComprarBolsas.setBackgroundImage(UIImage(named: Constants.Imagenes.imgBtnSinFondoLargo), for: .normal)
            btnRecargar.isHidden = true
            labelTuSaldo.isHidden = true
            saldoActual.isHidden = true
            nombrePlan.frame.size.height = nombrePlan.frame.size.height + (nombrePlan.frame.size.height * 0.5)
        }

        if Globales.datosCliente.tieneDeuda {
            mostrarVistaConDeuda()
        } else {
            mostrarVistaSinDeuda()
        }

        validarReinicioCiclo()
        asignarFuncionesVistas()
    }

    /// Genera la lista con todas las lineas hijas
    func generarListadoMultilineas() {
        viewMultilineas.frame.size.height = factorMultilineaAbierto
        viewMultilineas.frame.size.height = viewMultilineas.frame.size.height + 100
        viewMultilineas.frame.size.width = Globales.anchuraDispositivo

        for (index, linea) in Globales.datosCliente.contratos.enumerated() {
            let storyboard = UIStoryboard(name: "Home_iphone", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "vistaLineaHija") as! VistaLineaHija

            controller.numero = linea.nroCelular

            if index == 0 {
                controller.view.viewWithTag(1)?.frame.origin.y = CGFloat(50)
            } else {
                controller.view.viewWithTag(1)?.frame.origin.y = (controller.view.viewWithTag(1)?.frame.size.height)! * CGFloat(index)
                controller.view.viewWithTag(1)?.frame.origin.y = (controller.view.viewWithTag(1)?.frame.origin.y)! + CGFloat(50)
            }

            if linea.tipoContrato.uppercased() == Constants.Indicadores.tipoPostpago {
                controller.esPostpago = true
            }

            let yourViewBorder = CAShapeLayer()
            yourViewBorder.strokeColor = DesignHelper.UIColorFromRGB(0x6B3C89).cgColor
            yourViewBorder.lineDashPattern = [2, 2]
            yourViewBorder.frame = controller.view.viewWithTag(1)!.bounds
            yourViewBorder.fillColor = nil
            yourViewBorder.path = UIBezierPath(rect: controller.view.viewWithTag(1)!.bounds).cgPath
            controller.view.viewWithTag(1)!.layer.addSublayer(yourViewBorder)

            let tapCompraBolsas = UITapGestureRecognizer(target:self, action:#selector(self.comprarBolsasMultilinea))
            controller.btnComprarBolsas.isUserInteractionEnabled = true
            controller.btnComprarBolsas.addGestureRecognizer(tapCompraBolsas)

            let tapRecargarSaldo = UITapGestureRecognizer(target:self, action:#selector(self.abrirRecarga(_:)))
            controller.btnRecargarSaldo.isUserInteractionEnabled = true
            controller.btnRecargarSaldo.addGestureRecognizer(tapRecargarSaldo)

            viewMultilineas.addSubview(controller.view.viewWithTag(1)!)
            controller.prepararVista()
        }
    }

    /// Abre popup para comprarle bolsa a la linea
    func comprarBolsasMultilinea(sender: AnyObject) {
        /// Datos del cliente de la linea elegida
        let datosCliente = EncabezadoHomeTO()
        /// vista obtenida desde el boton compra bolsas
        var vista = UIView()

        vista = sender.view

        if vista.superview?.viewWithTag(3)?.isHidden == true {
            datosCliente.tipoPlan = Constants.Indicadores.tipoPostpago
        }

        var numeroTelefono = UILabel()
        numeroTelefono = vista.superview?.viewWithTag(4) as! UILabel

        datosCliente.numeroTelefono = numeroTelefono.text!
        datosCliente.numeroTelefono = String(datosCliente.numeroTelefono.characters.dropFirst())
        datosCliente.ratePlan = UtilMultilinea.obtenerRatePlan(numero: datosCliente.numeroTelefono)

        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        let popupController = storyboard.instantiateViewController(withIdentifier: "popupCompraBolsasMultilinea") as! PopupCompraBolsasMultilinea

        popupController.datosCliente = datosCliente

        Globales.tabBarSeleccionado.addChildViewController(popupController)
        popupController.view.frame = Globales.tabBarSeleccionado.view.frame
        Globales.tabBarSeleccionado.view.addSubview(popupController.view)
        popupController.didMove(toParentViewController: Globales.tabBarSeleccionado)
    }

    /// Arma vista verificando bigboss
    func generarVistaMultilinea() {
        /// Ajuste de logo wom al no ser big boss
        let ajusteLogoWom = CGFloat(15)

        if Globales.datosCliente.esBigBoss {
            self.btnMultiLineas.setImage(UIImage(named: Constants.Imagenes.imgMultilinea), for: .normal)
            factorBigBoss = 0
            self.logoWom.isHidden = false
            generarListadoMultilineas()
        } else {
            self.logoWom.isHidden = true
            self.btnMultiLineas.isUserInteractionEnabled = false
            self.viewNivelWomer.isHidden = true
            self.btnMultiLineas.setImage(UIImage(named: Constants.Imagenes.imgLogoWom), for: .normal)
            self.lblMultiLineas.isHidden = true
            self.viewContenedora.frame.origin.y = self.viewContenedora.frame.origin.y - factorBigBoss
            self.heightAreaFacturacionAbierta = heightAreaFacturacionAbierta - factorBigBoss
            self.heightPagarCuentaAbierta = heightPagarCuentaAbierta - factorBigBoss
            self.btnMultiLineas.frame.origin.x = self.btnMultiLineas.frame.origin.x - ajusteLogoWom
        }
    }

    /// Se guardan estados de las vistas de area de facturacion
    func guardarEstadoVistas() {
        Globales.estadoVistasAreaFacturacion["vistaPagarCuenta"] = vistaPagarCuentaAbierta
        Globales.estadoVistasAreaFacturacion["vistaDetalleCiclo"] = vistaAreaFacturacionAbierta
        Globales.estadoVistasAreaFacturacion["vistaMultilinea"] = vistaMultilineaAbierta
    }

    /// Se regenera la vista a como estaba antes de hacer refresco
    func generarVistaGuardada() {
        vistaPagarCuentaAbierta = Globales.estadoVistasAreaFacturacion["vistaPagarCuenta"]!
        vistaAreaFacturacionAbierta = Globales.estadoVistasAreaFacturacion["vistaDetalleCiclo"]!
        vistaMultilineaAbierta = Globales.estadoVistasAreaFacturacion["vistaMultilinea"]!

        if vistaPagarCuentaAbierta {
            self.viewPagarCuenta.frame.size.height = CGFloat(112)
            self.viewAreaFacturacion.frame.origin.y = self.viewAreaFacturacion.frame.origin.y + CGFloat(73)
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y + CGFloat(73)
            self.btnVerUltimaBoletaPagada.alpha = 1
            self.btnVerBoleta.alpha = 1
            self.btnPagarAhora.alpha = 1
            self.imgChevronPagarCuenta.image = UIImage(named: Constants.Imagenes.imgChevronWhiteUp)
            self.viewContenedora.frame.size.height = heightPagarCuentaAbierta
        } else if vistaAreaFacturacionAbierta {
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y + CGFloat(165)
            self.imgChevronAreaFacturacion.image = UIImage(named: Constants.Imagenes.imgChevronWhiteUp)
            self.viewContenedora.frame.size.height = heightAreaFacturacionAbierta
        } else if vistaMultilineaAbierta {
            self.viewAreaFacturacion.frame.origin.y = self.viewAreaFacturacion.frame.origin.y + factorMultilineaAbierto
            self.viewContenedoraPagarCuenta.frame.origin.y = self.viewContenedoraPagarCuenta.frame.origin.y + factorMultilineaAbierto
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y + factorMultilineaAbierto
            self.viewContenedora.frame.size.height = self.heightSeccionInicial + factorMultilineaAbierto
            self.viewPerfil.frame.origin.y = self.viewPerfil.frame.origin.y - CGFloat(65)
            self.viewCerrarMultilinea.isHidden = false
            self.viewMultilineas.isHidden = false
        }
    }

    /// Le asigna funciones al presionar sobre vistas
    func asignarFuncionesVistas() {
        let tapImagenWomer = UITapGestureRecognizer(target:self, action:#selector(self.abrirSeleccionImagen))
        imagenWomer.isUserInteractionEnabled = true
        imagenWomer.addGestureRecognizer(tapImagenWomer)

        if Globales.datosCliente.urlFactura != "" || Globales.datosCliente.tieneDeuda {
            let tapViewPagarCuenta = UITapGestureRecognizer(target:self, action:#selector(self.presionarVistaPagarCuenta))
            viewPagarCuenta.isUserInteractionEnabled = true
            viewPagarCuenta.addGestureRecognizer(tapViewPagarCuenta)
        }

        let tapViewAreaFacturacion = UITapGestureRecognizer(target:self, action:#selector(self.presionarVistaAreaFacturacion))
        viewAreaFacturacion.isUserInteractionEnabled = true
        viewAreaFacturacion.addGestureRecognizer(tapViewAreaFacturacion)

        let tapViewCerrarMultilinea = UITapGestureRecognizer(target:self, action:#selector(self.abrirMultilinea))
        viewCerrarMultilinea.isUserInteractionEnabled = true
        viewCerrarMultilinea.addGestureRecognizer(tapViewCerrarMultilinea)
    }

    /// Se especifican las fechas y textos para reinicio de ciclo
    func validarReinicioCiclo() {
        var newDateComponents = DateComponents()
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        newDateComponents.day = 1
        let fechaProximoCiclo = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: Globales.datosCliente.proximoConsumoDate, options: NSCalendar.Options.init(rawValue: 0))
        let fechaManiana = (Calendar.current as NSCalendar).date(byAdding: newDateComponents, to: calendar.startOfDay(for: Date()), options: NSCalendar.Options.init(rawValue: 0))
        let components = Calendar.current.dateComponents([.day], from: calendar.startOfDay(for: Date()), to: fechaProximoCiclo!)

        lblFechaReinicioCiclo.text = "(\(Util.transformarFechaNSDate(fechaProximoCiclo!, formato: "d/MM/yyyy")))"

        if fechaProximoCiclo == Date() || components.day! < 1 {
            lblInfoReinicioConsumo.text = NSLocalizedString("label_reinicio_ciclo_hoy", comment: "label reinicio ciclo hoy")
            lblReinicioCiclo.text = NSLocalizedString("label_dias_vencimiento", comment: "label texto hoy")
        } else if fechaProximoCiclo == fechaManiana || components.day! == 1 {
            lblInfoReinicioConsumo.text = NSLocalizedString("label_reinicio_ciclo_manana", comment: "label reinicio ciclo mañana")
            lblReinicioCiclo.text = NSLocalizedString("label_manana", comment: "label texto mañana")
        } else {
            lblInfoReinicioConsumo.text = NSLocalizedString("label_reinicio_ciclo_en_dias", comment: "label reinicio ciclo en x dias")
            lblReinicioCiclo.text = NSLocalizedString("label_reinicio_ciclo_en_x_dias", comment: "label con los dias faltantes a reiniciar")
            lblReinicioCiclo.text = lblReinicioCiclo.text!.replacingOccurrences(of: "{dia}", with: String(describing: components.day!))
        }
    }

    /// Animación spring para vista con deuda
    func animarVistaConDeuda() {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 7, options: .curveEaseInOut, animations: {
            self.viewPagarCuenta.alpha = 1
            self.viewPagarCuenta.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }

    /// Adecua la vista para mostrar con deuda
    func mostrarVistaConDeuda() {
        if Globales.datosCliente.urlFactura == "" {
            btnPagarAhora.frame.size.width = CGFloat(322)
            btnPagarAhora.setBackgroundImage(UIImage(named: Constants.Imagenes.imgBtnWhiteGrande), for: .normal)
            btnVerBoleta.isHidden = true
        }
        viewPagarCuenta.backgroundColor = DesignHelper.UIColorFromRGB(0xF69D18)
        imgIconoPagarCuenta.image = UIImage(named: Constants.Imagenes.imgIconoConDeuda)
        lblPagarCuenta.text = NSLocalizedString("label_cuenta_con_deuda", comment: "texto para cuando el cliente tiene deuda")
        btnVerUltimaBoletaPagada.isHidden = true
        btnVerBoleta.isHidden = false
        btnPagarAhora.isHidden = false
        self.viewPagarCuenta.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.viewPagarCuenta.alpha = 0
    }

    /// Se prepara vista pagar cuenta sin la animacion spring
    func mostrarVistaPagarCuentaSinAnimacion() {
        self.viewPagarCuenta.transform = CGAffineTransform(scaleX: 1, y: 1)
        self.viewPagarCuenta.alpha = 1
    }
    /// Adecua la vista para mostrar sin deuda
    func mostrarVistaSinDeuda() {
        if Globales.datosCliente.urlFactura == "" {
            imgChevronPagarCuenta.isHidden = true
        }
        viewPagarCuenta.backgroundColor = DesignHelper.UIColorFromRGB(0x685676)
        imgIconoPagarCuenta.image = UIImage(named: Constants.Imagenes.imgIconoSinDeuda)
        lblPagarCuenta.text = NSLocalizedString("label_cuenta_sin_deuda", comment: "texto para cuando el cliente no tiene deuda")
        btnVerBoleta.isHidden = true
        btnPagarAhora.isHidden = true
        btnVerUltimaBoletaPagada.isHidden = false
    }

    /// Animacion para esconder vista de area de facturación
    func esconderVistaAreaFacturacion() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y - CGFloat(165)
            self.viewContenedora.frame.size.height = self.heightSeccionInicial
        }, completion: nil)
        self.imgChevronAreaFacturacion.image = UIImage(named: Constants.Imagenes.imgChevronWhiteDown)
    }

    /// Animacion para esconder vista multilinea
    func esconderVistaMultilinea() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.viewAreaFacturacion.frame.origin.y = self.viewAreaFacturacion.frame.origin.y - self.factorMultilineaAbierto
            self.viewContenedoraPagarCuenta.frame.origin.y = self.viewContenedoraPagarCuenta.frame.origin.y - self.factorMultilineaAbierto
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y - self.factorMultilineaAbierto
            self.viewContenedora.frame.size.height = self.heightSeccionInicial
            self.viewPerfil.frame.origin.y = self.viewPerfil.frame.origin.y + CGFloat(65)
        }, completion: { _ in self.viewMultilineas.isHidden = true})
        self.viewCerrarMultilinea.isHidden = true
    }

    /// Animaciones para esconder vista de pagar cuenta
    func esconderVistaPagarCuenta() {
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
            self.btnVerUltimaBoletaPagada.alpha = 0
            self.btnVerBoleta.alpha = 0
            self.btnPagarAhora.alpha = 0
        }, completion: nil)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
            self.viewPagarCuenta.frame.size.height = CGFloat(60)
            self.viewAreaFacturacion.frame.origin.y = self.viewAreaFacturacion.frame.origin.y - CGFloat(73)
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y - CGFloat(73)
            self.viewContenedora.frame.size.height = self.heightSeccionInicial
        }, completion: nil)
        self.imgChevronPagarCuenta.image = UIImage(named: Constants.Imagenes.imgChevronWhiteDown)
    }

    /// Animación para mostrar area de facturacion
    func mostrarVistaAreaFacturacion() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y + CGFloat(165)
            self.viewContenedora.frame.size.height = self.heightAreaFacturacionAbierta
        }, completion: nil)
        self.imgChevronAreaFacturacion.image = UIImage(named: Constants.Imagenes.imgChevronWhiteUp)
    }

    /// Animaciones para mostrar la vista de pagar cuenta
    func mostrarVistaPagarCuenta() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
            self.viewPagarCuenta.frame.size.height = CGFloat(112)
            self.viewAreaFacturacion.frame.origin.y = self.viewAreaFacturacion.frame.origin.y + CGFloat(73)
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y + CGFloat(73)
            self.viewContenedora.frame.size.height = self.heightPagarCuentaAbierta
        }, completion: { _ in
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
                self.btnVerUltimaBoletaPagada.alpha = 1
                self.btnVerBoleta.alpha = 1
                self.btnPagarAhora.alpha = 1
            }, completion: nil)})
        self.imgChevronPagarCuenta.image = UIImage(named: Constants.Imagenes.imgChevronWhiteUp)
    }

    /// Animación para mostrar vista multilinea
    func mostrarVistaMultilinea() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.viewAreaFacturacion.frame.origin.y = self.viewAreaFacturacion.frame.origin.y + self.factorMultilineaAbierto
            self.viewContenedoraPagarCuenta.frame.origin.y = self.viewContenedoraPagarCuenta.frame.origin.y + self.factorMultilineaAbierto
            self.viewFooter.frame.origin.y = self.viewFooter.frame.origin.y + self.factorMultilineaAbierto
            self.viewContenedora.frame.size.height = self.heightSeccionInicial + self.factorMultilineaAbierto
            self.viewPerfil.frame.origin.y = self.viewPerfil.frame.origin.y - CGFloat(65)
        }, completion: nil)
        self.viewMultilineas.isHidden = false
        self.viewCerrarMultilinea.isHidden = false
    }

    /// Actualiza la altura del encabezado del home
    ///
    /// - Parameters:
    ///   - indicadorSeccion: Indica cual altura se debe usar en el controlador del home
    ///   - animacionDoble: Indicador de si se realizan ambas animaciones
    func actualizarAlturaEncabezado(indicadorSeccion: Int, animacionDoble: Bool) {
        if let delegateFinal = delegate as? HomeControladoIphone {
            delegateFinal.actualizarAlturaEncabezado(indicadorSeccion: indicadorSeccion, animacionDoble: animacionDoble)
        } else if let delegateFinal = delegate as? HomePostpagoIphone {
            delegateFinal.actualizarAlturaEncabezado(indicadorSeccion: indicadorSeccion, animacionDoble: animacionDoble)
        }
    }

    /// Despliega/pliega vista donde se ve boleta de facturacion
    func presionarVistaPagarCuenta() {

        if vistaPagarCuentaAbierta {
            actualizarAlturaEncabezado(indicadorSeccion: seccionesEscondidas, animacionDoble: false)
            esconderVistaPagarCuenta()
        } else {
            actualizarAlturaEncabezado(indicadorSeccion: seccionVistaPagarCuentaAbierta, animacionDoble: vistaAreaFacturacionAbierta)
            if vistaAreaFacturacionAbierta {
                esconderVistaAreaFacturacion()
                vistaAreaFacturacionAbierta = !vistaAreaFacturacionAbierta
            } else if vistaMultilineaAbierta {
                esconderVistaMultilinea()
                vistaMultilineaAbierta = !vistaMultilineaAbierta
            }
            mostrarVistaPagarCuenta()
        }
        vistaPagarCuentaAbierta = !vistaPagarCuentaAbierta
        guardarEstadoVistas()
    }
    /// Despliega/pliega vista del area de facturación
    func presionarVistaAreaFacturacion() {
        if vistaAreaFacturacionAbierta {
            actualizarAlturaEncabezado(indicadorSeccion: seccionesEscondidas, animacionDoble: false)
            esconderVistaAreaFacturacion()
        } else {
            actualizarAlturaEncabezado(indicadorSeccion: seccionAreaFacturacionAbierta, animacionDoble: false)
            if vistaPagarCuentaAbierta {
                esconderVistaPagarCuenta()
                vistaPagarCuentaAbierta = !vistaPagarCuentaAbierta
            } else if vistaMultilineaAbierta {
                esconderVistaMultilinea()
                vistaMultilineaAbierta = !vistaMultilineaAbierta
            }
            mostrarVistaAreaFacturacion()
        }
        vistaAreaFacturacionAbierta = !vistaAreaFacturacionAbierta
        guardarEstadoVistas()
    }

    /// Abre vista para modificar imagen de perfil
    func abrirSeleccionImagen() {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        Globales.homeController.navigationItem.backBarButtonItem = backItem
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_MOD_INGRESO_FACIL", comment: "marca boton ingreso facil"))

        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controladorSeleccionImagen = storyboard.instantiateViewController(withIdentifier: "personalizaPerfilRRSS") as! PersonalizaPerfilHomeIphone

        Globales.homeController.navigationController!.pushViewController(controladorSeleccionImagen as UIViewController, animated: true)
    }
    /**
     Acción para redirigir a tab comprar bolsas
     - parameter sender: enlace a boton que abre tab comprar bolsas
     */
    @IBAction func comprarBolsas(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_COMPRA_BOLSA", comment: "marca home boton comprar bolsa"))

        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabComprarBolsas

    }
    /**
     Acción para abrir pagina de recargar saldo
     - parameter sender: enlace a boton que abre recarga saldo
     */
    @IBAction func abrirRecarga(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_RECARGA", comment: "marca home boton recarga"))

        var vista = UIView()
        vista = sender.view

        var lblnumeroTelefono = UILabel()
        var telefono = String()
        lblnumeroTelefono = vista.superview?.viewWithTag(4) as! UILabel

        telefono = lblnumeroTelefono.text!
        telefono = String(telefono.characters.dropFirst())

        UtilBolsas.abrirRecargaBrowser(saldoFaltante: "", numeroTelefono: telefono)
    }

    /// Acción para pagar cuentas
    ///
    /// - parameter sender: objeto de botón pagar cuentas
    @IBAction func pagarCuenta(_ sender: AnyObject) {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_PAGO", comment: "marca home boton pagar "))

        var urlPagarCuenta = URLServicios.pagarCuenta.replacingOccurrences(of: "{rut}", with: String(Globales.datosCliente.rut.characters.dropLast()))
        urlPagarCuenta = urlPagarCuenta.replacingOccurrences(of: "{dv}", with: String(Globales.datosCliente.rut.characters.last!))

        let url = URL(string: urlPagarCuenta)!
        UIApplication.shared.openURL(url)
    }

    /// Se abre en browser ultima boleta pagada
    ///
    /// - Parameter sender: objeto de envio accion
    @IBAction func verUltimaBoletaPagada(_ sender: Any) {
        let url = URL(string: Globales.datosCliente.urlFactura)!
        UIApplication.shared.openURL(url)
    }

    /// Abre seccion multilineas
    ///
    /// - Parameter sender: objeto que realiza accion
    @IBAction func abrirMultilinea(_ sender: Any) {
        if vistaMultilineaAbierta {
            actualizarAlturaEncabezado(indicadorSeccion: seccionesEscondidas, animacionDoble: false)
            esconderVistaMultilinea()
        } else {
            actualizarAlturaEncabezado(indicadorSeccion: seccionMultilineaAbierta, animacionDoble: false)
            if vistaPagarCuentaAbierta {
                esconderVistaPagarCuenta()
                vistaPagarCuentaAbierta = !vistaPagarCuentaAbierta
            } else if vistaAreaFacturacionAbierta {
                esconderVistaAreaFacturacion()
                vistaAreaFacturacionAbierta = !vistaAreaFacturacionAbierta
            }
            mostrarVistaMultilinea()
        }
        vistaMultilineaAbierta = !vistaMultilineaAbierta
        guardarEstadoVistas()
    }
}
