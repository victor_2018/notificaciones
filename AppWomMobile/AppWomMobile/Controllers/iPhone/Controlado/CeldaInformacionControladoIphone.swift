//
//  CeldaInformacionControladoIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda que entrega informacion al womer.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaInformacionControladoIphone: UITableViewCell {
    // Texto con información.
    @IBOutlet weak var labelInformacion: UILabel!
}
