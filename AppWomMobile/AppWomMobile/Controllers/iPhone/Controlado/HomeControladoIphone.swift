//
//  HomeControladoIphone.swift
//  AppWomMobile
//
//  Controlador de la vista principal de la aplicación.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import SwiftyJSON
import Foundation

protocol ProtocoloHomeControlado: class {
    func actualizarAlturaEncabezado(indicadorSeccion: Int, animacionDoble: Bool)
}

class HomeControladoIphone: UIViewController, UITableViewDataSource, UITableViewDelegate {
    // Enlace con tabla de bolsas en vista
    @IBOutlet weak var tablaBolsas: UITableView!
    // Enlace a activity indicator en la vista
    @IBOutlet weak var viewActivityIndicator: UIView!

    // arreglo de bolsas datos moviles
    var bolsasDatos = [BolsaTO]()
    // arreglo de bolsas datos moviles basal
    var bolsasDatosBasales = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales
    var bolsasDatosPromo = [BolsaTO]()
    // arreglo de bolsas datos moviles Agrupada
    var bolsasDatosAgrupada = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales Agrupada
    var bolsasDatosPromoAgrupada = [BolsaTO]()
    // arreglo de bolsas minutos
    var bolsasMinutos = [BolsaTO]()
    // arreglo de bolsas minutos basal
    var bolsasMinutosBasales = [BolsaTO]()
    // arreglo de bolsas minutos
    var bolsasMinutosAgrupada = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales
    var bolsasMinutosPromo = [BolsaTO]()
    // arreglo de bolsas datos moviles promocionales Agrupada
    var bolsasMinutosPromoAgrupada = [BolsaTO]()
    // arreglo de bolsas mensajes
    var bolsasMensajes = [BolsaTO]()
    // índice de fila seleccionada
    var indexSeleccionado = Int()
    // Secciones de la tabla
    var cantidadSecciones = Int()
    // indice de fila seleccionada
    var selectedIndexPath: IndexPath?
    // indicador de bolsas de datos basales vacia
    var bolsasDatosBasalesVacia = false
    // indicador de bolsas de datos vacia
    var bolsasDatosCompradasVacia = false
    // indicador de bolsas de datos vacia
    var bolsasDatosPromoVacia = false
    // indicador de bolsas de minutos vacia
    var bolsasMinutosCompradasVacia = false
    // indicador de bolsas de minutos vacia
    var bolsasMinutosPromoVacia = false
    // indicador de bolsas de minutos vacia
    var bolsasMinutosVacia = false
    // indicador de bolsas de sms vacia
    var bolsasMensajesVacia = false
    // sección del encabezado
    let seccionEncabezado = 0
    // sección datos móviles
    var seccionDatosMoviles = Int()
    // sección basales datos móviles
    var seccionDatosBasales = Int()
    // sección bolsas compradas datos móviles
    var seccionBolsasCompradas = Int()
    // sección bolsas promocionales datos móviles
    var seccionBolsasPromo = Int()
    // sección voz
    var seccionMinutos = Int()
    // sección basales minutos
    var seccionBolsasMinutosBasales = Int()
    // sección bolsas minutos compradas
    var seccionBolsasMinutosCompradas = Int()
    // sección bolsas minutos promocionales
    var seccionBolsasMinutosPromo = Int()
    // sección datos móviles
    var seccionSMS = Int()
    // sección SMS Basales
    var seccionSMSBasal = Int()
    // sección final de lista de bolsas
    var seccionPiePagina = Int()
    // indice con fila de informacion en seccion de datos moviles.
    var indiceInformacionDatos = Int()
    // numero de lineas para celda de informacion datos
    let numeroLineasDatos = 1
    // numero de lineas para celda de pie de pagina
    let numeroLineasPiePagina = 3
    // objeto con seccion de encabezado
    var viewSectionEncabezadoControlado = CeldaEncabezadoHomeControladoIphone()
    // objeto con seccion de encabezado
    var viewSectionEncabezadoPrepago = CeldaEncabezadoHomePrepagoIphone()
    /// Variable con que se maneja refresco de vista
    var refreshControl: UIRefreshControl!
    // Indicador de tamaño del encabezado
    var indicadorSeccionesEncabezado = 4
    // cuenta las animaciones para restringir la primera, primero es -1 para la primera vez, con 1 se muestra animación y con 0 no
    var contadorAnimacionVistaConDeuda = -1

    override func viewDidDisappear(_ animated: Bool) {
        contadorAnimacionVistaConDeuda = 1
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.viewActivityIndicator.isHidden = true
        self.tablaBolsas.dataSource = self
        self.tablaBolsas.delegate = self
        Globales.homeController = self
        tablaBolsas.setContentOffset(CGPoint.zero, animated: false)

        if !Globales.modoMultilinea {
            Util.agregarActivityIndicator(self.tabBarController!.view)
            UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
                Util.removerActivityIndicator(self.tabBarController!.view)
                if retorno {
                    self.llamarServicios()
                }
            }
        }
    }

    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refrescarHome), for: UIControlEvents.valueChanged)
        tablaBolsas.addSubview(refreshControl)
        self.tablaBolsas.separatorStyle = UITableViewCellSeparatorStyle.none
        UtilHome.determinarSecciones(cantidadSecciones: &self.cantidadSecciones, seccionDatosMoviles: &self.seccionDatosMoviles, seccionBasales: &self.seccionDatosBasales, seccionBolsasCompradas: &self.seccionBolsasCompradas, seccionBolsasPromo: &self.seccionBolsasPromo, seccionMinutos: &self.seccionMinutos, seccionMinutosBasales: &self.seccionBolsasMinutosBasales, seccionMinutosCompradas: &self.seccionBolsasMinutosCompradas, seccionMinutosPromo: &self.seccionBolsasMinutosPromo, seccionSMS: &self.seccionSMS, seccionPiePagina: &self.seccionPiePagina, seccionSMSBasal: &self.seccionSMSBasal)
    }

    /// Funcion que detecta scroll en tabla
    ///
    /// - Parameter scrollView: objeto de scroll en tabla
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        DesignHelper.bloquearBounceInferior(scrollView: scrollView)
    }

    /// Función que se llama despues de deslizar la pantalla en el home y que refresca vista
    ///
    /// - Parameter sender: objeto realiza acción
    func refrescarHome(sender: AnyObject) {
        viewSectionEncabezadoControlado.guardarEstadoVistas()
        contadorAnimacionVistaConDeuda = 1
        Util.agregarActivityIndicator(self.tabBarController!.view)
        UtilApp.validarEstadoServicios(controller: self, abrirVista: true) { retorno in
            Util.removerActivityIndicator(self.tabBarController!.view)
            if retorno {
                self.llamarServicios()
            }
        }
        refreshControl.endRefreshing()
    }

    /**
     Función que se activa al volver del home del dispositivo

     - parameter notification: notificacion de reproductor.
     */
    func willEnterForeground(_ notification: Notification) {
        if Globales.tabBarSeleccionado.selectedIndex == Constants.Indicadores.tabHome {
            llamarServicios()
        }
    }

    /**
     Hace llamada a servicios faltantes para cargar home
     */
    func llamarServicios() {
        Util.agregarActivityIndicator(self.tabBarController!.view)

        UtilServicios.obtenerValorSaldo { retorno in
            if retorno {
                UtilApp.generarMigaDePan(NSLocalizedString("HOME_ONLOAD", comment: " marca home onload "))

                // Se reinicializa tabla al volver desde otra vista
                self.bolsasDatos.removeAll()
                self.bolsasDatosBasales.removeAll()
                self.bolsasDatosPromo.removeAll()
                self.bolsasDatosAgrupada.removeAll()
                self.bolsasDatosPromoAgrupada.removeAll()
                self.bolsasMinutos.removeAll()
                self.bolsasMinutosBasales.removeAll()
                self.bolsasMinutosPromo.removeAll()
                self.bolsasMinutosAgrupada.removeAll()
                self.bolsasMinutosPromoAgrupada.removeAll()
                self.bolsasMensajes.removeAll()
                self.tablaBolsas.reloadData()

                self.generarArreglos()

                self.tablaBolsas.reloadData()
                self.tablaBolsas.separatorStyle = UITableViewCellSeparatorStyle.none

                if Globales.modoCampaignManager {
                    UtilServiciosPromos.generarBadgePromos(self.tabBarController!)
                    UtilServiciosPromos.desplegarHomeBanner(self, tabBarController: self.tabBarController!)
                }
            } else {
                if Globales.modoCampaignManager {
                    self.tabBarController?.tabBar.items?[4].badgeValue = "0"
                }
                UtilApp.generarMigaDePan(NSLocalizedString("ERROR_GENERICO", comment: "miga de pan error ayuda") + NSLocalizedString("HOME_ONLOAD", comment: " marca home onload "))
                Util.alertErrorGeneral(self)
                self.tablaBolsas.reloadData()
            }
            Util.removerActivityIndicator(self.tabBarController!.view)
        }
    }
    /// Se generan los arreglso de las bolsas de la tabla
    func generarArreglos() {
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasDatosBasales, listadoBolsas: &self.bolsasDatosBasales, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasDatosAgrupada, listadoBolsas: &self.bolsasDatos, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasDatosPromoAgrupada, listadoBolsas: &self.bolsasDatosPromo, tipoBolsa: Constants.Indicadores.tipoTraficoDatos)

        if [Constants.TipoHome.homeControladoSimple, Constants.TipoHome.homePostpagoVozSMS].contains(Globales.homeSeleccionado) {
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasMinutosBasales, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMinutosBasales, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMinutosBasales, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
        } else {
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasMinutosBasales, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMinutosAgrupada, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
            UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMinutosPromoAgrupada, listadoBolsas: &self.bolsasMinutos, tipoBolsa: Constants.Indicadores.tipoTraficoMinutos)
        }
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsas, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBolsasPromocionales, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)
        UtilBolsas.generarArreglosHome(Globales.listaGrupoBasales, listadoBolsasAgrupada: &self.bolsasMensajes, listadoBolsas: &self.bolsasMensajes, tipoBolsa: Constants.Indicadores.tipoTraficoSMS)

        indiceInformacionDatos = bolsasDatosPromoAgrupada.count
    }
    /**
     Función que determina el número de secciones
     - parameter tablaBolsas: tabla de bolsas en vista.
     - returns: número de celdas.
     */
    func numberOfSections(in tablaBolsas: UITableView) -> Int {
        let seccionEncabezado = 1
        let seccionInformacionPiePagina = 1

        return cantidadSecciones + seccionEncabezado + seccionInformacionPiePagina
    }
    /**
     Función donde se setea el número de filas en la tabla
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     sección en tabla
     - returns: valor de filas que se mostrarán
     */
    func tableView(_ tablaBolsas: UITableView, numberOfRowsInSection section: Int) -> Int {
        // fila de informacion en seccion de datos
        let pieDeSeccion = 1

        switch section {
        case seccionDatosBasales:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosBasales.count, indicadorBolsasVacias: &bolsasDatosBasalesVacia, esconderSinBolsas: true)
        case seccionBolsasCompradas:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosAgrupada.count, indicadorBolsasVacias: &bolsasDatosCompradasVacia, esconderSinBolsas: false) + pieDeSeccion
        case seccionBolsasPromo:
            return UtilBolsas.determinarNumeroFilas(self.bolsasDatosPromoAgrupada.count, indicadorBolsasVacias: &bolsasDatosPromoVacia, esconderSinBolsas: true)
        case seccionBolsasMinutosBasales:
            if [Constants.TipoHome.homePostpagoVozSMS, Constants.TipoHome.homeControladoSimple].contains(Globales.homeSeleccionado) {
                return UtilBolsas.determinarNumeroFilas(self.bolsasMinutosBasales.count, indicadorBolsasVacias: &bolsasDatosBasalesVacia, esconderSinBolsas: false) + pieDeSeccion
            } else {
                return UtilBolsas.determinarNumeroFilas(self.bolsasMinutosBasales.count, indicadorBolsasVacias: &bolsasDatosBasalesVacia, esconderSinBolsas: true)
            }
        case seccionBolsasMinutosCompradas:
            return UtilBolsas.determinarNumeroFilas(self.bolsasMinutosAgrupada.count, indicadorBolsasVacias: &bolsasMinutosCompradasVacia, esconderSinBolsas: false) + pieDeSeccion
        case seccionBolsasMinutosPromo:
            return UtilBolsas.determinarNumeroFilas(self.bolsasMinutosPromoAgrupada.count, indicadorBolsasVacias: &bolsasMinutosPromoVacia, esconderSinBolsas: true)
        case seccionSMSBasal:
            return UtilBolsas.determinarNumeroFilas(self.bolsasMensajes.count, indicadorBolsasVacias: &bolsasMensajesVacia, esconderSinBolsas: false)
        default:
            Logger.log("default switch número de filas seccion: \(section)")
        }
        return 0
    }

    /// Función que realiza las acciones al desplegar los header.
    ///
    /// - parameter tableView: tabla de bolsas.
    /// - parameter view:      vista de header.
    /// - parameter section:   número de sección.
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == 0 {
            if ![Constants.TipoHome.homeControladoSimple, Constants.TipoHome.homeControladoAntiguo].contains(Globales.homeSeleccionado) {
                if Globales.datosCliente.esBigBoss {
                    DesignHelper.realizarAnimacionNivelWomer(viewSectionEncabezadoControlado.viewNivelWomer)
                }
                DesignHelper.animacionPagarCuenta(contadorAnimacionVistaConDeuda: &contadorAnimacionVistaConDeuda, viewSectionEncabezado: viewSectionEncabezadoControlado)
            }
        }
    }
    /**
     Función donde se construye las secciones de la tabla

     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:     índice de sección en tabla

     - returns: objeto de la sección generada
     */
    func tableView(_ tablaBolsas: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case seccionEncabezado:
            if [Constants.TipoHome.homeControladoSimple, Constants.TipoHome.homeControladoAntiguo].contains(Globales.homeSeleccionado) {
                let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaEncabezadoHomePrepago")! as! CeldaEncabezadoHomePrepagoIphone
                viewSection.prepararVista()
                viewSectionEncabezadoPrepago = viewSection
                return viewSection
            } else {
                let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaEncabezadoHomeControlado")! as! CeldaEncabezadoHomeControladoIphone
                viewSection.prepararVista()
                viewSection.delegate = self
                viewSectionEncabezadoControlado = viewSection
                return viewSection
            }
        case seccionPiePagina:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
            return viewSection
        case seccionBolsasCompradas, seccionBolsasPromo, seccionDatosBasales:

            let viewSection = UtilBolsas.obtenerCeldaSeccionInterna(tablaBolsas, seccion: section, seccionBasales: seccionDatosBasales, seccionBolsasCompradas: seccionBolsasCompradas, seccionBolsasPromo: seccionBolsasPromo, tipoTrafico: Constants.Indicadores.tipoTraficoDatos)

            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        case seccionBolsasMinutosPromo, seccionBolsasMinutosCompradas, seccionBolsasMinutosBasales:
            let viewSection = UtilBolsas.obtenerCeldaSeccionInterna(tablaBolsas, seccion: section, seccionBasales: seccionBolsasMinutosBasales, seccionBolsasCompradas: seccionBolsasMinutosCompradas, seccionBolsasPromo: seccionBolsasMinutosPromo, tipoTrafico: Constants.Indicadores.tipoTraficoMinutos)

            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        case seccionSMSBasal:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaSeccionBolsasInternaIphone")! as! CeldaSeccionBolsasInternaIphone
            viewSection.asignarDatos(Constants.Indicadores.tipoBolsaSMS)
            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        default:
            let viewSection = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionTabla")! as! CeldaSeccionBolsasHomeIphone

            switch section {
            case seccionDatosMoviles:
                viewSection.asignarDatos(Constants.Indicadores.seccionDatosMoviles)
            case seccionMinutos:
                viewSection.asignarDatos(Constants.Indicadores.seccionMinutos)
            case seccionSMS:
                viewSection.asignarDatos(Constants.Indicadores.seccionSMS)
            default:
                viewSection.asignarDatos("")
            }

            let viewFinal = UIView(frame: viewSection.frame)
            viewFinal.addSubview(viewSection)
            return viewFinal
        }

    }
    /**
     Función que estructura las filas de historial de bolsas
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila
     - returns: celda seleccionada
     */
    func tableView(_ tablaBolsas: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case seccionBolsasPromo, seccionBolsasCompradas:
            return UtilHome.obtenerCeldaDatos(indexPath, tablaBolsas: tablaBolsas, bolsasDatosPromoVacia: bolsasDatosPromoVacia, bolsasDatosCompradasVacia: bolsasDatosCompradasVacia, bolsasDatosBasalesVacia: bolsasDatosBasalesVacia, bolsasDatosPromoAgrupada: bolsasDatosPromoAgrupada, bolsasDatosBasales: bolsasDatosBasales, bolsasDatosAgrupada: bolsasDatosAgrupada, seccionBolsasPromo: seccionBolsasPromo, seccionBasales: seccionDatosBasales, seccionBolsasCompradas: seccionBolsasCompradas)
        case seccionDatosBasales:
            return UtilHome.obtenerCeldaBolsaHome(indexPath, tablaBolsas: tablaBolsas, listadoBolsas: bolsasDatosBasales, seccion: Constants.Indicadores.seccionDatosMovilesBasales, labelSinBolsas: NSLocalizedString("label_sin_bolsas_datos", comment: "sin bolsas datos"))
        case seccionBolsasMinutosBasales:
            if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosBasales, indexPath: indexPath, bolsasAgrupada: bolsasMinutosBasales) && [Constants.TipoHome.homePostpagoVozSMS, Constants.TipoHome.homeControladoSimple].contains(Globales.homeSeleccionado) {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            } else {
                return UtilHome.obtenerCeldaBolsaHome(indexPath, tablaBolsas: tablaBolsas, listadoBolsas: bolsasMinutosBasales, seccion: Constants.Indicadores.seccionDatosMovilesBasales, labelSinBolsas: NSLocalizedString("label_sin_bolsas_voz", comment: "sin bolsas datos"))
            }
        case seccionBolsasMinutosCompradas:
            if UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosCompradas, indexPath: indexPath, bolsasAgrupada: bolsasMinutosAgrupada) {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "seccionInformacionDatos")! as! CeldaInformacionIphone
                return celda
            } else {
                return UtilHome.obtenerCeldaPromocional(tablaBolsas: tablaBolsas, seccion: Constants.Indicadores.seccionDatosMovilesBolsasCompradas, indexPath: indexPath, indicadorBolsasVacia: bolsasMinutosCompradasVacia, listadoBolsas: bolsasMinutosAgrupada, mensajeSinBolsas: NSLocalizedString("label_sin_bolsas_voz", comment: "mensaje sin bolsas de minutos"))
            }
        case seccionBolsasMinutosPromo:
            if !bolsasMinutosPromoVacia {
                let celda = tablaBolsas.dequeueReusableCell(withIdentifier: "celdaBolsasPromocionalesHome", for: indexPath) as! CeldaBolsaPromocionalesHomeIphone
                celda.asignarDatos(bolsasMinutosPromoAgrupada[indexPath.row], seccion: Constants.Indicadores.seccionDatosMovilesBolsasPromocionales, fila: indexPath.row)
                return celda
            }
        case seccionMinutos:
            return UtilHome.obtenerCeldaBolsaHome(indexPath, tablaBolsas: tablaBolsas, listadoBolsas: bolsasMinutos, seccion: Constants.Indicadores.seccionMinutos, labelSinBolsas: NSLocalizedString("label_sin_bolsas_voz", comment: "sin bolsas minutos"))
        case seccionSMSBasal:
            return UtilHome.obtenerCeldaSMS(indexPath, tablaBolsas: tablaBolsas, bolsasMensajes: bolsasMensajes, bolsasMensajesVacia: bolsasMensajesVacia)
        default:
            Logger.log("Default switch cellForRowAtIndexPath")
        }
        return UITableViewCell()
    }
    /**
     Función que determina alto de secciones.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter section:   índice de secciones
     - returns: tamaño secciones
     */
    func tableView(_ tablaBolsas: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var heightEncabezado = CGFloat(390)
        let heightSecciones = CGFloat(70)
        let heightSeccionPiePagina = CGFloat(20)
        let heightSeccionBasales = CGFloat(55)
        let heightSeccionInterna = CGFloat(80)
        var heightSeccionPagarCuentaAbierta = CGFloat(460)
        var heightSeccionAreaFacturacionAbierta = CGFloat(560)

        if !Globales.datosCliente.esBigBoss {
            heightEncabezado = heightEncabezado - CGFloat(50)
            heightSeccionPagarCuentaAbierta = heightSeccionPagarCuentaAbierta - CGFloat(50)
            heightSeccionAreaFacturacionAbierta = heightSeccionAreaFacturacionAbierta - CGFloat(50)
        }

        if section == seccionBolsasMinutosPromo && bolsasMinutosPromo.count == 0 {
            return 0
        } else if section == seccionBolsasPromo && bolsasDatosPromo.count == 0 {
            return 0
        }

        switch section {
        case seccionEncabezado:
            if indicadorSeccionesEncabezado == 1 {
                return heightSeccionPagarCuentaAbierta
            } else if indicadorSeccionesEncabezado == 2 {
                return heightSeccionAreaFacturacionAbierta
            } else if indicadorSeccionesEncabezado == 3 {
                return heightEncabezado + CGFloat(Globales.datosCliente.contratos.count * 165)
            } else {
                return heightEncabezado
            }
        case seccionDatosBasales, seccionBolsasMinutosBasales, seccionSMSBasal:
            return heightSeccionBasales
        case seccionPiePagina:
            return heightSeccionPiePagina
        case seccionBolsasMinutosPromo, seccionBolsasMinutosCompradas, seccionBolsasPromo, seccionBolsasCompradas:
            return heightSeccionInterna
        default:
            return heightSecciones
        }
    }
    /**
     Función que determina altura de celdas.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de celda
     - returns: tamaño celda
     */
    func tableView(_ tablaBolsas: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heightCompraBolsa = CGFloat(85)

        var validacionFilaInfoDatos = (UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasCompradas, indexPath: indexPath, bolsasAgrupada: bolsasDatosAgrupada) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosCompradas, indexPath: indexPath, bolsasAgrupada: bolsasMinutosAgrupada) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionSMSBasal, indexPath: indexPath, bolsasAgrupada: bolsasMensajes))
        if [Constants.TipoHome.homePostpagoVozSMS, Constants.TipoHome.homeControladoSimple].contains(Globales.homeSeleccionado) {
            validacionFilaInfoDatos = (UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasCompradas, indexPath: indexPath, bolsasAgrupada: bolsasDatosAgrupada) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosCompradas, indexPath: indexPath, bolsasAgrupada: bolsasMinutosAgrupada) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionSMSBasal, indexPath: indexPath, bolsasAgrupada: bolsasMensajes) || UtilHome.validadorFilaPieSeccion(seccionEnBusqueda: seccionBolsasMinutosBasales, indexPath: indexPath, bolsasAgrupada: bolsasMinutosBasales))
        }
        var listadoBolsasAgrupada = [BolsaTO]()

        listadoBolsasAgrupada = seleccionListadoBolsasAgrupadas(indexPath.section)

        if bolsasDatosCompradasVacia && indexPath.section == seccionBolsasCompradas && indexPath.row == 0 || bolsasMinutosCompradasVacia && indexPath.section == seccionBolsasMinutosCompradas && indexPath.row == 0 {
            return heightCompraBolsa
        } else {
            return UtilHome.determinarAlturaAnimacionCeldaBolsas(indexPath: indexPath, seccionEncabezado: seccionEncabezado, validacionFilaInfoDatos: validacionFilaInfoDatos, listadoBolsasAgrupada: listadoBolsasAgrupada)
        }
    }
    /// Busca el listado de bolsas agrupadas
    ///
    /// - Parameter seccion: seccion de la lista
    /// - Returns: listado de bolsa agrupada
    func seleccionListadoBolsasAgrupadas(_ seccion: Int) -> [BolsaTO] {
        var listadoBolsasAgrupada = [BolsaTO]()

        if seccion == seccionBolsasPromo {
            listadoBolsasAgrupada = bolsasDatosPromoAgrupada
        } else if seccion == seccionDatosBasales {
            listadoBolsasAgrupada = bolsasDatosBasales
        } else if seccion == seccionBolsasCompradas {
            listadoBolsasAgrupada = bolsasDatosAgrupada
        } else if seccion == seccionBolsasMinutosBasales {
            listadoBolsasAgrupada = bolsasMinutosBasales
        } else if seccion == seccionBolsasMinutosPromo {
            listadoBolsasAgrupada = bolsasMinutosPromoAgrupada
        } else if seccion == seccionBolsasMinutosCompradas {
            listadoBolsasAgrupada = bolsasMinutosAgrupada
        } else if seccion == seccionSMSBasal {
            listadoBolsasAgrupada = bolsasMensajes
        }

        return listadoBolsasAgrupada
    }
    /**
     Desplegar/plegar info bolsas.
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter indexPath:   índice de fila
     */
    func tableView(_ tablaBolsas: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case seccionBolsasPromo:
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosPromoAgrupada, listadoBolsas: &bolsasDatosPromo, indexPath: indexPath)
        case seccionDatosBasales:
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosBasales, listadoBolsas: &bolsasDatosBasales, indexPath: indexPath)
        case seccionBolsasCompradas:
            if !bolsasDatosCompradasVacia {
                UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasDatosAgrupada, listadoBolsas: &bolsasDatos, indexPath: indexPath)
            }
        case seccionBolsasMinutosBasales:
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMinutosBasales, listadoBolsas: &bolsasMinutosBasales, indexPath: indexPath)
        case seccionBolsasMinutosPromo:
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMinutosPromoAgrupada, listadoBolsas: &bolsasMinutosPromo, indexPath: indexPath)
        case seccionBolsasMinutosCompradas:
            if !bolsasMinutosCompradasVacia {
                UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMinutosAgrupada, listadoBolsas: &bolsasMinutos, indexPath: indexPath)
            }
        case seccionSMSBasal:
            UtilBolsas.aplicarAnimacionCelda(tablaBolsas, listadoBolsasAgrupada: &bolsasMensajes, listadoBolsas: &bolsasMensajes, indexPath: indexPath)
        default:
            Logger.log()
        }
    }
    /**
     Función que se ejecuta antes de mostrar celda
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter cell: celda de bolsas en vista
     - parameter indexPath:   índice de celda
     */
    func tableView(_ tablaBolsas: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let celdaBolsas = cell as? CeldaBolsasHomeIphone {
            celdaBolsas.watchFrameChanges()
        }
    }
    /**
     Función que se ejecuta despues de mostrar celda
     - parameter tablaBolsas: tabla de bolsas en vista
     - parameter cell: celda de bolsas en vista
     - parameter indexPath:   índice de celda
     */
    func tableView(_ tablaBolsas: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let celdaBolsas = cell as? CeldaBolsasHomeIphone {
            celdaBolsas.ignoreFrameChanges()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Botón de navegación para volver al Home
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
    }
}

extension HomeControladoIphone: ProtocoloHomeControlado {
    func actualizarAlturaEncabezado(indicadorSeccion: Int, animacionDoble: Bool) {
        indicadorSeccionesEncabezado = indicadorSeccion
        tablaBolsas.reloadSections([1], with: .none)
        if (indicadorSeccion == 1 && !animacionDoble) || indicadorSeccion == 2 || indicadorSeccion == 3 {
            tablaBolsas.moveSection(0, toSection: 0)
        }
    }
}
