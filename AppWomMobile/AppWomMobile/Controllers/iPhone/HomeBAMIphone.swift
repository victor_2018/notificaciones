//
//  HomeBAMIphone.swift
//  AppWomMobile
//
//  Controlador de home plan BAM.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class HomeBAMIphone: UIViewController {

    override func viewDidLoad() {
        UtilApp.generarMigaDePan(NSLocalizedString("HOME_ONLOAD", comment: "marca home onload "))
    }

    /// Acción para abrir sucursal online
    ///
    /// - parameter sender: objeto botón ir a sucursal online
    @IBAction func abrirSucursalOnline(_ sender: AnyObject) {
        let url = URL(string: URLServicios.sucursalOnline)!
        UIApplication.shared.openURL(url)
    }
}
