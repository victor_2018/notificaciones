//
//  ListadoPaisesRoaming.swift
//  AppWomMobile
//
//  Controlador para listado de paises de bolsas roaming habilitadas.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class ListadoPaisesRoaming: UIViewController {

    @IBOutlet weak var lblTipoRoaming: UILabel!
    @IBOutlet weak var imgCerrar: UIImageView!
    @IBOutlet weak var txtViewPaises: UITextView!
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var btnVolver: UIButton!
    @IBOutlet weak var viewContenido: UIView!
    @IBOutlet weak var viewCabecera: UIView!

    var modoListado = Bool()
    var tituloRoaming = String()
    var listadoPaises = String()

    weak var delegate: ProtocoloSeleccionBolsaIphone?

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        txtViewPaises.setContentOffset(CGPoint.zero, animated: false)
    }

    override func viewDidLoad() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista
        viewContenido.clipsToBounds = true

        DesignHelper.mostrarAnimacionPopUp(self.view)
        txtViewPaises.flashScrollIndicators()

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(cerrarVista))
        imgCerrar.isUserInteractionEnabled = true
        imgCerrar.addGestureRecognizer(tapViewBoton)
        lblTipoRoaming.text = tituloRoaming

        generarVista()
    }

    /// Genera la vista para el pop up de paises
    func generarVista() {
        viewCabecera.backgroundColor = DesignHelper.UIColorFromRGB(0x551261)
        switch tituloRoaming {
        case NSLocalizedString("titulo_seccion_roaming_preferente", comment: "titulo seccion preferente"):
            txtViewPaises.text = Globales.paisesRoaming.paisesPreferente
        case NSLocalizedString("titulo_seccion_roaming_europa", comment: "titulo seccion europa"):
            txtViewPaises.text = Globales.paisesRoaming.paisesEuropa
        case NSLocalizedString("titulo_seccion_roaming_resto_mundo", comment: "titulo seccion resto del mundo"):
            txtViewPaises.text = Globales.paisesRoaming.paisesRestoDelMundo
        default:
            Logger.log("Roaming no encontrado")
        }

        txtViewPaises.text = String(txtViewPaises.text.characters.dropFirst(2))

        if modoListado {
            btnVolver.isHidden = true
            btnAceptar.isHidden = false
            self.viewContenido.backgroundColor = UIColor.white
        } else {
            btnVolver.isHidden = false
            btnAceptar.isHidden = true
            self.viewContenido.backgroundColor = DesignHelper.UIColorFromRGB(0x551261)
            txtViewPaises.textColor = UIColor.white
        }
    }

    @IBAction func volverAPopUp(_ sender: Any) {
        delegate?.flipAListadoPaises(tipoBolsa: "")
    }

    @IBAction func cerrarVista(_ sender: Any) {
        // duracion de la animacion
        let duracionAnimacion = 0.25
        // escala inicial de la vista
        let escalaInicialVista = CGFloat(1.3)

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.view.transform = CGAffineTransform(scaleX: escalaInicialVista, y: escalaInicialVista)
            self.view.alpha = 0.0
        }, completion: {(finished: Bool)  in
            if finished {
                self.willMove(toParentViewController: nil)
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
                self.delegate?.recuperarBotonBack()
                if !self.modoListado {
                    self.delegate?.cerrarPopup()
                }
            }
        })
    }
}
