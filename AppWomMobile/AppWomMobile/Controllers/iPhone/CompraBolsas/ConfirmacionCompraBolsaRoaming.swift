//
//  ConfirmacionCompraBolsaRoaming.swift
//  AppWomMobile
//
//  Controlador para listado de paises de bolsas roaming habilitadas.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class ConfirmacionCompraBolsaRoaming: UIViewController {

    // delegate a vista padre
    weak var delegate: ProtocoloSeleccionBolsaIphone?
    // Enlace a imagen para cerrar el popup
    @IBOutlet weak var imgCerrarVista: UIImageView!
    // Enlace a vista con el contenido del popup
    @IBOutlet weak var viewContenido: UIView!

    /// Controlador del popup de multilinea
    var controllerPopupMultilinea = PopupCompraBolsasMultilinea()
    /// Indicador de si se encuentra en flujo multilinea
    var esMultilinea = false

    override func viewDidLoad() {
        // borde de esquinas que se le da a la vista
        let radioBordeVista = CGFloat(10.0)
        // transparencia de fondo
        let transparenciaVista = CGFloat(0.5)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista

        let tapViewBoton = UITapGestureRecognizer(target:self, action:#selector(cerrarVista(_:)))
        imgCerrarVista.isUserInteractionEnabled = true
        imgCerrarVista.addGestureRecognizer(tapViewBoton)

        DesignHelper.mostrarAnimacionPopUp(self.view)
    }

    /// Cierra el popup
    ///
    /// - Parameter sender: objeto que envia accion
    @IBAction func cerrarVista(_ sender: Any) {
        if esMultilinea {
            controllerPopupMultilinea.cerrarPopup()
        } else {
            removerAnimacion()
            delegate?.recuperarBotonBack()
        }
    }

    /**
     Remueve la vista con animación.
     */
    func removerAnimacion() {
        // escala con la animación inicial
        let escalaAnimacionInicial = CGFloat(1.3)
        // transparencia inicial de la vista
        let transparenciaInicial = CGFloat(0.0)
        // duracion de la animacion
        let duracionAnimacion = 0.25

        UIView.animate(withDuration: duracionAnimacion, animations: {
            self.view.transform = CGAffineTransform(scaleX: escalaAnimacionInicial, y: escalaAnimacionInicial)
            self.view.alpha = transparenciaInicial
        }, completion: { (finished: Bool)  in
            if finished {
                self.delegate?.cerrarSeleccionTipoBolsas()
            }
        })
    }
}
