//
//  PrototipoBotonBolsa.swift
//  AppWomMobile
//
//  Controlador prototipo para boton para ingresar a mostrar bolsas.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class PrototipoBotonBolsa: UIView {
    // titulo de la bolsa
    var tituloBolsa = UILabel()
    // boton para comprar la bolsa
    var botonBackground = UIButton()
    // icono de boton
    var iconoBoton = UIImageView()
    // delegate para acceder al view padre
    var arregloBolsas = [String]()
    // indice de selector de bolsas de datos
    let tituloBolsasDatos = NSLocalizedString("tipo_bolsa_datos", comment: "texto datos")
    // indice de selector de bolsas de voz
    let tituloBolsasVoz = NSLocalizedString("tipo_bolsa_voz", comment: "texto voz")
    // titulo de selector de bolsas SMS
    let tituloBolsasSMS = NSLocalizedString("tipo_bolsa_sms", comment: "texto sms")
    // titulo de selector de bolsas Mixtas
    let tituloBolsasMixtas = NSLocalizedString("tipo_bolsa_mixtas", comment: "texto mixtas")
    // titulo de selector de bolsas tematicas
    let tituloBolsasTematicas = NSLocalizedString("tipo_bolsa_tematicas", comment: "texto tematicas")
    // titulo de selector de bolsas roaming
    let tituloBolsasRoaming = NSLocalizedString("tipo_bolsa_roaming", comment: "texto roaming")
    // indice de bolsa dentro de arreglo
    var indiceBolsa = Int()
    // factor que se suma para adiminstrar en vista programatica
    var factorDimensional = CGFloat()
    // delegate para comunicarse con vista padre
    weak var delegate: ProtocoloSeleccionTipoBolsaIphone?

    override init (frame: CGRect) {
        super.init(frame: frame)

        if Globales.isIphonePlus {
            factorDimensional = 0.3
        } else if Globales.isIphone6 {
            factorDimensional = 0.2
        } else {
            factorDimensional = 0
        }
        generarView()
    }

    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError(NSLocalizedString("msj_no_soporta_nscoding", comment: "mensaje que indica que no soporta NSCoding"))
    }

    /**
     Genera vista de bolsa
     */
    func generarView() {
        // margen top de imagen de bolsa
        let margenImagenBolsa = CGFloat(18 + 18 * factorDimensional)
        // margen superior del titulo de la bolsa
        let margenTituloBolsa = CGFloat(16 + 16 * factorDimensional)
        // altura de la imagen
        let alturaImagen = CGFloat(28 + 28 * factorDimensional)
        // altura de titulo
        let alturatitulo = CGFloat(100 + 100 * factorDimensional)
        // tamaño de la fuente del titulo
        let tamanioTextoTitulo = 15 + Int(15 * factorDimensional)

        // Boton para comprar bolsa
        botonBackground.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        botonBackground.setBackgroundImage(UIImage(named: Constants.Imagenes.imgBotonBgBolsa), for: UIControlState())
        botonBackground.addTarget(self, action: #selector(PrototipoBotonBolsa.abrirBolsas), for: .touchUpInside)

        //Imagen bolsa
        iconoBoton.contentMode = .scaleAspectFit
        iconoBoton.frame = CGRect(x: 0, y: margenImagenBolsa, width: self.frame.size.width, height: alturaImagen)

        //Titulo bolsa
        tituloBolsa.frame = CGRect(x: 5, y: margenTituloBolsa, width: self.frame.size.width - 10, height: alturatitulo)
        tituloBolsa.textAlignment = NSTextAlignment.center
        tituloBolsa.font = UIFont (name: Constants.TipoFuente.ceraBold, size: CGFloat (tamanioTextoTitulo))
        tituloBolsa.textColor = DesignHelper.UIColorFromRGB(0x720184)
        tituloBolsa.adjustsFontSizeToFitWidth = true

        self.addSubview(botonBackground)
        self.addSubview(iconoBoton)
        self.addSubview(tituloBolsa)
    }

    /**
     Funcion que redirige a la función del padre para comprar bolsa.
     */
    func abrirBolsas () {
        delegate?.abrirSeleccionBolsas(indiceBolsa)
    }
    /**
     Funcion que arma el objeto de la bolsa.

     - parameter titulo: objeto de bolsa.
     - parameter nombreImagen: nombre de la imagen a asignar.
     - parameter indice: indice de la bolsa.
     */
    func asignarDatos(_ titulo: String, nombreImagen: String, indice: Int) {

        self.tituloBolsa.text = obtenerTituloBoton(titulo)
        self.iconoBoton.image = UIImage(named: nombreImagen)
        self.indiceBolsa = indice
    }

    /// Asigna el titulo al botón de compra de bolsa
    ///
    /// - Parameter tituloBolsa: Titulo enviado desde la vista principal
    /// - Returns: String con titulo
    func obtenerTituloBoton(_ tituloBolsa: String) -> String {
        switch tituloBolsa {
        case tituloBolsasDatos:
            return NSLocalizedString("titulo_btn_bolsa_datos", comment: "titulo boton datos")
        case tituloBolsasVoz:
            return NSLocalizedString("titulo_btn_bolsa_voz", comment: "titulo boton voz")
        case tituloBolsasSMS:
            return NSLocalizedString("titulo_btn_bolsa_sms", comment: "titulo boton sms")
        case tituloBolsasMixtas:
            return NSLocalizedString("titulo_btn_bolsa_mixtas", comment: "titulo boton mixtas")
        case tituloBolsasTematicas:
            return NSLocalizedString("titulo_btn_bolsa_tematicas", comment: "titulo boton tematicas")
        case tituloBolsasRoaming:
            return NSLocalizedString("titulo_btn_bolsa_roaming", comment: "titulo boton roaming")
        default:
            Logger.log("default switch obtener imagen boton")
        }
        return ""
    }
}
