//
//  SeccionBolsaRoaming.swift
//  AppWomMobile
//
//  Controlador prototipo para seccion de roaming.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class SeccionBolsaRoaming: UIViewController {

    @IBOutlet weak var lblTituloSeccion: UILabel!
    @IBOutlet weak var viewBtnVerPaises: UIView!

    override func viewDidLoad() {
        DesignHelper.generarVistaBoton(view: viewBtnVerPaises, textoBoton: NSLocalizedString("btn_ver_paises", comment: "texto de boton ver paises"), sizeFuente: 12)
    }
}
