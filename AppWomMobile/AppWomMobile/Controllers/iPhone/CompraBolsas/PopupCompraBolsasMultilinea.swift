//
//  PopupCompraBolsasMultilinea.swift
//  AppWomMobile
//
//  Controlador de popup para comprar bolsas en multilinea.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class PopupCompraBolsasMultilinea: UIViewController {

    /// Enlace a vista del contenido del popup
    @IBOutlet weak var viewContenido: UIView!

    // borde de esquinas que se le da a la vista
    let radioBordeVista = CGFloat(10.0)
    // transparencia de fondo
    let transparenciaVista = CGFloat(0.5)

    /// Datos de la linea ingresada en listado multilinea
    var datosCliente = EncabezadoHomeTO()
    /// Controlador con primer paso de compra bolsa
    var popupController: SeleccionTipoBolsaIphone!


    override func viewDidLoad() {
        Globales.modoMultilinea = true

        self.view.backgroundColor = UIColor.black.withAlphaComponent(transparenciaVista)
        viewContenido.layer.cornerRadius = radioBordeVista
        viewContenido.layer.borderColor = UIColor.white.cgColor
        viewContenido.layer.borderWidth = 2
        viewContenido.clipsToBounds = true

        let storyboard = UIStoryboard(name: "Tab2_iphone", bundle: nil)
        popupController = storyboard.instantiateViewController(withIdentifier: "seleccionTipoBolsa") as! SeleccionTipoBolsaIphone

        popupController.esMultilinea = true
        popupController.cliente = datosCliente
        popupController.controllerPopupMultilinea = self

        self.addChildViewController(popupController)

        popupController.view.frame.size.height = self.viewContenido.frame.size.height
        popupController.view.frame.size.width = self.viewContenido.frame.size.width
        popupController.view.viewWithTag(1)!.frame.size.height = self.viewContenido.frame.size.height
        popupController.view.viewWithTag(1)!.frame.size.width = self.viewContenido.frame.size.width
        popupController.view.viewWithTag(1)!.layer.cornerRadius = radioBordeVista
        popupController.viewSuperiorMultilinea.isHidden = false

        let tapCerrarVista = UITapGestureRecognizer(target:self, action:#selector(self.cerrarPopup))
        popupController.btnCerrar.isUserInteractionEnabled = true
        popupController.btnCerrar.addGestureRecognizer(tapCerrarVista)

        self.viewContenido.addSubview(popupController.view.viewWithTag(1)!)
        popupController.didMove(toParentViewController: self)

        UtilMultilinea.generarLabelTelefono(labelNumeroTelefono: popupController.lblNumeroMultilinea, numeroTelefono: datosCliente.numeroTelefono)

        DesignHelper.mostrarAnimacionPopUp(self.view)
    }

    /// Cierra popup inicial de multilinea
    func cerrarPopup() {
        Globales.modoMultilinea = false
        self.view.removeFromSuperview()
    }

    /// Recarga servicios de primer paso compra bolsa
    func recargarServicios() {
        popupController.validarMantencion()
    }
}
