//
//  CeldaSeccionBolsasHomeIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de las secciones de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import Firebase

class CeldaSeccionBolsasHomeIphone: UITableViewCell {

    // titulo de la seccion
    @IBOutlet weak var titulo: UILabel!
    // imagen de la seccion
    @IBOutlet weak var imagen: UIImageView!
    // boton para ver historial
    @IBOutlet weak var btnVerHistorial: UIButton!

    var controller = Globales.homeController
    var tipoSeccion = String()

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //
    }
    /**
     Función donde se asignan los datos a la sección de la tabla.
     - parameter seccion: índice de sección en tabla
     */
    func asignarDatos(_ seccion: String) {
        self.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.layoutMargins = UIEdgeInsets.zero

        switch seccion {
        case Constants.Indicadores.seccionDatosMoviles:
            tipoSeccion = Constants.Indicadores.seccionDatosMoviles
            imagen.image = UIImage(named: Constants.Imagenes.imgSeccionDatosMoviles)
            titulo.text = NSLocalizedString("titulo_seccion_datos_moviles", comment: "titulo de sección datos móviles")
            btnVerHistorial.accessibilityIdentifier = "seccion_datos_moviles"
            
        case Constants.Indicadores.seccionMinutos:
            tipoSeccion = Constants.Indicadores.seccionMinutos
            imagen.image = UIImage(named: Constants.Imagenes.imgSeccionMinutos)
            titulo.text = NSLocalizedString("titulo_seccion_minutos", comment: "titulo de sección minutos")
            btnVerHistorial.accessibilityIdentifier = "seccion_minutos"

        case Constants.Indicadores.seccionSMS:
            tipoSeccion = Constants.Indicadores.seccionSMS
            imagen.image = UIImage(named: Constants.Imagenes.imgSeccionMensajes)
            titulo.text = NSLocalizedString("titulo_seccion_mensajes", comment: "titulo de mensajes")
            btnVerHistorial.isHidden = true
            btnVerHistorial.accessibilityIdentifier = "historial_bolsas_sms"
        default:
            imagen.image = UIImage()
            titulo.text = ""
            btnVerHistorial.isHidden = true
        }
    }

    /// Abre el historial de bolsas
    ///
    /// - parameter sender: objeto de boton
    @IBAction func verHistorial(_ sender: AnyObject) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        controller.navigationItem.backBarButtonItem = backItem

        generarMigaPan()

        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let controladorHistorial = storyboard.instantiateViewController(withIdentifier: "historialBolsas") as! HistorialBolsasIphone
        controladorHistorial.seccionSeleccionada = tipoSeccion
        controller.navigationController!.pushViewController(controladorHistorial as UIViewController, animated: true)
    }

    /// Genera la miga de pan en dashboard appcelerator segun seccion presionada
    func generarMigaPan() {
        switch tipoSeccion {
        case Constants.Indicadores.seccionDatosMoviles:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_HISTORIAL_BOLSA_DATO", comment: "marca home boton historial bolsa dato"))
        case Constants.Indicadores.seccionMinutos:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_HISTORIAL_BOLSA_VOZ", comment: "marca home boton historial bolsa VOZ"))
        case Constants.Indicadores.seccionSMS:
            UtilApp.generarMigaDePan(NSLocalizedString("HOME_BTN_HISTORIAL_BOLSA_SMS", comment: "marca home boton historial bolsa SMS"))
        default:
            Logger.log("default switch generarMigaPan - Historial")
        }
    }
}
