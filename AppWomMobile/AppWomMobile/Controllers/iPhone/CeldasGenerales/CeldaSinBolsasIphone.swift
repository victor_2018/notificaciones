//
//  CeldaSinBolsasIphone.swift
//  AppWomMobile
//
//  Controlador con celda que indica que no existen bolsas en la celda indicada.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaSinBolsas: UITableViewCell {
    // mensaje que indica que no hay bolsas
    @IBOutlet weak var mensajeSinBolsas: UILabel!

    func asignarDatos(_ mensaje: String) {
        mensajeSinBolsas.text = mensaje
        self.isUserInteractionEnabled = false
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
}
