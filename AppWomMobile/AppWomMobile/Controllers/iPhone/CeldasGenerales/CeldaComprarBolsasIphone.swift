//
//  CeldaComprarBolsas.swift
//  AppWomMobile
//
//  Celda que aparece cuando no existen bolsas compradas en el home.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaComprarBolsasIphone: UITableViewCell {
    /// Acción para redirigir a tab comprar bolsas
    ///
    /// - Parameter sender: enlace a boton que abre tab comprar bolsas
    @IBAction func comprarBolsas(_ sender: AnyObject) {
        Globales.tabBarSeleccionado.selectedIndex = Constants.Indicadores.tabComprarBolsas
    }
}
