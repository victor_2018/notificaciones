//
//  CeldaBolsasPromocionalesHomeIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la celda de bolsas.
//  @author Esteban Pavez A. (TINet).
//

import UIKit
import MarqueeLabel

// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class CeldaBolsaPromocionalesHomeIphone: UITableViewCell {
    // imagen indicador promocional
    @IBOutlet weak var imagenPromocion: UIImageView!
    // datos de bolsa restantes
    @IBOutlet weak var restanteBolsa: MarqueeLabel!
    // nombre de la bolsa
    @IBOutlet weak var nombreBolsa: MarqueeLabel!
    // boton desplegable
    @IBOutlet weak var btnDesplegable: UIImageView!
    // label "vence" de bolsa
    @IBOutlet weak var labelVence: UILabel!
    // view contenedor de view
    @IBOutlet weak var viewDesplegable: UIView!
    // fecha activacion de bolsa
    @IBOutlet weak var fechaActivada: UILabel!
    // fecha vencimiento de bolsa
    @IBOutlet weak var fechaVencimientoDesplegable: UILabel!
    // valor total bolsa
    @IBOutlet weak var valorTotal: UILabel!
    // View que contiene los datos de la celda
    @IBOutlet weak var viewContenedora: UIView!
    // cantidad de bolsas agrupadas
    @IBOutlet weak var cantidadBolsas: UILabel!
    // Hora de activación de la bolsa
    @IBOutlet weak var horaActivacion: UILabel!
    // Hora de finalización de la bolsa
    @IBOutlet weak var horaFinalizacion: UILabel!
    // imagen representativa de bolsa
    @IBOutlet weak var imagenBolsa: UIImageView!
    // margen que separa costado izquierdo morado de los datos de la bolsa al lado derecho
    @IBOutlet weak var margenVistaContenedora: NSLayoutConstraint!
    // Imagen de sombreado superior en celdas desplegadas
    @IBOutlet weak var sombraSuperior: UIImageView!
    // Icono de la bolsa izquierdo
    @IBOutlet weak var iconoBolsa: UIImageView!
    @IBOutlet weak var separadorSuperior: UIView!

    // imagen que se anima por el background
    var bgImage: UIImageView!
    // porcentaje consumido de bolsa
    var avance: CGFloat!
    // observador de frames
    var isObserving = false
    // indicador de si esta desplegado
    var estaDesplegado = false
    // valor de celda expandida
    var anchoMaxNombreBolsa = CGFloat()
    var anchoMaxRestanteBolsa = CGFloat()
    var primeraVez = 0

    class var heightExpandida: CGFloat {
        get {
            return 149
        }
    }
    // valor de celda plegada
    class var heightDefecto: CGFloat {
        get {
            return 85
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        var cellFrame = self.contentView.frame
        cellFrame.size.height = CeldaBolsasHomeIphone.heightDefecto
        cellFrame.size.width = Globales.anchuraDispositivo

        self.bgImage = UIImageView(frame: cellFrame)
        self.bgImage.image = UIImage(named: Constants.Imagenes.imgBarraEstado)
        self.bgImage.frame.origin.x = 0

        determinarAnchuraLabel()
    }

    /// determina anchura maxima de labels del nombre de la bolsa y el restante
    func determinarAnchuraLabel() {
        if Globales.isIphone5 {
            anchoMaxNombreBolsa = CGFloat(176)
            anchoMaxRestanteBolsa = CGFloat(185)
        } else if Globales.isIphone6 {
            anchoMaxNombreBolsa = CGFloat(206)
            anchoMaxRestanteBolsa = CGFloat(217)
        } else {
            anchoMaxNombreBolsa = CGFloat(228)
            anchoMaxRestanteBolsa = CGFloat(240)
        }
    }
    /**
     Oculta vista desplegable si el tamaño del frame es menor a lo determinado en expandida.
     */
    func checkHeight() {
        viewDesplegable.isHidden = (frame.size.height < CeldaBolsasHomeIphone.heightExpandida)
    }
    /**
     Agrega observador.
     */
    func watchFrameChanges() {
        if !isObserving {
            addObserver(self, forKeyPath: "frame", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.initial], context: nil)
            isObserving = true
        }
    }
    /**
     Remueve observador.
     */
    func ignoreFrameChanges() {
        if isObserving {
            removeObserver(self, forKeyPath: "frame")
            isObserving = false
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "frame" {
            checkHeight()
        }
    }
    /**
     Asigna los datos a la celda.
     - parameter bolsa: porcentaje de avance de carga en background
     - parameter seccion: seccion que se encuentra la bolsa
     - parameter fila: indice de fila en sección
     */
    func asignarDatos(_ bolsa: BolsaTO, seccion: String, fila: Int) {
        determinarDiseñoAgrupaciones(bolsa)

        if !bolsa.esBasal {
            iconoBolsa.isHidden = true
            imagenBolsa.isHidden = false
            self.margenVistaContenedora.constant = CGFloat(85)
        } else {
            iconoBolsa.isHidden = false
            imagenBolsa.isHidden = true
            self.margenVistaContenedora.constant = 0
        }

        self.layoutMargins = UIEdgeInsets.zero
        self.nombreBolsa.text = bolsa.nombreBolsa
        self.valorTotal.text = bolsa.precio
        self.nombreBolsa.type = .continuous
        self.nombreBolsa.animationCurve = .linear
        self.restanteBolsa.type = .continuous
        self.restanteBolsa.animationCurve = .linear

        UtilBolsas.correccionProblemaFechas(bolsa: bolsa, fechaActivada: fechaActivada, fechaVencimientoDesplegable: fechaVencimientoDesplegable, horaActivacion: horaActivacion, horaFinalizacion: horaFinalizacion)

        if bolsa.imagen != nil {
            self.imagenBolsa.image = UIImage(named: bolsa.imagen)
        }

        if nombreBolsa.intrinsicContentSize.width >= anchoMaxNombreBolsa {
            nombreBolsa.restartLabel()
            nombreBolsa.text?.append("      ")
        }

        if restanteBolsa.intrinsicContentSize.width >= anchoMaxRestanteBolsa {
            restanteBolsa.restartLabel()
            restanteBolsa.text?.append("     ")
        }

        labelVence.text = UtilBolsas.asignarDiaVencimientoBolsa(bolsa, vigenciaMultiple: bolsa.cantidadAgrupacion)

        self.btnDesplegable.isHidden = !bolsa.esColapsable
        self.isUserInteractionEnabled = bolsa.esColapsable

        determinarImagenDesplegable(bolsa)
        validarImagenPromocional(bolsa: bolsa)
        determinarDatosRestantes(seccion, bolsa: bolsa)
        aplicarAnimacion(bolsa, bolsaKey: seccion + String(fila) + String(bolsa.esHijo))
        UtilHome.verificarIndicadorPlataforma(bolsa: bolsa, restanteBolsa: restanteBolsa, labelVence: labelVence)

        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    /// Determina la imagen desplegable que debe tener la fila
    ///
    /// - Parameter bolsa: objeto de bolsa
    func determinarImagenDesplegable(_ bolsa: BolsaTO) {
        if !bolsa.esCabecera {
            if bolsa.colapsado {
                self.btnDesplegable.image = UIImage(named: Constants.Imagenes.imgPlegarBolsas)
            } else {
                self.btnDesplegable.image = UIImage(named: Constants.Imagenes.imgDesplegarBolsas)
            }
        } else {
            btnDesplegable.isHidden = true
        }
    }

    /// Valida si se debe mostrar la imagen promocional
    ///
    /// - Parameter bolsa: objeto con datos de bolsa
    func validarImagenPromocional(bolsa: BolsaTO) {
        let categoriaPromocional = 3
        if bolsa.categoria == categoriaPromocional && !bolsa.esHijo {
            self.imagenPromocion.isHidden = false
        } else {
            self.imagenPromocion.isHidden = true
        }
    }

    /// Determina la forma de mostrar celdas a las agrupaciones de bolsas
    ///
    /// - Parameter bolsa: bolsa con datos
    func determinarDiseñoAgrupaciones(_ bolsa: BolsaTO) {
        if  bolsa.esCabecera == true {
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: "\(bolsa.cantidadAgrupacion) \(NSLocalizedString("label_agrupacion_bolsas", comment: "label que muestra el texto despues del numero de bolsas"))", attributes: underlineAttribute)
            self.cantidadBolsas.attributedText = underlineAttributedString
            self.cantidadBolsas.isHidden = false
            self.backgroundColor = DesignHelper.UIColorFromRGB(0x3A1F4F)
            self.sombraSuperior.isHidden = true
            self.imagenBolsa.alpha = 1
            self.separadorSuperior.isHidden = false
        } else if bolsa.esHijo == true {
            self.cantidadBolsas.isHidden = true
            self.backgroundColor = DesignHelper.UIColorFromRGB(0x635271)
            self.imagenBolsa.alpha = 0.5
            viewDesplegable.frame.origin.x = 0
            viewDesplegable.frame.size.width = self.viewContenedora.frame.size.width
            if bolsa.indiceEnListado > 0 {
                self.sombraSuperior.isHidden = true
            } else {
                self.sombraSuperior.isHidden = false
            }
            self.separadorSuperior.frame.origin.x = CGFloat(85)
        } else if !bolsa.esCabecera && !bolsa.esHijo {
            self.separadorSuperior.isHidden = false
            self.imagenBolsa.alpha = 1
            self.sombraSuperior.isHidden = true
            viewDesplegable.frame.size.width = Globales.anchuraDispositivo

            if Globales.isIphone6 {
                viewDesplegable.frame.origin.x = CGFloat(self.viewContenedora.frame.size.width - Globales.anchuraDispositivo)
            } else if Globales.isIphone5 {
                if primeraVez == 0 {
                    viewDesplegable.frame.size.width = Globales.anchuraDispositivo * 1.2
                } else {
                    viewDesplegable.frame.size.width = Globales.anchuraDispositivo
                }
                viewDesplegable.frame.origin.x = CGFloat(-85)
            } else if Globales.isIphonePlus {
                if primeraVez == 0 {
                    viewDesplegable.frame.size.width = Globales.anchuraDispositivo * 0.92
                } else {
                    viewDesplegable.frame.size.width = Globales.anchuraDispositivo
                }
                viewDesplegable.frame.origin.x = CGFloat(-85)
            }
            primeraVez += 1
            self.cantidadBolsas.isHidden = true
            self.backgroundColor = DesignHelper.UIColorFromRGB(0x3A1F4F)
        }
    }
    /// Le aplica la animación al fondo de la fila
    ///
    /// - parameter bolsa: objeto con datos de la bolsa
    /// - parameter bolsaKey: llave que identifica a la bolsa en la lista.
    func aplicarAnimacion(_ bolsa: BolsaTO, bolsaKey: String) {
        let total = bolsa.totalBolsa.datoEnKilos
        let restante = bolsa.restanteBolsa.datoEnKilos
        var porcentajeRestante = CGFloat()
        // duracion de la animacion
        let duracionAnimacion = Double(1)

        if total == 0 {
            porcentajeRestante = 0
        } else {
            porcentajeRestante = restante! / total!
        }

        if !bolsa.limite {
            porcentajeRestante = 1
        }

        if Globales.arregloAnimacionBolsas[bolsaKey] == nil {
            Globales.arregloAnimacionBolsas[bolsaKey] = true
            self.viewContenedora.insertSubview(self.bgImage, at: 0)
            self.viewContenedora.sendSubview(toBack: self.bgImage)

            UIView.animate(withDuration: duracionAnimacion, delay: 0, options: .curveLinear, animations: {
                self.bgImage.frame.origin.x = self.viewContenedora.frame.size.width
                }, completion: { _ in
                    UIView.animate(withDuration: duracionAnimacion, delay: 0, options: .curveLinear, animations: {
                        self.bgImage.frame.origin.x = self.viewContenedora.frame.size.width * porcentajeRestante
                        }, completion: {  _ in
                            if porcentajeRestante == 0 {
                                self.viewContenedora.backgroundColor = DesignHelper.UIColorFromRGB(0xF0F0F0)
                                self.bgImage.removeFromSuperview()
                            }
                    })
            })
        } else {
            if porcentajeRestante == 0 {
                self.viewContenedora.backgroundColor = DesignHelper.UIColorFromRGB(0xF0F0F0)
                self.bgImage.removeFromSuperview()
            } else {
                self.viewContenedora.backgroundColor = UIColor.white
                self.viewContenedora.insertSubview(self.bgImage, at: 0)
                self.viewContenedora.sendSubview(toBack: self.bgImage)
                self.bgImage.frame.origin.x = self.viewContenedora.frame.size.width * porcentajeRestante
            }
        }
    }
    /**
     Función que asigna a la celda el valor de los datos (MB/min/sms) restantes.
     - parameter seccion:   sección en que se encuentra celda.
     - parameter bolsa:   bolsa con los datos de ella.
     */
    func determinarDatosRestantes(_ seccion: String, bolsa: BolsaTO) {
        switch seccion {
        case Constants.Indicadores.seccionDatosMovilesBasales, Constants.Indicadores.seccionDatosMovilesBolsasCompradas, Constants.Indicadores.seccionDatosMovilesBolsasPromocionales, Constants.Indicadores.seccionMinutosBasales, Constants.Indicadores.seccionMinutosBolsasCompradas, Constants.Indicadores.seccionMinutosPromocionales:
            if !bolsa.esCabecera {
                if !bolsa.limite {
                    if Int(bolsa.horario) > 0 || bolsa.muestraConsumo == 0 {
                        self.restanteBolsa.text = NSLocalizedString("label_consumo_ilimitado", comment: "texto consumo ilimitado")
                        self.restanteBolsa.textColor = DesignHelper.UIColorFromRGB(0xBA007C)
                    } else {
                        self.restanteBolsa.text = "Has consumido \(Util.verificarCerosEnDecimales(String(format: "%.2f", bolsa.consumido.dato))) \(bolsa.consumido.metrica!) "
                    }
                } else {
                    self.restanteBolsa.text = "Te quedan \(Util.verificarCerosEnDecimales(String(format: "%.2f", bolsa.restanteBolsa.dato))) \(bolsa.restanteBolsa.metrica!)  "
                }
            } else {
                self.restanteBolsa.text = "Te quedan \(Util.verificarCerosEnDecimales(String(format: "%.2f", bolsa.restanteBolsa.dato))) \(bolsa.restanteBolsa.metrica!)  "
            }
            if [Constants.Indicadores.seccionMinutosBasales, Constants.Indicadores.seccionMinutosBolsasCompradas, Constants.Indicadores.seccionMinutosPromocionales].contains(seccion) && bolsa.restanteBolsa.datoEnKilos == Constants.Indicadores.consumoIlimitado {
                self.restanteBolsa.text = NSLocalizedString("label_consumo_ilimitado", comment: "texto consumo ilimitado")
                self.restanteBolsa.textColor = DesignHelper.UIColorFromRGB(0xBA007C)
            }
        case Constants.Indicadores.seccionMinutos:
            if bolsa.restanteBolsa.dato >= 1 {
                self.restanteBolsa.text = "Te quedan \(Int(bolsa.restanteBolsa.dato)) \(bolsa.restanteBolsa.metrica!)  "
            } else {
                self.restanteBolsa.text = "Te quedan \(Util.verificarCerosEnDecimales(String(format: "%.2f", bolsa.restanteBolsa.dato))) \(bolsa.restanteBolsa.metrica!)  "
            }
            if bolsa.restanteBolsa.datoEnKilos == Constants.Indicadores.consumoIlimitado {
                self.restanteBolsa.text = NSLocalizedString("label_consumo_ilimitado", comment: "texto consumo ilimitado")
                self.restanteBolsa.textColor = DesignHelper.UIColorFromRGB(0xBA007C)
            }
        case Constants.Indicadores.seccionSMS:
            self.restanteBolsa.text = "Te quedan \(Int(bolsa.restanteBolsa.dato)) \(bolsa.restanteBolsa.metrica!)  "
        default:
            Logger.log("default switch tabla datos")
        }
    }
}
