//
//  VistaLineaHija.swift
//  AppWomMobile
//
//  Controlador prototipo para celdas de lineas hijas en multilinea.
//
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class VistaLineaHija: UIViewController {

    /// Enlace a numero de telefono
    @IBOutlet weak var numeroTelefono: UILabel!
    /// Enlace a boton para comprar bolsas
    @IBOutlet weak var btnComprarBolsas: UIView!
    /// Enlace a boton para recargar saldo
    @IBOutlet weak var btnRecargarSaldo: UIView!

    /// Numero de telefono enviado desde vista padre
    var numero = String()
    /// Indicador de linea postpago
    var esPostpago = false

    /// Genera vista inicial de linea hija
    func prepararVista() {
        var stringCompraBolsa = NSLocalizedString("boton_comprarle_bolsa", comment: "label boton comprarle bolsa")
        numeroTelefono.text = "+" + numero

        if esPostpago {
            btnComprarBolsas.frame.size.width = btnComprarBolsas.frame.size.width * 2
            btnRecargarSaldo.isHidden = true
            stringCompraBolsa = NSLocalizedString("boton_comprarle_bolsa_postpago", comment: "label boton comprarle bolsa postpago")
        }

        DesignHelper.generarVistaBoton(view: btnComprarBolsas, textoBoton: stringCompraBolsa, sizeFuente: 16)
        DesignHelper.generarVistaBoton(view: btnRecargarSaldo, textoBoton: NSLocalizedString("boton_recargarle_saldo", comment: "label boton recargarle saldo"), sizeFuente: 16)
    }

}
