//
//  CeldaSeccionBolsasInternaIphone.swift
//  AppWomMobile
//
//  Controlador con los datos de la seccion interna en cada tabla.
//  @author Esteban Pavez A. (TINet).
//

import UIKit

class CeldaSeccionBolsasInternaIphone: UITableViewCell {

    // titulo de la seccion
    @IBOutlet weak var tituloBolsas: UILabel!
    // texto de la descripcion de la seccion
    @IBOutlet weak var descripcionSeccion: UILabel!
    // fondo de la seccion
    @IBOutlet weak var fondoSeccion: UIImageView!
    // vista que contiene a la vista
    @IBOutlet weak var viewContenedora: UIView!
    // Indice de seccion de bolsas basales para postpago/controlado
    let seccionBolsasBasalesPostpago = "BASAL"
    // Indice de seccion de bolsas compradas para postpago/controlado
    let seccionBolsasCompradasPostpago = "COMPRADA"
    // Indice de seccion de bolsas promocionales para postpago/controlado
    let seccionBolsasPromoPostpago = "PROMOCIONAL"


    /// Asigna textos a seccion
    ///
    /// - Parameter seccion: Indice de seccion
    func asignarDatos(_ seccion: String) {
        let movimientoAdicionalTitulo = CGFloat(0.5)
        self.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.layoutMargins = UIEdgeInsets.zero

        switch seccion {
        case Constants.Indicadores.tipoBasal, Constants.Indicadores.tipoBolsaMinutosBasal:
            if seccion == Constants.Indicadores.tipoBolsaMinutosBasal {
                tituloBolsas.text = NSLocalizedString("titulo_seccion_bolsas_minutos_basales", comment: "titulo seccion bolsas de minutos basales")
            } else {
                tituloBolsas.text = NSLocalizedString("titulo_seccion_bolsas_basales", comment: "titulo seccion bolsas basales")
            }
            descripcionSeccion.text = ""
            tituloBolsas.frame.size.height = tituloBolsas.frame.size.height + (tituloBolsas.frame.size.height * movimientoAdicionalTitulo)
        case Constants.Indicadores.tipoBolsaComprada:
            tituloBolsas.text = NSLocalizedString("titulo_seccion_bolsas_compradas", comment: "titulo seccion bolsas compradas")
            descripcionSeccion.text = NSLocalizedString("descripcion_seccion_bolsas_compradas", comment: "descripcion seccion bolsas compradas")
        case Constants.Indicadores.tipoBolsaPromocional, Constants.Indicadores.tipoBolsaMinutosPromocional:
            tituloBolsas.text = NSLocalizedString("titulo_seccion_bolsas_promo", comment: "titulo seccion bolsas promocionales")
            descripcionSeccion.text = NSLocalizedString("descripcion_seccion_bolsas_promocionales", comment: "descripcion seccion bolsas promocionales")
        case Constants.Indicadores.tipoBolsaSMS:
            tituloBolsas.text = NSLocalizedString("titulo_seccion_bolsas_sms_basales", comment: "titulo seccion bolsas sms")
            tituloBolsas.frame.size.height = tituloBolsas.frame.size.height + (tituloBolsas.frame.size.height * movimientoAdicionalTitulo)
            descripcionSeccion.text = ""
        default:
            Logger.log("Seccion no existente")
        }
    }
}
