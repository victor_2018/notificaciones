Feature: Home APP

Scenario Outline: Visualizar HOME
  	Given the app has launched
  	And Presionar boton texto "EMPEZAR"
	And Ingresar número móvil "<linea>"
	And Presionar boton id "btn_Solicitar_Codigo"
	And Ingresar código "<codigo>"
	And Presionar boton id "btnVerificar"
	And Presionar "OMITIR" rs
	And Presionar boton texto "OMITIR"
	When Se ingresa nombre "TestName"
	And Se ingresa apellido "TestApellido"
	And Presionar boton id "btnComenzar"
	Then Existe texto "TestName"
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
    And  I swipe up 
    And Capturar evidencia
    And  I swipe up 
    And Capturar evidencia
    And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	And  I swipe up 
    And Capturar evidencia
	
    
	Examples: Lineas
    | linea     | codigo | 
    | 930719645 |  9986  |    
    | 935114741 |  7366  |     
    | 935131807 |  3017  |     
    | 935136328 |  5910  |     
    | 935140230 |  3928  |     
    | 935227013 |  9813  |     
    | 935255131 |  3037  |     
    | 949407211 |  9447  |     
    | 949465173 |  2451  |     
    | 964583333 |  5838  |     
    | 995484836 |  2038  |	