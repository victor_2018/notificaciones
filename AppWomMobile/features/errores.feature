# encoding: utf-8


Feature: mensajes de errores darksite y limbo 
  Scenario Outline: Validar mensaje de error DARKSITE para numero sin whatsaap habilitado
    Given Presionar boton texto "EMPEZAR"
    And Ingresar número móvil "<linea>"
    And Presionar boton texto "SOLICITAR CÓDIGO"
    Then Existe texto "Hemos detectado que tu numero pertenece al Darkside Únete al lado de la conveniencia y pórtate a WOM"
    And Capturar evidencia
    And Presionar boton texto "CONTÁCTATE CON UN EJECUTIVO"
    And Capturar evidencia
    Then Se despliega la botonera de marcado del teléfono
    And Capturar evidencia

    Examples: Lineas
    | linea     | codigo | 
    | 949465173 |  2451  |
     

  Scenario Outline: Validar mensaje de error DARKSITE para numero con whatsaap habilitado
    Given Presionar boton texto "EMPEZAR"
    And Ingresar número móvil "<linea>"
    And Presionar boton texto "SOLICITAR CÓDIGO"
    Then Existe texto "Hemos detectado que tu numero pertenece al Darkside Únete al lado de la conveniencia y pórtate a WOM"
    And Capturar evidencia
    And Presionar boton texto "PÓRTATE A WOM AHORA CON UN EJECUTIVO"
    And Capturar evidencia
    Then Se despliega whatsaap 
    And Capturar evidencia
    

    Examples: Lineas
    | linea     | codigo | 
    | 949465173 |  2451  | 


  Scenario Outline: Validar mensaje de error LIMBO
    Given Presionar boton texto "EMPEZAR"
    And Ingresar número móvil "<linea>"
    And Presionar boton texto "SOLICITAR CÓDIGO"
    And Capturar evidencia

    Examples: Lineas
    | linea     | codigo | 
    | 949465173 |  2451  | 
        


