def go_back_after_waiting
  sleep 2
  touch("navigationItemButtonView first")
  sleep 2
end

def scroll_to_id(id)
    while query("* marked:'#{id}'").empty? do
      swipe :up, force: :light
    end
  end

Given(/^the app has launched$/) do
  wait_for do
    !query("*").empty?
  end
end

Given(/^Presionar boton texto "([^"]*)"$/) do |texto|
    
   	wait_for_element_exists("* marked: '#{texto}'")
	  touch("* marked:'#{texto}'")
		 sleep 7
end

Given(/^Ingresar número móvil "([^"]*)"$/) do |numero|
	sleep 5
	touch("* marked:'txtFieldNumeroTelefono'")
	wait_for_keyboard
	keyboard_enter_text("#{numero}")
	sleep 4
end

Given(/^Ingresar código "([^"]*)"$/) do |codigo|
	sleep 5
	touch("* marked:'inputCodigoVerificacion'")
	wait_for_keyboard
	keyboard_enter_text("#{codigo}")
end

Given(/^Presionar "([^"]*)" rs$/) do |texto|
	sleep 4
  	
		touch("* marked:'#{texto}'")
	sleep 4
end

Given(/^Presionar "([^"]*)"$/) do |texto|
sleep 4
  
	touch("* marked:'#{texto}'")
	
end

When(/^Se ingresa nombre "([^"]*)"$/) do |nombre|
	
sleep 2
   	touch("* marked:'inputNombre'")
   	wait_for_keyboard
   	keyboard_enter_text("#{nombre}")
end

When(/^Se ingresa apellido "([^"]*)"$/) do |apellido|
   	touch("* marked:'inputApellido'")
   	wait_for_keyboard
   	keyboard_enter_text("#{apellido}")
end

Then (/^Existe texto "([^\"]*)"$/) do |text|
	wait_for_element_exists("* text:'#{text}'", timeout: 10)
	
end

Then(/^Capturar evidencia$/) do
	sleep 1
	  
		screenshot({:prefix => $save_path})
		
end



Then (/^Presionar boton id "([^\"]*)"$/) do |id|
  touch("* marked:'#{id}'")
end









