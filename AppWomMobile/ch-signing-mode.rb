require 'rubygems'
require 'bundler/setup'

# require your gems as usual
require 'xcodeproj'

# Switch ProvisioningStyle to Manual for all targets in the given project
def enable_manual_provisioning(project_file)
  project = Xcodeproj::Project.open(project_file)
  target_attributes = project.root_object.attributes["TargetAttributes"]
  target_attributes.each do |id, attrs|
    if attrs["TestTargetID"].nil?
      puts "Se modificará el tipo de firmado en " + id + "."
      attrs["ProvisioningStyle"] = 'Manual'
    end
  end

  project.targets.each do |target|
    if target.name == 'AppWomMobile'
      puts target.name + ". Se modificarán los atributos de firma..."
      target.build_configurations.each do |config|
        config.build_settings['CODE_SIGN_STYLE'] ||= "Manual"
      end
    else
      puts target.name + " Mantener."
    end
  end

  project.save
  puts "Cambios completados."
end

enable_manual_provisioning('AppWomMobile.xcodeproj')