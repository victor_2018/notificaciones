# Proyecto App Mobile WOM para iOS

Esta carpeta contiene el código fuente de la aplicación móvil de WOM en su versión iOS.

## Instrucciones para construcción

## Instrucciones para integración con SonarQube

La integración con SonarQube se realiza mediante el uso de un plugin llamado `sonar-swift`, el cual debe ser instalado en el servidor SonarQube a utilizar. Adicionalmente en el equipo de desarrollo se deben realizar la instalación de algunos componentes adicionales (ver script `prepare-sonar-swift.sh`).

Para más información del procedimiento de instalación, diríjase al [siguiente enlace](https://github.com/Backelite/sonar-swift).

Un dato muy importante es que se debe ajustar los parámetros de configuración del servidor SonarQube, asi como los datos de acceso mediante el archivo `sonar-project.properties`, en especial la URL de acceso a SonarQube (propiedad `sonar.host.url`) y las credenciales de acceso al mismo mismo (propiedades `sonar.login` y `sonar.password`).

## Instrucciones para pipeline VSTS

### Firmado del artefacto

Para firmar el artefacto en un entorno de ejecución de CI como VSTS se requiere realizar las siguientes tareas:

1. Instalar un certificado de desarrollador o de distribución. Este se puede descargar desde [la sección de certificados](https://developer.apple.com/account/ios/certificate/) del sitio de developers de Apple.
1. Instalar un perfil de aprovisionamiento que permita testear o distribuir la aplicación y que esté asociado al certificado anterior. Este puede ser descargado desde [la sección de perfiles](https://developer.apple.com/account/ios/profile/) de la página de developers de Apple.
1. Cambiar el estilo de firmado del proyecto, desde **automático** (como se utiliza en el entorno de desarrollo) a **manual**. Esto se debe realizar mediante la ejecución del script Ruby llamado `ch-signing-mode.rb` (1). Antes de poder ejecutar este script se requiere ejecutar el comando `bundle install`, el cual descargará las dependencia necesarias.

> Nota (1): El script se basa en la información que se encuentra en [https://lyricsboy.github.io/2016/11/30/xcode-8-automatic-signing-ci/](https://lyricsboy.github.io/2016/11/30/xcode-8-automatic-signing-ci/) y el reposotorio [https://github.com/lyricsboy/xcode-ci-automatic-signing/blob/master/fastlane/Fastfile](https://github.com/lyricsboy/xcode-ci-automatic-signing/blob/master/fastlane/Fastfile).

### Integración con SonarQube

Para la integración automática con SonarQube desde VSTS utilizando un nodo MacOS hosteado (AZURE) se recomienda realizar los siguientes pasos.

1. Ejecutar la shell `prepare-sonar-swift.sh`, la cual se encarga de realizar la instalación de los componentes necesarios en el nodo antes de gatillar el análisis.
1. Incorporar las propiedades `sonar.host.url` (URL del servidor SonarQube), `sonar.login` (con el valor del API Key de acceso a SonarQube) y `sonar.branch` (con el nombre de la rama que gatilla la ejecución) al archivo `sonar-project.properties` en forma automática. Estos datos pueden configurarse (Variable custom) y obtenerse (Variable `Build.SourceBranch`) en el pipeline en VSTS.
